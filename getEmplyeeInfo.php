<?php
    ini_set('memory_limit', '256M');
    $op = array(
        'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );

$pid = rand(100,999);

    $tempDB = new db($op);
    $sqlcmd = "select empID,empName,depID,jobID from emplyee";
    $rsX    = $tempDB->query($sqlcmd);
    while ($rx = $tempDB->fetch_array($rsX)) {
        $emplyeeinfo[$rx[0]] = $rx[1];
        $emplyeeDept[$rx[0]] = substr($rx[2],0,1);
        $emplyeeRealDept[$rx[0]] = $rx[2];
        $emplyeeJobID[$rx[0]]=$rx[3];
    }

    $sqlcmd = "select depID,depName,leadID,depTitle from department order by PID,depID";
    $rsX    = $tempDB->query($sqlcmd);
    while ($rx = $tempDB->fetch_array($rsX)) {
        $depAdminfo[$rx[0]] = $rx[3];
        $departmentlead[$rx[0]] = $rx[2];
        $depInfo[$rx[0]] = $rx[1];
    }

    $sqlcmd = "select jobID,jobName from jobs";
    $rsX    = $tempDB->query($sqlcmd);
    while ($rx = $tempDB->fetch_array($rsX)) {
        $jobinfo[$rx[0]] = $rx[1];
    }

    //取得副主管以上職銜
    $sqlGjob = "select jobID FROM jobs WHERE jobID LIKE 'G%' AND jobID <= 'G006'";
    $rsGJob    = $tempDB->query($sqlGjob);
    while ($rGJob = $tempDB->fetch_array($rsGJob)) {
        $Gjob[$rGJob[0]] = $rGJob[0];
    }

    $tempDB->close();

    //外部組織 
    $orgDB = new db();
    $sql ="select id ,title from organization";
    $rs = $orgDB->query($sql);
    while($r=$orgDB->fetch_array($rs)){
        $departmentinfo[$r[0]] = $r[1];
    }

    $sql = "select id ,title from org_class";
    $rs = $orgDB->query($sql);
    while($r=$orgDB->fetch_array($rs)){
        $orgClass[$r[0]] = $r[1];
    }
    $orgDB->close();

    $edisDB =new db();
    //目前存在收文單位
    // $sql = "SELECT distinct o.id,if((length(o.sTitle) > 0), o.sTitle, o.title) title 
    //         FROM organization o, map_orgs mo, edis ed
    //         WHERE o.id = mo.oid AND mo.eid = ed.id  AND mo.act='R' AND ed.`odType` = '收文'";
    $sql = "SELECT o.id, if((length(o.sTitle) > 0), o.sTitle, o.title) title
            FROM inner_receiving_sending_unit irsu
            LEFT JOIN organization o
            ON o.id = irsu.oid
            WHERE irsu.type = 'R'";
    $rs = $edisDB->query($sql);
    while($r=$edisDB->fetch_array($rs)){
        $rodRact[$r[0]] = $r[1];
    }
    //目前存在發文單位
    // $sql = "SELECT distinct o.id,if((length(o.sTitle) > 0), o.sTitle, o.title) title 
    //         FROM organization o, map_orgs mo, edis ed
    //         WHERE o.id = mo.oid AND mo.eid = ed.id  AND mo.act='S' AND ed.`odType` = '發文'"; 
    $sql = "SELECT o.id, if((length(o.sTitle) > 0), o.sTitle, o.title) title
            FROM inner_receiving_sending_unit irsu
            LEFT JOIN organization o
            ON o.id = irsu.oid
            WHERE irsu.type = 'S'";
    $rs = $edisDB->query($sql);
    while($r=$edisDB->fetch_array($rs)){
        $sodSact[$r[0]] = $r[1];
    }
