<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once "../config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "$root/system/ListControl.php";
include "$root/system/ViewControl.php";
include_once "$root/inc_vars.php";
include_once "$root/getEmplyeeInfo.php";

$pageTitle = "";
//$pageSize=5; //使用預設
$typeList =array('S'=>'用印人員','F'=>'財務人員');
$tableName = "seals";
// for Upload files and Delete Record use
$ulpath = "/data/seals/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}
$delField  = 'attach';
$delFlag   = true;
$empID = $_REQUEST['id'];

//發文機關選項
$sql = "select id,sTitle from organization where id in('".implode("','",$orgUse)."')";
$db = new db();
$rs = $db->query($sql);
while ($r = $db->fetch_array($rs)) {
    $soOrgs[$r[0]] = $r[1];
}
//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, true, SearchKeyword),
    "newBtn"    => array("＋新增", false, true, doAppend),
    "editBtn"   => array("－修改", false, true, doEdit),
    "delBtn"    => array("Ｘ刪除", false, true, doDel),
);

    //:ListControl  ---------------------------------------------------------------
    $opList = array(
        "alterColor"=>true,         //交換色
        "curOrderField"=>'empType,sealsid', //主排序的欄位
        "sortMark"=>'ASC',          //升降序 (ASC | DES)
        "searchField"=>array('sealsid','empid'),   //要搜尋的欄位    
    );
        
    $fieldsList = array(
        "sealsid"=>array("編號","60px",true,array('Id',"gorec(%d)")),
        "empid"=>array("職員","120px",true,array('Define','getMan')),
        "empType"=>array("領據設定類型","120px",true,array('Define','getSealType')),
        "startDate"=>array("開始日期","100px",true,array('Date')),
        "endDate"=>array("結束日期","100px",true,array('Date'))
    );
    
    //:ViewControl -----------------------------------------------------------------
    $fieldA = array(
        "sealsid"=>array("編號","readonly",15,''),
        "empID"=>array("員工ID","readonly",30),        
        "empType"=>array("類型","select",1,'',$typeList),
        "depts"=>array("負責機構","Define",1,'','deplist'),
        "startDate"=>array("開始日期","date",15,'','',array(true,PTN_DATE,'請輸入日期時間')),
        "endDate"=>array("結束日期","date",15,'','',array(false,PTN_DATE,'請輸入日期時間'))
    );
//  $fieldA_Data = array("empID"=>$_REQUEST['empID']);
    
    $fieldE = array(
        "sealsid"=>array("編號","readonly",15,''),
        "empID"=>array("員工ID","readonly",30),        
        "empType"=>array("類型","select",1,'',$typeList),
        "depts"=>array("負責機構","Define",1,'','deplistE'),
        "startDate"=>array("開始日期","date",15,'','',array(true,PTN_DATE,'請輸入日期時間')),
        "endDate"=>array("結束日期","date",15,'','',array(false,PTN_DATE,'請輸入日期時間'))
    );
