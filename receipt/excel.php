<?
  if($_REQUEST['sqltx']) {
    $filename = '領據_'.date("Y-m-d H:i:s",time()).'.xlsx';
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");
    include '/../config.php';
    include '/../inc_vars.php';
    include '../system/db.php';
    include '/../getEmplyeeInfo.php';  

    include 'func.php';

    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);
    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    // $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
    //     array(
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '969696')
    //         ),
    //         'font'   => array('bold' => true,
    //             'size' => '12',
    //             'color' => array('argb' => 'FFFFFF')
    //         )
    //     )
    // );

    $objPHPExcel->getActiveSheet()->getStyle('A2:L2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
  
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:L1");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:L2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
   // VERTICAL_CENTER 垂直置中
   // VERTICAL_TOP
   // HORIZONTAL_CENTER
   // HORIZONTAL_RIGHT
   // HORIZONTAL_LEFT
   // HORIZONTAL_JUSTIFY
       
    $objPHPExcel->getActiveSheet()->setCellValue('A1',(date("Y")-1911).'年領據總表');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','序號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('B2','申請日期'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C2','受文單位'); 
    $objPHPExcel->getActiveSheet()->setCellValue('D2','領據內容');
    $objPHPExcel->getActiveSheet()->setCellValue('E2','申請單位');
    $objPHPExcel->getActiveSheet()->setCellValue('F2','申請人');
    $objPHPExcel->getActiveSheet()->setCellValue('G2','領據金額(元)');
    $objPHPExcel->getActiveSheet()->setCellValue('H2','發文編號');
    $objPHPExcel->getActiveSheet()->setCellValue('I2','檔案');
    $objPHPExcel->getActiveSheet()->setCellValue('J2','已匯入');
    $objPHPExcel->getActiveSheet()->setCellValue('K2','核銷冊');        
    $objPHPExcel->getActiveSheet()->setCellValue('L2','備註');

    $sql = substr($_REQUEST['sqltx'],0,strpos($_REQUEST['sqltx'],"limit"));
    $DB = new db();
    $rsX    = $DB->query($sql); 
    $i = 3;
   

    while($rsX && $r=$DB->fetch_array($rsX)) {
      
      $did = getDid($r['edisid']);
      $isImport = ($r['isImport']) ? 'V' : '' ;
      $isNuclearBook = ($r['isNuclearBook']) ? 'V' : '' ;
      $isAttach = ($r['attach']) ? 'V' : '' ;
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$r['receiptNo']);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$r['applyDate']);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$departmentinfo[$r['rOid']]);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$r['content']);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$departmentinfo[$r['sOid']]);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$emplyeeinfo[$r['sid']]);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$r['amount']);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$did);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$isAttach);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$isImport);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.$i,$isNuclearBook);            
      $objPHPExcel->getActiveSheet()->setCellValue('L'.$i,$r['comment']);

      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
      
    }
    
    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);

    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);



    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
    ob_clean();
    $objWriter->save('php://output');




  }


  
?>