<?
	include("../thumb.php");
	include '../public/lib.php';
	// include 'func.php';
	//處理上傳的附件

	$fnlst = array();
	foreach($_FILES as $k=>$v) {
		switch($fieldA[$k][1]) {
			case 'file': 
				$fn = $_FILES[$k]['name'];
				if ($_FILES[$k][tmp_name] != "") {
					$fnlst[$k] =  md5(myPathInfo($fn,'filename').date('Y-m-d H:i:s')).'.'.myPathInfo($fn,'extension');
					$dstfn = $root.$ulpath.$fnlst[$k]; 
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'],$dstfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
				}
				break;
			case 'multifile':
				$aa = array();
				foreach($_FILES[$k]["name"] as $k2=>$v2){
					if(!$v2) continue; 
					$finf = myPathInfo($v2);
					$fn = md5($_FILES[$k]["name"][$k2]).'.'.$finf['extension'];
					$dstfn = $root.$ulpath.$fn;
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'][$k2],$dstfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
					array_push($aa,$fn);				
				}
				$fnlst[$k] = implode(',',$aa);
				break;
		}
	}

	//建立資料庫物件
	$my = new db($tableName);	
	foreach($fieldA as $k=>$data) {
		switch($data[1]) {
			case "Define":
			case "uLABEL":
			case "date" : $my->row[$k]="'".$_REQUEST[$k]."'";
			case "hidden" :if($k=='isImport'|| $k=='isNuclearBook'){$db->row[$k]=(isset($_REQUEST[$k])? 1:0);}; break;
			case "select" :
			case "readonly" : $my->row[$k]="'$_REQUEST[$k]'"; break;
  			case "text" :
			case "textbox" :
			case "queryID" :
  			case "textarea" :	$my->row[$k]="'".addslashes($_REQUEST[$k])."'"; break;
			case "queryIDs" :	$my->row[$k]="'".addslashes( join(',',$_REQUEST[$k]) )."'"; break;
  			case "checkbox" : $my->row[$k]=(isset($_REQUEST[$k])? 1:0); break;
			case "cklist" : if( $_REQUEST[$k] ) { $sum=0; foreach($_REQUEST[$k] as $v) $sum += $v; $my->row[$k]=$sum; }; break;	
			case "multifile":
			case "file" : $my->row[$k]="'$fnlst[$k]'"; break;	
  		}	
	}

	$did = getReceiptNo($_REQUEST['soOid']);
	$my->row['receiptNo'] = "'".$did."'";
	$my->insert();	
	
	//檢查是否需要移動位置
	if($newExtDir) {
		$dirName = $root.$ulpath.($my->row[$newExtDir] ? str_replace("'",'',$my->row[$newExtDir]) : $my->lastInsertId);		//surport 自訂Key or Auto Key
		if(!file_exists($dirName)) mkdir($dirName);
		$aa = array();
		foreach($fieldA as $k=>$data) {
			$aa = explode(',',$fnlst[$k]);
			foreach($aa as $fName) {
				$srcfn = $root.$ulpath.$fName;
				$dstfn = $dirName.'/'.$fName;
				rename($srcfn,$dstfn);
			}
		}
	}
	$_REQUEST['ListFmParams'] .= "&did=".$did;
	PostHeader($_SERVER['HTTP_REFERER'],$_REQUEST['ListFmParams']);

	function myPathInfo($filename,$type = null){
		$path = array();
		$path['dirname']   = rtrim(substr($filename, 0, strrpos($filename, '/')), '/').'/';
		$path['basename']  = ltrim(substr($filename, strrpos($filename, '/')), '/');
		$path['extension'] = substr(strrchr($filename, '.'), 1);
		$path['filename']  = ltrim(substr($path['basename'], 0, strrpos($path['basename'], '.')), '/');
		switch ($type) {
			case 'dirname': 	return $path['dirname']; 	break;
			case 'basename': 	return $path['basename']; 	break;
			case 'extension': 	return $path['extension']; 	break;
			case 'filename': 	return $path['filename']; 	break;
			default: 			return $path; 				break;
		}
	}

?>