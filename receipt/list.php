<?
session_start();
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */
include 'init.php';

if($_POST['did']){
    echo '<script>alert("取得領據字號:'.$_POST['did'].'");</script>';
}

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $aa    = array_keys($fieldsList);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    //echo $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);
}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_addrE()
{
    global $county, $fieldE_Data;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $ck = ($fieldE_Data[county] == $k ? 'selected' : '');
        $rtn .= "<option value='$k' $ck>$v</option>";
    }
    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'>" . ($fieldE_Data[city] ? "<option>$fieldE_Data[city]</option>" : '') . "</select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' value='$fieldE_Data[address]' />";
    return $rtn;
}

function func_addr()
{
    global $county;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $rtn .= "<option value='$k'>$v</option>";
    }

    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'></select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' />";
    return $rtn;
}

function view_addr($value, $obj)
{
    global $county;
    return $county[$obj->curRow[county]] . $obj->curRow[city] . $value;
}

function getMan($v)
{
    global $emplyeeinfo; return $emplyeeinfo[$v];
}

?>
<title><?=$CompanyName . '-' . $pageTitle?></title>
<link href="/system/PageControl.css" rel="stylesheet">
<link href="/system/ListControl.css" rel="stylesheet">
<link href="/system/ViewControl.css" rel="stylesheet">
<link href="/system/TreeControl.css" rel="stylesheet">

<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">
<link href="/css/formRwd.css" rel="stylesheet">


<link href="/Scripts/daterangepicker/daterangepicker.css" rel="stylesheet"></link>
<link href="/Scripts/daterangepicker/MonthPicker.css" rel="stylesheet"/>


<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js"></script>


<script src="/Scripts/daterangepicker/moment.min.js"></script>
<script src="/Scripts/daterangepicker/daterangepicker.min.js"></script>
<script src="/Scripts/daterangepicker/MonthPicker.js"></script>


<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>

<?
$whereStr = ''; //initial where string

if($empReceiptType=='A'||$empReceiptType=='B'){//登入者為領據用印/財務人員
    $wherefStr = ' (SUBSTRING(receiptNo,2,1) in('.$receiptOrgs.') '. "or sid='".$_SESSION['empID']."')";
}else{
    $wherefStr = ' 1=1';
}

//:filter handle  ---------------------------------------------------
$whereAry  = array();
if($opList['filters']) {
    foreach($opList['filters'] as $k=>$v) {
        $wType = $opList['filterType'][$k];
        switch($wType) {
            case 'Month':
                if($_REQUEST[$k]) $curMh = $_REQUEST[$k];
                elseif($opList['filterOption'][$k]) $curMh=$opList['filterOption'][$k][0];
                $Days  = date('t',strtotime("$curMh/01"));
                $bDate = date($curMh.'/01');
                $eDate = date($curMh.'/'.$Days);    
                if($curMh) $whereAry[]="($k between '$bDate' and '$eDate 23:59:59')";
                break;
            case 'DateRange':
                $aa = array();
                if($_REQUEST[$k]) {
                    $aa = explode('~',$_REQUEST[$k]);
                } elseif($opList['filterOption'][$k]) {
                    $aa = explode('~',$opList['filterOption'][$k][0]);
                }
                if(count($aa)) $whereAry[] = "($k between '$aa[0]' AND '$aa[1] 23:59:59')";
                break;
            default:        
                $oprator = $opList['filterCondi'][$k]?$opList['filterCondi'][$k]:"%s = '%s'"; 
                if($_REQUEST[$k] || ($_REQUEST[$k]=='0' && $k!='depID')) $whereAry[]=sprintf($oprator,$k,$_REQUEST[$k]);
                else if($opList['filterOption'][$k]) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);
        }
    }
    if (count($whereAry)) {
        $wherefStr .= ' and '.join(' and ', $whereAry);
    }
}


//:Search filter handle  ---------------------------------------------
    $whereAry  = array();
    $wherekStr = '';
    if ($opList['keyword']) {
        $key = $opList['keyword'];
        foreach ($opList['searchField'] as $v) {
            $whereAry[] = "$v like '%$key%'";
        }

        $n = count($whereAry);
        if ($n > 1) {
            $wherekStr = '(' . join(' or ', $whereAry) . ')';
        } else if ($n == 1) {
            $wherekStr = join(' or ', $whereAry);
        }

    }
//:Merge where
    if ($wherefStr || $wherekStr) {
        $whereStr = 'where';
        $flag     = false;
        if ($wherefStr) {
            $whereStr .= $wherefStr;
            $flag = true;
        }
        if ($wherekStr) {
            if ($flag) {
                $whereStr .= ' and ' . $wherekStr;
            } else {
                $whereStr .= $wherekStr;
            }
        }
    }

    $db     = new db();
    $sql    = sprintf('select 1 from %s %s', $tableName, $whereStr);
    $rs     = $db->query($sql);
    $rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
if (!$pageSize) {
    $pageSize = 10;
}

$pinf = new PageInfo($rCount, $pageSize);
if ($recno > 1) {
    $pinf->setRecord($recno);
} else if ($page > 1) {
    $pinf->setPage($page);
    $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;}
    ;
    /*add by Tina 20190104 ----編輯按鈕控制start----*/
    $n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
    $sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
        $tableName, $whereStr,
        $opList['curOrderField'], $opList['sortMark'],
        $n * $pinf->pageSize, $pinf->pageSize
    );
    $rs    = $db->query($sql);
    $pdata = $db->fetch_all($rs);

    $sql = sprintf('select * from %s %s order by %s %s',
        $tableName, $whereStr,
        $opList['curOrderField'], $opList['sortMark']
    );
    $rs    = $db->query($sql);
    $recDeptData = $db->fetch_all($rs);
//可查看所有領據,只可修改部門申請之資料(判斷申請人之部門)
    $recDept = $recDeptData[$pinf->curRecord]['soOid'];

    if(in_array(substr($recDept,0,1), $receiptDep)){
        $_SESSION['editFlag'] = false;
    }else{ 
        if($pdata[$pinf->curRecord]['sid'] == $_SESSION['empID']){
            $_SESSION['editFlag'] = false; 
        }else{
            $_SESSION['editFlag'] = true; 
        }
    }

//目前點擊之資料是否可編輯//財務室人員/總文書

    if($empReceiptType=='B'){
        $opPage['editBtn'][1] = $_SESSION['editFlag'];
        $opPage['delBtn'][1] = $_SESSION['editFlag'];
    }

    /*add by Tina 20190104 ----end----*/
//: Header -------------------------------------------------------------------------
    echo "<h1 align='center'>$pageTitle ";
    if ($opList['keyword']) {
        echo " - 搜尋:" . $opList['keyword'];
    }

    echo "</h1>";

//:PageControl ---------------------------------------------------------------------
    $obj = new EditPageControl($opPage, $pinf);

//:ListControl Object --------------------------------------------------------------------
    $n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
    $sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
        $tableName, $whereStr,
        $opList['curOrderField'], $opList['sortMark'],
        $n * $pinf->pageSize, $pinf->pageSize
    );
    // echo $sql;
    $rs    = $db->query($sql);
    $pdata = $db->fetch_all($rs);

//add by tina 20180326 List:總收發人顯示名字
    $arrCount=0;

    foreach ($pdata as &$pdataInfo) {
        $recDept = '\''.$emplyeeRealDept[$pdataInfo['sid']].'\'';
//登入者只是申請部門人員或總文書 只可查看/修改部門申請之資料

        if($empReceiptType=='C'||$empReceiptType=='D'){
// if($pdata[$arrCount]['sid'] == $_SESSION['empID']) continue;
            if(!in_array($recDept, $empDeptArray) && ($pdata[$arrCount]['sid'] != $_SESSION['empID'])){
                unset($pdata[$arrCount]);
            }
        }

        $revManId = $pdataInfo['revMan'];
        if (isset($revManId)) {

            $pdataInfo['revManName'] = $emplyeeinfo[$revManId];

        }
        $mainRevManId = $pdataInfo['mainRevMan'];
        if (isset($mainRevManId)) {

            $pdataInfo['mainRevManName'] = $emplyeeinfo[$mainRevManId];

        }    
        unset($pdataInfo);
        $arrCount++;
    }

//陣列順序重新排序
    $pdata = array_values($pdata);
//更新資料筆數
    $pinf->records = count($pdata);
    echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//update by tina 20180412 新增/修改不顯示清單
    if (empty($act) or $act=='del') {
        $obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
    }


//:ViewControl Object ------------------------------------------------------------------------
    $listPos = $pinf->curRecord % $pinf->pageSize;
    switch ($act) {
        case 'edit': //修改
            $op3 = array(
                "type"      => "edit",
                "form"      => array('form1', "$tableName/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
                "submitBtn" => array("確定修改", false, ''),
                "cancilBtn" => array("取消修改", false, ''));
            $fieldE_Data = $pdata[$listPos];
            $ctx         = new BaseViewControl($fieldE_Data, $fieldE, $op3);
        break;
        case 'new': //新增
            $op3 = array(
                "type"      => "append",   
                "form"      => array('form1', "$tableName/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
                "submitBtn" => array("確定新增", false, ''),
                "cancilBtn" => array("取消新增", false, ''));
            $ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
        break;
    }

include 'public/inc_listjs.php';


?>
<form name="excelfm" action="receipt/excel.php" method="post">
    <input type="hidden" name="sqltx" value="<?=$sql?>">
</form>
<script>
    //add by Tina 20190307 不可點選連結進入編輯,需判斷當前記錄是否為可編輯
    function gorec(recno) { //go edit ViewControl
        if($('.editBtn').attr('disabled')=='disabled' && window.event.target.tagName=='A') return;
        ListControlForm.recno.value = recno;
        if(window.event.target.tagName=='A') ListControlForm.act.value = 'edit';
        ListControlForm.submit();
    }

    $(function () { //::Prepare 
        var act ="<?echo $act; ?>";
        if(act){
            $("#edisid").attr('readonly','readonly');
            $("#soOid").attr('readonly','readonly');
            $("#rAct").attr('readonly','readonly');
            $("#admDep").attr('readonly','readonly');
        }
        if(act == 'edit'){
            var funact = 'getdid';
            var edisid = $('#edisid').attr('value');
            $.ajax({
                url: 'receipt/api.php?act='+funact+'&edisid='+edisid,
                type:"GET",
                dataType:'text',
                async: false,
                success: function(response){
                    if(response !==''){
                        $('#edisid').next().next().html(response);
                        result=true;
                    }else{
                        result=false;
                        alert(errMsg);
                    }
                }
            });
        }
        $('[name="isImport"]').on('change',function(){
            if($(this).attr('checked')){
                $(this).val(1);
            }else{
                $(this).val(0);
            }
        });
        $('[name="isNuclearBook"]').on('change',function(){
            if($(this).attr('checked')){
                $(this).val(1);
            }else{
                $(this).val(0);
            }
        });
    });


    $("[name='Submit']").click(function() {
        var act ="<?echo $act; ?>";
        var funact = '';
        var checkResult =false;
        var edisid = $('#edisid').attr('value');
        var receiptNo = $("[name='receiptNo']").attr('value');
        var soOid = $('#soOid').attr('value');
        var applyDate = $("[name='applyDate']").attr('value');
        var rOid= $("[name='rOid']").attr('value');
        var content= $("[name='content']").attr('value');
        var sOid= $("[name='sOid']").attr('value');
        var sid= $("[name='sid']").attr('value');
        var amount= $("[name='amount']").attr('value');


        if(act == 'new'){
            if(soOid=='' || applyDate=='' ||rOid=='' ||content=='' ||sOid=='' ||sid=='' ||amount==''){
            result=false;//為了使submit繼續執行,顯示尚未填入的欄位,設定為true
            alert('必填欄位尚未完成');
        }else{
            result=true;            
        }       
    }else if(act == 'edit'){
        result = true;
    }
    return result;
})    
//產生報表
$("button[data-handler='receipt_report']").click(function() {
    var sql = "<? echo $sql; ?>";
    excelfm.submit();
});

function setSod(id, title){
    var act = "<?echo $act; ?>";
    var returnValue = new Array();
    $.ajax({
        url: 'receipt/api.php?act=setSod'+'&inValue='+id,
        type:"GET",
        dataType: "json",
        async: false,
        success: function(response){
            if(response !=='' && act == 'new'){
                returnValue = response;
                $.each(returnValue, function(key, value) {
                    var rOid = $('input[name="rOid"]');
                    $(rOid[0]).val(value['rOid']);
                    $(rOid[0]).next().next('span').html(value['rOidName']);
                    var content = $('textarea[name="content"]');
                    $(content[0]).val(value['subjects']);
                    var sOid = $('input[name="sOid"]');
                    $(sOid[0]).val(value['sOid']);
                    $(sOid[0]).next().next('span').html(value['sOidName']);
                    var soOid = $('input[name="soOid"]');
                    $(soOid[0]).val(value['soOid']);
                    $(soOid[0]).next().next('span').html(value['soOidName']);                     
                    var sid = $('input[name="sid"]');
                    $(sid[0]).val(value['sid']);
                    $(sid[0]).next().next('span').html(value['sidName']);
                });

                result=true;
            }else{
                if(act == 'edit') result = true;
                else {
                    result = false;    
                    alert('error');
                }
            }

        }
    }) 
}
</script>