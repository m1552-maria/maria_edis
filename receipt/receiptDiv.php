<?php
include_once '../system/db.php';
include_once "../config.php";
include_once "$root/getEmplyeeInfo.php";
include_once 'func.php';

function genReceiptDom($edisId)
{
    global $emplyeeinfo;
    $db = new db();
    //公文主檔
    $sql = "SELECT r.*,(SELECT title FROM organization WHERE id = r.rOid) rOidName,(SELECT title FROM organization WHERE id = r.sOid) sOidName FROM receipt r where r.edisid = " . $_REQUEST['id'];
    $rs  = $db->query($sql);
    if ($rs) {
        $r = $db->fetch_array($rs);
    }

    $entity = html_entity_decode('<div id="receipt"> <table id="recepitTable" cellpadding="4">
      <tr>
         <td align="right"><span class="require">*</span>申請日期：</td>
         <td><input type="text" name="applyDate" id="applyDate" required value="' . date_format(date_create($r['applyDate']), 'Y/m/d') . '"/><input type="hidden" name="edisid" id="edisid" value="' . $_REQUEST['id'] . '"><input type="hidden" name="receiptid" id="receiptid" value="' . $r['receiptid'] . '"></td>
      </tr>
      <tr>
        <td align="right"><span class="require">*</span>受文單位：</td>
        <td align="left"><input name="rOid" type="text" class="queryID" id="rOid" value="' . $r['rOid'] . '" size="6" required title="' . $r['rOidName'] . '" onkeypress="return checkInput(event,\'depName\',this)" />
        </td>
      </tr>
      <tr>
        <td align="right"><span class="require">*</span>領據內容：</td>
        <td><textarea name="content" id="content" rows="5" cols="30"  required >' . $r['content'] . '</textarea></td>
      </tr>
      <tr>
        <td align="right"><span class="require">*</span>申請單位：</td>
        <td align="left"><input name="sOid" type="text" class="queryID" id="sOid" value="' . $r['sOid'] . '" size="6" required title="' . $r['sOidName'] . '" onkeypress="return checkInput(event,\'depName\',this)" />
        </td>
      </tr>
      <tr>
        <td align="right"><span class="require">*</span>申請人：</td>
        <td align="left">
        <input name="sid" type="text" class="queryID" id="sid" required value="' . $r['sid'] . '" size="6" title="' . $emplyeeinfo[$r['sid']] . '" onkeypress="return checkInput(event,\'empName\',this)">
        </td>
      </tr>
      <tr>
        <td align="right"><span class="require">*</span>領據金額(元)：</td>
        <td><input type="number" name="amount" id="amount" value="' . $r['amount'] . '" required></td>
      </tr>
      <tr>
        <td align="right">檔案：</td>
        <td><span id="attachName" name="attachName">' . $r['attach'] . '</span><input type="file" name="attach" id="attach" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf"><input type="hidden" name="attachField" id="attachField" value="' . $r['attach'] . '">
        <td>
        </td>
      </tr>
      <tr>
        <td align="right">備註：</td>
        <td><input type="text" name="comment" id="comment" value="' . $r['comment'] . '"></td>
      </tr>
      <tr>
        <td colspan="2" align="center">
        <button type="button" onclick="return insertReport(\'receipt\')">確定</button>
        <button type="button" onclick="return cancelReport(\'receipt\')">刪除/取消</button></td></tr></table> </div>');

    echo $entity;

}

function insertReceipt($edisid,$field, $file)
{

	$ulpath = '/data/receipt/';
    $db = new db();
    if (empty($field['receiptid'])) {
        //未新增
        if ($field['receiptFlag'] == 'T') {
            //新增
            if(empty($field['attachField'])){
              $fileName = attachManager($ulpath, $file, $field['attachField'], 'new');
            }else{
        	  $fileName = attachManager($ulpath, $file, $field['attachField'], 'update');
            }
            $receiptNo = getReceiptNo();
            $db                   = new db('receipt');
            $db->row['applyDate'] = "'" . $field['applyDate'] . "'";
            $db->row['receiptNo'] = "'" . $receiptNo . "'";
            $db->row['edisid']    = "'" . $edisid . "'";
            $db->row['rOid']      = "'" . $field['rOid'] . "'";
            $db->row['content']   = "'" . $field['content'] . "'";
            $db->row['sOid']      = "'" . $field['sOid'] . "'";
            $db->row['sid']       = "'" . $field['sid'] . "'";
            $db->row['amount']    = "'" . $field['amount'] . "'";
            $db->row['attach']    = "'" . $fileName . "'";
            $db->row['comment']   = "'" . $field['comment'] . "'";
            $db->insert();

        } else if ($field['receiptFlag'] == 'F') { //不新增
            # code...
        }

    } else {
        //已新增
        if ($field['receiptFlag'] == 'T') {
            //修改
            if(empty($field['attachField'])){
              $fileName = attachManager($ulpath, $file, $field['attachField'], 'new');
            }else{
        	  $fileName = attachManager($ulpath, $file, $field['attachField'], 'update');
            }
            $sql = "update  receipt set "
                . "applyDate='" . $field['applyDate'] . "',"
                . "edisid='" . $edisid . "',"
                . "rOid='" . $field['rOid'] . "',"
                . "content='" . $field['content'] . "',"
                . "sOid='" . $field['sOid'] . "',"
                . "sid='" . $field['sid'] . "',"
                . "amount='" . $field['amount'] . "',"
                . "attach='" . $fileName . "',"
                . "comment='" . $field['comment'] . "' "
                . "where receiptid ='" . $field['receiptid'] . "'";

            $db->query($sql);
        }
    }

}
//處理附件 $ulpath:上傳路徑 $file:上傳資料 $uploadName:原先上傳名稱 $type:處理類型(new/update/delete)
function attachManager($ulpath, $file, $uploadName, $type)
{  //$ulpath範例:/data/receipt/
	     global $root;
        $fileOldName = substr($uploadName, 0, strripos($uploadName, '.'));
        $fn          = $root.$ulpath.$uploadName;	

    if ($type == 'new') {
        $srcfn = $root.$ulpath . time() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
        $rzt   = move_uploaded_file($file['tmp_name'], $srcfn);
        if ($rzt) {
            $sLocation = strripos($srcfn, '/') + 1;
            $length    = strlen($srcfn) - strripos($srcfn, '/');
            $fileName  = substr($srcfn, $sLocation, $length);
        }

    } else if ($type == 'update') {
       if(!empty($file['name'])){//有上傳新檔案
            //刪除原上傳資料
            if (file_exists($fn)) {
                @unlink($fn);
            }
            //更新上傳資料
            $fileNewName = $fileOldName . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
            $srcfn       = $root.$ulpath. $fileNewName;
            $rzt         = move_uploaded_file($file['tmp_name'], $srcfn);
            $fileName = $fileNewName;
        }else{
            $fileName = $uploadName;
        }
    } else if ($type == 'delete') {
        if (file_exists($fn)) {
            @unlink($fn);
        }
        $fileName = '';

    }

    return $fileName;

}
