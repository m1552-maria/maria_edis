<?
function getReceiptNo($oid)
{
    $db      = new db();
    $orgsql  = "select numberTitle FROM organization WHERE id='".$oid."'";
    $rsOrg   = $db->query($orgsql);
    $ro      = $db->fetch_array($rsOrg);  
    $receiptCode = '領'.$ro['numberTitle'];

    $sql    = "select MAX(CONVERT(SUBSTR(receiptNo,6,3),SIGNED)) lastnum from receipt where SUBSTR(receiptNo,1,5)='".$receiptCode.(date("Y") - 1911)."'";
    $rs     = $db->query($sql);
    $r      = $db->fetch_array($rs);
    if($r) $newNum = $receiptCode. (date("Y") - 1911) . str_pad(($r['lastnum'] + 1), 3, "0", STR_PAD_LEFT);
    else $newNum = $receiptCode. (date("Y") - 1911) . str_pad( 1, 3, "0", STR_PAD_LEFT);
    return $newNum;

}

function getDid($v)
{
    $db  = new db();
    $sql = "select did from edis where id = '" . $v . "' ";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $did = $r['did'];
    return $r['did'];
}

function getOrgName($v){
    global $depInfo, $depAdminfo;
    $db  = new db();
    $sql = "select sTitle,title from organization where id = '" . $v . "' ";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);

    if($r){
        if(empty($r['sTitle'])){
            $name = $r['title'];
        }else{
            $name = $r['sTitle'];
        }
    }else{ // org 沒有去adm找部門名稱
        if(empty($depInfo[$v])){
            $name = $depAdminfo[$v];
        }else{
            $name = $depInfo[$v];
        }
    }
    
    return $name;
}

function checkRepeat($v, $r)
{
    if (!empty($v)) {
        $db = new db();
        if (empty($r)) {
            $sql = "select receiptid from receipt where edisid = '" . $v . "' ";
        } else {
            $sql = "select receiptid from receipt where edisid = '" . $v . "' and receiptNo !='" . $r . "'";
        }
        $rs = $db->query($sql);
        $r  = $db->fetch_array($rs);
    }
    return $r['receiptid'];

}

function getReceiptEmpDept($empId)
{
    $empDept = getEmpDeptReceipt($empId, 0);
    return $empDept;
}

function getEmpDeptReceipt($empID, $downFlag)
{
    $op = array(
        'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db  = new db($op);
    $sql = "select depID from emplyee where empID = $empID ;";
    $rs  = $db->query($sql);

    $aa = array();

    while ($r = $db->fetch_array($rs)) {
        if ($downFlag == 'Son') {
            $subSql = "select depID from department where depID like '" . $r[0] . "%'";
            $rsSub = $db->query($subSql);
            while ($rSub = $db->fetch_array($rsSub)) {
                $aa[] = "'$rSub[0]'";
            }
        }elseif ($downFlag == 'noSon') {
            $aa[] = "'$r[0]'";
        }
    }

    //文書所負責小組
    $revMan = getAllRevMan();
    if(in_array($empID, $revMan)){
        $sql = "select id from organization where revMan = '$empID' or proxyRevMan = '$empID'";
        $db  = new db();
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)){
            $aa[] = "'$r[0]'";
        }        
    }

    //總文書可查詢負責的機構
    $isMainRevMan = isMainRevMan($empID);
    if($downFlag == 'Org'){
        $aa = array();
        if ($isMainRevMan) {
            $db  = new db();
            $sql = "select distinct id from organization where mainRevMan = '$empID' AND numberTitle !='';";
            $rs  = $db->query($sql);
            while ($r = $db->fetch_array($rs)) {
                $aa[] = "'$r[0]'";
            }
        }
    }

    if (count($aa) > 0) {
        $lstss = join(',', array_unique($aa));
    }

    return $lstss;
}

//20181227 add by Tina 領據查看/編輯權限控制
function receiptEmpCheck($empId)
{
    $return_value = '';
//是否為用印人員
    $db           = new db();
    $sql          = "select id from organization where mainRevMan = '" . $empId . "'";
    $rs           = $db->query($sql);
    $mainRevCount = $db->num_rows($rs);

    $sql          = "select sealsid from seals where empid = '" . $empId . "' and empType='S' and now() between startDate and IF(endDate='0000-00-00',now(),IFNULL(endDate,now())) ";
    $rs           = $db->query($sql);
    $sealsCount   = $db->num_rows($rs);
    
    $sql          = "select sealsid from seals where empid = '" . $empId . "' and empType='F' and now() between startDate and IF(endDate='0000-00-00',now(),IFNULL(endDate,now())) ";
    $rs           = $db->query($sql);
    $financeCount   = $db->num_rows($rs);    

    //是,可查看/修改領據人員設定勾選的機構資料(領據編號)A
    if ( $sealsCount > 0) {
        $return_value = 'A';
    } else {
        //否,檢核是否為財務室人員
        if ($financeCount>0 ) {
            //是,可查看領據人員設定勾選的機構資料(領據編號),只可修改部門申請之資料B
            $return_value = 'B';
        } else if($mainRevCount > 0){ //總文書
            //否,只可查看/修改部門申請之資料C
            $return_value = 'D';
        } else {//一般人員
            //否,只可查看/修改部門申請之資料C
            $return_value = 'C';
        }
    }

    return $return_value;

}

function setSod($edisid){
    global $emplyeeinfo;
    global $departmentinfo;
    global $depInfo;
    $return_val = array();
    $db  = new db();
    $sql = "select ed.*, ogS.oid as sOid, ogR.oid as rOid, ogSo.oid as soOid 
            FROM edis ed 
            LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
            LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )
            LEFT JOIN map_orgs ogSo ON( ogSo.eid = ed.id AND ogSo.act = 'SO' )
            where ed.id = '" . $edisid . "' ";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    if (!empty($r['did'])) {
        $return_val[] = 
            array(
                'rOid'      => $r['rOid'],
                'rOidName'  => $departmentinfo[$r['rOid']],
                'subjects'  => $r['subjects'],
                'sOid'      => $r['depID'],
                'sOidName'  => $depInfo[$r['depID']],
                'sid'       => $r['creator'],
                'sidName'   => $emplyeeinfo[$r['creator']],
                'soOid'     => $r['sOid'],
                'soOidName' => $departmentinfo[$r['sOid']]
            );
    }
    return json_encode($return_val);
}
