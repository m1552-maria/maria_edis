<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	
	include_once('../config.php'); 	
    include_once "$root/inc_vars.php";
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
	include "$root/getEmplyeeInfo.php";
	include_once "../edis/func.php";
	include_once "func.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "領據";
	//$pageSize=3; 使用預設
	$tableName = "receipt";
	// for Upload files and Delete Record use
	$ulpath = "/data/receipt/"; 
	if(!file_exists($root.$ulpath)) mkdir($root.$ulpath);
	$delField = 'attach';
	$delFlag = true;
    //登入者類型
    $empReceiptType = receiptEmpCheck($_SESSION['empID']);
    //登入者部門及下屬部門
    $empDept = getReceiptEmpDept($_SESSION['empID']);
    $empDeptArray = explode(",",$empDept);
    //領據人員設定之負責機構之編碼簡稱
    $receiptOrgs = getreceiptOrgs($_SESSION['empID'],$empReceiptType);

    if($empReceiptType == 'B'){
	    $db  = new db();
	    $sql = "select depts from seals where empid='".$_SESSION['empID']."'";
	    $rs = $db->query($sql);
	    $r = $db->fetch_array($rs);
	    $receiptDep = explode(",",$r['depts']);
	}else{
		$receiptDep = array();
	}

	//發文機關選項
	$sql = "select id,sTitle from organization where id in('".implode("','",$orgUse)."')";
	$db = new db();
	$rs = $db->query($sql);
	while ($r = $db->fetch_array($rs)) {
	    $soOrgs[$r[0]] = $r[1];
	}

	$orgs = array();
	$orgs['']='-發文機構-'; //空字串:全部
	foreach ($soOrgs as $k=>$v) {
	    $orgs[$k] = $v;
	}

	//發文機關選項
	$sql = "select id,sTitle from organization where orgClass='IN'";
	$db = new db();
	$rs = $db->query($sql);
	while ($r = $db->fetch_array($rs)) {
	    $soOrgs[$r[0]] = $r[1];
	}

	$orgINs = array();
	$orgINs['']='-承辦單位-'; //空字串:全部
	foreach ($soOrgs as $k=>$v) {
	    $orgINs[$k] = $v;
	}

	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel),
		"extraButton" => array(array("統計報表",false,true,receipt_report))
	);
	//:ListControl  ---------------------------------------------------------------		
	//當年第一天
	$year=date("Y",time());
    $first=$year."-01-01";
	//當年最後一天
	$end=$year."-12-31";
	$opList = array(
		"canScroll"=>true,
		"alterColor"=>true,			//交換色
		"curOrderField"=>'receiptNo',	//主排序的欄位
		"sortMark"=>'DESC',			//升降序 (ASC | DES)
		"searchField"=>array('receiptNo','content'),	//要搜尋的欄位	
		"filterOption"=>array(
			'autochange'=>false,
			'applyDate'=>array($first.' ~ '.$end,false),	//預設Key值,是否隱藏
			'goBtn'=>array("確定送出",false,true,'')
		),
		"filterType"=>array(
			'applyDate'=>'DateRange'	//不定義就是 <select>, Month:月選取， DateRange:日期範圍
		),
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%s%%'"
		),
		"filters"=>array(
			'soOid' =>$orgs,
			'sOid' =>$orgINs,
			'applyDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
		),
		// "extraButton"=>array(
		// 	array("統計報表",false,true,'receipt_report'),
		// )

	);

	$fieldsList = array(
		//"receiptid"=>array("ID","30px",true,array('Id',"gorec(%d)")),
		//"receiptNo"=>array("序號","30px",true,array('Text')),
		"receiptNo"=>array("序號","25px",true,array('Id',"gorec(%d)")),
		"applyDate"=>array("申請日期","30px",true,array('Date')),
		"rOid"=>array("受文單位","35px",true,array('Define', getOrgName)),//func.php
		"content"=>array("領據內容","150px",false,array('Text')),
		"sOid"=>array("承辦單位", "30px",true,array('Define', getOrgName)),//func.php
		"sid"=>array("申請人", "25px", true, array('Define', getMan)),//func.php
		"amount"=>array("領據金額(元)","20px",true,array('Text')),
		"edisid"=>array("發文編號","30px",true,array('Define',getDid)),//func.php
		"attach"=>array("檔案","20px",false,array('File',60)),
		"isImport"=>array("已匯入","15px",false,array('Bool')),
		"isNuclearBook"=>array("核銷冊","15px",false,array('Bool')),
		"comment"=>array("備註","120px",false,array('Text'))
	);

	//:ViewControl -----------------------------------------------------------------
	if($empReceiptType=='B'){
	    $fieldA = array(
			"receiptNo"=>array("序號","readonly",20,'',true,array('','','',50)),
	        "edisid"=>array("發文編號","queryID",20,'','',array(false,'','',50)),
	        "soOid"=>array("發文機關","queryID",20,'soOid','departmentinfo',array(true,'','',50)),		
			"applyDate"=>array("申請日期","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
			"rOid"=>array("受文單位","queryID",20,'rAct','departmentinfo',array(true,'','',50)),
			"content"=>array("領據內容","textbox",60,10,'',array(true,'','',100)),
	        "sOid"=>array("承辦單位","queryID",20,'admDep','departmentinfo',array(true,'','',50)),
	        "sid"=>array("申請人","queryID",20,'empID','emplyeeinfo',array(true,'','',50)),
			"amount"=>array("領據金額(元)","text",20,'','',array(true,PTN_NUMBER,'',50)),
			"attach"=>array("檔案","file",60,''),
			"isImport"=>array("已匯入","checkbox",20),
			"isNuclearBook"=>array("核銷冊","checkbox",20),
			"comment"=>array("備註","text",60,'','',array(false,'','',150))

		);
		//$fieldA_Data = array("empID"=>$_SESSION['empID'],'isNuclearBook'=>1,'isImport'=>1);
		
		$fieldE = array(
			"receiptNo"=>array("序號","readonly",20,'',true,array('','','',50)),	
	        "edisid"=>array("發文編號","queryID",20,'','',array(false,'','',50)),		
			"applyDate"=>array("申請日期","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
			"rOid"=>array("受文單位","queryID",20,'rAct','departmentinfo',array(true,'','',50)),
			"content"=>array("領據內容","textbox",60,10,'',array(true,'','',100)),
	        "sOid"=>array("承辦單位","queryID",20,'admDep','departmentinfo',array(true,'','',50)),
	        "sid"=>array("申請人","queryID",20,'empID','emplyeeinfo',array(true,'','',50)),
			"amount"=>array("領據金額(元)","text",20,'','',array(true,PTN_NUMBER,'',50)),
			"attach"=>array("檔案","file",60,''),
			"isImport"=>array("已匯入","checkbox",60,5,''),
			"isNuclearBook"=>array("核銷冊","checkbox",60,5,''),	
			"comment"=>array("備註","text",60,'','',array(false,'','',150))	
		);
       
	}else{
		$fieldA = array(
			"receiptNo"=>array("序號","readonly",20,'',true,array('','','',50)),
	        "edisid"=>array("發文編號","queryID",20,'','',array(false,'','',50)),
	        "soOid"=>array("發文機關","queryID",20,'soOid','departmentinfo',array(true,'','',50)),		
			"applyDate"=>array("申請日期","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
			"rOid"=>array("受文單位","queryID",20,'rAct','departmentinfo',array(true,'','',50)),
			"content"=>array("領據內容","textbox",60,10,'',array(true,'','',100)),
	        "sOid"=>array("承辦單位","queryID",20,'admDep','departmentinfo',array(true,'','',50)),
	        "sid"=>array("申請人","queryID",20,'sId','emplyeeinfo',array(true,'','',50)),
			"amount"=>array("領據金額(元)","text",20,'','',array(true,PTN_NUMBER,'',50)),
			"attach"=>array("檔案","file",60,''),
			// "isImport"=>array("已匯入","hidden",60,5,''),
			// "isNuclearBook"=>array("核銷冊","hidden",60,5,''),				
			"comment"=>array("備註","text",60,'','',array(false,'','',50))	
		);
		//$fieldA_Data = array("empID"=>$_SESSION['empID'],'isNuclearBook'=>1,'isImport'=>1);
		
		$fieldE = array(
			"receiptNo"=>array("序號","readonly",20,'',true,array('','','',50)),	
	        "edisid"=>array("發文編號","queryID",20,'','',array(false,'','',50)),		
			"applyDate"=>array("申請日期","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
			"rOid"=>array("受文單位","queryID",20,'rAct','departmentinfo',array(true,'','',50)),
			"content"=>array("領據內容","textbox",60,10,'',array(true,'','',100)),
	        "sOid"=>array("承辦單位","queryID",20,'admDep','departmentinfo',array(true,'','',50)),
	        "sid"=>array("申請人","queryID",20,'sId','emplyeeinfo',array(true,'','',50)),
			"amount"=>array("領據金額(元)","text",20,'','',array(true,PTN_NUMBER,'',50)),
			"attach"=>array("檔案","file",60,''),
			// "isImport"=>array("已匯入","hidden",60,5,''),
			// "isNuclearBook"=>array("核銷冊","hidden",60,5,''),	
			"comment"=>array("備註","text",60,'','',array(false,'','',50))	
		);
		
    }
?>