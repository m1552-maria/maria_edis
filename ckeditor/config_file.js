﻿CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	//config.skin = 'v2'; //office2003 , kama
	// config.language = 'zh';
	// config.uiColor = '#AADC6E';
	
	//::上傳用
	//config.filebrowserBrowseUrl = '/browser/browse.php';
  //config.filebrowserUploadUrl = '/uploader/upload.php';
	config.filebrowserImageBrowseUrl = '/ckeditor/browse.php?type=Images';
	config.filebrowserImageUploadUrl = '/ckeditor/upload.php?type=img'; 
	//config.filebrowserFlashUploadUrl = './upload.php?type=flash'; 

	config.filebrowserImageWindowWidth = '640';
  config.filebrowserImageWindowHeight = '480';


	/*::工具列
	config.toolbar = [
    ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent'],
    ['JustifyLeft','JustifyCenter','JustifyRight'],
    ['Link','Unlink','Image','Table','HorizontalRule'],['Undo','Redo'],
		'/',
    ['Format','Font','FontSize'],['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','Source','Save']
	]; */
	
	config.toolbar = 'Full';
	config.contentsCss = '/css/index.css';
	config.scayt_autoStartup = false;
	config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
	config.protectedSource.push( /<style.+>[\S\s]*<\/style>/gi );
};
