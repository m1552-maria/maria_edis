﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.skin = 'v2'; //office2003 , kama
	// config.language = 'zh';
	// config.uiColor = '#AADC6E';
	
	//::上傳用
	config.filebrowserImageBrowseUrl = '/ckeditor/browse.php?type=Images';
	config.filebrowserImageUploadUrl = '/ckeditor/upload.php?type=img'; 	
	//config.filebrowserFlashUploadUrl = './upload.php?type=flash'; 

	//::工具列
	config.toolbar = [
    ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent'],
    ['JustifyLeft','JustifyCenter','JustifyRight'],
    ['Link','Unlink','Image','Table','HorizontalRule'],['Undo','Redo'],
		'/',
    ['Format','Font','FontSize'],['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','Source']
	]; //*/
	
	/*::工具列
	config.toolbar = [
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','Source','-','Undo','Redo','Templates']
	]; */
	config.scayt_autoStartup = false;
	config.contentsCss = 'ckeditor/all.css';	
	config.protectedSource.push( /<\?[\s\S]*?\?>/g ); 
	config.protectedSource.push( /<style.+>[\S\s]*<\/style>/gi );
	//config.templates_files = [ '/ckeditor/mytemplates.js'];
};
