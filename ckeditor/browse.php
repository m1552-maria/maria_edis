<?php
	@session_start();
	/* close for php5.x
	function scandir($dir) {
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh))) $files[] = $filename;
		return $files;
	} */
	//print_r($_REQUEST);
	$callback = $_REQUEST[CKEditorFuncNum];
	if($_SESSION["Account"]) $dirV = "/data/contents/".$_SESSION["Account"]; else	$dirV = "/data/contents";
	$dir = $_SERVER['DOCUMENT_ROOT'].$dirV;	
	//刪除檔案
	if ($_REQUEST[delfn]) unlink($_REQUEST[delfn]);		
	if(!file_exists($dir)) mkdir($dir,777,true);		
	$flst = scandir($dir); 
	//過濾不需要的項目及子目錄
	$aa = array();
	foreach($flst as $fn) {
		$inf = pathinfo($fn);
		$ext = strtolower($inf['extension']);
		if ($ext=='jpg' or $ext=='gif' or $ext=='png')  $aa[] = $fn;	
	}
	$aa = array_reverse($aa);
	$pgct = 98;
	$all = count($aa);
	$pages = ceil($all / $pgct);
	$curP = $_GET['P']?$_GET['P']:0;
	$ab = array_slice($aa,$curP*$pgct,$pgct);
	$pUrl = "?type=$_GET[type]&CKEditor=$_GET[CKEditor]&CKEditorFuncNum=$_GET[CKEditorFuncNum]&langCode=$_GET[langCode]";
	$bUrl = $_SERVER['PHP_SELF'].$pUrl;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>檔案瀏覽</title>
<style type="text/css">
	img { cursor:pointer}
	body { font-size:12px; margin: 0px; background-color:#F0F0F0; overflow:hidden }
	.colhd { font-size:larger; background-color:#CCCCCC }
</style>
<script type="text/javascript">
function doSel() {
	var Caption = document.getElementById('Caption');
	fn = '<?=$dirV?>'+'/'+Caption.value;
	window.opener.CKEDITOR.tools.callFunction(<?=$callback?>,fn); 
	window.close();
}
function imgClick(fn) { document.getElementById('Caption').value = fn; }
function changePage(elm) {
	var pn = elm[elm.selectedIndex].value;
	location.href="<?=$bUrl?>"+"&P="+pn;
}
</script>
</head>

<BODY>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr>
	<td class="colhd" colspan="4">&nbsp;圖片總數量：<?=$all?> &nbsp;|&nbsp; 每頁：<?=$pgct?>
  		&nbsp;|&nbsp; 目前在：<select name="pagen" size="1" onChange="changePage(this)"><? for($i=1; $i<=$pages; $i++) {
			$v = $i-1;
			echo "<option value='$v' ".($v==$curP?'selected':'').">第".$i."頁</option>";
		} ?></select>
  </td>
	<td colspan="3" valign="absmiddle" class="colhd" >
		<INPUT id="NumRows" type="hidden" size="0" name="NumRows" onkeypress="event.returnValue=IsDigit();">
		<INPUT id="NumCols" type="hidden" size="0" name="NumCols" onkeypress="event.returnValue=IsDigit();">
		圖片名稱：<INPUT type="text" size="30" name="Caption" id="Caption" readonly>&nbsp;
		<BUTTON id="Ok" onClick="doSel()" >插入圖片</BUTTON>&nbsp;<BUTTON onclick="window.close();">取消</BUTTON>
	</td>
</tr>
</table>
<div style='overflow:auto; padding: 5px; height:620px'>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr bgcolor="#F0F0F0">
<?
	$count = 0;
	foreach($ab as $fn) {
		echo "<td><image src='$dirV/$fn' width=100 onclick=\"imgClick('".$fn."')\"><br>[<a id='reload' href='browse.php".$pUrl."&delfn=$dir/$fn'>刪除</a>] $fn</td>";
		$count++;
		if ($count>6) {	$count = 0;	echo "<tr bgcolor='#F0F0F0'>"; }		
	}
?>
</table>
</div>
</BODY>
</html>