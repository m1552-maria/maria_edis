<?php 
@session_start();
include 'config.php';
include 'system/db.php';
include 'getEmplyeeInfo.php';
include_once 'setting.php';
include_once 'edis/func.php';

//管理平台參數解密
function mic_crypt($bases, $string, $action = 'e') {
    $output = false;
    $secret_key = $bases.'_key';
    $secret_iv = $bases.'_iv';
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $secret_key );
    $iv = substr(hash('sha256', $secret_iv ),0,16);
    if( $action == 'e' ) {
        $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
    }else if( $action == 'd' ){
        $output = openssl_decrypt(base64_decode($string ), $encrypt_method, $key, 0, $iv);
    } 
    return $output;
}

//add by Tina 20190125 檢核登入身分別(文書/主管/一般/執行長)
function setLoginType($empID, $jobID){
    $db = new db();
    $rs = $db->query("select * from organization where (revMan='".$empID."' or mainRevMan ='".$empID."' or proxyMainRevMan = '".$empID."' or proxyRevMan = '".$empID."')");

    if($db->num_rows($rs)>0){//文書
        $_SESSION["loginType"] ='rev';
    }else{
        if($jobID=='G002' or $jobID == 'G001'){ //執行長or董事長
            $_SESSION["loginType"] ='cxo';
        }else{
            $op = array(
                'dbSource'   => DB_ADMSOURCE,
                'dbAccount'  => DB_ACCOUNT,
                'dbPassword' => DB_PASSWORD,
                'tableName'  => '',
            );
            $db       = new db($op);
            $rs       = $db->query("select * from department where leadID = '".$empID."'");
            if($db->num_rows($rs)>0){//主管       
                $_SESSION["loginType"] ='lead';
            }else{//一般
                $_SESSION["loginType"] ='normal';
            }
        }
    }
}

//add by Tina 20190226 檢核登入者是否設定功能權限
function setPermission($empID){
    $db       = new db();
    $rs       = $db->query("select menuID from sys_menu_permission where (empID=".$empID.")");
    $_SESSION["menu_permission"] = array();//建立收文權限
    while ($r = $db->fetch_array($rs)) {
        $_SESSION["menu_permission"][$r['menuID']]=$r['menuID'];
    }
}

if (isset($_REQUEST['Submit'])) {
    $volBol   = 0;
    //愛心志工登入
    if($_REQUEST['empID'] == 'love' && $_REQUEST['password'] == 'pwdlove'){
        $volBol   = 1;
        $db       = new db();
        $volrs    = $db->query("select * from organization where id='A'");
        $rVol     = $db->fetch_array($volrs);
        $empID    = trim($rVol['revMan']);
        $op       = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );
        $db       = new db($op);
        $rs       = $db->query("select password from emplyee where empID=? ", array($empID));
        $r        = $db->fetch_array($rs);
        $password = trim($r['password']);

    }else if($_REQUEST['empID'] == 'love1' && $_REQUEST['password'] == 'pwdlove'){
        $volBol   = 1;
        $db       = new db();
        $volrs    = $db->query("select * from organization where id='D'");
        $rVol     = $db->fetch_array($volrs);
        $empID    = trim($rVol['revMan']);
        $op       = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );
        $db       = new db($op);
        $rs       = $db->query("select password from emplyee where empID=? ", array($empID));
        $r        = $db->fetch_array($rs);
        $password = trim($r['password']);

    }else{
        $empID    = trim($_REQUEST['empID']);
        $password = getRulPwd($empID, trim($_REQUEST['password']));
    }


    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db       = new db($op);
    if($volBol){
        $rs       = $db->query("select * from emplyee where empID=? and password = ?", array($empID, $password));
    }else{
        $rs       = $db->query("select * from emplyee where empID=? and password = SHA2(?, 256)", array($empID, $password));
    }
    
    
    if ($db->eof($rs)) {
        echo "<script>window.onload=function(){alert('帳號或密碼錯誤');}</script>";
    } else {
        session_unset(); //防止由後台直接過來
        $r                   = $db->fetch_array($rs);
        $_SESSION["empName"] = $r['empName'];
        $_SESSION["empID"]   = $r['empID'];
        $_SESSION["depID"]   = $r['depID'];
        $_SESSION["depTL"]   = $departmentinfo[$r['depID']];
        $_SESSION["jobID"]   = $r['jobID'];
        $_SESSION["jobTL"]   = $jobinfo[$r['jobID']];
        $_SESSION["loginTm"] = date("H:i:s");

        if($_REQUEST['empID'] == 'love' && $_REQUEST['password'] == 'pwdlove'){
            $_SESSION["loginType"] ='vol';
        }else if($_REQUEST['empID'] == 'love1' && $_REQUEST['password'] == 'pwdlove'){
            $_SESSION["loginType"] ='vol';
        }else{
            setLoginType($r['empID'],$r['jobID']);
            setPermission($r['empID']);
        }
        
        header('Location: index.php');
        exit;
    }
}

//add by Tina 20190304 檢核是否由管理平台進入
$dcStr = mic_crypt($_REQUEST['act'], $_REQUEST['cryp'], 'd'); 
$orgV  = json_decode($dcStr);

if(strlen($orgV->empID)>0){

    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db       = new db($op);
    $empID    = trim($orgV->empID);
    $sql      = "select * from emplyee where empID='".$empID."'";
    $rs       = $db->query($sql);
    if ($db->num_rows($rs)>0) {
        session_unset(); //防止由後台直接過來
        $r                   = $db->fetch_array($rs);
        $_SESSION["empName"] = $r['empName'];
        $_SESSION["empID"]   = $r['empID'];
        $_SESSION["depID"]   = $r['depID'];

        $_SESSION["depTL"]   = $departmentinfo[$r['depID']];
        $_SESSION["jobID"]   = $r['jobID'];
        $_SESSION["jobTL"]   = $jobinfo[$r['jobID']];
        $_SESSION["loginTm"] = date("H:i:s");

        setLoginType($r['empID'],$r['jobID']);
        setPermission($r['empID']);
        header('Location: index.php');
        exit;  
    }
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <title><?php  echo $CompanyName?></title>
        <link href="css/login.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="index">
        	<div class="main">
        		<div class="table">
                    <div class="table-row"><div class="logo"><div class="img"></div></div></div>
                    <div class="table-row"><div class="title">瑪利亞公文系統</div></div>
                    <div class="table-row">
                        <div class="form">
                            <form name="form1" method="post" action="">
                              	<input type="text" class="input username" name="empID" placeholder="員工編號" />
                              	<input type="password" class="input password"  name="password" placeholder="登入密碼" />
            				    <input type="checkbox" name="remember" value="1"/> 記住我的帳號<br/>
                  	            <input type="submit" class="button" name="Submit" value= "送出"  />
                  	            <input type="reset" class="button reset" name="reset"  value= "重填"  />
                            </form>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="info">瑪利亞公文系統</div>
                    </div>
        		</div>
        	</div>
            <div class = "footer">2017 © Created by 雲碼資訊.</div>
        </div>
    </body>
</html>