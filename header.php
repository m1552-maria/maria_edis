<?php
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/inc_vars.php";
include_once "$root/edis/func.php";
//登入者為誰的代理人及已離職職員之主管

error_log(date('Y-m-d H:i:s')." before query for couting each status.\n", 3, 'C:/wamp/logs/haotung-debug.log');
$db  = new db();
$ID  = $_SESSION[empID];
$n1  = 0; //::應簽收文
$n2  = 0; //::過期應簽收文
$n3  = 0; //::簽核未通過收文
$n4  = 0; //::應簽發文
$n5  = 0; //::簽核未通過發文
$sql = "SELECT m.deadline, m.did 
        FROM map_orgs_sign mos
        JOIN edis m ON(mos.edisid = m.id)
        WHERE mos.signMan = '$ID' 
          AND mos.isSign IS NULL 
          AND (m.void = '0' or m.void IS NULL)
          AND m.odType = '收文'
          AND mos.signLevel <> '4'";
$rs  = $db->query($sql);
while ($r = $db->fetch_array($rs)) {
    if(empty($r['deadline'])) continue;

    if($r['deadline'] >= date("Y-m-d")){
        $n1++;
    }elseif($r['deadline'] < date("Y-m-d")){
        $n2++;
    }
}

$sql = "SELECT m.deadline, m.did 
        FROM map_orgs_sign mos
        JOIN edis m ON(mos.edisid = m.id AND m.signStage = mos.signLevel)
        WHERE mos.signMan = '$ID' 
          AND mos.isSign IS NULL 
          AND (m.void = '0' or m.void IS NULL)
          AND m.odType = '發文'
          AND m.signStage <> '4'";
$rs  = $db->query($sql);
while ($r = $db->fetch_array($rs)) {
    $n4++;
}

//::簽核未通過收文
$sql  = "SELECT * 
         FROM edis ed
         WHERE (ed.creator = '$ID' OR ed.sId = '$ID')
           AND EXISTS(SELECT 1 FROM map_orgs_sign WHERE edisid = ed.id AND isSign = '0')
           AND (ed.void = '0' or ed.void IS NULL)
           AND ed.signStage <> '4'";
$rs  = $db->query($sql);
while ($r = $db->fetch_array($rs)) {
    if($r['odType'] == '收文'){
        $n3++;
    }else if($r['odType'] == '發文'){
        $n5++;
    }
}

error_log(date('Y-m-d H:i:s')." after query for couting each status.\n", 3, 'C:/wamp/logs/haotung-debug.log');

$nTotal = $n1+$n2+$n3+$n4+$n5;
?>
<div class="header navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">  <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="container-fluid">
      <a class="brand" href="/index.php">瑪利亞公文系統</a>  <!-- BEGIN LOGO -->
      <!-- BEGIN RESPONSIVE MENU TOGGLER -->
      <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse"><img src="media/image/menu-toggler.png" /></a>
      <!-- BEGIN TOP NAVIGATION MENU -->
<?php  if($_SESSION["loginType"] !=='vol') { ?>
      <ul class="nav pull-right">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <li class="dropdown" id="header_notification_bar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-warning-sign"></i><span class="badge"><?php  echo $nTotal;?></span>
        </a>
        <ul class="dropdown-menu extended notification">
            <li><p>資訊小助理</p></li>
            <li><a href="/index.php?funcUrl=edis/rod/unsign.php&muID=0"><span class="label label-warning"><i class="icon-bell"></i></span>您有 <font color="#0000FF"><?php  echo $n1;?></font> 個應簽收文</a></li>
            <li><a href="/index.php?funcUrl=edis/rod/unsign.php&muID=0"><span class="label label-warning"><i class="icon-bell"></i></span>您有 <font color="#0000FF"><?php  echo $n2;?></font> 個過期應簽收文</a></li>
            <li><a href="/index.php?funcUrl=edis/rod/myod.php&muID=0"><span class="label label-warning"><i class="icon-bell"></i></span>您有 <font color="#0000FF"><?php  echo $n3;?></font> 個簽核未通過收文</a></li>
            <li><a href="/index.php?funcUrl=edis/sod/unsign.php&muID=1"><span class="label label-warning"><i class="icon-bell"></i></span>您有 <font color="#0000FF"><?php  echo $n4;?></font> 個應簽發文</a></li>
            <li><a href="/index.php?funcUrl=edis/sod/myod.php&muID=1"><span class="label label-warning"><i class="icon-bell"></i></span>您有 <font color="#0000FF"><?php  echo $n5;?></font> 個簽核未通過發文</a></li>
        </ul>

        <!-- BEGIN USER LOGIN DROPDOWN -->
        <li class="dropdown user">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="http://adm.maria.org.tw/data/emplyee/<?php  echo $ID;?>.jpg" style="max-height:28px; padding-right:5px" /><span class="username"><?php  echo $_SESSION["empName"] . ' ' . $_SESSION["jobTL"];?></span><i class="icon-angle-down"></i>
        </a>
        <ul class="dropdown-menu">
            <li><a href="logout.php"><i class="icon-power-off"></i> 登出系統</a></li>
        </ul>
    </li>
</ul>
<?php  }else{ ?>
<ul class="nav pull-right">  
    <!-- BEGIN USER LOGIN DROPDOWN -->
    <li class="dropdown user">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="username">愛心家園志工</span><i class="icon-angle-down"></i>
    </a>
    <ul class="dropdown-menu">
        <li><a href="logout.php"><i class="icon-power-off"></i> 登出系統</a></li>
    </ul>
</li>          
</ul>
<?php  } ?>      
</div>
</div>
</div>