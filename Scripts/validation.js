/* JavaScript 資料驗證 - 函式庫 */

function engstr(str){	//是否有中文字
   var subStr = "";
   for (i=0,n=str.length;i<n;i++){
      subStr = str.charCodeAt(i);
      if ((subStr > 256)) return false;
   }
   return true;
}

function checkPID(string) {	// 簡易身份證字號驗證
	re = /^[A-Z]\d{9}$/;
	if (re.test(string)) {
		return true;
	} else {
		alert("身份證字號格式錯誤!");
		return false;
	}
}

function checkCreditCard(string) { //簡易信用卡卡號驗證
	re = /^\d{4}-\d{4}-\d{4}-\d{4}$/;
	//re = /^(\d{4}-){3}\d{4}$/;
	if (re.test(string)) {
		alert("成功：符合「" + re + "」的格式！");
		return true;
	} else {
		alert("失敗：不符合「" + re + "」的格式！");
		return false;
	}
}

function checkEmail(string) { //電子郵件格式驗證(可以避開含有空白的電子郵件帳號)
	re = /^[^\s]{3,}@[^\s]+\.[^\s]{2,3}$/;
	if (re.test(string)) {
		return true;
	} else {
		alert("E-Mail格式錯誤 !");
		return false;
	}
}


// 檢查有無特殊字元
function checkSpecialLetter(ckstr) {
	var allValid = true;
	var ckLetters = "~`!@#$%^&*+=\|[]{}'/?<>,";
	for (i=0; i<ckstr.length; i++) {
		ch = ckstr.charAt(i);
		for (j=0; j<ckLetters.length; j++)
			if (ch == ckLetters.charAt(j)) break;
		if (j != ckLetters.length) { allValid=false; break; }
	}
	if (!allValid) {alert("您的名稱中有特殊字元\n請修正您的名稱！");	return false;	}
	return true;
}

// 字串欄位空白檢查 ------------------------------------------- //
function StrValidate(TEdit,nlen,sTitle) {
	if (TEdit.value.length==0) {
		alert("["+sTitle+"] 欄位 不能為空白！");
		TEdit.focus();
		return false;
	}	else if (TEdit.value.length>nlen) {
		alert("["+sTitle+"] 欄位 長度超過！");
		TEdit.focus();
		return false;
	}
	return true;
}

// 數值欄位資料正確性檢查 ------------------------------------- //
function NumValidate(value) {
  if (value == "") { alert("資料不能空白，請於欄位中輸入一數值"); return false; }
  var checkOK = "0123456789-.,";
  var checkStr = value;
  var allValid = true;
  var decPoints = 0;
  var allNum = "";
  for (i = 0; i<checkStr.length; i++) {
    ch = checkStr.charAt(i);
    for (j = 0; j<checkOK.length; j++) if (ch == checkOK.charAt(j)) break;
    if (j == checkOK.length) { allValid = false; break; }
    if (ch != ",") allNum += ch;
  }
  if (!allValid) { alert("只能輸入數字字元。"); return false; }
  return true;
}

// 電話號碼欄位資料正確性檢查 ------------------------------------- //
function TelValidate(value) {
  if (value == "") { alert("資料不能空白，請於欄位中輸入一數值。"); return (false); }
	if(value.charAt(0)!='0') { alert("資料格式錯誤。"); return (false); }
  var checkOK = "0123456789";
  var checkStr = value;
  var allValid = true;
  var decPoints = 0;
  var allNum = "";
  for (i = 0;  i < checkStr.length;  i++) {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)  if (ch == checkOK.charAt(j)) break;
    if (j == checkOK.length)  {  allValid = false; break; }
    if (ch != ",") allNum += ch;
  }
  if (!allValid) { alert("只能輸入 數字 字元。");  return (false); }
  return (true);
}

// 日期欄位資料正確性驗證 ----------------------------------- //
function DateValidate(value) {
  if (value == "") {
    alert("日期欄位不可以空白 !");
    return (false);
  }
  
  if ((value.charAt(4)=="/") && ((value.charAt(7)=="/") || (value.charAt(6)=="/")))
    return (true);
  else {
    alert("日期格式錯誤，請用 yyyy/mm/dd 的方式輸入 !");
    return (false); 
  }
}

function DateTimeValidate(value) {
	return value.match(/^\d{4}\/\d{2}\/\d{2}\s+\d{2}:\d{2}$/);
}