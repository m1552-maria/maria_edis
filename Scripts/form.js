// JavaScript Document
var id2proc = { 
		edisid: 		'/edis/query_sod_id.php',

		sid: 			'/emplyee/query_id.php',
		sId: 			'/emplyee/query_idT.php',
		sIds: 			'/emplyee/query_idTs.php',
		empID: 			'/emplyee/query_id.php',
		revMan: 		'/emplyee/query_id.php',
		sidMgr: 		'/emplyee/query_lead_id.php',
		orderEmpid: 	'/emplyee/query_order_id.php',
		proofreader:	'/emplyee/query_id.php',
		empIDt:  		'/emplyee/query_idT.php',

		sAct: 			'/organization/query_inner_id.php',
		rAct: 			'/organization/query_org_id.php',
		sOid: 			'/organization/query_id.php',
		rOid: 			'/organization/query_id.php',
		soAct:			'/organization/query_inner_id.php',
		roAct: 			'/organization/query_inner_id.php',
		admDep: 		'/organization/query_dep_id.php',
		admDeps: 		'/organization/query_admDep_ids.php',
		orgIDs: 		'/organization/query_org_ids.php',
		depID: 			'/organization/query_id.php',
		depIDs: 		'/organization/query_dep_ids.php',
		depInnerID:		'/organization/query_inner_id.php',
		soOid: 			'/organization/query_inner_id.php',
		
		orgClass: 		'/org_class/query_id.php',
		// mainRevMan: '/emplyee/query_id.php',//
		// newSigner:'/emplyee/query_id.php',//
		// secretViewer:'/emplyee/query_id.php',//
		
		// empFincID:'/emplyee/query_id_finc.php',//
		// OutdepID:'/organization/query_outer_id.php',//	
		// nAct: '/organization/query_inner_id.php',// 0911暫時修改
		// cAct: '/organization/query_inner_id.php',// 0911
		//	
	};

var id2check = {
	sId:'empName'
};

//var obj;
var param;
var rzt = false;
function showDialog(aObj) {

	if(rzt){ rzt.close();	rzt = false; } 
	param = $(aObj).prev(); 
	var parid = param.attr('id');
	var procP = id2proc[parid];
	var depID;
	var paramsObj=[];
	if(parid!='depID') {

		if(typeof qryField != 'undefined') {
			depID = $('#'+qryField).val();			
		} else {
			depID = $('#depID').val();
			//add by Tina 20190307 總表管理:領據 申請人需以申請單位篩選
			if(parid=='empID' && depID==undefined){
			 depID = $('#depInnerID').val();
			}
		}

		///if(depID)	procP += ('?depID='+depID);
		if(depID)	paramsObj.push('depID='+depID);

	}
	

	//add by Tina 20180904 發文:發文單位需連動副本第一欄及受文者需連動正本第一欄
	paramsObj.push('parid='+parid);
	paramsObj.push('odtype='+param.attr('data-odtype'));
	paramsObj.push('act='+param.attr('data-act'));

	if(param.attr("allowAll"))	paramsObj.push('allowAll='+param.attr("allowAll"));
	if(param.attr("uiType"))	paramsObj.push('uiType='+param.attr("uiType"));
	if(paramsObj.length>0)	procP += "?"+paramsObj.join("&");
	if (rzt == false) {
		if(parid=='pmtID' || parid=='pmpID' || parid=='orgIDs') rzt = openBrWindow(procP,param.val(),650,600);
		else if(parid=='depID_other'){
			str = '';
			$('[name="depID[]"]').each(function(){ paramsObj.push($(this).val());	})
			str += paramsObj.join(",");
			str +='/';
			paramsObj=[];
			$('[name="empID[]"]').each(function(){ paramsObj.push($(this).val());	})
			str += paramsObj.join(",");
			rzt = openBrWindow(procP,str,300,450);
		}else if(parid=='depIDs'){

		//str = 'A,A1,C/aaa,bbb,FFF';	//範例
		/*
		paramsObj=[];
		$('[name="depID[]"]').each(function(){ paramsObj.push($(this).val());	})
		str += paramsObj.join(",");
		str +='/';
		paramsObj=[];
		$('[name="empID[]"]').each(function(){ paramsObj.push($(this).val());	})
		str += paramsObj.join(","); */
		str ='';
		rzt = openBrWindow(procP,str,300,450);	//打卡多選
	}	else rzt = openBrWindow(procP,param.val(),300,450);
	rzt.focus();
}

	//rzt.postMessage('message',domain);
	/*setTimeout(function(){
		rzt.postMessage('in', procP);
	},500);*/

	/*if(rzt) { 
		alert(rzt);
		param.val(rzt[0]); 
		$(aObj).next().text(rzt[1]); 
		//:: Handler for callback
		var cbFunc = param.attr('callback');
		if(cbFunc) setTimeout(cbFunc+'("'+rzt[0]+'")',0);
	}*/
	
}

function returnValue(data) {

	if(data) {
		param.val(data[0]);
		param.trigger("focus");
		param.next().next('span').html(data[1]); 
		var cbFunc = param.attr('callback');
		if(cbFunc) setTimeout(cbFunc+'("'+data[0]+'")',0);
	}
}

function returnValue_other(data) {
	if(data) {
		detail = '';
		detail_emp = '';
		$.each(data,function(e,v){
			if(v['type']=='depID'){
				if(v['privilege']=='Y'){
					detail = detail + '<li><input type="hidden" name="depID[]" value="'+v['id']+'">'+v['name']+'<img src="/images/remove.png" width="18" align="absmiddle" onclick="d_remove(this)"></li>';
				}else{
					detail = detail + '<li style="display:none"><input type="hidden" name="depID[]" value="'+v['id']+'">'+v['name']+'</li>';
				}
				
			}
			if(v['type']=='empID'){
				if(v['privilege']=='Y'){
					detail_emp = detail_emp + '<li><input type="hidden" name="empID[]" value="'+v['id']+'">'+v['name']+'<img src="/images/remove.png" align="absmiddle" width="18" onclick="d_remove(this)"></li>';
				}else{
					detail_emp = detail_emp + '<li style="display:none"><input type="hidden" name="empID[]" value="'+v['id']+'">'+v['name']+'</li>';	
				}
			}
		})
		$('.depID_list').html('<ul>'+detail+'</ul>');
		$('.empID_list').html('<ul>'+detail_emp+'</ul>');
	}
}

/*** 對話框keyin的資料作檢查  ***/
function checkInput(evt,act,elm) {
	var v = elm.value;
	function handleRezult(data) { //Closure
		if(data) {
			if(data=='無此編號！') {
				$(elm).val('');
				$(elm).next().next().text(v+' '+data); 
			} else $(elm).next().next().text(data); 
		}		
	}
	
	if( typeof evt === 'undefined' ) { //with ViewControl
		$.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
		return false;
	} else {  //without ViewControl
		if(evt.keyCode==13) {
			$.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
			return false;
		}
	}
}

function checkEmpID(value) {
	if ( !NumValidate(value) ) return false;
	if ( value.length!=5 ) { alert('請輸入完整的5位數員工編號'); return false; }
	return true;
} 

function d_remove(now){
	$(now).parent('li').remove();
}


var ppppp;
function openBrWindow(theURL,inParam,win_width,win_height) {
	var PosX = (document.body.clientWidth-win_width)/2; 
	var PosY = (document.body.clientHeight-win_height)/2; 
	features = "width="+win_width+",height="+win_height+",top="+PosY+",left=-400,scrollbars=yes"; 
	var newwin = window.open(theURL,'',features);
	ppppp = inParam;	
	newwin.selid = inParam;	//send to callee
	return newwin;
} 

function genQueryIcon(idx,elm) {
	var str = $(elm).attr('title');
	if(str) {	
		$(elm).after("<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)' align='absmiddle'/><span class='formTxS'>"+str+"</span>");
	} else {
		$(elm).after("<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)' align='absmiddle'/><span class='formTxS'></span>");
	}	
}


$(function() {
	var idstr='';
	$(".queryID").each(genQueryIcon);
	//$('.queryDate').datepicker(	{showOn: 'button', buttonImage: '/images/calendar.jpg', buttonImageOnly: true, dateFormat:'R/mm/dd'});
	$('.queryDate').datepicker(	{showOn: 'button', buttonImage: '/images/calendar.jpg', buttonImageOnly: true});
	$('.queryDate').datepicker('option', 'duration', '');
	$('.queryDate').datepicker($.datepicker.regional['zh-TW']);

	if($('.queryDateTime').length>0) $('.queryDateTime').datetimepicker();
	
	$("select[name='county']").change(function(){
		var city = $(this).next().val();	//鄉鎮市要緊接縣市
		var cd2 = this.options[this.selectedIndex].value; 
		$.getJSON('/getptycd3.php',{'cd2':cd2})
		.done(function(data){
			var str = '';
			$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
			$("select[name='city']").html(str);
		})
		.fail(function(err){ 
			console.log(err.responseText); 
		});
	});
	$("select[name='county']").change();
});
