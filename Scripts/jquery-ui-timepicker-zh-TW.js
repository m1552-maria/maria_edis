/* Chinese translation for the jQuery Timepicker Addon */
/* Written by Alang.lin */
(function($) {
	$.timepicker.regional['zh-TW'] = {
		monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
		monthNamesShort : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
		dayNamesMin : ['日','一','二','三','四','五','六'],
		dayNamesShort: ['周日','週一','週二','週三','週四','週五','週六'],		
		timeOnlyTitle: '選擇時分秒',
		timeText: '時間',
		hourText: '時',
		minuteText: '分',
		secondText: '秒',
		millisecText: '毫秒',
		timezoneText: '時區',
		currentText: '現在時間',
		closeText: '確定',
		dateFormat: 'yy/mm/dd',
		timeFormat: 'hh:mm tt',
		amNames: ['上午', 'AM', 'A'],
		pmNames: ['下午', 'PM', 'P'],
		ampm: false
	};
	$.timepicker.setDefaults($.timepicker.regional['zh-TW']);
})(jQuery);
