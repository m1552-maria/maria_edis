<?
	define('PTN_NUMBER','[0-9]*');
	//define('PTN_DECIMAL','^\d+(\.\d+)?$');
	define('PTN_DECIMAL','^[0-9]+(\.[0-9]+)?$');
	define('PTN_CREDITCARD','[0-9]{4}[-][0-9]{4}[-][0-9]{4}[-][0-9]{4}');
	define('PTN_DATE','[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01])');
	define('PTN_DATETIME','[0-9]{4}/(0[1-9]|1[012])/(0[1-9]|1[0-9]|2[0-9]|3[01]) \d\d:\d\d');
	define('PTN_EMAIL','[^@\s]+@[^@\s]+\.[^@\s]+');
	define('PTN_PERSONID','[A-Z]\d{9}');
	define('PTN_TEL','\d{2,3}[\-]\d{7,8}');
	define('PTN_MOBILE','\d{4}[\-]\d{6}');

	class BaseViewControl {
	/* Init 欄位設定参述說明:
		 *field spec : key=>array(
		 * 名稱,
		 * 形態,
		 * 寬度,
		 * 高度(QueryID: Name(empID,depID,...), Others: Rows),
		 * 形態搭配用途(readonly:顯示用的Array變數; text:單位; select:array; Define:func-name; queryID:ID->NAME的變數),
		 * Validation-Array(required,pattern,Hint,maxLength)
		 * )
	*/
		protected $defaultOption = array(
			"templet"=>"",				//append.html,'空值'表示不使用版型
			"form"=>array('form1','','post','multipart/form-data',''),	//name,action,post,enctype,onSubmit-handler
			"type"=>'view',				//view | append | edit
			"dataKey"=>true,			//get data with Key
			"submitBtn"=>array("確定變更",FALSE,''),	//name,disabled,event-handler
			"resetBtn"=>array("重新輸入",FALSE,''),
			"cancilBtn"=>array("取消編輯",FALSE,'')
		);
		
		//UI	
		protected $submitBtn = "<button type='submit' name='Submit' class='submitBtn'";
		protected $resetBtn  = "<button type='reset' name='Reset' class='resetBtn'";
		protected $cancilBtn = "<button type='button' class='cancilBtn' onclick='history.back()'";
		
		protected $mData   = array();	//View's Data
		protected $mFields = array(); 	
			
		function __construct($data=array(), $fields=array(), $option=array())	{
			if(!empty($option)) {
				if($option['templet']) $this->defaultOption['templet'] = $option['templet'];
				if($option['form']) $this->defaultOption['form'] = $option['form'];
				if($option['type']) $this->defaultOption['type'] = $option['type'];
				if(isset($option['dataKey'])) $this->defaultOption['dataKey'] = $option['dataKey'];
				if($option['submitBtn']) $this->defaultOption['submitBtn'] = $option['submitBtn'];
				if($option['resetBtn']) $this->defaultOption['resetBtn'] = $option['resetBtn'];
				if($option['cancilBtn']) $this->defaultOption['cancilBtn'] = $option['cancilBtn'];
			}
			
			$this->mFields = $fields;
			$this->mData = $data; 
			echo "<div id='ViewControl'>";
				
			$ctx = "<form name='". $this->defaultOption['form'][0]
					 . "' action='" . $this->defaultOption['form'][1]
			     . "' method='" . $this->defaultOption['form'][2]
			     . "' enctype='". $this->defaultOption['form'][3] . "'";
			if($this->defaultOption['form'][4]) $ctx .= " data-handler='".$this->defaultOption['form'][4]."'";     
			$ctx .= ">";
			if($this->defaultOption['type']=='view') $ctx .= "<fieldset disabled>";

			if($this->defaultOption['templet']) {	//有版型
				$tpl = file_get_contents($this->defaultOption['templet']);
				$idx = 0;
				foreach($this->mFields as $key=>$item ) {
					if($item[1]=='uLABEL') continue;	//排除，由引用者產生				
					$str = $this->genDOM($idx,$key,$item);
					$tpl = str_replace("{#$key}", $str, $tpl);
					$idx++;
				}
			 
				//::Surport Detail Table ---------------------------------
				if($option['detailField']) {
					//for new Row varieants
					if($option['type'] !== 'view'){
					foreach($option['detailField'] as $key=>$item ) {
						if($item[1]=='uLABEL') continue;	//排除，由引用者產生					
						$str = $this->genDOM(0,$key,$item,array());
						$tpl = str_replace_first("{[#$key]}", $str, $tpl);					
					 }
					}		
					if($option['detailData']) {
						$PATTERN = '/<!-- \[Detail LIST\] (.+) -->/';
						preg_match($PATTERN,$tpl,$match);
						
						$insStr = '';
						foreach ($option['detailData'] as $idx => $aData) {
							$tStr = $match[1];
							foreach($option['detailField'] as $key=>$item ) {
								if($item[1]=='uLABEL') continue;	//排除，由引用者產生
								$str = $this->genDOM($idx,$key,$item,$aData);
								$tStr = str_replace("{[#$key]}", $str, $tStr);
							}
							$insStr .= $tStr;
						}
						//echo '<!--'.$insStr.'-->';
						$tpl = preg_replace($PATTERN,$insStr,$tpl);			
					}
				}
			
				$ctx .= $tpl;
			} else { //無版型 
				$ctx .= '<table>'; 
				$idx = 0;
				foreach($this->mFields as $key=>$item ) {
					if($item[1]=='uLABEL') continue;	//排除，由引用者產生
					if($item[1]=='hidden') $ctx .= "<tr style='display:none'>"; else $ctx .= "<tr>";
					$ctx .= "<td class='colLabel'>".$item[0]."</td>";
					$ctx .= "<td class='colData'>"; 
					$ctx .= $this->genDOM($idx,$key,$item);
					$ctx .= "</td></tr>"; 
					$idx++;
				}

				if(!$this->defaultOption['submitBtn'][1] || !$this->defaultOption['resetBtn'][1] || !$this->defaultOption['cancilBtn'][1]) {
					$ctx .= "<tr><td colspan='2' class='rowSubmit'>";
					$ctx .= "<input type='hidden' name='ListFmParams'>";	//預埋傳遞Listform的参數
					if(!$this->defaultOption['submitBtn'][1]) $ctx .= $this->genButton($this->submitBtn, $this->defaultOption['submitBtn']);
					if(!$this->defaultOption['resetBtn'][1]) $ctx .= $this->genButton($this->resetBtn, $this->defaultOption['resetBtn']);
					if(!$this->defaultOption['cancilBtn'][1]) $ctx .= $this->genButton($this->cancilBtn, $this->defaultOption['cancilBtn']);
					$ctx .= "</td></tr>";
				}
				$ctx .= "</table>";
			}				
								
			if($this->defaultOption['type']=='view') $ctx .= '</fieldset>';  
			$ctx .= "</form>";			
			echo $ctx;
			
			if(get_class($this)=='BaseViewControl') echo "</div>";
		}
		
		private function genDOM($idx,$key,$item,$aData=NULL) {
			global $emplyeeinfo,$departmentinfo,$departmentlead,$jobinfo,$vIDinfo,$cIDinfo,$pdCInfo,$pdInfo,$respManInfo,$seller;	//需要caller included			
			$fType = $item[1];
			$fSize = $item[2];
			$fRows = $item[3];	//QueryID: Name, Others: Rows
			if($aData===NULL) { //for Detail table
				$aData=$this->mData;
				$name = $key;
			} else {
				$name = 'dt_'.$key.'[]';
			}	
			$fData = $this->defaultOption['dataKey']?$aData[$key]:$aData[$idx];
			$validation = $item[5];
			if($validation) {
				if($validation[0]) $vdStr='required';
				if($validation[1]) $vdStr.=" pattern='$validation[1]'";
				if($validation[2]) $vdStr.=" title='$validation[2]'";
				if($validation[3]) $vdStr.=" maxlength='$validation[3]'";
			}
  
			switch ($fType) {
				case "readonly": 
					//if(!empty($item[4])) $fData = $item[4][$fData]; !fastOut
					if(!empty($item[4])) $ts = $item[4][$fData];	
					$str= "<input type='text' name='$name' size='$fSize' value='$fData' class='input' readonly>$ts"; break;
				case "text"    : $ts=$item[4]; $str= "<input type='text' name='$name' size='$fSize' value='$fData' $vdStr class='input'>$ts"; break;
				case "textbox" : $str= "<textarea name='$name' cols='$fSize' rows='$fRows' $vdStr class='input'>$fData</textarea>"; break;
				case "textarea":
					$cn =  $this->defaultOption['type']=='view'?'input':'ckeditor';
					$str= "<textarea name='$name' cols='$fSize' rows='$fRows' class='$cn'>".$fData."</textarea>"; 
					break;
				case "checkbox": $str= "<input type='checkbox' name='$name' value='$fData' class='input'"; if ($fData) $str.=" checked"; $str.='>'; break;
				case "date"    : $str= "<input type='text' id='$key' name='$name' size='$fSize' value='".( $fData&&$fData!='0000-00-00'?date('Y/m/d',strtotime($fData)):'' )."' $vdStr class='queryDate'>"; break;
				case "datetime": $str= "<input type='text' id='$key' name='$name' size='$fSize' value='".( $fData&&$fData!='0000-00-00'?date('Y/m/d H:i',strtotime($fData)):'' )."' $vdStr class='queryDateTime'>"; break;	
				case "select" : 
					$str= "<select name='$name' size='$fSize' class='input'>"; 
					foreach($item[4] as $k=>$v) if($k==$fData) $str.="<option value='$k' selected>$v</option>"; else $str.="<option value='$k'>$v</option>"; 
					$str.="</select>"; break;
				case "combo" :
					$str= "<input type='text' name='$name' size='$fSize' value='$fData' $vdStr class='input' list='ls_$name'>";
					$str.="<datalist id='ls_$name'>";
					foreach($item[4] as $k=>$v) $str.="<option value='$k'>$v</option>";
					$str.="</datalist>"; 
					break;	
				case "file" :
					$str="<input type='file' name='$key' size='$fSize' class='input'>";
					if($fData) $str .= $this->genFileview($fData,$fSize,$key);
					 break;
				case "multifile" :
					$str="<input type='file' name='".$key."[]' size='$fSize' class='input multifile'>";					
					if($fData) {
						$aa = explode(',',$fData);
						foreach($aa as $itm) if($itm) $str .= $this->genFileview($itm,$fSize,$key);
					}
					$str.="<div class='photoItem'><img /><span id='fName'></span> . <span class='spanBtn' onclick='delfile(\"$fData\",this,\"$key\")'>刪除</span></div>";
					break;
				case "id" 		:	$str= $fData."<input type='hidden' name='$name' value='$fData'>"; break;
				case "hidden" : $str= "<input type='hidden' name='$name' value='$fData'>"; break;
				case "queryID":	$varN=$$item[4]; $ts=$fRows?$fRows:$key; $str="<input type='text' name='$name' id='$ts' class='queryID' size='$fSize' value='$fData' $vdStr title='".$varN[$fData]."'>"; break;
				case "queryIDs":
					$varN=$$item[4]; 
					$ts=$fRows?$fRows:$key;
					$lineAry = array();
					$ct = 0;
					foreach(explode(',',$fData) as $v) {
						$lineAry[$ct]="<div><input type='text' name='".$name."[]' id='$ts' class='queryID' size='$fSize' value='$v' title='".$varN[$v]."'>";
						if($this->defaultOption['type']=='append' || $this->defaultOption['type']=='edit') {
							if($ct==0) $lineAry[$ct] .= "<span class=\"sBtn\" onclick=\"addItem(this,'depID','admDep')\"> +新增其它部門</span></div?";
							else $lineAry[$ct] .= "<span class='formTxS'></span> <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";  
						}
						$ct++;
					}
					$str = join('<br>',$lineAry);  
					break;
				case 'Define' : $func=$item[4];	$str=$func($fData); break;
				default : $str=$fData;	//ex. label : 只show字串
			}
			return $str;
		}
		
		private function genFileview($fData,$fSize,$key) {
			global $ulpath,$icons;		
			$ext = pathinfo($fData,PATHINFO_EXTENSION);
			$str="<div class='photoItem' style='display:block'>";
			if(isImage($ext)) {
				$str.="<img src='$ulpath$fData' width='$fSize'> <a href='$ulpath$fData' target='_blank'>原圖檢視</a> . <span class='spanBtn' onclick='delfile(\"$fData\",this,\"$key\")'>刪除</span>";
			} else {
				$fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
				$str.="<img src='/images/$fn' align='absmiddle'/> <a href='$ulpath$fData' target='_new'>下載</a> . <span class='spanBtn' onclick='delfile(\"$fData\",this,\"$key\")'>刪除</span>";
			}
			$str.="</div>";
			return $str;
		}
		
		protected function genButton($btn,$setting) {
			$rtn = $btn;
			if($setting[1]) $rtn .= " disabled";
			if($setting[2]) $rtn .= " data-handler='".$setting[2]."'";
			$rtn .= ">".$setting[0]."</button>";
			return $rtn;
		}
		
	}
	
?>