<?
	/*
		===== For 1 List Table Basic use =====
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	include 'init.php';	
	
	/* define DB Connect info
	$op2 = array(
		'dbSource'=>"mysql:host=localhost;dbname=mariaadm",
		'dbAccount'=>DB_ACCOUNT,
		'dbPassword'=>DB_PASSWORD,
		'tableName'=>''
	); */
	if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='del') { //:: Delete record -----------------------------------------
		$db = new db();
		foreach ($_POST['ID'] as $key => $v) {
			$sql = "select * from $tableName where id like '$v%'";
			$rs = $db->query($sql);
			while ($r = $db->fetch_array($rs)) {
				$sql = "delete from $tableName where id='".$r['id']."'";
				$db->query($sql);
			}
		}
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	$page = $_REQUEST['page']?$_REQUEST['page']:1; 
	$recno = $_REQUEST['recno']?$_REQUEST['recno']:1;

	//:Defined PHP Function -------------------------------------------------------------
	function getvID($id) { global $vIDinfo; return $vIDinfo[$id]; }
	
?>	
<title><?=$CompanyName.'-'.$pageTitle?></title>
<link href="/system/PageControl.css" rel="stylesheet">
<link href="/system/ListControl.css" rel="stylesheet">
<link href="/system/ViewControl.css" rel="stylesheet">
<link href="/system/TreeControl.css" rel="stylesheet">

<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<?
	$whereStr = '';	//initial where string
	//:filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if($opList['filters']) {
		foreach($opList['filters'] as $k=>$v) {
			if($_REQUEST[$k]) $whereAry[]="$k = '".$_REQUEST[$k]."'";
			else if($opList['filterOption'][$k]) $whereAry[]="$k = '".$opList['filterOption'][$k][0]."'";
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if($opList['keyword']) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	if($_POST['pid'] == '0' || !isset($_POST['pid'])) if(empty($whereStr)) $whereStr = "where pid='0' and id <>'IN'";

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	if(!$pageSize) $pageSize=12;
	$pinf = new PageInfo($rCount,$pageSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	//: Header -------------------------------------------------------------------------
	echo "<h1 align='center'>$pageTitle ";
	if($opList['keyword']) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ---------------------------------------------------------------------
	if($act=='edit' || $act=='new') $opPage['hideControl']=true;
	$obj = new EditPageControl($opPage, $pinf);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div><hr>";

	echo "<table width='100%'><tr valign='top'><td width='180'>";
	//:TreeControl ---------------------------------------------------------------------
	$op = array(
				'ID'	=> 'id',
				'Title'	=> 'title',
				'TableName' => 'org_class',
				'RootClass' => 0,
				'RootTitle' => $pageTitle,
				'DB' => $db,
				'Modal' => false,
				'URLFmt' => "javascript:filter(\"%s\",\"%s\")",
				'Where' => "id <>'IN'"
			 );
	new BaseTreeControl($op);
	echo "</td>";
	echo "<td>";

	
	//:ListControl Object --------------------------------------------------------------------
	if($act=='edit' || $act=='new') $opList['hideControl']=true;
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
		$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize
	);
	// echo $sql;
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
	//:ViewControl Object ------------------------------------------------------------------------
	$listPos = $pinf->curRecord % $pinf->pageSize;
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"type"=>"edit",
				"form"=>array('form1',"/$tableName/doedit.php",'post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
			"type"=>"append",
			"form"=>array('form1',"/$tableName/doappend.php",'post','multipart/form-data','form1Valid'),
			"submitBtn"=>array("確定新增",false,''),
			"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
	} 
	echo "</td></tr></table>";
	include '/public/inc_listjs.php';		
?>
<script>
function filter(key,cname) {
	while(ListControlForm.pid.options[0]) $(ListControlForm.pid.options[0]).remove();
	var op = document.createElement('option');
	op.value = key;
	op.text = cname;
	ListControlForm.pid.add(op);
	ListControlForm.cname.value = cname;
	ListControlForm.submit();		
}
</script>
