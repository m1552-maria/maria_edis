<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../config.php');
	include_once('../inc_vars.php');
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php";
	include "$root/getEmplyeeInfo.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";

	$pageTitle = "外部組織類別管理";
	$pageSize=20; //使用預設
	$tableName = "org_class";
	// for Upload files and Delete Record use
	$ulpath = "/data/org_class/";
	$delField = 'logo';
	$delFlag = false;
	
	//20200212 權限管制
	$db = new db();
	$sql = "select count(1) no from organization where (revMan='".$_SESSION['empID']."' or mainRevMan ='".$_SESSION['empID']."') and id like 'A%'";
	$rs  = $db->query($sql);
	$r   = $db->fetch_array($rs);
    if ($r[0] > 0) { $useButton = true; }
    else { $useButton = false; }
    $db->close();
// echo $useButton;
	//:PageControl ------------------------------------------------------------------
if($useButton){
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,false,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
}else{
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,false,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,false),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,false,doAppend),
		"editBtn" => array("－修改",false,false,doEdit),
		"delBtn" => array("Ｘ刪除",false,false,doDel)
	);
}

	//:ListControl  ---------------------------------------------------------------+
	if($_REQUEST['pid']) $opAR=array($_REQUEST['pid']=>$_REQUEST['cname']);
	else $opAR = array('0'=>'全部');
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title'),	//要搜尋的欄位	
		"filterOption"=>array('autochange'=>true,'goBtn'=>array("確定",false,true,'')),
		"filters"=>array('pid'=>$opAR)
	);
	
	$fieldsList = array(
		"id"=>array("編號","100px",true,array('Id',"gorec(%d)")),
		"title"=>array("組織類別名稱","250px",true,array('Text')),
		"description"=>array("組織類別說明","250px",true,array('Text')),
	);
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"id"=>array("編號","hidden","","",""),
		"title"=>array("<span class='need'>*</span>組織類別名稱","text","","","",array(true,'','','')),
		"pid"=>array("上層編號","hidden","","",""),
		"description"=>array('類別說明',"text","","","")
	);
	/*新增時的預設值*/
	$pid = $_REQUEST['pid']?$_REQUEST['pid']:0;
	$db = new db();
	$sql = "select id from $tableName where pid='".$pid."' order by id desc LIMIT 1";
	$rs = $db->query($sql);
	$rPID = $db->fetch_array($rs);
	$sqlCC = $rPID['id'];

	if(!empty($sqlCC)){
		if($pid == '0') $cCode = ++$sqlCC;
		elseif($sqlCC){
			if(preg_match('/[A-Z]+[0-9]+/', $sqlCC)) $cCode = ++$sqlCC;
			elseif(preg_match('/[_]+[0-9]+/', $sqlCC)) $cCode = ++$sqlCC;
		}
	}else{
		if($pid == '0') $cCode = "A";
		else{
			$sql = "select id from $tableName where id='".$pid."'";
			$rs = $db->query($sql);
			$rClass = $db->fetch_array($rs);
			if(preg_match('/[A-Z]+[0-9]+/', $rClass['id'])) $cCode = $rClass['id'].'_01';
			else $cCode = $rClass['id'].'01';
		}
	}
	$fieldA_Data = array('id'=>$cCode,'pid'=>$pid);
	
	$fieldE = array(
		"id"=>array("編號","hidden","","",""),
		"title"=>array("<span class='need'>*</span>組織類別名稱","text","","","",array(true,'','','')),
		"pid"=>array("上層編號","hidden","","",""),
		"description"=>array('類別說明',"text","","","")
	);
?>