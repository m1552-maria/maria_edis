<?php
	include 'init.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title><?=$pageTitle?>-代碼選擇</title>
	<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
		.selItem {background-color:#CCCCCC; color:#F90; }
		.gray{color:#999;}
		#TreeControl { border: none; } /* override */
	</style>
  
	<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
	<script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		
		$(document).ready(function(){
			if(!selid) selid = window.opener.ppppp;
			if(selid) $("#browser a[href^=setID").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});

	  function setID(id,title,elm) {
			console.log(id,title,elm);
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#333333" width="100%">
<tr><th id="tblHead" bgcolor="#CCCCCC">::: 類別代碼表 :::</th></tr>
<tr><td bgcolor="#FFFFFF">
<?
	$db = new db();
	//:TreeControl ---------------------------------------------------------------------
	$op = array(
		'ID'		=> 'id',
		'Title'	=> 'title',
		'TableName' => $tableName,
		'RootClass' => 0,
		'RootTitle' => $pageTitle,
		'DB' => $db,
		'Modal' => false,
		'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",
		'Where' => ""
	);
	new BaseTreeControl($op);
?>	
</td></tr>
</table>
</body>
</html>