<?php
	@session_start();
	header("Cache-control: private");
	include 'config.php';
	include 'mobile_agent.php';
	include "system/db.php";
	if(!$_SESSION['empID']) header('Location: login.php');

	if ($_REQUEST['funcUrl']) {
	    $funcUrl = $_REQUEST['funcUrl'];
	} else {
		if($_SESSION["loginType"] !=='cxo'){
	  		if($_SESSION["loginType"] !=='vol'){
	    		$funcUrl = 'edis/rod/myod.php';
		  	}else{
				$funcUrl = 'edis/rod/new.php';
			}
		}else{
			$funcUrl = 'edis/rod/cxo_myod.php';
		}
	}
	if($_REQUEST['muID']) $aa = explode('.',$_REQUEST['muID']); 
	else $muID = 0;

	if(is_array($aa)) {
		$muID=$aa[0]; 
		$muID2=$aa[1];
	}
	error_log(date('Y-m-d H:i:s')." index.php request started.\n", 3, 'C:/wamp/logs/haotung-debug.log');

	//:: prepare Menu --------------------------------
	$muAr = array();	//選單資料陣列
	$muDb = new db();
	$muSql ="select * from sys_menu where loginType ='".$_SESSION["loginType"]."' order by seq";
	$rsMu = $muDb->query($muSql);
	
	while($rMu = $muDb->fetch_array($rsMu)) {
		$aa = explode('.',$rMu['seq']);
		$n = count($aa);
		$ba = array(
			'title'=>$rMu['title'],
			'icon'=>$rMu['icon'],
			'funcUrl'=>$rMu['funcUrl'],
			'href'=>$rMu['href'],
			'muID'=>$rMu['muID'],
			'isLeaf'=>$rMu['isLeaf'],
			'id'=>$rMu['id'],
			'seq'=>$rMu['seq'],
			'isAll'=>$rMu['isAll'],
			'target'=>$rMu['target']);
		if($n==1) {	//第一層
			$muAr[$aa[0]] = $ba;
		} if($n==2) { 
			$muAr[$aa[0]][$aa[1]] = $ba;
		} if($n==3) { 
			$muAr[$aa[0]][$aa[1]][$aa[2]] = $ba;
		} if($n==4) { 
			$muAr[$aa[0]][$aa[1]][$aa[2]][$aa[3]] = $ba;
		}
	}
	
	function genSubmenu($k,$v) {
		global $funcUrl,$muID, $muID2;
		$uriAr =  explode('&',$_SERVER['REQUEST_URI']);
		$n = count($uriAr)-1;
		if(strpos($uriAr[$n],'muID')===false) $uri="/index.php?funcUrl=".$funcUrl;	else $uri=str_replace('&'.$uriAr[$n],'',$_SERVER['REQUEST_URI']);
		echo '<ul class="sub-menu">';
		foreach($v as $k2=>$v2) {
			if(is_array($v2)) {
                $isInAry =in_array($v2['id'],$_SESSION["menu_permission"]);
				if((($_SESSION["menu_permission"][$v2['id']]&1)==0 && !$v2['isAll'] && !$isInAry)) continue;
				$href = $v2['href']?$v2['href']:'javascript:;';
				$isLeaf = $v2['isLeaf']?'':"<span class='arrow'/>";
				if($v2['isLeaf']) {
					if($v2['href']) echo "<li ".($uri==$v2['href']?"class='open'":'')."><a href='$v2[href]&muID=$v2[muID]&isMenu=1' target='$v2[target]'>$v2[title]</a></li>";					
				} else {
					echo "<li class='". ($muID2==$k2?'active':'') ."'>";
					echo "<a href='$href'><i class='$v2[icon]'></i><span class='title'>$v2[title]</span>$isLeaf</a>";
					genSubmenu($k2,$v2);
					echo "</li>";
				}
			}
		}
		echo '</ul>';	
	}
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$CompanyName?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
  <!--link href="media/css/jquery-ui-1.10.1.custom.min.css" type="text/css"/ 日曆控件 不相容  -->
  <link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
	<link href="media/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="media/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="media/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="media/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="media/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="media/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
  <link href="/css/all.css" rel="stylesheet" type="text/css"/>

 	<script src="media/js/jquery-1.10.1.min.js"></script>
	<script src="media/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="media/js/jquery-ui-1.10.1.custom.min.js"></script>      
</head>

<body class="page-header-fixed">
	<? include 'header.php' ?> <!-- BEGIN HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container"><!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<ul class="page-sidebar-menu">
				<li><div class="sidebar-toggler hidden-phone"></div></li> <!-- SIDEBAR TOGGLER BUTTON -->
		 <? 
		 foreach($muAr as $k=>$v) {

		 	        $isInAry =in_array($v['id'],$_SESSION["menu_permission"]);
		 			if(($_SESSION["menu_permission"][$v['id']]&1)==0 && !$v['isAll'] && !$isInAry) continue;
					echo "<li class='". ($muID==$k?'active':'') ."'>";
					$href = $v['href']?$v['href']:'javascript:;';
					if(!$v['isLeaf'] && strlen($v['href'])>0){//第一層menu且無子menu
                        $href = $href.'&muID='.$v['muID'];
                        $isLeaf = $v['isLeaf']?'':"<span class='title'/>";
						echo "<a href='$href' target='$v[target]'><i class='$v[icon]'></i><span class='title'>$v[title]</span>$isLeaf</a>";                        
					}else{
						$isLeaf = $v['isLeaf']?'':"<span class='arrow'/>";
						echo "<a href='$href' target='$v[target]'><i class='$v[icon]'></i><span class='title'>$v[title]</span>$isLeaf</a>";
						if(!$v['isLeaf']) genSubmenu($k,$v);						
					}

			  } 
		 ?>
			</ul>
		</div>

		<!-- BEGIN Content PAGE -->
		<div class="page-content"><div class="container-fluid" style="padding-left:5px"><? include $funcUrl; ?></div></div>
	</div>
	<? include 'footer.php' ?><!-- BEGIN FOOTER -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="media/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="media/js/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="media/js/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="media/js/jquery.cookie.min.js" type="text/javascript"></script>

	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="media/js/app.js" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		});
		$('.icon-refresh').on('click',function(){
			$.ajax({
				type: "POST",
				url: "login.php", //(檔案名/方法名稱)
				data: { 
					event: 'infoAssistant' 
				},
				success: function(data) {
				}
			})
		});
	</script>
</body>
</html>