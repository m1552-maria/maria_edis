<?php 
/*一般設定*/
//收文流程歸檔階段
$rodLastLevel = '4';
//發文流程歸檔階段
$sodLastLevel = '4';

//myod_view sql 查詢
// IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
$myRod  =  "SELECT ed.*, ogR.oid as rAct,
                ogS.title AS sActName,
                IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName,
                (SELECT IF((COUNT(mos.id) = 0), 0, 1) FROM map_orgs_sign mos WHERE ( mos.edisid = ed.id AND mos.isSign = 0 )) AS isReject
            FROM edis ed 
            LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
            LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )";

//IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName,
$mySod  =  "SELECT ed.*, ogSo.oid as soAct, ogS.oid as sAct,
                IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
                ogR.title AS rActName,
                IF((LENGTH(ogSo.sTitle) > 0), ogSo.sTitle, ogSo.title) AS soname,
                (SELECT IF((COUNT(mos.id) = 0), 0, 1) FROM map_orgs_sign mos WHERE ( mos.edisid = ed.id AND mos.isSign = 0 )) AS isReject
            FROM edis ed 
            LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
            LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )
            LEFT JOIN map_orgs ogSo ON( ogSo.eid = ed.id AND ogSo.act = 'SO' )";

// 應簽
$unsignedRod = "SELECT ed.*, mos.id as mosid, mos.signMan,
                    IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
                    IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName,
                    IF((LENGTH(org.sTitle) > 0), org.sTitle, org.title) AS sOidName
                FROM edis ed 
                LEFT JOIN map_orgs_sign mos ON( ed.id = mos.edisid )
                LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
                LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )
                LEFT JOIN organization org ON( ed.sOid = org.id )
                LEFT JOIN map_dep  dep ON( dep.eid = ed.id )
                WHERE (void is null or void = '0') 
                AND IF( (SELECT count(1) FROM map_orgs_sign mos2 WHERE ed.id = mos2.edisid AND mos2.signLevel IN ('1','2','3') AND mos2.isSign is null ) > 0, mos.signLevel IN ('1','2','3'), mos.signLevel = '4' ) ";
$unsignedSod = "SELECT ed.*, mos.id as mosid, mos.signMan, 
                    IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
                    IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName,
                    IF((LENGTH(ogSo.sTitle) > 0), ogSo.sTitle, ogSo.title) AS soname,
                    IF((LENGTH(org.sTitle) > 0), org.sTitle, org.title) AS sOidName
                FROM edis ed 
                LEFT JOIN map_orgs_sign mos ON( ed.id = mos.edisid AND ed.signStage = mos.signLevel )
                LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
                LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )
                LEFT JOIN map_orgs ogSo ON( ogSo.eid = ed.id AND ogSo.act = 'SO' )
                LEFT JOIN organization org ON( ed.sOid = org.id )
                LEFT JOIN map_dep  dep ON( dep.eid = ed.id )
                WHERE (void is null or void = '0') ";

//應簽公文where條件,為因應多重查詢條件與$unsigned拆開,方便組合
$unsignWhere = " AND isnull(mos.isSign)
                 GROUP BY ed.id";

// 已簽
$signedRod =   "SELECT ed.*,
                    IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
                    IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName
                FROM edis ed 
                JOIN map_orgs_sign mos ON( ed.id = mos.edisid )
                LEFT JOIN map_orgs ogS ON( ogS.eid = ed.id AND ogS.act = 'S' )
                LEFT JOIN map_orgs ogR ON( ogR.eid = ed.id AND ogR.act = 'R' )
                LEFT JOIN map_dep  dep ON( dep.eid = ed.id )
                WHERE (void is null or void = '0') AND (`mos`.`isSign` IS NOT NULL)";
$signedSod =   "SELECT ed.*,
                    IF((LENGTH(ogS.sTitle) > 0), ogS.sTitle, ogS.title) AS sActName,
                    IF((LENGTH(ogR.sTitle) > 0), ogR.sTitle, ogR.title) AS rActName,
                    IF((LENGTH(ogSo.sTitle) > 0), ogSo.sTitle, ogSo.title) AS soname
                FROM edis ed 
                JOIN map_orgs_sign mos  ON( ed.id = mos.edisid )
                LEFT JOIN map_orgs ogS  ON( ogS.eid = ed.id AND ogS.act = 'S' )
                LEFT JOIN map_orgs ogR  ON( ogR.eid = ed.id AND ogR.act = 'R' )
                LEFT JOIN map_orgs ogSo ON( ogSo.eid = ed.id AND ogSo.act = 'SO' )
                LEFT JOIN map_dep  dep  ON( dep.eid = ed.id )
                WHERE (void is null or void = '0') AND (`mos`.`isSign` IS NOT NULL)";
?>
