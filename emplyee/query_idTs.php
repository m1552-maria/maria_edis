<?php
	include 'init.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title><?=$pageTitle?>-代碼選擇</title>
	<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
		.selItem {background-color:#CCCCCC; color:#F90; }
		.gray{color:#999;}
		#TreeControl { border: none; } /* override */
	</style>
  
	<script src="/media/js/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
	<script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		
		$(document).ready(function(){
			if(!selid) selid = window.opener.ppppp;
			if(selid) $("#browser a[href*=setID(\""+selid+"\"]").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});

		function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				if(id) $("#browser a[href*=setID(\""+id+"\"]").addClass('selItem');
			} else { 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				window.close(); 
			}
		}
		function returnSeletions() {
			var selObjs = new Array();
			selObjs = getSelections('empID');
			// selObjs = selObjs.concat(getSelections('empID'));
			// console.log(selObjs);
			window.opener.returnValues(selObjs);
			window.opener.rzt = false;
			window.close(); 
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#333333" width="100%">
<tr><th id="tblHead" bgcolor="#CCCCCC" style="text-align: left;"><button onClick='returnSeletions()'>確認選取</button></th></tr>
<tr><td bgcolor="#FFFFFF">
<?

	$op = array(
	    'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
	    'dbAccount'  => DB_ACCOUNT,
	    'dbPassword' => DB_PASSWORD,
	    'tableName'  => '',
	);
	$db = new db($op);
	//:TreeControl ---------------------------------------------------------------------
	$op = array(
		'ID'		=> 'depID',
		'Title'	=> 'depTitle',
		'TableName' => 'department',
		//'Where'=>"depID in ('A','B')",
		'RootClass' => 0,
		'RootTitle' => $CompanyName,
		'DB' => $db,
		'Modal' => false,
		'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",
		'ChildTbl'=>'emplyee',
		'FKey'=>'depID',
		'cWhere'=>'isOnduty=1',		//在職員工
		'cID'=>'empID',
		'cIcon'=>'people',
		'cTitle'=>'empName'
	);
	new CboxTreeControl($op);
?>	
</td></tr>
</table>
</body>
</html>