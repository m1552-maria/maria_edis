<?php
	include 'init.php';

$op = array(
    'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
    'dbAccount'  => DB_ACCOUNT,
    'dbPassword' => DB_PASSWORD,
    'tableName'  => '',
);

	if($_REQUEST[depID]){
		$sql="select * from emplyee where isOnduty=1 and depID like '$_REQUEST[depID]%'"; 
	}else if($_SESSION['user_classdef'][2]){
		$ary=array();
		foreach($_SESSION['user_classdef'][2] as $k=>$v){
			array_push($ary,"depID like '".$v."%'");
		}
		$sql="select * from emplyee where isOnduty=1 and (".join(" or ",$ary).")";
	}else{
		$sql = "select * from emplyee where isOnduty=1";
	}
	//::Search
	if(strlen(trim($_REQUEST['key']))>0) { $key=$_REQUEST['key']; $sql .= " and (empID like '%$key%' or empName like '%$key%') "; }
	$db = new db($op);
  $rs = $db->query($sql);
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>員工代碼表</title>
	<style type="text/css">
	.whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
	.whitebg a:link { text-decoration:none }
	.whitebg a:visited { text-decoration: none;}
	.whitebg a:hover {	color: #FF0000;	text-decoration: none;}
	.whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		list-style-image: url(../../images/man.png);
		list-style-position: outside;
		padding: 0 4px 0 4px; 
		cursor:pointer;
	}
	
	#searchKeyW { margin-bottom: 2px; }
	#searchKeyW input {	border:solid 1px #ddd; width: 100%; background-color:#f8f8f8; border-radius: 5px; }
	#searchKeyW input.search {
		padding-left:20px;
		background-repeat: no-repeat;
		background-position-y: 1px;
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAASCAYAAABb0P4QAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADbSURBVDhP5ZI9C4MwEIb7//+BEDgICA6C4OQgBJy6dRIEB6EgCNkEJ4e3iT2oHzH9wHbpAwfyJvfkJDnhYH4kHDVKlSAigSAQoCiBKjVGXvaxFXZnxBQYkSlBICII+22K4jM63rbHSthCSdsskVX9Y6KxR5XJSSpVy6GbpbBKp6aw0BzM0ShCe1iKihMXC6EuQtMQwukzPFu3fFd4+C+/cimUNxy6WQkNnmdzL3NYPfDmLVuhZf2wZYz80qDkKX1St3CXAfVMqq4cz3hTaGEpmctxDPmB0M/fCYEbAwZYyVKYcroAAAAASUVORK5CYII=);
	}
  </style>
  <script type="text/javascript" src="/Scripts/jquery.js"></script>  
  <script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){
			//selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}

			//add by Tina 20190122 發文:承辦人需連動校對
			var odtype = '<?echo $_REQUEST['odtype'];?>';
            var parid = '<?echo $_REQUEST["parid"];?>';
            if(odtype =='sod'){			
				if(parid =='sId'){
	                window.opener.setRroofreader(id,title);
				}
			}

	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr><!-- Search Bar Start -->
   	<td id="tblHead" bgcolor="#CCCCCC">
    <form id="searchKeyW">
    	<? if($_REQUEST['depID']) echo "<input type='hidden' name='depID' value='$_REQUEST[depID]'/>"; ?>
    	<input type="text" class="search" name="key"  value="<?=$_REQUEST['key']?>" placeholder="請輸入名稱或編號">
		</form>
    </td>
  </tr><!-- Search Bar End. -->
  
	<tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <? while($r=$db->fetch_array($rs)) echo "<li><a onClick=setID('$r[empID]','$r[empName]',this)>$r[empName] - $r[empID]</a></li>"; ?> 
    </ul></div>
	</td></tr>
</table>
</body>
</html>