<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../config.php');
	include_once("$root/inc_vars.php");
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php";
	include '../getEmplyeeInfo.php';
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "員工資料維護";
	//$pageSize=3; 使用預設
	$tableName = "emplyee";
	// for Upload files and Delete Record use
	$ulpath = "/data/";
	$delField = '';
	$delFlag = false;
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	
	//:<Select> Array -------------------------------------------------------------
	$sex = array('1'=>'男','0'=>'女');
	$blood = array('A'=>'A','B'=>'B','O'=>'O','AB'=>'AB');
	
	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'empID',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('empID','empName'),	//要搜尋的欄位	
	);
		
	$fieldsList = array(
		"empID"=>array("員工編號","100px",true,array('Id',"gorec(%d)")),
		"empName"=>array("員工名稱","90px",true,array('Text')),
		"depID"=>array("部門","100px",true,array('Define',getDepName)),
		"jobID"=>array("掌職","100px",true,array('Define',getJobName)),
		"sex"=>array("性別","50px",true,array('Select',$sex)),
		"birthday"=>array("生日","100px",true,array('Date')),
		"hireDate"=>array("到職日","100px",true,array('Date')),
		"leaveDate"=>array("離職日","100px",true,array('Date')),
		"rdate"=>array("登錄時間","150px",true,array('DateTime'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"empID"=>array("員工編號","text",20,'','',array(true,'','',16)),
		"empName"=>array("員工名稱","text",20,'','',array(true,'','',50)),
		"depID"=>array("部門","queryID",20,0,'departmentinfo'),
		"jobID"=>array("掌職","queryID",20,0,'jobinfo'),
		"sex"=>array("性別","select",1,'',$sex),
		"blood"=>array("性別","select",1,'',$blood),
		"Marriage"=>array("婚姻狀況","select",1,'',$Marriage),
		"isOnduty"=>array("職況","select",1,'',$jobStatus),
		"perID"=>array("身分字號","text",20,'','',array(true,'','',50)),
		"birthday"=>array("生日","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
		"hireDate"=>array("到職日","date",20,'','',array(true,PTN_DATE,'請輸入日期時間')),
		"leaveDate"=>array("離職日","date",20,'','',array(false,PTN_DATE,'請輸入日期時間'))
	);
	$fieldA_Data = array();
	
	$fieldE = array(
		"empID"=>array("員工編號","id",20,),
		"empName"=>array("員工名稱","text",20,),
		"depID"=>array("部門","queryID",20,0,'departmentinfo'),
		"jobID"=>array("掌職","queryID",20,0,'jobinfo'),
		"sex"=>array("性別","select",1,'',$sex),
		"blood"=>array("性別","select",1,'',$blood),
		"Marriage"=>array("婚姻狀況","select",1,'',$Marriage),
		"isOnduty"=>array("職況","select",1,'',$jobStatus),
		"perID"=>array("身分字號","text",20,'','',array(true,'','',50)),
		"birthday"=>array("生日","date",20,),
		"hireDate"=>array("到職日","date",20,),
		"leaveDate"=>array("離職日","date",20,),
		"rdate"=>array("登錄時間","datetime",20,)
	);
?>