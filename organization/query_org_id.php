<?php
/*
 *  可複選對話框
 */
	include 'init.php';
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title><?=$pageTitle?>-代碼選擇</title>
  <link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
	.selItem {background-color:#CCCCCC; color:#F90; }
	.gray{color:#999;}
	#TreeControl { border: none; } /* override */
	#tblHead { padding: 5px; text-align:left }
	.whitebg {background-color:white; font:15px "微軟正黑體",Verdana;}
	.whitebg a:link { text-decoration:none }
	.whitebg a:visited { text-decoration: none;}
	.whitebg a:hover {	color: #FF0000;	text-decoration: none;}
	.whitebg a:active { color: #808080; text-decoration: none;	font-weight: bold;}
	.selItem {background-color:#CCCCCC; color:#F90; }
	#browser li {
		cursor:pointer;
	}
	#searchKeyW input {	border:solid 1px #ddd; width: 80%; background-color:#f8f8f8; border-radius: 5px; }
	#searchKeyW input.search {
		padding-left:20px;
		background-repeat: no-repeat;
		background-position-y: 1px;
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAASCAYAAABb0P4QAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADbSURBVDhP5ZI9C4MwEIb7//+BEDgICA6C4OQgBJy6dRIEB6EgCNkEJ4e3iT2oHzH9wHbpAwfyJvfkJDnhYH4kHDVKlSAigSAQoCiBKjVGXvaxFXZnxBQYkSlBICII+22K4jM63rbHSthCSdsskVX9Y6KxR5XJSSpVy6GbpbBKp6aw0BzM0ShCe1iKihMXC6EuQtMQwukzPFu3fFd4+C+/cimUNxy6WQkNnmdzL3NYPfDmLVuhZf2wZYz80qDkKX1St3CXAfVMqq4cz3hTaGEpmctxDPmB0M/fCYEbAwZYyVKYcroAAAAASUVORK5CYII=);
	}
  </style>
  	<script src="/media/js/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
  	<script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		$(document).ready(function(){
			// if(!selid) selid =  window.opener.ppppp;
			// if(selid !==''){
			// var aaa = selid.split('/');
			// var list = aaa[0].split(','); //部門(第一層)
			// list.forEach(function(itm,idx,arr){ $("#browser input[name*='depID'][value='"+itm+"']").prop('checked',true); });
			
			// var list = aaa[1].split(','); //員工(第二層)
			// list.forEach(function(itm,idx,arr){ $("#browser input[name*='empID'][value='"+itm+"']").prop('checked',true); });
			// }	
			if(get) $('#browser').removeClass('treeview');
		});

		function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 

				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}

			//add by Tina 20180904 發文:發文單位需連動副本第一欄及受文者需連動正本第一欄
             var odtype = '<?echo $_REQUEST['odtype'];?>';
             var parid = '<?echo $_REQUEST["parid"];?>';			
			if(odtype =='sod'){
				if(parid =='sAct'){
	                window.opener.setcAct(id,title);
				}else if(parid =='rAct'){
					window.opener.setnAct(id,title);
				}
			}
	  	}
		
		function returnSeletions() {
			var selObjs = new Array();
			selObjs = getSelections('orgID');
			selObjs = selObjs.concat(getSelections('id'));
			selObjs['act'] = '<?=$_GET['act'] ?>';
			console.log(selObjs);
			window.opener.returnValues(selObjs);
			window.opener.rzt = false;
			window.close(); 
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#333333" width="100%">
	<tr><!-- Search Bar Start -->
   	<td id="tblHead" bgcolor="#CCCCCC">
    <form id="searchKeyW">
    	<input type="text" class="search" name="key"  value="<?=$_REQUEST['key']?>" placeholder="請輸入名稱或編號">
    	<input type="hidden" class="search" name="odtype"  value="<?=$_REQUEST['odtype']?>">
    	<input type="hidden" class="search" name="parid"  value="<?=$_REQUEST['parid']?>">
    	<input type="hidden" class="search" name="act"  value="<?=$_REQUEST['act']?>">
	</form>
    </td>
    </tr><!-- Search Bar End. -->
<tr><td bgcolor="#FFFFFF">
<?
$dbinfo = array(
    'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
    'dbAccount'  => DB_ACCOUNT,
    'dbPassword' => DB_PASSWORD,
    'tableName'  => '',
);

	$db = new db();
	if($_REQUEST['odtype']=='sod' and ($_REQUEST['parid']=='sAct'or $_REQUEST['parid']=='soAct')){
		if(strlen(trim($_REQUEST['key']))>0) { 
			$key = $_REQUEST['key'];
			$sql ="select * from organization where (id like '%$key%' or title like '%$key%') AND orgType='SELF' and (offShelf IS NULL or offShelf = '0')";
			$rs = $db->query($sql);

			echo ' <div id="markup" class="whitebg"><ul id="browser">';
			while($r=$db->fetch_array($rs)){ 
			    if(strlen($r['sTitle'])>0){
					echo "<li><a onClick=\"setID('$r[id]','$r[sTitle]',this)\"> $r[id] - $r[sTitle] </a></li>";
			    }else{
			    	echo "<li><a onClick=\"setID('$r[id]','$r[title]',this)\"> $r[id] - $r[title] </a></li>";	
			    }
		    }
		    echo '</ul></div>';
		    echo "<script>var get = true;</script>";
		}else{
			$key = $_REQUEST['key'];
			$sql ="select * from organization where orgType='SELF' and (offShelf IS NULL or offShelf = '0')";
			$rs = $db->query($sql);

			echo ' <div id="markup" class="whitebg"><ul id="browser">';
			while($r=$db->fetch_array($rs)){ 
			    if(strlen($r['sTitle'])>0){
					echo "<li><a onClick=\"setID('$r[id]','$r[sTitle]',this)\"> $r[id] - $r[sTitle] </a></li>";
			    }else{
			    	echo "<li><a onClick=\"setID('$r[id]','$r[title]',this)\"> $r[id] - $r[title] </a></li>";	
			    }
		    }
		    echo '</ul></div>';
		    echo "<script>var get = true;</script>";
		}
	}else{
		if(strlen(trim($_REQUEST['key']))>0) { 

			$key = $_REQUEST['key'];
			$sql ="select * from organization where (id like '%$key%' or title like '%$key%') and (offShelf IS NULL or offShelf = '0')";
			$rs = $db->query($sql);

			echo ' <div id="markup" class="whitebg"><ul id="browser">';
			while($r=$db->fetch_array($rs)){ 
			    if(strlen($r['sTitle'])>0){
					echo "<li><a onClick=\"setID('$r[id]','$r[sTitle]',this)\"> $r[id] - $r[sTitle] </a></li>";
			    }else{
			    	echo "<li><a onClick=\"setID('$r[id]','$r[title]',this)\"> $r[id] - $r[title] </a></li>";	
			    }
		    }
		    echo '</ul></div>';
		    echo "<script>var get = true;</script>";
		}else{
			//:TreeControl ---------------------------------------------------------------------
			$op = array(
				'ID'		=> 'id',
				'Title'		=> 'title',
				'TableName' => 'org_class',
				'RootClass' => 0,
				'RootTitle' => $CompanyName,
				'DB' => $db,
				'Modal' => false,
				'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",

				'ChildTbl'=>'organization',
				'FKey'=>'orgClass',
				'cID'=>'id as orgID',
				'cTitle'=>'title',
				'cWhere'=>"(offShelf IS NULL or offShelf = '0')"
			);
			new BaseTreeControl($op);
		}
	}
?>	
</td></tr>
</table>
</body>
</html>