<?php
/*
 *  可複選對話框
 */
	include 'init.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title><?=$pageTitle?>-代碼選擇</title>
	<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
	.selItem {background-color:#CCCCCC; color:#F90; }
	.gray{color:#999;}
	#TreeControl { border: none; } /* override */
	#tblHead { padding: 5px; text-align:left }
	input.search {
		border:solid 1px #ddd; width: 80%; background-color:#f8f8f8; border-radius: 5px;
		padding-left:20px;
		background-repeat: no-repeat;
		background-position-y: 1px;
		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAASCAYAAABb0P4QAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADbSURBVDhP5ZI9C4MwEIb7//+BEDgICA6C4OQgBJy6dRIEB6EgCNkEJ4e3iT2oHzH9wHbpAwfyJvfkJDnhYH4kHDVKlSAigSAQoCiBKjVGXvaxFXZnxBQYkSlBICII+22K4jM63rbHSthCSdsskVX9Y6KxR5XJSSpVy6GbpbBKp6aw0BzM0ShCe1iKihMXC6EuQtMQwukzPFu3fFd4+C+/cimUNxy6WQkNnmdzL3NYPfDmLVuhZf2wZYz80qDkKX1St3CXAfVMqq4cz3hTaGEpmctxDPmB0M/fCYEbAwZYyVKYcroAAAAASUVORK5CYII=);
	}
  	</style>
  	<script src="/media/js/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
  	<script type="text/javascript">
		var selid;					//送入的 id

		$(document).ready(function(){
			//判斷是否有get搜尋值
			if(get) $('#browser').removeClass('treeview');

			if(!selid) selid =  window.opener.ppppp;
			if(selid !==''){
				var aaa = selid.split('/');
				var list = aaa[0].split(','); //部門(第一層)
				list.forEach(function(itm,idx,arr){ $("#browser input[name*='depID'][value='"+itm+"']").prop('checked',true); });
				
				var list = aaa[1].split(','); //員工(第二層)
				list.forEach(function(itm,idx,arr){ $("#browser input[name*='empID'][value='"+itm+"']").prop('checked',true); });
			}
		});

		function setID(id,title,elm) {
			if(id) $("#browser input[value='"+id+"']").attr('checked',true);
		}
		
		function returnSeletions() {
			var selObjs = new Array();
			selObjs = getSelections('orgID');
			selObjs = selObjs.concat(getSelections('id'));
			selObjs['act'] = '<?=$_GET['act'] ?>';
			console.log(selObjs);
			window.opener.returnValues(selObjs);
			window.opener.rzt = false;
			window.close(); 
		}
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="0" cellspacing="0" bgcolor="#333333" width="100%">
<tr><th id="tblHead" bgcolor="#CCCCCC"><button onClick='returnSeletions()'>確認選取</button></th></tr>
<tr><th id="tblHead" bgcolor="#CCCCCC">
	<form id="searchKeyW">
		<input type="text" class="search" name="search" placeholder="請輸入名稱或編號">
		<input type="hidden" class="search" name="odtype"  value="<?=$_REQUEST['odtype']?>">
		<input type="hidden" class="search" name="parid"  value="<?=$_REQUEST['parid']?>">
		<input type="hidden" class="search" name="act"  value="<?=$_REQUEST['act']?>">
	</form>
</th></tr>
<tr><td bgcolor="#FFFFFF">
<?
$dbinfo = array(
    'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
    'dbAccount'  => DB_ACCOUNT,
    'dbPassword' => DB_PASSWORD,
    'tableName'  => '',
);

	$db = new db();
	//:TreeControl ---------------------------------------------------------------------
	if(strlen(trim($_REQUEST['search']))>0) { 

		$key = $_REQUEST['search'];
		$sql ="select * from organization where (id like '%$key%' or title like '%$key%') and (offShelf IS NULL or offShelf = '0')";
		$rs = $db->query($sql);

		echo ' <div id="TreeControl" class="whitebg"><ul id="browser" style="list-style-type:none">';
		while($r=$db->fetch_array($rs)){ 
		    if(strlen($r['sTitle'])>0){
				echo "<li style='font-size:16px;'><input type='checkbox' name='orgID[]' value='$r[id]' title='$r[sTitle]'>$r[id] - $r[sTitle]</li>";
		    }else{
		    	echo "<li style='font-size:16px;'><input type='checkbox' name='orgID[]' value='$r[id]' title='$r[title]'>$r[id] - $r[title]</li>";	
		    }
	    }
	    echo '</ul></div>';
	    echo "<script>var get = true;</script>";
	}else{
		$op = array(
			'ID'		=> 'id',
			'Title'		=> 'title',
			'TableName' => 'org_class',
			'RootClass' => 0,
			'RootTitle' => $CompanyName,
			'DB' => $db,
			'Modal' => false,
			'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",
			
			'ChildTbl'=>'organization',
			'FKey'=>'orgClass',
			'cID'=>'id as orgID',
			'cTitle'=>'title',
			'cWhere'=>"(offShelf IS NULL or offShelf = '0')"
		);
		new CboxTreeControl($op);
	}
?>	
</td></tr>
</table>
</body>
</html>