<?
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */
include 'init.php';
include "$root/getEmplyeeInfo.php";
include "$root/organization/func.php";
if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $aa    = array_keys($fieldsList);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    //echo $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);
}
//新增(複製)被勾選之部門
if ($act == 'copy') {
    $db = new db();
    foreach ($_POST['ID'] as $v) {
        $info = array('orgType'=>'OUTER','title'=>'OUTER'.$v);
        $orgid = getOrgNumCopy($info);

        $sql = "INSERT INTO organization(id,
        title,
        sTitle,
        selfTitle,
        numberTitle,
        county,
        city,
        address,
        PosCode,
        orgType,
        email,
        tel,
        fax,
        contactMan,
        revMan,
        attach,
        mainRevMan)
        SELECT '$orgid',
        '複製部門編號:$v',
        sTitle,
        selfTitle,
        numberTitle,
        county,
        city,
        address,
        PosCode,
        orgType,
        email,
        tel,
        fax,
        contactMan,
        revMan,
        attach,
        mainRevMan
        FROM organization
        WHERE id = $v";
        $db->query($sql);
    }
    $act = '';
}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function getOrgNumCopy($info){
    //外部組織
    $db     = new db();
    //檢核有無重複
    $sql    = "select MAX(CONVERT(id,SIGNED)) lastnum from organization where orgType ='OUTER' and (offShelf IS NULL or offShelf = '0')";
    $rs     = $db->query($sql);
    $r      = $db->fetch_array($rs);
    $newNum = str_pad(($r['lastnum'] + 1), 4, "0", STR_PAD_LEFT);    
    return $newNum; 
}

function orgClass($id) { 
    global $orgClass; 
    return $orgClass[$id]; 
}
function func_addrE()
{
    global $county, $fieldE_Data;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $ck = ($fieldE_Data[county] == $k ? 'selected' : '');
        $rtn .= "<option value='$k' $ck>$v</option>";
    }
    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'>" . ($fieldE_Data[city] ? "<option>$fieldE_Data[city]</option>" : '') . "</select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' value='$fieldE_Data[address]' />";
    return $rtn;
}

function func_addr()
{
    global $county;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $rtn .= "<option value='$k'>$v</option>";
    }

    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'></select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' />";
    return $rtn;
}

function view_addr($value, $obj)
{
    global $county;
    return $county[$obj->curRow[county]] . $obj->curRow[city] . $value;
}
?>
<title><?=$CompanyName . '-' . $pageTitle?></title>
<link href="/system/PageControl.css" rel="stylesheet">
<link href="/system/ListControl.css" rel="stylesheet">
<link href="/system/ViewControl.css" rel="stylesheet">
<link href="/system/TreeControl.css" rel="stylesheet">

<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">
<link href="/css/formRwd.css" rel="stylesheet">

<style type="text/css">
#secretDiv {
    position:absolute;
    left: 50%;
    background-color:#eeeeee;
    padding: 20px;
    display:none;
    margin-left: 100px;
    margin-top: -250px;
}
</style>

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>
<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>

<?
$whereStr = ''; //initial where string
//:filter handle  ---------------------------------------------------
$whereAry  = array();
$wherefStr = '';
if ($opList['filters']) {
    foreach ($opList['filters'] as $k => $v) {
        if ($_REQUEST[$k]) {
            $whereAry[] = "$k = '" . $_REQUEST[$k] . "'";
        } else if ($opList['filterOption'][$k]) {
            $whereAry[] = "$k = '" . $opList['filterOption'][$k][0] . "'";
        }
    }
    if (count($whereAry)) {
        $wherefStr = join(' and ', $whereAry);
    }

}

//:Search filter handle  ---------------------------------------------
$whereAry  = array();
$wherekStr = "orgType ='OUTER'";
if ($opList['keyword']) {
    $key = $opList['keyword'];
    foreach ($opList['searchField'] as $v) {
        $whereAry[] = "$v like '%$key%'";
    }

    $n = count($whereAry);
    if ($n > 1) {
        $wherekStr = '(' . join(' or ', $whereAry) . ')';
    } else if ($n == 1) {
        $wherekStr = join(' or ', $whereAry);
    }

}

//:Merge where
if ($wherefStr || $wherekStr) {
    $whereStr = 'where ';
    $flag     = false;
    if ($wherefStr) {
        $whereStr .= $wherefStr;
        $flag = true;}
        if ($wherekStr) {
            if ($flag) {
                $whereStr .= ' and ' . $wherekStr;
            } else {
                $whereStr .= $wherekStr;
            }

        }
    }

    $db     = new db();
    $sql    = sprintf('select 1 from %s %s', $tableName, $whereStr);
    $rs     = $db->query($sql);
    $rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
    if (!$pageSize) {
        $pageSize = 20;
    }

    $pinf = new PageInfo($rCount, $pageSize);
    if ($recno > 1) {
        $pinf->setRecord($recno);
    } else if ($page > 1) {
        $pinf->setPage($page);
        $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;}
        ;

//: Header -------------------------------------------------------------------------
        echo "<h1 align='center'>$pageTitle ";
        if ($opList['keyword']) {
            echo " - 搜尋:" . $opList['keyword'];
        }

        echo "</h1>";

//:PageControl ---------------------------------------------------------------------
        $obj = new EditPageControl($opPage, $pinf);
        echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//:ListControl Object --------------------------------------------------------------------
        $n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
        $sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
            $tableName, $whereStr,
            $opList['curOrderField'], $opList['sortMark'],
            $n * $pinf->pageSize, $pinf->pageSize
        );
//echo $sql;
        $rs    = $db->query($sql);
        $pdata = $db->fetch_all($rs);

//add by tina 20180326 List:總收發人顯示名字
        foreach ($pdata as &$pdataInfo) {
            $revManId = $pdataInfo['revMan'];
            if (isset($revManId)) {

                $pdataInfo['revManName'] = $emplyeeinfo[$revManId];

            }
            unset($pdataInfo);

        }
//update by tina 20180412 新增/修改不顯示清單
        if (empty($act) or $act == 'del') {
            $obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
        }

//:ViewControl Object ------------------------------------------------------------------------
        $listPos = $pinf->curRecord % $pinf->pageSize;
        switch ($act) {
    case 'edit': //修改
    $op3 = array(
        "type"      => "edit",
        "form"      => array('form1', "$tableName/outer/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定修改", false, ''),
        "cancilBtn" => array("取消修改", false, ''));
    $fieldE_Data = $pdata[$listPos];
    $ctx         = new BaseViewControl($fieldE_Data, $fieldE, $op3);
    break;
    case 'new': //新增
    $op3 = array(
        "type"      => "append",
        "form"      => array('form1', "$tableName/outer/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定新增", false, ''),
        "cancilBtn" => array("取消新增", false, ''));
    $ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
    break;
}

include 'public/inc_listjs.php';
?>
<form name="excelForm" action="/organization/outer/insertExcel.php" method="post" enctype="multipart/form-data">
    <div id="excelModal" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h4 class="modal-title">匯入excel</h4>
                </div>
                <div class="modal-body">
                    <table cellpadding="4" id="giftForm">
                        <tr>
                            <input type="file" name="file" id="file" accept=".xls,.xlsx">
                        </tr>
                        <tr style="font-size: 14px;">
                            <td>
                                <a href="/organization/outer/外部組織範例檔.xlsx" style="font-size: 18px;">《匯入格式下載》</a><br><br>
                                備註1：請將全部資料放到第一個頁籤，請勿分多頁簽。<br>
                                備註2：組織類別，請參照外部組織類別管理的類別名稱。<br>
                                備註3：組織型別，請一律填入 OUTER 即可。<br>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" onclick="checkFile()">匯入</button>
                    <button type="button" class="btn" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div id="secretDiv">
    <button type="button" onclick="$('#secretDiv').hide()">關閉</button>
    <p>可能重複清單</p>
    <ul class="checkOrg">
        
    </ul>
</div>

<script>
    $(document).ready(function(){
        $('[name="title"]').on('focusout',function(){
            $.ajax({
                url: 'organization/api.php',
                type: "POST",
                data: {
                    act : 'checkOrgName',
                    text : $(this).val()
                },
                dataType: 'json',
                async: false,
                success: function(response){
                    $(".checkOrg").empty();
                    $(".checkOrg").append(response);
                    $("#secretDiv").show();
                }
            });
        });
    });

    $("[name='Submit']").click(function() {
        var act ="<?echo $act; ?>";
        if(act == 'new'){
            var funact  = 'orgnum';
            var OrgType = 'OUTER';
            var title   = $("[name='title']").val();
            $.ajax({
                url: 'organization/api.php?act='+funact+'&OrgType='+OrgType+'&title='+title,
                type:"GET",
                dataType:'text',
                async: false,
                success: function(response){
                    if(response !==''){
                        $("[name='id']").val(response);
                        alert("新增外部組織:編號:"+response+',名稱:'+$("[name='title']").val());
                        result=true;
                    }else{
                        result=false;
                        alert('名稱重複');
                    }

                }
            })
            return result;
        }
    })
//複製部門
function copyDept(){
    if(copyCheckItem()) {
       ListControlForm.act.value = 'copy';
       ListControlForm.page.value = page;
       ListControlForm.submit();
   }
}
//檢查是否勾選資料
function copyCheckItem() {
    var obj1 = document.getElementsByName('ID[]');
    if (obj1!=null) {
            if (obj1.length==undefined) { //only one item
                if (obj1.checked)   return true; else   { alert('請選擇項目！'); return false; }
            } else {    // multiple items
                var selfg=false;
                for(i=0; i<obj1.length; i++) {
                    if(obj1[i].checked) {
                        selfg=true;
                        break;
                    }
                }
                if(!selfg) alert('請選擇項目！');
                return selfg;
            }
        } else {    alert("沒有資料項目！"); return false; }
    }

    function updateExcel(argument) {
        $('#excelModal').modal('toggle');
    }

    function checkFile(){
        if($('#file')[0].files.length > 0){
            excelForm.submit();
        }else alert("請上傳匯入檔案");
    }
</script>