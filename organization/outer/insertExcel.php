<?
    include "../../config.php";
    include "../../inc_vars.php";
    include "../../system/db.php";
    require "../../Classes/PHPExcel.php";
    require "../../Classes/PHPExcel/IOFactory.php";
    // // 暫時檔名
    $aa = explode('.', $_FILES['file']['name']);
    $sTmpFile = "$root/organization/outer/excel/orgExcel".date('Ymd').".".$aa[1];

    move_uploaded_file($_FILES['file']['tmp_name'], $sTmpFile);
    
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');

    $db = new db();
    $objReader->setReadDataOnly(true);

    $objPHPExcel = $objReader->load($sTmpFile);
    $objPHPExcel->setActiveSheetIndex(0);
    $objWorksheet = $objPHPExcel->getActiveSheet();
    $numRows      = $objWorksheet->getHighestRow();

    $success = 0;
    $fail = 0;

    for ($i = 2; $i <= $numRows; $i++) {
        $title    = $objPHPExcel->getActiveSheet()->getCell('A' . $i)->getFormattedValue();  // 名稱
        $countyS  = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getFormattedValue();  // 城市
        $city     = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getFormattedValue();  // 區
        $address  = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getFormattedValue();  // 地址
        $PosCode  = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getFormattedValue();  // 郵遞區號
        $orgClass = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getFormattedValue();  // 組織類別
        $orgType  = $objPHPExcel->getActiveSheet()->getCell('G' . $i)->getFormattedValue();  // 內外部

        $sql = "SELECT 1 FROM organization WHERE title = '".$title."'";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);

        if(!$r){
            $sql = "SELECT MAX(id) as id FROM organization WHERE orgType = 'OUTER'";
            $rs  = $db->query($sql);
            $r   = $db->fetch_array($rs);
            $id  = $r['id']+1;

            $countyS = array_search($countyS, $county);

            $sql = "SELECT id FROM org_class WHERE title = '".$orgClass."'";
            $rs  = $db->query($sql);
            $r   = $db->fetch_array($rs);
            $orgClass = $r['id'];

            $sql = "INSERT INTO organization (id,title, county, city, address, PosCode, orgClass, orgType) VALUES('$id', '$title', '$countyS', '$city', '$address', '$PosCode', '$orgClass', '$orgType')";
            $result = $db->query($sql);
            if($result){
                $success++;
            }else{
                $fail++;
            }
        }
    }
    
    echo "<script>alert('成功新增 ".$success." 筆，失敗 ".$fail." 筆。');window.history.back(-1);</script>"
?>