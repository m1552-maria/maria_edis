<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../config.php');
    include_once "$root/inc_vars.php";
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
	include "$root/getEmplyeeInfo.php";
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "內部組織";
	//$pageSize=3; 使用預設
	$tableName = "organization";
	// for Upload files and Delete Record use
	$ulpath = "/data/organization/"; if(!file_exists($root.$ulpath)) mkdir($root.$ulpath);
	$delField = 'attach';
	$delFlag = true;
	$self = array_slice($orgType, 0,1); 
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('id','title',),	//要搜尋的欄位	
	);
		
	$fieldsList = array(
		"id"=>array("編號","60px",true,array('Id',"gorec(%d)")),
		"title"=>array("名稱","150px",true,array('Text')),
		"sTitle"=>array("簡稱","100px",true,array('Text')),
		"numberTitle"=>array("編號簡稱","100px",true,array('Text')),
		//"address"=>array("地址","360px",false,array('Define',view_addr)),
		//"PosCode"=>array("郵遞區號","100px",true,array('Text')),
		//"email"=>array("EMail","60px",false,array('Text')),
		//"tel"=>array("電話","100px",false,array('Text')),
		"revManName"=>array("文書","120px",false,array('Text')),
		"proxyRevManName"=>array("代理文書","120px",false,array('Text')),
		"mainRevManName"=>array("總文書","120px",false,array('Text')),
		"proxyMainRevManName"=>array("代理總文書","120px",false,array('Text'))
		//"attach"=>array("簽名檔","100px",false,array('MultiFile',60))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"id"=>array("編號","text",60,'','',array(true,'','',50)),
		"title"=>array("名稱","text",60,'','',array(true,'','',50)),
		"sTitle"=>array("簡稱","text",60,'','',array(true,'','',50)),
		"selfTitle"=>array("自稱","text",60,'','',array(false,'','',50)),
		"numberTitle"=>array("編號簡稱","text",60,'','',array(false,'','',50)),
		"county"=>array("縣市","uLABEL"),
		"city"=>array("鄉鎮區","uLABEL"),
		"address"=>array("地址","Define",90,'',func_addr),
		"PosCode"=>array("郵遞區號","text",60,'','',array(true,PTN_NUMBER,'',50)),
		"orgType"=>array("機關行別","select",1,'',$self),
		"email"=>array("EMail","text",30,'','',array(true,PTN_EMAIL,'',60)),
		"tel"=>array("電話","text",60,'','',array(true,PTN_TEL,'',20)),
		"fax"=>array("傳真","text",60,'','',array(false,PTN_TEL,'',20)),
		"revMan"=>array("文書","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"mainRevMan"=>array("總文書","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"proxyRevMan"=>array("文書代理","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"proxyMainRevMan"=>array("總文書代理","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"attach"=>array("簽名檔","multifile",60,'',),
		"orgClass"=>array("類別",'hidden'),
		"notes"=>array("備註",'textbox','50','2')
	);
	$fieldA_Data = array("empID"=>$_REQUEST['empID'],"orgClass"=>'IN');
	
	$fieldE = array(
		"id"=>array("編號","id",60,),
		"title"=>array("名稱","text",60,'','',array(true,'','',50)),
		"sTitle"=>array("簡稱","text",60,'','',array(true,'','',50)),
		"selfTitle"=>array("自稱","text",60,'','',array(false,'','',50)),
		"numberTitle"=>array("編號簡稱","text",60,'','',array(false,'','',50)),		
		"county"=>array("縣市","uLABEL"),
		"city"=>array("鄉鎮區","uLABEL"),
		"address"=>array("地址","Define",90,'',func_addrE),
		"PosCode"=>array("郵遞區號","text",60,'','',array(true,PTN_NUMBER,'',50)),
		"orgType"=>array("機關行別","select",1,'',$self),
		"email"=>array("EMail","text",30,'','',array(true,PTN_EMAIL,'',60)),
		"tel"=>array("電話","text",60,'','',array(true,PTN_TEL,'',20)),
		"fax"=>array("傳真","text",60,'','',array(false,PTN_TEL,'',20)),	
		"revMan"=>array("文書","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"mainRevMan"=>array("總文書","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"proxyRevMan"=>array("文書代理","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"proxyMainRevMan"=>array("總文書代理","queryID",60,'sId','emplyeeinfo',array(false,'','',50)),
		"attach"=>array("簽名檔","multifile",60,'',),
		"notes"=>array("備註",'textbox','50','2')
	);
?>