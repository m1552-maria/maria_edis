<?php
	include 'init.php';
	// $db = new db();
 //    $sql = "select * from $tableName where 1=1 ";

    //::Search
	// if(strlen(trim($_REQUEST['key']))>0) { $key=$_REQUEST['key']; $sql .= " and (id like '%$key%' or sTitle like '%$key%' or title like '%$key%') "; }
	//  $sql .= " order by orgType desc ,id asc";

	//  $rs = $db->query($sql);	 
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>組織機關代碼表</title>
	<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
	<link href="/system/TreeControl.css" rel="stylesheet">
	<style type="text/css">
		.selItem {background-color:#CCCCCC; color:#F90; }
		.gray{color:#999;}
		#TreeControl { border: none; } /* override */
	</style>
	<script src="/media/js/jquery-1.10.1.min.js"></script>
	<script src="/Scripts/jquery.treeview.js"></script>
	<script src="/system/TreeControl.js"></script>
	<script type="text/javascript">
		var selid;					//送入的 id
		var mulsel = false;	//可複選
		var selObjs = new Array();
		$(document).ready(function(){

			//selid = dialogArguments;
			if(selid) $("#browser a[onClick*=('"+selid+"']").addClass('selItem');
			if(mulsel) $('#tblHead').append("<button style='margin-left:30px' onClick='returnSeletions()'>確定</button>");
		});
			
	  function setID(id,title,elm) {
			if(mulsel) {
				selObjs.push({'id':id,'title':title});
				$(elm).addClass('selItem');
			} else { 
				//opener.source.postMessage([id,title], opener.origin);
				//window.returnValue = [id,title]; 
				window.opener.returnValue([id,title]);
				window.opener.rzt = false;
				//opener.source.postMessage('OK');
				window.close(); 
			}
	  }
		function returnSeletions() {
			for(key in selObjs) alert(selObjs[key].id+':'+selObjs[key].title);
		}		
  </script>
</head>

<body leftmargin="0" topmargin="0" style="overflow:auto">	
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
	<tr><!-- Search Bar Start -->
<?
	$db = new db();
	//:TreeControl ---------------------------------------------------------------------
	$op = array(
		'ID'		=> 'id',
		'Title'	=> 'title',
		'TableName' => 'org_class',
		'RootClass' => 0,
		'RootTitle' => $CompanyName,
		'DB' => $db,
		'Modal' => false,
		'URLFmt' => "javascript:setID(\"%s\",\"%s\",this)",
		
		'ChildTbl'=>'organization',
		'FKey'=>'orgClass',
		'cID'=>'id',
		'cTitle'=>'title',
		'cWhere'=>"(offShelf IS NULL or offShelf = '0')"
	);
	new BaseTreeControl($op);
?>	
	</tr>
</table>
</body>
</html>