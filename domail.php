<?php
/* PHP SMTP 寄信模組 by: michael rou from 2010/01/17, lastest fix on 2015/6/29
 * required class.phpmailer.php, class.smtp.php
 * surport 2 mode : Function Call & Form Post
 */
//header("Content-Type:text/html; charset=utf-8");

include_once "class.phpmailer.php"; 			//::PHPMailer寄信物件,初始化

$MailInfo["SMTP_HOST"] = "mail.maria.org.tw";
$MailInfo["MAIL_USERNAME"] = "adm@mail.maria.org.tw";
$MailInfo["MAIL_PASSWORD"] = "adm05557110";

$MailInfo["SMTPAuth"]	 = true; 								//是否要驗證
$MailInfo["MAIL_FROM"] = "adm@mail.maria.org.tw"; 		//寄信信箱
$MailInfo["MAIL_NAME"] = "瑪利老爹網"; 	//寄信人名稱
$Pverson = "X-Program: mediaLog ver 0.7.1(2009-08-18) add xpost_data3"; //程式版本序號
$CustomHeader[] = $Pverson; // 自訂 header

if($_REQUEST[POSTMAIL]) {	//:for表單大量寄發模式		
	$tomail = explode(';', $_REQUEST[tomail]);	//收件人
	foreach($tomail as $v) if($v) $MailInfo["mailTo"][]=array("MailName" => $v,	"address" => $v); 
	$MailInfo["Subject"] = $_REQUEST[subject];				//主旨
	$body = file_get_contents($_REQUEST[body]);				//內文樣板檔
	$MailInfo["Body"] = $body;								
	$msg = SMTP_Send_Mail($MailInfo, $CustomHeader ); 	//寄信		
}

function send_mail($to, $subject='No subject', $body) {	//::for函數呼叫模式,可單人或多人
	global $MailInfo,$CustomHeader;
	$tomail = explode(';', $to);
	foreach($tomail as $v) if($v) $MailInfo["mailTo"][]=array("MailName"=>$v,	"address"=>$v); 
	$MailInfo["Subject"] = $subject;									 	//主旨	
	$MailInfo["Body"] = $body;													//內文
	return SMTP_Send_Mail($MailInfo, $CustomHeader ); 	//寄信	
}

function SMTP_Send_Mail( $MailInfo=array(), $CustomHeader=array() ){	// 預設寄信程序

	$mail = new PHPMailer();
	$mail->IsSMTP();							
	//$mail->SMTPDebug= true;																			
	$mail->Hostname = $MailInfo["SMTP_HOST"];
	$mail->Host		  = $MailInfo["SMTP_HOST"] or die( "No SMTP_HOST value" );		
	$mail->SMTPAuth = $MailInfo["SMTPAuth"];
	$mail->Username = $MailInfo["MAIL_USERNAME"] or die( "No MAIL_USERNAME value" );
	$mail->Password = $MailInfo["MAIL_PASSWORD"] or die( "No MAIL_PASSWORD value" );
	$mail->From		  = $MailInfo["MAIL_FROM"] or die( "No MAIL_FROM value" );
	$mail->FromName	= $MailInfo["MAIL_NAME"] or die( "No MAIL_NAME value" );

	if( isset( $MailInfo["Return-Path"] ) && $MailInfo["Return-Path"] != '' ) {
		$mail->Sender	= $MailInfo["Return-Path"];
	}
	//執行 $mail->AddAddress() 加入收件者，可以多個收件者
	if( isset( $MailInfo["mailTo"] ) && is_array( $MailInfo["mailTo"] ) && count( $MailInfo["mailTo"] ) > 0 ) {
		foreach( $MailInfo["mailTo"] AS $vv ) $mail->AddAddress( $vv["address"], $vv["MailName"] );
	} else return "收件人資料錯誤!";
	//執行 $mail->AddCC() 加入副本收件者，可以多個收件者
	if( isset( $MailInfo["cc"] ) && is_array( $MailInfo["cc"] ) && count( $MailInfo["cc"] ) > 0 ) {
		foreach( $MailInfo["cc"] AS $vv ) $mail->AddCC( $vv["address"], $vv["MailName"] );
	}
	//執行 $mail->AddBCC() 加入密件副本收件者，可以多個收件者
	if( isset( $MailInfo["bcc"] ) && is_array( $MailInfo["bcc"] ) && count( $MailInfo["bcc"] ) > 0 ) {
		foreach( $MailInfo["bcc"] AS $vv ) $mail->AddBCC( $vv["address"], $vv["MailName"] );
	}

	$mail->WordWrap = 100; // set word wrap
	$mail->IsHTML( true ); // send as HTML
	$mail->Subject =  "=?UTF-8?B?".base64_encode( $MailInfo["Subject"] )."?=";
	$mail->CharSet = "UTF-8";

	if( isset( $CustomHeader ) && is_array( $CustomHeader ) && count( $CustomHeader ) > 0 ) {
		foreach( $CustomHeader AS $vv ) $mail->AddCustomHeader( trim( $vv ) );
	}
	if( isset( $MailInfo["Body"] ) && $MailInfo["Body"] != '' ) {
		$mail->Body = $MailInfo["Body"]; // HTML 格式
	} else {
		return "信件內容不可以是空字串!";
	}
	if( isset( $MailInfo["AltBody"] ) && $MailInfo["AltBody"] != '' ) {
		$mail->AltBody = $MailInfo["AltBody"]; // text 格式
	} else {
		$mail->AltBody = $MailInfo["Body"]; // text 格式
	}

	if(!$mail->Send()) {
		return "Message was not sent! Mailer Error: " . $mail->ErrorInfo;
	} else {
		return "Mail Send success";
	}
}
?>