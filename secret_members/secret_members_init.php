<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */


include_once "../config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/inc_vars.php";
include_once "func.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "$root/system/ListControl.php";
include "$root/system/ViewControl.php";
include_once "$root/getEmplyeeInfo.php";

$pageTitle = "";
//$pageSize=5; //使用預設
$tableName = "secret_members";
// for Upload files and Delete Record use
$ulpath = "/data/secret_members/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}
$delField  = 'attach';
$delFlag   = true;
$edisid = $_REQUEST['id'];
$empid = $_REQUEST['empid'];



//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, false, SearchKeyword),
    "newBtn"    => array("＋新增", false, true, doAppend),
    "editBtn"   => array("－修改", false, true, doEdit),
    "delBtn"    => array("Ｘ刪除", false, true, doDel),
);

    //:ListControl  ---------------------------------------------------------------
    $opList = array(
        "alterColor"=>true,         //交換色
        "curOrderField"=>'edisid,empid', //主排序的欄位
        "sortMark"=>'ASC',          //升降序 (ASC | DES)
        "searchField"=>array('empid'),   //要搜尋的欄位    
    );
        
    $fieldsList = array(
        "secret_members_id"=>array("編號","60px",true,array('Id',"gorec(%d)")),
        "empid"=>array("職員","120px",true,array('Define','getMan'))
    );
    
    //:ViewControl -----------------------------------------------------------------
    $fieldA = array(
        "empid"=>array("員工","queryID",20,'empIDt','emplyeeinfo',array(true,'','',50)),
        "edisid"=>array("公文ID","readonly",30),
        "createrid"=>array("建立者ID","readonly",30)        
    );
//  $fieldA_Data = array("empID"=>$_REQUEST['empID']);
    
    $fieldE = array(
        "secret_members_id"=>array("編號","readonly",15,''),
        "empid"=>array("員工","queryID",20,'empIDt','emplyeeinfo',array(true,'','',50))
    );
