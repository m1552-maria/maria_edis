<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../config.php');
    include_once "$root/getEmplyeeInfo.php";		
    include_once "$root/inc_vars.php";
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php";


	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
    include "ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	if ($odMenu == 'rod') {
	    $odTypeName = '收文';
	} else if ($odMenu == 'sod') {
	    $odTypeName = '發文';

	}	
	$pageTitle = "機密權限管理";
	//$pageSize=3; 使用預設
	$tableName = "(SELECT * FROM edis WHERE id IN(SELECT distinct edisid  FROM secret_members ) ) ta";
	// for Upload files and Delete Record use
	//$ulpath = "/data/organization/"; if(!file_exists($root.$ulpath)) mkdir($root.$ulpath);
	$delField = 'attach';
	$delFlag = true;

	/*query sql add by Tina 20190508 優化效能不使用view,將view的select 搬出來*/
	// $querySql = $myod;

	//: Select field Prepared -----------------------------------------------------
	if ($odMenu == 'rod') {
	$actArr = $rodRact;
	} else if ($odMenu == 'sod') { 
	 $actArr = $sodSact;
	}   

	$orgs = array();
	$orgs['']='-全部組織-'; //空字串:全部
	foreach ($actArr as $k=>$v) {
	    $orgs[$k] = $v;
	}


	/*
	//當前時間前六個月
	$year=date("Y",time());
	$halfMonth= date("Y-m-d", strtotime("-6 month"));
	$first=$halfMonth;
	//當年最後一天
	$end=$year."-12-31";
	*/
	$date=date('Y-m-d');  //當前日期

	$wfirst=1; //$wfirst =1 表示每周星期一為開始日期 0表示每周日為開始日期

	$w=date('w',strtotime($date));  //取得當前周的第幾天 周日是 0 周一到周六是 1 - 6 

	$first=date('Y-m-d',strtotime("$date -".($w ? $w - $wfirst : 6).' days')); //取得本周開始日期，如果$w是0，則表示周日，減去 6 天

	$end=date('Y-m-d',strtotime("$now_start +6 days"));  //本周结束日期

	//$last_start=date('Y-m-d',strtotime("$now_start - 7 days"));  //上周开始日期

	//$last_end=date('Y-m-d',strtotime("$now_start - 1 days"));  //上周结束日期	
	
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,false,doAppend),
		"editBtn" => array("－修改",false,false,doEdit),
		"delBtn" => array("Ｘ刪除",false,false,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
if ($odMenu == 'rod') {
    //收發文之欄位名稱不同
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '來文日期';
    $FsActName = '來文機關';
    $FrActName = '收文機關';
    $FName     = 'rAct';
    $opList = array(
        "alterColor"=>true,         //交換色
        "curOrderField" => 'rDate', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('did', 'sActName', 'creator', 'subjects'), //要搜尋的欄位   
        "filterOption"=>array(
            'autochange'=>false,
            'rDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'rDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filterCondi"=>array(   //比對條件
            'depID'=>"%s like '%s%%'"
        ),
        "filters"=>array(
            $FName =>$orgs,
            'rDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            //array("統計報表",false,true,'receipt_report'),
        )

    );    
    $fieldsList = array(
        "did"      => array($Fdid, "30px", true, array('Id', "gorec(%d)")),
        "rDate"    => array('承辦日期', "30px", true, array('Date')),
        "deadline" => array("期限", "30px", true, array('Date')),
        "sActName" => array($FsActName, "30px", true, array('Text')),
        "subjects" => array("主旨", "80px", false, array('Text'))
    );
} else if ($odMenu == 'sod') {
    //收發文之欄位名稱不同
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '發文日期';
    $FsActName = '發文機關';
    $FrActName = '受文者';
    $FName     = 'sAct';
    $opList = array(
        "alterColor"=>true,         //交換色
        "curOrderField" => 'sDate', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('did', 'rActName', 'creator', 'subjects'), //要搜尋的欄位   
        "filterOption"=>array(
            'autochange'=>false,
            'sDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'sDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filterCondi"=>array(   //比對條件
            'depID'=>"%s like '%s%%'"
        ),
        "filters"=>array(
            $FName =>$orgs,
            'sDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            //array("統計報表",false,true,'receipt_report'),
        )

    );    
    $fieldsList = array(
        "did"      => array($Fdid, "30px", true, array('Id', "gorec(%d)")),
        "sDate"    => array($FsDate, "30px", true, array('Date')),
        "rActName" => array($FrActName, "30px", true, array('Text')),
        "subjects" => array("主旨", "80px", false, array('Text')),
        "sId"  => array("承辦", "30px", true, array('Define', getMan))

    );
}
	
?>