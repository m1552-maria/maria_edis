<?php
	header("Content-type: text/html; charset=utf-8");
	include "inc_vars.php";
	
	$fp=fopen("inc_vars.php","r");
	$c=-1;
	$data=array();
	$var;
	$fixAry=array();//不能被修改的data
	while (!feof($fp)){
		$str=trim(fgets($fp));
		if(strlen($str)==0)	continue;//跳過空行
		if(substr($str,0,2)=="/*"){//group name
			$c++;
			$end=strpos($str,"*/");
			$groupName=substr($str,2,strlen($str)-2-2);
			if(!is_array($data[$c]))	$data[$c]=array();
			$data[$c]['groupName']=$groupName;
			continue;
		}
		
		if(substr($str,0,3)=="//:"){//註解說明
			continue;
		}else if(substr($str,0,3)=="//@"){//不能更改的var name
            //update by tina 20180403 修正相同var name 不同type也會一並不可更改問題
			$title =explode("//@",$str);
			$temp =explode("-",$title[1]);
			$fix[$temp[0]] = explode(",",$temp[1]);
			$fixAry = array_merge($fixAry, $fix);

			continue;
		}else if(substr($str,0,2)=="//"){//var name
			$varName=substr($str,2,strlen($str)-2);
			if(!is_array($data[$c]['varName']))	$data[$c]['varName']=array();
			array_push($data[$c]['varName'],$varName);			
			continue;
		}
		if(substr($str,0,1)=="$"){//處理陣列
			$index=strpos($str,"=");
			$ary=trim(substr($str,1,$index-1));
			$cnt=trim(substr($str,$index+1,strlen($str)-$index));
			$cnt=substr($cnt,6,strlen($cnt)-6-2);
			$a1=explode(",",$cnt);
			$cnt=array();
			
			$var.="var ".$ary."=[];\n";
			for($i=0;$i<count($a1);$i++){
				$s=explode("=>",$a1[$i]);
				$cnt[$s[0]]=$s[1];
				$var.=$ary."[".$s[0]."]=".$s[1].";\n";
			}
			///print_r( $cnt)."<br>";
			$$ary=$cnt;
			if(!is_array($data[$c]['aryName']))	$data[$c]['aryName']=array();
			if(!is_array($data[$c]['aryData']))	$data[$c]['aryData']=array();
			//if(!is_array($data[$c]['aryDataJson']))	$data[$c]['aryDataJson']=array();
			array_push($data[$c]['aryName'],$ary);
			array_push($data[$c]['aryData'],$$ary);
			//echo $$ary."<br>";
			//print_r($cnt);
			//$$ary=json_encode($$ary);
			//array_push($data[$c]['aryDataJson'],$$ary);
		}
	}
	fclose($fp);
	$fixData=json_encode($fixAry);
?>

<html><head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" href="css/FormUnset.css">
<style type="text/css">
table.grid{  border: 1px solid #ccc; border-collapse: collapse; width:100%;}
table.grid .title { font-size:14px; background-color:#dedede }
talbe.grid tr, td { border: 1px solid #999;	padding:3px;	font-size:12px;	background: #ffffff;}
.grid td{	padding:4px;}
#d_relation{
	width: 100%;
	font-family: Verdana, "微軟正黑體";
}
#d_list{
	height:300px;
	overflow:auto;
	margin-top:8px;
	margin-bottom:8px;
	width:400px;
}
#d_container{	
	position:fixed;
	left:35%;
	margin-left:-105px;
	overflow:auto;
	display:none;	
	top:5%;
	border:1px solid #999999;
	border-radius: 4px;
}
.dTop{	background:#dedede; text-align:left;border:1px solid #cdcdcd;padding:5px;	background: #ffffff;}
.dBottom{	background:#ffffff; text-align:center;padding-bottom:4px;border:1px solid #cdcdcd;}
.tbTitle{background:#f0f0f0; font-size:12px;}
.delBtn{	width:26px;}
.keyWidth{	vertical-align:middle; width:180px;}

.divBorder1 { /* 內容區 第一層外框*/
	margin:10px;
}
.divBorder2 { /* 內容區 第二層外框 */
	padding:8px; 
	border:1px solid #CCC;
	background:#FFFFFF;
	font-size:12px;
	border-radius: 4px;
}

.bg{	background: #dedede;	}
.divTitle{
	color: #fff;
	background-color: #900;
	border: 0px;
	padding: 5px;
	margin: 0px;
	font-size: 14px;
}
.divTitle2{	color:#fff;	background-color:#666;}
.sBtn{
	cursor:pointer;
	display:inline-block;
	text-decoration:none;
	font-size:12px;
	padding:3px 0px 1px 3px;
	width:45px;
	border-style:solid;
	border-width:1px;
	border-radius:4px;
	box-shadow:0 1px 1px rgba(255,255,255,0.5) inset;
	
	background:#555555;
	border-color:#999;
	border-color:rgba(0,0,0,0.1);
	color:#fff; 
}
.btn2{
	background: #EE9F35;
	margin-top:5px;
	padding:0 10px;
}
input[readonly]{
  border:none;
}
</style>
<script type="text/javascript" src="/Scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript">
var ACT_TYPE='';//目前正處理哪一個類別
var fixAry =JSON.parse('<?=$fixData?>');
<?= $var ?>;
$(function() {
});
function openPnl(id,act,type,typeStr,title){
	if(act=="Y"){
		$("#"+id).draggable({scroll: true });	
		$('#'+id).show();
	}else{
		$('#'+id).hide();
		return;
	}
	ACT_TYPE=typeStr;
	var str="<table class='grid' id='tb_"+ACT_TYPE+"'>";
	str+="<tr align='center' class='tbTitle'><td style='width:60px'>代碼</td><td >名稱</td><td style='width:50px'>刪除</td></tr>";
	var i=0;
	for(var k in type){
		str+="<tr align='center'>";

		if(typeof fixAry[ACT_TYPE] !=='undefined'){
		if(fixAry[ACT_TYPE].indexOf(k)>-1){
			str+='<td style="width:60px"><input type="text" size="10" readonly value="'+k+'" name="'+ACT_TYPE+'_key">';
			str+='<td ><input type="text" size="28" value="'+type[k]+'" readonly name="'+ACT_TYPE+'_val"></td>';
			str+='<td style="width:50px">已使用</td>';
		}else{
			str+='<td style="width:60px"><input type="text" size="10" value="'+k+'" name="'+ACT_TYPE+'_key">';
			str+='<td ><input type="text" size="28" value="'+type[k]+'" name="'+ACT_TYPE+'_val"></td>';
			str+='<td><img src="/images/del.png" class="delBtn" onClick="delItem(this,event)"></td>';	
		}

		}else{
			str+='<td style="width:60px"><input type="text" size="10" value="'+k+'" name="'+ACT_TYPE+'_key">';
			str+='<td ><input type="text" size="28" value="'+type[k]+'" name="'+ACT_TYPE+'_val"></td>';
			str+='<td><img src="/images/del.png" class="delBtn" onClick="delItem(this,event)"></td>';

		}
		str+="</tr>";
		i++;
	}
	title+="維護";
	$('#d_list').html(str);
	
	$('#dTitle').html(title);
}

function addItem(){//新增
	var tb=document.getElementById("tb_"+ACT_TYPE);
	var tbObj=tb.tBodies[0];
	var newRow=document.createElement('tr');
	var newcell1=document.createElement('td');
	var newcell2=document.createElement('td');
	var newcell3=document.createElement('td');
	newRow.style.height='30px';
	newRow.bgColor='#E3E3E3'; 
	newcell1.align='center'; 
	newcell1.innerHTML="<input type='text' size='10' name='"+ACT_TYPE+"_key'/>"; 
	newcell2.align='center'; 
	newcell2.innerHTML="<input type='text' size='28' name='"+ACT_TYPE+"_val'/>";
	newcell3.align='center'; 
	newcell3.innerHTML='<img src="/images/del.png" class="delBtn" onClick="delItem(this,event)">'; 
	newRow.appendChild(newcell1);
	newRow.appendChild(newcell2);
	newRow.appendChild(newcell3);
	tbObj.appendChild(newRow);
	//document.all.tbContext.focus(); 
	var objDiv = document.getElementById("d_list");
	objDiv.scrollTop = objDiv.scrollHeight;
}
function delItem(obj,e){//刪除
	var evt = e || window.event;
    var current = evt.target || evt.srcElement;
	var currRowIndex=current.parentNode.parentNode.rowIndex; 
	var tb = document.getElementById("tb_"+ACT_TYPE);
    if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
} 
function saveItem(){
	var keyAry=new Array();
	var valAry=new Array();
	var objKey=document.getElementsByName(ACT_TYPE+"_key");
	var valKey=document.getElementsByName(ACT_TYPE+"_val");
	var bNull=false;
	for(var i=0;i<objKey.length;i++){
		if(objKey[i].value==""){	
			alert("代碼不能空白");	
			objKey[i].focus();	
			bNull=true;	
			break;	
		}
		if(valKey[i].value==""){	
			alert("名稱不能空白");	
			valKey[i].focus();	
			bNull=true;	
			break;	
		}
		if(jQuery.inArray(objKey[i].value,keyAry)>-1){
			alert("代碼不能重覆");	
			objKey[i].focus();	
			bNull=true;	
			break;	
		}
		if(jQuery.inArray(valKey[i].value,valAry)>-1){
			alert("名稱不能重覆");	
			valKey[i].focus();	
			bNull=true;	
			break;	
		}
		keyAry.push(objKey[i].value);
		valAry.push(valKey[i].value);
	}
	if(bNull){	return;	}
	var keyStr=keyAry.join(",");
	var valStr=valAry.join(",");
	//return;
	$.ajax({ 
	  type: "POST", 
	  url: "varsSet_lib.php", 
	  data: "act=saveItem&type="+ACT_TYPE+"&keyStr="+keyStr+"&valStr="+valStr
	}).done(function( rs ) {
		location.reload();
	});
}
</script>
</head>
<body>
<div id='d_relation' class='divBorder1' > 
  <div class='divTitle'>欄位資料對應表</div>
  <div  class='divBorder2' >
	<?
  $tb="";
  $cols=5;
    foreach($data as $k=>$v){
    $tb.="<table class='grid'>";
    $tb.="<tr><td colspan='".$cols."' class='title'>".$v['groupName']."</td></tr>";
    $trC=0;
    $varC=count($v["varName"]);
    foreach($v["varName"] as $k1=>$v1){
      if($trC%$cols==$cols){
        $tb.="<tr align='right'>";
      }
      if($trC==($varC-1)){
        $tb.="<td colspan='".abs($cols-$trC)."'>".$v1."：<input type='button' onClick='openPnl(\"d_container\",\"Y\",".$v['aryName'][$k1].",\"".$v['aryName'][$k1]."\",\"".$v["varName"][$k1]."\")' class='sBtn' value='維護'></td>";
      }else{
        $tb.="<td class='keyWidth'>".$v1."：<input type='button' onClick='openPnl(\"d_container\",\"Y\",".$v['aryName'][$k1].",\"".$v['aryName'][$k1]."\",\"".$v["varName"][$k1]."\")' class='sBtn' value='維護'></td>";
      }
      if($trC%$cols==$cols-1){
        $tb.="</tr>";
      }
      $trC++;
    }
    
    $tb.="</table><br>";
  }
  echo $tb;
	?>
  </div>
</div>
<div id='d_container' > 
  <div id='dTitle' class='divTitle divTitle2'></div>
    <div class='divBorder2 bg' >
      <div id='d_list' align="center"></div>
    <div class='dBottom'>
	    <input type="button" id='btnAdd' onClick="addItem();" class='sBtn btn2' value="新增">
    	<input type="button" id='btnSave' class='sBtn btn2' onClick="saveItem()" value="儲存">
    	<input type="button" class='sBtn btn2' id='btnCancel' onClick="openPnl('d_container','N')" value="取消">
    </div>
  </div>
</div>
</body>
</html>