<?
	/* ---------------------------------------
		 支援 MASTER - DETAIL 運作
		 Create By Michael Rou from 2018/6/8
	------------------------------------------ */
	include("../thumb.php");
	include 'lib.php';
	
	//處理上傳的附件
	$fnlst = array();
	foreach($_FILES as $k=>$v) {
		switch($fieldA[$k][1]) {
			case 'file': 
				$fn = $_FILES[$k]['name'];
				if ($_FILES[$k][tmp_name] != "") {
					$fnlst[$k] =  md5(pathinfo($fn,PATHINFO_FILENAME)).'.'.pathinfo($fn,PATHINFO_EXTENSION);
					$dstfn = $root.$ulpath.$fnlst[$k]; 
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'],$dstfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
				}
				break;
			case 'multifile':
				$aa = array();
				foreach($_FILES[$k]["name"] as $k2=>$v2){
					if(!$v2) continue; 
					$finf = pathinfo($v2); 
					$fn = md5($_FILES[$k]["name"][$k2]).'.'.$finf['extension'];
					$dstfn = $root.$ulpath.$fn;
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'][$k2],$dstfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
					array_push($aa,$fn);				
				}
				$fnlst[$k] = implode(',',$aa);
				break;
		}
	}

	//建立主檔資料庫物件
  $my = new db($tableName);	
	foreach($fieldA as $k=>$data) {
		switch($data[1]) {
			case "Define":
			case "uLABEL":
			case "date" :
			case "hidden" :
			case "select" :
			case "readonly" : $my->row[$k]="'$_REQUEST[$k]'"; break;
  		case "text" :
			case "textbox" :
			case "queryID" :
  		case "textarea" :	$my->row[$k]="'".addslashes($_REQUEST[$k])."'"; break;
			case "queryIDs" :	$my->row[$k]="'".addslashes( join(',',$_REQUEST[$k]) )."'"; break;
  		case "checkbox" : $my->row[$k]=(isset($_REQUEST[$k])? 1:0); break;
			case "cklist" : if( $_REQUEST[$k] ) { $sum=0; foreach($_REQUEST[$k] as $v) $sum += $v; $my->row[$k]=$sum; }; break;	
			case "multifile":
			case "file" : $my->row[$k]="'$fnlst[$k]'"; break;	
  	}	
	}
	$my->insert();	
	
	//建立明細檔資料庫物件
	$n = count($_REQUEST['dt_'.key($Dfields)]);
	if($n>0) {
		$db = new db($DetailTable);
		for($i=0; $i<$n; $i++) {
			$db->row[$fkID] = $my->row[$mkID];
			foreach($Dfields as $k=>$data) {
				$value = $_REQUEST['dt_'.$k][$i];
				switch($data[1]) {
					case "Define":
					case "uLABEL":
					case "date" :
					case "hidden" :
					case "select" :
					case "readonly" : $db->row[$k]="'$value'"; break;
					case "text" :
					case "textbox" :
					case "queryID" :
					case "textarea" :	$db->row[$k]="'".addslashes($value)."'"; break;
					case "checkbox" : $db->row[$k]=(isset($value)?1:0); break;
				}	
			}	
			$db->insert();
		}
	}
	
	PostHeader($_SERVER['HTTP_REFERER'],$_REQUEST['ListFmParams']);
?>