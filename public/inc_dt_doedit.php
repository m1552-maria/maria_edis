<?
	/* ---------------------------------------
		 支援 MASTER - DETAIL 運作
		 Create By Michael Rou from 2018/6/8
	------------------------------------------ */
	include("../thumb.php");
	include 'lib.php';
	
	//建立資料庫物件
  $db = new db($tableName);	  
	//::取的原本的檔名資訊
	$aa = array_keys($fieldE);
	$whereStr = "$aa[0]='".$_REQUEST[$aa[0]]."'";
	$sql = "select * from $tableName where $whereStr";
	$rs = $db->query($sql);
	$r = $db->fetch_array($rs);
	
	//::刪除檔案
	$delst = array();	//要移除的檔案
	if($_REQUEST['delfnList']) {
		$aa = explode(',',$_REQUEST['delfnList']);
		foreach($aa as $fn) {
			$bb = explode(':',$fn);
			$k = $bb[0];
			$fp = $root.$ulpath.$bb[1];
			if(file_exists($fp)) @unlink($fp);
			if(!isset($delst[$k])) $delst[$k]=array();
			array_push($delst[$k],$bb[1]);
		}	
		$ca = explode(',',$r[$k]);
		$fnlst[$k] = array_diff($ca, $delst[$k]);
		//var_dump($fnlst[$k]);
	}	
	
	//處理上傳的附件	
	foreach($_FILES as $k=>$v) {
		switch($fieldE[$k][1]) {
			case 'file': 
				if ($_FILES[$k][tmp_name] != "") {
					$fn = $_FILES[$k]['name'];
					$fnlst[$k] = $r[$k] ? $r[$k] : md5(pathinfo($fn,PATHINFO_FILENAME)).'.'.pathinfo($fn,PATHINFO_EXTENSION);
					$srcfn = $root.$ulpath.$fnlst[$k];
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'],$srcfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
				}
				break;
			case 'multifile':
				$newlst = array(); //新加入的檔案
				foreach($_FILES[$k]["name"] as $k2=>$v2){
					if(!$v2) continue; 
					$finf = pathinfo($v2); 
					$fn = md5($finf['filename']).'.'.$finf['extension'];
					$dstfn = $root.$ulpath.$fn;
					$rzt = move_uploaded_file($_FILES[$k]['tmp_name'][$k2],$dstfn);
					if(!$rzt) { echo '附件上傳作業失敗！'; exit;	}	
					if(!isset($newlst[$k])) $newlst[$k]=array();
					array_push($newlst[$k],$fn);
				}
				//重新建立新的檔案清單
				if(!isset($fnlst[$k])) $fnlst[$k]=explode(',',$r[$k]);
				if(isset($newlst[$k])) $fnlst[$k] = array_merge($newlst[$k],$fnlst[$k]);
				//var_dump($fnlst[$k]);
				break;	
		}
	}
	
	foreach($fieldE as $k=>$data) {
		switch($data[1]) {
			case "Define":
			case "uLABEL":
			case "date" :
			case "hidden" :
			case "select" :
			case "readonly" : $db->row[$k]="'$_REQUEST[$k]'"; break;
  			case "text" :
			case "textbox" :
			case "queryID" :
  			case "textarea" :	$db->row[$k]="'".addslashes($_REQUEST[$k])."'"; break;
			case "queryIDs" :	$db->row[$k]="'".addslashes( join(',',$_REQUEST[$k]) )."'"; break;
  			case "checkbox" : $db->row[$k]=(isset($_REQUEST[$k])? 1:0); break;
			case "cklist" : if( $_REQUEST[$k] ) { $sum=0; foreach($_REQUEST[$k] as $v) $sum += $v; $db->row[$k]=$sum; }; break;	
			case "multifile":	if( isset($fnlst[$k]) ) $db->row[$k]="'".implode(',',$fnlst[$k])."'"; break;		
			case "file" : if( isset($fnlst[$k]) ) $db->row[$k]="'".$fnlst[$k]."'"; break;	
		}	
	}	
	$db->update($whereStr);	
	
	//建立明細檔資料庫物件
	$id = $db->row[$mkID];
	$n = count($_REQUEST['dt_'.key($Dfields)]);
	if($n>0) {
		$db = new db($DetailTable);
		$db->delete("$fkID=$id"); //刪除明細檔明細
		for($i=0; $i<$n; $i++) {
			$db->row[$fkID] = $id;
			foreach($Dfields as $k=>$data) {
				$value = $_REQUEST['dt_'.$k][$i];
				switch($data[1]) {
					case "Define":
					case "uLABEL":
					case "date" :
					case "hidden" :
					case "select" :
					case "readonly" : $db->row[$k]="'$value'"; break;
					case "text" :
					case "textbox" :
					case "queryID" :
					case "textarea" :	$db->row[$k]="'".addslashes($value)."'"; break;
					case "checkbox" : $db->row[$k]=(isset($value)?1:0); break;
				}	
			}	
			$db->insert();
		}
	}

	//::儲存異動紀錄
	if($logFlag) {
		$sql = sprintf("insert into eventlog(tableName,tableID,creator,operation) values('%s','%s','%s','%s')",$tableName,$_REQUEST[$aa[0]],$_SESSION["empID"],'edit');
		$db->query($sql);	
	}
	
	PostHeader($_SERVER['HTTP_REFERER'],$_REQUEST['ListFmParams']);
?>
