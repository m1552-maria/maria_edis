<script>
var page = <?=$pinf->curPage?>;
var recno = <?=$pinf->curRecord+1?>;
/* Override Object Event
$(function () {
	$('#PageControl .firstBtn').unbind('click');
	$('#PageControl .firstBtn').bind('click',function(){alert('Override PageControl.firstBtn');});
}); */

function nextBtnClick(evt){
	page++;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function prevBtnClick(evt){
	page--;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function firstBtnClick(evt){
	page=1;	
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function lastBtnClick(evt){
	page=<?=$pinf->pages?>; 
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function goBtnClick(evt) {
	var n=$('#PageControl .numEdit').val();
	if(n<1) page=1;
	else if(n>=<?=$pinf->pages?>) page=<?=$pinf->pages?>;
	else page=n;
	ListControlForm.page.value = page;
	ListControlForm.submit();
}
function gorec(rno) { //go edit ViewControl
  ListControlForm.recno.value = Math.abs(rno);
  if(window.event.target.tagName=='A' && rno>=0) {
    ListControlForm.act.value = 'edit';
  } else if(window.event.target.tagName=='A' && rno<0) return; 
  if(window.event.target.tagName!='A' && recno==rno) return;
  ListControlForm.submit();
}
function viewrec(recno) { //go Only view ViewControl
	ListControlForm.recno.value = recno;
	if(window.event.target.tagName=='A') ListControlForm.act.value = 'view';
	ListControlForm.submit();
}

function SearchKeyword() {
	rzlt = prompt("請輸入關鍵字",'<?=$_REQUEST["keyword"]?>');
	if (rzlt!=null) {
		ListControlForm.keyword.value = rzlt;
		ListControlForm.submit();		
	}	 
}

function doEdit() {
	ListControlForm.recno.value = recno;	//:by recno
	ListControlForm.act.value = 'edit';
	ListControlForm.submit();	
}
function doAppend() {
	page=<?=$pinf->pages?>;
	ListControlForm.page.value = page;
	ListControlForm.act.value = 'new';
	ListControlForm.submit();
}
function doDel() {
	delMsgBox();
	if(delCheckItem()) {
		ListControlForm.act.value = 'del';
		ListControlForm.page.value = page;
		ListControlForm.submit();
	}
}

function form1Valid(evt) {
	if( typeof form1PreCheck !== 'undefined' ) if(!form1PreCheck(evt)) return false;	//Submit前置檢查
	var pAry = [];
	if(typeof ListControlForm !== "undefined") {
		<? if($act=='edit') { ?>
		ListControlForm.recno.value = recno;
		<? } else { ?>
		ListControlForm.page.value = page;
		<? } ?>
		$(ListControlForm).find('input[type="hidden"]').each(function(){
			if(this.value) pAry.push(this.name+'='+this.value);
		});
	}
	<? if(!$listObj) foreach($_REQUEST as $k=>$v) {
			if($k=='act' || $k=='sqlSelect' || $k=='sqlTable' || $k=='sqlWhere') continue;
			if($v) echo "pAry.push('$k=$v');"; 
		 }
	?>
	if(pAry.length>0) {
		var aForm = evt.currentTarget;
		if(!aForm.ListFmParams) {
			var inp = document.createElement('input');
			inp.type = 'hidden';
			inp.name = 'ListFmParams';
			aForm.appendChild(inp);
		}
		//要帶入 doApend or doEdit
		aForm.ListFmParams.value = pAry.join('&');
	}
	return true;
}

function filter(key,cname,filterName) {	//IE 不支援ES6預設參數
	var filterName = (typeof filterName!=='undefined'?filterName:'depID');
	while(ListControlForm[filterName].options[0]) $(ListControlForm[filterName].options[0]).remove();
	var op = document.createElement('option');
	op.value = key;
	op.text = cname;
	ListControlForm[filterName].add(op);
	ListControlForm.cname.value = cname;
	ListControlForm.submit();		
}
</script>