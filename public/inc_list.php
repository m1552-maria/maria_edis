<?
	/*
		===== For 1 List Table Basic use =====
		Handle parameter and user operation.
		Create By Michael Rou from 2017/9/21
	*/
	if(isset($_REQUEST['act'])) $act=$_REQUEST['act'];
	if($act=='del') { //:: Delete record -----------------------------------------
		$aa = array();
		foreach($_POST['ID'] as $v) $aa[] = "'$v'";
		$lstss = join(',',$aa);
		$aa =array_keys($fieldsList);		
		$db = new db();	
		//檢查是否要順便刪除檔案
		if(isset($delFlag)) { 
			$sql = "select * from $tableName where $aa[0] in ($lstss)";
			$rs = $db->query($sql);
			while($r=$db->fetch_array($rs)) {
				$fns = $r[$delField];
				if($fns) {
					$ba = explode(',',$fns);
					foreach($ba as $fn) {
						$fn = $root.$ulpath.$fn;
						//echo $fn;
						if(file_exists($fn)) @unlink($fn);
					}
				}
			}
		}
		$sql = "delete from $tableName where $aa[0] in ($lstss)";
		//echo $sql; exit;
		$db->query($sql);
		
		//::儲存異動紀錄
		if($logFlag) {
			$sql = sprintf("insert into eventlog(tableName,tableID,creator,operation) values('%s',%s,'%s','%s')",$tableName,$lstss,$_SESSION["empID"],'delete');
			$db->query($sql);	
		}
	}
	
	//::處理排序 ------------------------------------------------------------------------
	if (isset($_REQUEST["curOrderField"])) $opList['curOrderField']= $_REQUEST["curOrderField"];
	if (isset($_REQUEST["sortMark"])) $opList['sortMark'] = $_REQUEST["sortMark"];
	if (isset($_REQUEST["keyword"])) $opList['keyword'] = $_REQUEST["keyword"]; 
	
	//::處理跳頁 -------------------------------------------------------------------------
	$page = $_REQUEST['page']?$_REQUEST['page']:1; 
	$recno = $_REQUEST['recno']?$_REQUEST['recno']:1;

	//:Defined public PHP Function -------------------------------------------------------------

?>	
<title><?=$CompanyName.'-'.$pageTitle?></title>
<link href="/system/PageControl.css" rel="stylesheet">
<link href="/system/ListControl.css" rel="stylesheet">
<link href="/system/ViewControl.css" rel="stylesheet">
<link href="/system/TreeControl.css" rel="stylesheet">

<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/jquery.treeview.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">

<link href="/Scripts/daterangepicker/daterangepicker.css" rel="stylesheet"></link>
<link href="/Scripts/daterangepicker/MonthPicker.css" rel="stylesheet"/>

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script> 
<script src="/Scripts/form.js"></script>

<script src="/Scripts/daterangepicker/moment.min.js"></script>
<script src="/Scripts/daterangepicker/daterangepicker.min.js"></script>
<script src="/Scripts/daterangepicker/MonthPicker.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<?
	$whereStr = '';	//initial where string
	//:Filter handle  ---------------------------------------------------
	$whereAry = array();
	$wherefStr = '';
	if($opList['filters']) {
		foreach($opList['filters'] as $k=>$v) {
			$wType = $opList['filterType'][$k];
			switch($wType) {
				case 'Month':	
					if($_REQUEST[$k]) $curMh = $_REQUEST[$k];
					elseif($opList['filterOption'][$k]) $curMh=$opList['filterOption'][$k][0];
					$Days  = date('t',strtotime("$curMh/01"));
					$bDate = date($curMh.'/01');
					$eDate = date($curMh.'/'.$Days);	
					if($curMh) $whereAry[]="($k between '$bDate' and '$eDate 23:59:59')";
					break;
				case 'DateRange':
					$aa = array();
					if($_REQUEST[$k]) {
						$aa = explode('~',$_REQUEST[$k]);
					} elseif($opList['filterOption'][$k]) {
						$aa = explode('~',$opList['filterOption'][$k][0]);
					}
					if(count($aa)) $whereAry[] = "($k between '$aa[0]' AND '$aa[1] 23:59:59')";
					break;
				default:		
					$oprator = $opList['filterCondi'][$k]?$opList['filterCondi'][$k]:"%s = '%s'"; 
					if($_REQUEST[$k] || ($_REQUEST[$k]=='0' && $k!='depID')) $whereAry[]=sprintf($oprator,$k,$_REQUEST[$k]);
					else if($opList['filterOption'][$k]) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);
			}
		}
		if(count($whereAry)) $wherefStr = join(' and ',$whereAry);
	}
	
	//:Search filter handle  ---------------------------------------------
	$whereAry = array();
	$wherekStr = '';
	if($opList['keyword']) {
		$key = $opList['keyword'];
		foreach($opList['searchField'] as $v) $whereAry[] = "$v like '%$key%'";
		$n = count($whereAry);
		if($n>1) $wherekStr = '('.join(' or ',$whereAry).')';
		else if($n==1) $wherekStr = join(' or ',$whereAry);
	}
	
	//:Merge where
	if($wherefStr || $wherekStr) {
		$whereStr = 'where ';
		$flag = false;
		if($wherefStr) { $whereStr .= $wherefStr; $flag = true; }
		if($wherekStr) {
			if($flag) $whereStr .= ' and '.$wherekStr; else $whereStr .= $wherekStr;
		}
	}

	$db = new db();
	$sql = sprintf('select 1 from %s %s',$tableName,$whereStr);
	$rs = $db->query($sql);
	$rCount = $db->num_rows($rs);
	//:PageInfo -----------------------------------------------------------------------
	if(!$pageSize) $pageSize=10;
	$pinf = new PageInfo($rCount,$pageSize);
	if($recno>1) $pinf->setRecord($recno);
	else if($page>1) { $pinf->setPage($page); $pinf->curRecord = ($pinf->curPage-1)*$pinf->pageSize; };
	
	$n = ($pinf->curPage-1<0)?0:$pinf->curPage-1;
	if(!$fieldsStr) $fieldsStr = '*';	//prevent for init NOT SET
	$sql = sprintf('select %s from %s %s order by %s %s limit %d,%d',
		$fieldsStr,$tableName,$whereStr,
		$opList['curOrderField'],$opList['sortMark'],
		$n*$pinf->pageSize, $pinf->pageSize);
	//echo $sql;
	$rs = $db->query($sql);
	$pdata = $db->fetch_all($rs);
	$listPos = $pinf->curRecord % $pinf->pageSize;
	
	//: Header --------------------------------------------------------------------------------------------------------------------------
	switch($act) {
		case 'edit': echo "<h1 align='center'>修改 $pageTitle"; break;
		case 'new': echo "<h1 align='center'>新增 $pageTitle"; break;
		default: echo "<h1 align='center'>$pageTitle";
	}
	if($opList['keyword']) echo " - 搜尋:".$opList['keyword'];
	echo "</h1>";
	
	//:PageControl ----------------------------------------------------------------------------------------------------------------------
	//if($act=='edit' || $act=='new') $opPage['hideControl']=true;
	$obj = new EditPageControl($opPage, $pinf,$pdata[$listPos],$btnContraint);	
	echo "<div style='float:right'>頁面：".$pinf->curPage."/".$pinf->pages." 筆數：".$pinf->records." 記錄：".$pinf->curRecord."</div>";	
	if($act!='edit' && $act!='new') echo "<hr>";
	
	//:ListControl Object ---------------------------------------------------------------------------------------------------------------
	//if($act=='edit' || $act=='new') $opList['hideControl']=true;
	$obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
	
	//:ViewControl Object ------------------------------------------------------------------------
	switch($act) {
		case 'edit':	//修改
			$op3 = array(
				"type"=>"edit",
				"form"=>array('form1',"$tableName/doedit.php",'post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定修改",false,''),
				"cancilBtn"=>array("取消修改",false,'')	);
			$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);	
			break;
		case 'new':	//新增
			$op3 = array(
				"type"=>"append",
				"form"=>array('form1',"$tableName/doappend.php",'post','multipart/form-data','form1Valid'),
				"submitBtn"=>array("確定新增",false,''),
				"cancilBtn"=>array("取消新增",false,'')	);
			$ctx = new BaseViewControl($fieldA_Data, $fieldA,$op3);
			break;
	} 
?>