// JavaScript Document
var cancelFG;

$(function () {
	$('#ListControl .sortable').bind('click',function(){
		if(event.target.tagName=='TH'){
			var fName = event.target.dataset.id;
			if(ListControlForm.curOrderField.value==fName) ListControlForm.sortMark.value = (ListControlForm.sortMark.value=='ASC'?'DESC':'ASC')
			ListControlForm.curOrderField.value = fName;
			ListControlForm.submit();
		}
	});
	
	$('#ListControl .goBtn').bind('click',function(){
		this.form.submit();
	});
	
	$('#ListControl .etraBtn').bind('click',function(evt){
		func = window[evt.target.dataset.handler];
		if(isFunc(func)) func(evt);
	});
	
	$('.ListRow').click(function(e) {
		if(e.target.tagName=='TD') $(this).find('a[href="#"]').click();
	});
	
	$('.GroupRow').click(function(e) {
		var dom = $(this).find('i')[0];
		var icon = dom.className; console.log(icon);
		if(icon=='icon-folder-close') {
			elmName  = '.ListRowHide';
			elmClass = 'ListRow';
			dom.className = 'icon-folder-open';
		} else {
			elmName  = '.ListRow';
			elmClass = 'ListRowHide';
			dom.className = 'icon-folder-close';
		}
		var elm = $(this).next(elmName)[0];
		while(elm) {
			//console.log(elm);
			elm.className = elmClass;
			elm = $(elm).next(elmName)[0];
		}
	});
	
	if($('.MonthInp')[0]) {
		var mtFormt = 'yy/mm';
		if ( $('.MonthInp').attr('placeholder')=='作業月份' ) mtFormt = 'yymm'; 
		$('.MonthInp').MonthPicker({ 
			Button: false,
			MonthFormat: mtFormt,
			MinMonth: $('.MonthInp')[0].dataset.minmonth,
			MaxMonth: $('.MonthInp')[0].dataset.maxmonth
		});
	}
	
	if($("input.dateRange")[0]) $("input.dateRange").daterangepicker({
		"alwaysShowCalendars": true,
		opens: "right",
		//startDate: "2017-08-01",
		//endDate: "2017-09-01",
		ranges: {
			"今天": [moment(), moment()],
			"過去 7 天": [moment().subtract(6,"days"), moment()],
			"本月": [moment().startOf("month"), moment().endOf("month")],
			"上個月": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
		},
		locale: {
			format: "YYYY-MM-DD",
			separator: " ~ ",
			applyLabel: "確定",
			cancelLabel: "清除",
			fromLabel: "開始日期",
			toLabel: "結束日期",
			customRangeLabel: "自訂日期區間",
			daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
			monthNames: ["1月", "2月", "3月", "4月", "5月", "6月",	"7月", "8月", "9月", "10月", "11月", "12月"],
			firstDay: 1
		}
	});
	
	if($('td.columnCurrent').length>0 && $('#ListControl').attr('canScroll')) { //自動捲動
		window.setTimeout( scrolltoVisiable, 500 );
	}
});
function scrolltoVisiable() {
	var y1 = $('td.columnCurrent').offset().top; 
	var y2 = $('th.columnHead').offset().top;
	var off = $('#ListControl').attr('canScroll');
	window.scrollTo(0,y1-y2-off);
}

function delMsgBox() {	if(confirm("是否確定刪除 ?")) cancelFG = true;	 else	cancelFG = false;  }
function delCheckItem() {
	var obj1 = document.getElementsByName('ID[]');
	if(cancelFG) {
		if (obj1!=null) {
			if (obj1.length==undefined) { //only one item
				if (obj1.checked)	return true; else	{ alert('請選擇項目！'); return false; }
			} else {	// multiple items
				var selfg=false;	
				for(i=0; i<obj1.length; i++) {
					if(obj1[i].checked)	{
						selfg=true;
						break;
					}
				}
				if(!selfg) alert('請選擇項目！');
				return selfg;	
			}	
		} else {	alert("沒有資料項目！"); return false; }	
	} else return false;
}

