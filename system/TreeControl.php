<?
	class BaseTreeControl {	
		protected $mID				= 'id';
		protected $mTitle			= 'title';
		protected $mTableName	= '';
		protected $mWhere			= '1';	//where conditions
		protected $mDB				= null;
		protected $mURLFmt		= 'list.php?filter=%s&cnm=%s';
		/* 省略
		 * RootClass
		 * RootTitle
		 * Modal					//true: 崁入式, false:對話框
		*/
		protected $mChildTbl = '';	//子檔
		protected $mFKey='';				//Child Forient Key
		protected $mcWhere='1';			//where conditions
		protected $mcID='';					//child ID
		protected $mcTitle='';			//child Title
		protected $mcIcon='people';	//child icon class
		
		private $nTreeRecursiveLimit=0;
		
		function __construct($option=array())	{
			if(!empty($option)) {
				if($option['ID']) $this->mID = $option['ID'];
				if($option['Title']) $this->mTitle = $option['Title'];
				if($option['TableName']) $this->mTableName = $option['TableName'];
				if($option['DB']) $this->mDB = $option['DB'];	
				if($option['URLFmt']) $this->mURLFmt = $option['URLFmt'];
				
				if($option['ChildTbl']) $this->mChildTbl = $option['ChildTbl'];
				if($option['FKey']) $this->mFKey = $option['FKey'];
				if($option['Where']) $this->mWhere = $option['Where'];
				if($option['cWhere']) $this->mcWhere = $option['cWhere'];
				if($option['cID']) $this->mcID = $option['cID'];
				if($option['cTitle']) $this->mcTitle = $option['cTitle'];
				if($option['cIcon']) $this->mcIcon = $option['cIcon'];
			}			
			
			$RootClass = $option['RootClass']?$option['RootClass']:0;
			$RootTitle = $option['RootTitle']?$option['RootTitle']:'根目錄';
					 
			$tree = $this->getClassTree($RootClass);
			
			$this->nTreeRecursiveLimit = 0;
			if($option['Modal']) echo '<div id="TreeControl" class="modal">'; else echo '<div id="TreeControl">';
			//gen Root node
			$links = sprintf($this->mURLFmt, $RootClass, $RootTitle);
			echo "<span class='root'><a href='$links'>".$RootTitle."</a></span>".$sChild."\n";
			echo '<ul id="browser" class="filetree">';
			echo $this->showClassTree($tree);
			echo '</ul></div>';
		}
		
		function __destruct() {
			//reserved
    }
		
		public function getClassTree($nID = 0) {
			$this->nTreeRecursiveLimit++;
			if ($this->nTreeRecursiveLimit > 100) return false;	
			$db = $this->mDB;
			$rs = $db->query("select $this->mID as ClassID,$this->mTitle as Title from $this->mTableName where $this->mWhere and PID=?",array($nID));
			if($db->eof($rs)) {
				$this->nTreeRecursiveLimit--;
				return false;
			}
			$aTree = array();
			while($r=$db->fetch_array($rs)) {
				$aChild = $this->getClassTree($r["ClassID"]);
				if ($aChild) $aTree[$r["ClassID"]] = array("name"=>$r["Title"], "child"=>$aChild);
				else $aTree[$r["ClassID"]] = array("name"=>$r["Title"]);
				
				if($this->mChildTbl) { //read child table's data
					$rs2 = $db->query("select $this->mcID, $this->mcTitle from $this->mChildTbl where $this->mcWhere and $this->mFKey=?",array($r["ClassID"]));
					while($r2=$db->fetch_array($rs2)) {
						$aTree[$r["ClassID"]]['foreignTbl'][$r2[0]]=$r2[1];
					}
				}
			}
			$this->nTreeRecursiveLimit--;
			return $aTree;
		}
		
		public function showClassTree($tree) {
			$this->nTreeRecursiveLimit++;
			if ($this->nTreeRecursiveLimit > 100) return false;	
			if (!$tree) return "";		
			$sHTML = "";
			foreach($tree as $k=>$v) {
				if (is_array($v["child"])) {
					$sChild = "<ul>";
					foreach ($v["foreignTbl"] as $k2=>$v2) {
						$links = sprintf($this->mURLFmt, $k2, $v2);
						$sChild .="<li><span class='$this->mcIcon'><a href='$links' title='$k2'>".$v2."</a></span></li>\n";
					}						
					$sChild .= $this->showClassTree($v["child"]);
					$sChild .= "</ul>";		
				} else {
					if($v["foreignTbl"]) {
						$sChild = "<ul>";
						foreach ($v["foreignTbl"] as $k2=>$v2) {
							$links = sprintf($this->mURLFmt, $k2, $v2);
							$sChild .="<li><span class='$this->mcIcon'><a href='$links' title='$k2'>".$v2."</a></span></li>\n";
						}
						$sChild .= "</ul>";
					} else $sChild = "";
				}
								
				if($this->mChildTbl) {
					$sHTML .= "<li><span class='folder'>".$v['name']."</span>".$sChild."</li>\n";
				} else {
					$links = sprintf($this->mURLFmt, $k, $v["name"]);
					$sHTML .= "<li><span class='folder'><a href='$links'>".$v["name"]."</a></span>".$sChild."</li>\n";
				}
			}
			$this->nTreeRecursiveLimit--;
			return $sHTML;
		}
	}
	
	/* -------------------------------------------------------------------------------------------------------------------- */
	
	class CboxTreeControl extends BaseTreeControl {
		/*
		 * for Miltiple selection USE.
		 */
		function __construct($option=array())	{
			parent::__construct($option);			
		}
		
		//OverRide
		public function showClassTree($tree) {
			$this->nTreeRecursiveLimit++;
			if ($this->nTreeRecursiveLimit > 100) return false;	
			if (!$tree) return "";		
			$sHTML = "";
			foreach($tree as $k=>$v) {
				if (is_array($v["child"])) {
					$sChild = "<ul>";
					foreach ($v["foreignTbl"] as $k2=>$v2) $sChild .="<li><span class='$this->mcIcon'><input type='checkbox' name='".$this->mcID."[]' value='$k2' title='$v2'/>".$v2."</span></li>\n";						
					$sChild .= $this->showClassTree($v["child"]);
					$sChild .= "</ul>";		
				}	else {
					if($v["foreignTbl"]) {
						$sChild = "<ul>";
						foreach ($v["foreignTbl"] as $k2=>$v2) $sChild .="<li><span class='$this->mcIcon'><input type='checkbox' name='".$this->mcID."[]' value='$k2' title='$v2'/>".$v2."</span></li>\n";
						$sChild .= "</ul>";
					} else $sChild = "";
				}
				$sHTML .= "<li><span class='folder'><input type='checkbox' name='".$this->mID."[]' value='$k' title='$v[name]' />".$v["name"]."</span>".$sChild."</li>\n";				
			}
			$this->nTreeRecursiveLimit--;
			return $sHTML;
		}
	}
?>