<?
	class BasePageControl {
		// parameter: 
		// Key => [名稱,diabled,visible,自定事件]
		protected $defaultOption = array(
			"hideControl"=>false,			//隱藏控件
			"permitValue"=>0,					//編輯權限   ----DENV  D:Del, E:Edit, N:New, V:View
			"firstBtn"=>array("第一頁",FALSE,TRUE,''),
			"prevBtn"=>array("前一頁",FALSE,TRUE,''),
			"nextBtn"=>array("下一頁",FALSE,TRUE,''),
			"lastBtn"=>array("最末頁",FALSE,TRUE,'')
		);
		//UI
		public $firstBtn = "<button type='button' class='firstBtn'";
		public $lastBtn = "<button type='button' class='prevBtn'";
		public $nextBtn = "<button type='button' class='nextBtn'";
		public $prevBtn = "<button type='button' class='lastBtn'";
		
		//Can Binding pageInfo Object	
		function __construct($option=array(),$pgInfo=null)	{
			if(!empty($option)) {
				if($option['firstBtn']) $this->defaultOption['firstBtn'] = $option['firstBtn'];
				if($option['prevBtn']) $this->defaultOption['prevBtn'] = $option['prevBtn'];
				if($option['nextBtn']) $this->defaultOption['nextBtn'] = $option['nextBtn'];
				if($option['lastBtn']) $this->defaultOption['lastBtn'] = $option['lastBtn']; 
				if(isset($option['hideControl'])) $this->defaultOption['hideControl'] = $option['hideControl'];
				if(isset($option['permitValue'])) $this->defaultOption['permitValue'] = $option['permitValue'];
			}
			
			if($this->defaultOption['hideControl']) echo "<div id='PageControl' style='display:none'>"; else echo "<div id='PageControl'>";
			if($pgInfo) {
				if($pgInfo->pages<2) {
					$this->defaultOption['firstBtn'][1]=true;
					$this->defaultOption['prevBtn'][1]=true;
					$this->defaultOption['nextBtn'][1]=true;
					$this->defaultOption['lastBtn'][1]=true;
				} else {
					if($pgInfo->isFirst) {
						$this->defaultOption['firstBtn'][1]=true;
						$this->defaultOption['prevBtn'][1]=true;
					}
					if($pgInfo->isLast) {
						$this->defaultOption['nextBtn'][1]=true;
						$this->defaultOption['lastBtn'][1]=true;
					}
				} 
				
			}
			
			$this->genButton($this->firstBtn,$this->defaultOption['firstBtn']);
			$this->genButton($this->prevBtn,$this->defaultOption['prevBtn']);
			$this->genButton($this->nextBtn,$this->defaultOption['nextBtn']);
			$this->genButton($this->lastBtn,$this->defaultOption['lastBtn']);	
			
			if(get_class($this)=='BasePageControl') echo "</div>";
		}
		
		function __destruct() {
			//reserved
    }
		
		protected function genButton($btn,$setting) {
			echo $btn;
			if($setting[1]) echo " disabled";
			if(!$setting[2]) echo " style='display:none'";
			if($setting[3]) echo " data-handler='".$setting[3]."'";
			echo ">".$setting[0]."</button>";
		}
	}

	/* -------------------------------------------------------------------------------------------------------------------- */
	
	class NumPageControl extends BasePageControl {
		//default value
		private $_goBtn = array("確定",false,true,'');
		private $_numEdit = array(false,true);
		//UI
		public $numEdit = "<input type='text' class='numEdit' placeholder='頁次'";
		public $goBtn = "<button type='button' class='goBtn'";
				
		function __construct($option,$pgInfo=null)	{
			parent::__construct($option,$pgInfo);
			
			if($option['numEdit']) $this->defaultOption['numEdit'] = $option['numEdit']; else $this->defaultOption['numEdit'] = $this->_numEdit;
			if($option['goBtn']) $this->defaultOption['goBtn'] = $option['goBtn']; else $this->defaultOption['goBtn'] = $this->_goBtn;				
			
			$this->genEdit($this->numEdit,$this->defaultOption['numEdit']);			
			$this->genButton($this->goBtn,$this->defaultOption['goBtn']);
			
			if(get_class($this)=='NumPageControl') echo "</div>";
		}
			
		protected function genEdit($edit,$setting) {
			echo $edit;
			if($setting[0]) echo " disabled";
			if(!$setting[1]) echo " style='display:none'";
			echo ">";
		}	
	}
	
	/* -------------------------------------------------------------------------------------------------------------------- */
	
	class EditPageControl extends NumPageControl {
		//default value
		private $_searchBtn = array("◎搜尋",false,true,'');
		private $_newBtn = array("＋新增",false,true,'');
		private $_editBtn = array("－修改",false,true,'');
		private $_delBtn = array("Ｘ刪除",false,true,'');
				
		//UI
		public $searchBtn = "<button type='button' class='searchBtn'";
		public $newBtn = "<button type='button' class='newBtn'";
		public $editBtn = "<button type='button' class='editBtn'";
		public $delBtn = "<button type='button' class='delBtn'";
		public $etraBtn = "<button type='button' class='etraBtn'";
			
		function __construct($option,$pgInfo=null,$data=array(),$contraint=null)	{
			parent::__construct($option,$pgInfo);
			
			if($option['searchBtn']) $this->defaultOption['searchBtn'] = $option['searchBtn']; else $this->defaultOption['searchBtn'] = $this->_searchBtn;
			if($option['newBtn']) $this->defaultOption['newBtn'] = $option['newBtn']; else $this->defaultOption['newBtn'] = $this->_newBtn;
			if($option['editBtn']) $this->defaultOption['editBtn'] = $option['editBtn']; else $this->defaultOption['editBtn'] = $this->_editBtn;
			if($option['delBtn']) $this->defaultOption['delBtn'] = $option['delBtn']; else $this->defaultOption['delBtn'] = $this->_delBtn;
			if($option['extraButton']) $this->defaultOption['extraButton'] = $option['extraButton'];
			
			//::check pageInfo's records，沒有record則隱藏[修改]及[刪除]按鈕
			if(!$pgInfo->records) {
				$this->defaultOption['editBtn'][1] = true; $this->defaultOption['editBtn'][2] = false;
				$this->defaultOption['delBtn'][1] = true; $this->defaultOption['delBtn'][2] = false;
			}
			//::check 欄位資料限制
			if($contraint) {
				foreach($contraint as $k=>$str) {	//contraint可以多組 ex. isLock == 1 && id == 'aaa'
					$aa = explode(' ',$str);
					$n = intval( count($aa) / 4 );
					$evalStr = '';
					for($i=0; $i<=$n; $i++) {
						$idx = $i*4;
						$evalStr .= $data[$aa[$idx]].$aa[$idx+1].$aa[$idx+2];
						if($i<$n) $evalStr .= ' '. $aa[$idx+3].' ';
					}
					if($evalStr) {
						$evalStr = '$bFlag=('.$evalStr.');';
						//var_dump($evalStr);
						eval($evalStr);
						//var_dump($bFlag);
						$this->defaultOption[$k][1] = $bFlag;
					} 
				}
			}
			
			//::check 帳號權限
			if($this->defaultOption['permitValue']) {
				$permitValue = $this->defaultOption['permitValue'];
				if( !($permitValue&2) ) {	$this->defaultOption['newBtn'][1]=true; $this->defaultOption['newBtn'][2]=false; }
				if( !($permitValue&4) ) {	$this->defaultOption['editBtn'][1]=true; $this->defaultOption['editBtn'][2]=false; }
				if( !($permitValue&8) ) {	$this->defaultOption['delBtn'][1]=true; $this->defaultOption['delBtn'][2]=false; }
			}
			$this->genButton($this->searchBtn,$this->defaultOption['searchBtn']);		
			$this->genButton($this->newBtn,$this->defaultOption['newBtn']);
			$this->genButton($this->editBtn,$this->defaultOption['editBtn']);
			$this->genButton($this->delBtn,$this->defaultOption['delBtn']);
			
			if($this->defaultOption['extraButton']) {
				foreach ($this->defaultOption['extraButton'] as $item) {
					$this->genButton($this->etraBtn, $item);				
				}
			}
			
			if(get_class($this)=='EditPageControl') echo "</div>";
		}
		
	}
	
?>
