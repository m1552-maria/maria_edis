<?
	class PageInfo {
		public $pages = 0;
		public $curPage = 0;
		public $pageSize = 10;
		public $records = 0;
		public $curRecord = 0;
		public $pageBegin = 0;
		
		public $isFirst = false;
		public $isLast = false;
		
		function __construct($data,$size=10)	{
			$this->pageSize = $size;
			$type = gettype($data);
			switch ($type) {
				case 'integer':	$this->records = $data;	break;
				case 'array':	$this->records = count($data); break;
				case 'object': $this->records = count(get_object_vars($data)); break;
				default: break;
			}
			if($this->records>0) {
				$this->curPage =1;
				$this->isFirst = true;
			}
			$this->pages = intval(ceil($this->records/$this->pageSize));
		}
		
		public function setPage($no) {			
			if($no<=1) {
				$no=1;
				$this->isFirst = true;				
			} else if($no>=$this->pages) {
				$no=$this->pages;
				$this->isFirst = false;
				$this->isLast = true;
			} else {
				$this->isFirst = false;
				$this->isLast = false;
			}
			$this->curPage = $no;
			$this->pageBegin = ($no-1)*$this->pageSize;
		}
		
		public function setRecord($no) {
			if($no<1) {
				$no=1;
			} else if($no>=$this->records) {
				$no=$this->records;
			} 
			$this->curRecord = ($no-1); 
			$pg = intval(ceil($no/$this->pageSize));
			$this->setPage($pg);
		}
			
	}
	
?>