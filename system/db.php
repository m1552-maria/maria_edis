<?
	/*::PDO Database Class
		Created By Mic.Rou since 2017/9/14
	*/
	
	class db {
		private $db;
		public $result;						//recordset resouce
  		public $table;						//資料表
		public $row = array();				// fields in this table field=value
		public $lastInsertId;				// the ID of the last inserted row 
		
		//*** 建構子 */
		function __construct($option=array()) {
			$DB_SOURCE	= DB_SOURCE;
			$DB_ACCOUNT	= DB_ACCOUNT;
			$DB_PASSWORD= DB_PASSWORD;			
			if(is_array($option)) {	//define DSN
				if(!empty($option)) {
					if($option['dbSource']) $DB_SOURCE = $option['dbSource'];
					if($option['dbAccount']) $DB_ACCOUNT = $option['dbAccount'];
					if($option['dbPassword']) $DB_PASSWORD = $option['dbPassword'];
					if($option['tableName']) $this->table = $option['tableName'];
				}				
			}	else {	//define Table
				if($option) $this->table =	$option;	
			}							
			
			$this->db = new PDO($DB_SOURCE, $DB_ACCOUNT, $DB_PASSWORD);
			$this->db->exec("set names utf8");
		}
		
		//*** 解構子 */
		function __desteuct() {
			if($this->result)	$this->close();
		}
		
		/*** 新增ㄧ筆資料 : 使用前須完成 row 的內容 ***/
		public function insert() {
			if(count($this->row)==0) return false;
			$fa = array_keys($this->row);
			$va = array_values($this->row);
			$fs = join(',',$fa);
			$vs = join(',',$va);
			$sql =  sprintf("INSERT INTO %s (%s) VALUES (%s)",$this->table, $fs, $vs);
			// echo $sql; exit;
			$this->result = $this->query($sql);
			$this->lastInsertId = $this->db->lastInsertId();
			return $this->result;
		}
		
		public function update($condition) {
			if(count($this->row)==0) return false;
			$updateA = array();
			foreach($this->row as $f=>$v) $updateA[] = "$f=$v";
			$updateStr = join(',',$updateA);
			$sql = sprintf("UPDATE %s SET %s WHERE %s", $this->table, $updateStr, $condition);
		    // echo $sql; exit;
			$this->result = $this->query($sql);
			return $this->result;
		}

		
		public function delete($condition) {
			//if(count($this->row)==0) return false;
			$sql = sprintf("DELETE FROM %s WHERE %s", $this->table, $condition);
			//echo $sql; //exit;
			$this->result = $this->query($sql);
			return $this->result;
		}	
		

		public function query($sql) {
			$db = $this->db; 
			$paramCT = func_num_args();
			if($paramCT==1) {
				$rs = $db->prepare($sql);
				$rs->execute();
				//$rs->debugDumpParams();
				$this->lastInsertId = $this->db->lastInsertId();
				return $rs; 
			} else if($paramCT>1) {
				$paramAR = func_get_args();
				$rs = $db->prepare($sql);
				$rs->execute($paramAR[1]);
				//$rs->debugDumpParams();
				$this->lastInsertId = $this->db->lastInsertId();
				return $rs;
			} else return false;
		}
			
		public function fetch_array($rs) {
			return $rs->fetch();
		}
		
		public function fetch_all($rs) {
			return $rs->fetchAll();
		}
		
		public function num_rows($rs) {
			return $rs->rowCount();
		}
		
		public function eof($rs) {
			return $rs->rowCount()==0;
		}	
			
		public function close() {
			$this->db = null;
		}
		
	}
?>