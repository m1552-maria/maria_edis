// JavaScript Document
$(function () {
	$('#ViewControl form').bind('submit',function(evt){
		func = window[evt.target.dataset.handler];
		if(isFunc(func)) return func(evt);
	});	
	$('#ViewControl form').bind('keydown',function(e){
		code = e.keyCode ? e.keyCode : e.which; // in case of browser compatibility
    if(code == 13 && e.target.tagName=='INPUT') {
    	e.preventDefault();
    }
	});
	
	$('#ViewControl form').on('keydown','.queryID',function(e){
		if(e.which==13) {
			var id = e.target.id;
			if( id2check[id] ) checkInput(e,id2check[id],this); 
		}
	});
		
	var icons=[];
	icons['doc']='icon_doc.gif';
	icons['docx']='icon_doc.gif';
	icons['pdf']='icon_pdf.gif';
	icons['ppt']='icon_ppt.gif';
	icons['pptx']='icon_ppt.gif';
	icons['txt']='icon_txt.gif';
	icons['xls']='icon_xls.gif';
	icons['xlsx']='icon_xls.gif';
	icons['zip']='icon_zip.gif';
	icons['rar']='icon_rar.gif';
		
	$(".multifile").change(function(){
		var that = $(this);										//input-file
		var oTD = that.parents("td").eq(0);		//td-容器
		var fName=$(this.files)[0]['name'];		//單選，only 1
		var ary=fName.split(".");
		$(this.files).each(function(){
			var fReader = new FileReader();
			fReader.readAsDataURL(this);
			fReader.onloadend = function(event){
				var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);	//.photoItem
				oItem.after(oItem.clone(true)).show();
				var img = oItem.find("img").get(0);
				img.style.padding='1';		
				if(icons[ary[1]]){
					img.height=16;
					img.src='/images/'+icons[ary[1]];
				}else if(ary[1].toLowerCase()=="jpg" || ary[1].toLowerCase()=="png" || ary[1].toLowerCase()=="gif"){
					img.src=event.target.result;	
					img.height='80';
				}else{
					img.height=16;
					img.src='/images/icon_txt.gif';
				}
				var span=oItem.find("span").get(0);
				$(span).html(fName);
				oItem.get(0).input = that;	//紀錄是那一次的input				
				var oNewFile = that.clone(true);	//建立新的input,放在後面
				oNewFile.val('');
				that.hide().after(oNewFile);
			}
		});
	});
	
});

function delfile(fn,elm,field) {
	var aForm = $(elm).closest("form");
	if ( aForm[0].contains(document.getElementsByName('delfnList')[0]) ) {
		var inp = document.getElementsByName('delfnList')[0];
		if(inp.value) {
			inp.value += (','+field+':'+fn);
		} else {
			inp.value = field+':'+fn;
		}
	} else {
		var inp = document.createElement("input");
		inp.setAttribute('type', 'hidden');
		inp.name = 'delfnList';
		inp.value = field+':'+fn;
		aForm[0].appendChild(inp);
	}
	$(elm.parentElement.input).remove();
	$(elm.parentElement).remove();
}

/* -----------------
	 for Multi-query 
	 -----------------
*/
function addItem(tbtn,fld,qid) {
	var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
		+ "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
		+ " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	$(tbtn).after(newone);
}

function removeItem(tbtn) {
	var elm = tbtn.parentNode;
	$(elm).remove();
}
