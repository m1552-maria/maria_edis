<?php
$icons = array(
	'doc'=>'icon_doc.gif',
	'docx'=>'icon_doc.gif',
	'pdf'=>'icon_pdf.gif',
	'ppt'=>'icon_ppt.gif',
	'pptx'=>'icon_ppt.gif',
	'txt'=>'icon_txt.gif',
	'xls'=>'icon_xls.gif',
	'xlsx'=>'icon_xls.gif',
	'zip'=>'icon_zip.gif',
	'rar'=>'icon_rar.gif'
);

function str_replace_first($search, $replace, $subject) {
	$pos = strpos($subject, $search);
	if ($pos !== false) {
		return substr_replace($subject, $replace, $pos, strlen($search));
	}
	return $subject;
}

function isImage($fileExt) {
	$ext = strtolower($fileExt);
	return ($ext=="png" || $ext=="jpg" || $ext=="jpeg" || $ext=="gif");
}

function utf8_substr($StrInput,$strStart,$strLen) { //:: 對字串做URL Encode
	$StrInput = mb_substr($StrInput,$strStart,mb_strlen($StrInput));
	$iString = urlencode($StrInput);
	$lstrResult="";
	$istrLen = 0;
	$k = 0;
	do{
		$lstrChar = substr($iString, $k, 1);
		if($lstrChar == "%"){
			$ThisChr = hexdec(substr($iString, $k+1, 2));
			if($ThisChr >= 128){
				if($istrLen+3 < $strLen){
					$lstrResult .= urldecode(substr($iString, $k, 9));
					$k = $k + 9;
					$istrLen+=3;
				}else{
					$k = $k + 9;
					$istrLen+=3;
				}
			}else{
				$lstrResult .= urldecode(substr($iString, $k, 3));
				$k = $k + 3;
				$istrLen+=2;
			}
		}else{
			$lstrResult .= urldecode(substr($iString, $k, 1));
			$k = $k + 1;
			$istrLen++;
		}
 	} while ($k < strlen($iString) && $istrLen < $strLen); 
	return $lstrResult;
}

function delDir($dir) { //:: 遞迴目錄刪除
  $mydir = dir($dir);
  while(false !== ($file = $mydir->read())) {
    if((is_dir("$dir/$file")) && ($file != ".") && ($file != "..")){
      delDir("$dir/$file");
    } else {
      if(($file != ".") && ($file != "..")) {
        unlink("$dir/$file");
        //echo "unlink $dir/$file ok <br>";
      }
    }
  }
  $mydir->close();
  rmdir($dir);
  //echo "rmdir $dir ok <br>";
}

function utf8_2_big5($utf8_str) { //:: 逐字轉換utf8字串為big5
	$i=0;
	$len = strlen($utf8_str);
	$big5_str="";
	for ($i=0;$i<$len;$i++) {
		$sbit = ord(substr($utf8_str,$i,1));
		if ($sbit < 128) {
			$big5_str.=substr($utf8_str,$i,1);
		} else if($sbit > 191 && $sbit < 224) {
			$new_word=iconv("UTF-8","Big5",substr($utf8_str,$i,2));
			$big5_str.=($new_word=="")?"■":$new_word;
			$i++;
		} else if($sbit > 223 && $sbit < 240) {
			$new_word=iconv("UTF-8","Big5",substr($utf8_str,$i,3));
			$big5_str.=($new_word=="")?"■":$new_word;
			$i+=2;
		} else if($sbit > 239 && $sbit < 248) {
			$new_word=iconv("UTF-8","Big5",substr($utf8_str,$i,4));
			$big5_str.=($new_word=="")?"■":$new_word;
			$i+=3;
		}
	}
	return $big5_str;
} 

function m_scandir($dir) {	//:: 掃描目錄清單，加排除 . ..
	$dh  = opendir($dir);
	while (false !== ($filename = readdir($dh))) {
	  if ($filename=='.') continue;
		if ($filename=='..') continue; 
    $files[] = $filename;
	}
	return $files;
}

//檢查字串內碼方式
function checkStrCode($str,$code) {
	$ary = array('ASCII','UTF-8','BIG-5');
	return mb_detect_encoding($str,$ary)==strtoupper($code)?1:0;
}	

//取的字串內碼方式
function getStrCode($str) {
	$ary = array('ASCII','UTF-8','BIG-5');
	return mb_detect_encoding($str,$ary);
}	

//*** System Functions ***//
function showSessions() {
	echo "<p>[Session 變數]：<br>";
	foreach($_SESSION as $k=>$v) if(is_array($v)) {
		echo "$k = ";
		print_r($v);
		echo "<br>";
	} else echo "$k = $v <br>";
	echo "</p>";
}

function showRequest() {
	echo "<p>[Reques 變數]：<br>";
	foreach($_REQUEST as $k=>$v) echo "$k = $v <br>";
	echo "</p>";
}

function checkValidOn($str) {
 	$len = strlen($str);
	$sum = 0;
	for($i=0; $i<$len; $i++) {
		$c = ord(substr($str,$i,1));
		$sum += $c;
	}
	return md5($sum);
}

if($_REQUEST[showSessions]) {
	@session_start();
	header("Content-Type:text/html; charset=utf-8");
	showSessions();
}
?>