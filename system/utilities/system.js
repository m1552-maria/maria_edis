// JavaScript Document

function isIE () {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

function isFunc(funcName) {
  return typeof(funcName) === typeof(Function);
}

