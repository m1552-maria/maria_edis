// JavaScript Document
$(function () {
	$("#browser").treeview({persist:"location", collapsed: true, unique: true});
	
	$("#TreeControl input[type='checkbox']").on('change',function(e){
		var aInp = $(e.currentTarget); //ckbox
		var check = aInp.prop('checked'); 
		var aSpa = aInp.parent();			//span
		
		//取消或勾選 上層folder選取
		var dom = aSpa.parent();
		var limits = 10; 
		if(check) { //選取
			while(dom) {
				if(dom.prop('tagName')=='LI') {
					var flag1 = true;
					dom.siblings().each(function(idx,itm){
						var inp = $(this).children().children('input');
						if(inp && !inp.prop('checked')) {	flag1 = false; return false;	}
					});
				}
				//toDO::how to set Upper Chechbox
				dom = dom.parent();
				if(flag1) dom.siblings('span.folder').children().first().prop('checked',true);
				
				limits--;
				if(dom.prop('tagName')=='DIV') break;
				if(limits<0) break;				
			}
		} else { //取消
			while(dom) {
				if(dom.prop('tagName')=='LI' && dom.find('.folder')) dom.find('input').first().prop('checked',check);
				dom = dom.parent();
				limits--;
				if(dom.prop('tagName')=='DIV') break;
				if(limits<0) break;
			}
		}
		
		var aNxt = aSpa.next();
		if ( aNxt && aNxt.prop('tagName')=='UL') { //有下層
			var childs = aNxt.children();
			childs.each(function(idx,itm){
				var cInp = $(itm).find('input');
				cInp.prop('checked',check);
			});
		}
	});
});

function showTreeControl() {
	if($('#TreeControl.modal').css('display')=='none'){
		var x = event.pageX+10;
		var y = event.pageY+10;
		$('#TreeControl.modal').show();
		$('#TreeControl.modal').offset({top:y,left:x});
	} else {
		$('#TreeControl.modal').hide();
	}
}

function getSelections(fldName) {
	var rzt = new Array();
	$("#TreeControl input[name*='"+fldName+"[]']").each(function(){
		var obj = {};
		var id = $(this).val();
		var title = $(this).attr('title');
		obj['id']=id;
		obj['title']=title;
		obj['type']=fldName;
		if($(this).prop('checked')) rzt.push(obj);
	});	
	return rzt;
}
