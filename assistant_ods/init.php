<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once('../config.php');
	include_once "$root/system/db.php";
    include_once "$root/getEmplyeeInfo.php";	
    include_once "$root/inc_vars.php";
	include_once "$root/system/utilities/miclib.php";
	
	include_once "func.php";
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
    include "ListControl.php";
	include "$root/system/ViewControl.php";
	include "$root/system/TreeControl.php";
	
	$pageTitle = "跨單位管理公文設定";
	//$pageSize=3; 使用預設

	//取得所有主管職員並組成sql
	$tempDB = new db();
	$sql = "select distinct leadid from department where leadid is not null and leadid !='';";

	$rs = $tempDB->query($sql);
	$aa = array();
	$numResults = $tempDB->num_rows($rs);
	$counter = 0 ;   
	while ($r = $tempDB->fetch_array($rs)) {
	    if (++$counter == $numResults) {
	        $aa[] = " select "."'$r[0]'"." assistant,'".$emplyeeinfo[$r[0]]."' name FROM DUAL ";
	    } else {
	        $aa[] = " select "."'$r[0]'"." assistant,'".$emplyeeinfo[$r[0]]."' name FROM DUAL UNION ALL";
	    }
	}

	if (count($aa) > 0) {
	    $AllLeadList = join(' ', $aa);
	}
	if (empty($AllLeadList)) {
	    $AllLeadList = 'null';
	}
	$tempDB->close();
	//所有主管
	$getAllLead = $AllLeadList;
	//所有總文書
	$getMainRevMan = getMainRevManSql();
	// $tableName = "(select distinct assistant,name from (".$getAllLead." UNION ALL ".$getMainRevMan.") union_as) base";
	$tableName = "emplyee";
	// for Upload files and Delete Record use
	//$ulpath = "/data/organization/"; if(!file_exists($root.$ulpath)) mkdir($root.$ulpath);
	$delField = 'attach';
	$delFlag = true;
	//:PageControl ------------------------------------------------------------------
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,false,doAppend),
		"editBtn" => array("－修改",false,false,doEdit),
		"delBtn" => array("Ｘ刪除",false,false,doDel)
	);
	
	//:ListControl  ---------------------------------------------------------------
	if($_REQUEST['depID']) $opAR=array($_REQUEST['depID']=>$_REQUEST['cname']); else $opAR=array(''=>'全部');
	$opList = array(
		"alterColor"=>true,			//交換色
		// "curOrderField"=>'assistant,name',	//主排序的欄位
		"curOrderField"=>'empID',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('empID','empName'),//要搜尋的欄位	
		"filterCondi"=>array(	//比對條件
			'depID'=>"%s like '%s%%'"
		),
		"filters"=>array(
			'depID'=>$opAR
		),
		"extraButton"=>array(
			array("選取部門",false,true,showTreeControl)
		)	
	);
		
	$fieldsList = array(
		"empID"=>array("員工ID","60px",true,array('Id',"gorec(%d)")),
		"empName"=>array("名字","120px",false,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	/*$fieldA = array(
		"assistant"=>array("編號","text",60,'','',array(true,'','',50)),
		"assistantName"=>array("總文書","queryID",60,'empID','emplyeeinfo',array(false,'','',50))
	);
	$fieldA_Data = array("empID"=>$_REQUEST['empID']);
	
	$fieldE = array(
		"assistant"=>array("編號","id",60,),
		"assistantName"=>array("總文書","queryID",60,'empID','emplyeeinfo')
	);*/
?>