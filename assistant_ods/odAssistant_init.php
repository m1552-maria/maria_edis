<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once "../config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include "$root/getEmplyeeInfo.php";

//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "$root/system/ListControl.php";
include "$root/system/ViewControl.php";

$pageTitle = "";
//$pageSize=5; //使用預設
$tableName = "assistant_ods";
// for Upload files and Delete Record use
$ulpath = "/data/assistant_ods/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}

$delField  = 'Ahead';
$delFlag   = true;
$assistant = $_REQUEST['id'];
//取得公文所有編碼
    $db   = new db();
    $sql  = "select distinct numberTitle from organization where numberTitle is not null and numberTitle !=''; ";
    $rs   = $db->query($sql);
    $allOdCode = array();
    while ($r = $db->fetch_array($rs)) {
        $value = '瑪'.$r['numberTitle'];
     $allOdCode[$value]=$value ;
    }

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, true, SearchKeyword),
    "newBtn"    => array("＋新增", false, true, doAppend),
    "editBtn"   => array("－修改", false, true, doEdit),
    "delBtn"    => array("Ｘ刪除", false, true, doDel),
);

//:ListControl  ---------------------------------------------------------------
$opList = array(
    "alterColor"    => true, //交換色
    "curOrderField" => 'assistant_od_id', //主排序的欄位
    "sortMark"      => 'ASC', //升降序 (ASC | DES)
    "searchField"   => array('odNumCode'), //要搜尋的欄位
);
$fieldsList = array(
    "assistant_od_id" => array("編號", "", true, array('Id', "gorec(%d)")),
    "empID"           => array("姓名", "", true, array('Define', getMan)),
    "odNumCode"       => array("公文編碼", "", true, array('Text')),
);

//:ViewControl -----------------------------------------------------------------
$fieldA = array(
    "empID"=>array("員工ID","readonly",30,'',true,array('','','',30)),
    "odNumCode"=>array("公文編碼","select",1,'',$allOdCode)
);
$fieldA_Data = array("unValidDate" => date('Y/m/d', strtotime("+30 days")), 'isReady' => 1);

$fieldE = array(
    "assistant_od_id"=>array("編號","id",30),
    "empID"=>array("員工ID","readonly",30),
    "odNumCode"=>array("公文編碼","select",1,'',$allOdCode)
);
