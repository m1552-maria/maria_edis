<?
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */
include 'odAssistant_init.php';

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $aa    = array_keys($fieldsList);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    //echo $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);
}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_addrE()
{
    global $county, $fieldE_Data;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $ck = ($fieldE_Data[county] == $k ? 'selected' : '');
        $rtn .= "<option value='$k' $ck>$v</option>";
    }
    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'>" . ($fieldE_Data[city] ? "<option>$fieldE_Data[city]</option>" : '') . "</select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' value='$fieldE_Data[address]' />";
    return $rtn;
}

function func_addr()
{
    global $county;
    $rtn = "<select name='county'>";
    foreach ($county as $k => $v) {
        $rtn .= "<option value='$k'>$v</option>";
    }

    $rtn .= "</select>";
    $rtn .= "<select name='city' id='city' style='width:70'></select>";
    $rtn .= "<input name='address' type='text' id='address' size='34' />";
    return $rtn;
}

function view_addr($value, $obj)
{
    global $county;
    return $county[$obj->curRow[county]] . $obj->curRow[city] . $value;
}

function getMan($v)
{
    global $emplyeeinfo;
    return $emplyeeinfo[$v];
}
?>
<!-- ::Stand-alone -->
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="/media/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/media/js/jquery-ui-1.10.1.custom.min.js"></script>
<!-- -->
<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">


<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>

<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script type="text/javascript">
 $(function(){
    var empid = '<? echo $assistant; ?>';
  $("[name='empID']").val(empid);
 });
    
</script>
<?
$whereStr = ''; //initial where string
//:filter handle  ---------------------------------------------------
$whereAry  = array();
$wherefStr = " empID='" . $assistant . "'";
if ($opList['filters']) {
    foreach ($opList['filters'] as $k => $v) {
        if ($_REQUEST[$k]) {
            $whereAry[] = "$k = '" . $_REQUEST[$k] . "'";
        } else if ($opList['filterOption'][$k]) {
            $whereAry[] = "$k = '" . $opList['filterOption'][$k][0] . "'";
        }

    }
    if (count($whereAry)) {
        $wherefStr = join(' and ', $whereAry);
    }

}

//:Search filter handle  ---------------------------------------------
$whereAry  = array();
$wherekStr = '';
if ($opList['keyword']) {
    $key = $opList['keyword'];
    foreach ($opList['searchField'] as $v) {
        $whereAry[] = "$v like '%$key%'";
    }

    $n = count($whereAry);
    if ($n > 1) {
        $wherekStr = '(' . join(' or ', $whereAry) . ')';
    } else if ($n == 1) {
        $wherekStr = join(' or ', $whereAry);
    }

}

//:Merge where
if ($wherefStr || $wherekStr) {
    $whereStr = 'where ';
    $flag     = false;
    if ($wherefStr) {
        $whereStr .= $wherefStr;
        $flag = true;}
    if ($wherekStr) {
        if ($flag) {
            $whereStr .= ' and ' . $wherekStr;
        } else {
            $whereStr .= $wherekStr;
        }

    }
}

$db     = new db();
$sql    = sprintf('select 1 from %s %s', $tableName, $whereStr);
$rs     = $db->query($sql);
$rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
if (!$pageSize) {
    $pageSize = 10;
}

$pinf = new PageInfo($rCount, $pageSize);
if ($recno > 1) {
    $pinf->setRecord($recno);
} else if ($page > 1) {
    $pinf->setPage($page);
    $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;}
;

//: Header -------------------------------------------------------------------------
echo "<h1 align='center'>$pageTitle ";
if ($opList['keyword']) {
    echo " - 搜尋:" . $opList['keyword'];
}

echo "</h1>";

//:PageControl ---------------------------------------------------------------------
$obj = new EditPageControl($opPage, $pinf);
echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//:ListControl Object --------------------------------------------------------------------
$n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
$sql = sprintf('select * from %s %s order by %s %s limit %d,%d',
    $tableName, $whereStr,
    $opList['curOrderField'], $opList['sortMark'],
    $n * $pinf->pageSize, $pinf->pageSize
);
//echo $sql;
$rs    = $db->query($sql);
$pdata = $db->fetch_all($rs);

//add by tina 20180326 List:總收發人顯示名字
foreach ($pdata as &$pdataInfo) {
    $revManId = $pdataInfo['revMan'];
    if (isset($revManId)) {

        $pdataInfo['revManName'] = $emplyeeinfo[$revManId];

    }
    $mainRevManId = $pdataInfo['mainRevMan'];
    if (isset($mainRevManId)) {

        $pdataInfo['mainRevManName'] = $emplyeeinfo[$mainRevManId];

    }    
    unset($pdataInfo);

}

//update by tina 20180412 新增/修改不顯示清單
if (empty($act) or $act=='del') {
    $obj = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
}



//:ViewControl Object ------------------------------------------------------------------------
$listPos = $pinf->curRecord % $pinf->pageSize;
switch ($act) {
    case 'edit': //修改
        $op3 = array(
            "type"      => "edit",
            "form"      => array('form1', "doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
            "submitBtn" => array("確定修改", false, ''),
            "cancilBtn" => array("取消修改", false, ''));
        $fieldE_Data = $pdata[$listPos];
        $ctx         = new BaseViewControl($fieldE_Data, $fieldE, $op3);
        break;
    case 'new': //新增
        $op3 = array(
            "type"      => "append",
            "form"      => array('form1', "doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
            "submitBtn" => array("確定新增", false, ''),
            "cancilBtn" => array("取消新增", false, ''));
        $ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
        break;
}

include '../public/inc_listjs.php';
?>

