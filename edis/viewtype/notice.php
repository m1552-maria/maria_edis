<?
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once '../../organization/init.php';
include_once "$root/edis/func.php";
//echo $_REQUEST['ptype'];
function getROCDate($date, $dateType)
{
    if ($dateType == 'Y') {
        return date_format(date_create($date), 'Y') - 1911;
    } else if ($dateType == 'm') {
        return date_format(date_create($date), 'm');
    } else if ($dateType == 'd') {
        return date_format(date_create($date), 'd');
    }

}

$db = new db();
//公文
$id      = $_REQUEST['id'];
$edisSql = "select * from edis where id=$id";
$rs      = $db->query($edisSql);
$r       = $db->fetch_array($rs);


//承辦主管是否同意
$sginSql ="select * from map_orgs_sign where edisid =".$id." and `signLevel`='1' and `isSign` ='1'";
$rsSign      = $db->query($sginSql);
$rSignCount       = $db->num_rows($rsSign);

if($rSignCount=='1'){
    $dtypeName =$r['dType'];

}else{
    $dtypeName =$r['dType'].'(稿)';
}

//發文機關
$soActSql = "select mo.oid,o.title,o.PosCode,o.county,o.city,o.address,o.attach  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $_REQUEST['id'] . "' and mo.act ='SO' ";
$rsSOact  = $db->query($soActSql);
if ($rsSOact) {
    $rSOact                = $db->fetch_array($rsSOact);
    $rSOact['fulladdress'] = $rSOact['PosCode'] . $county[$rSOact['county']] . $rSOact['city'] . $rSOact['address'];
}

//受文者
$rActSql = "select mo.oid,o.title,o.PosCode,o.county,o.city,o.address   from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $_REQUEST['id'] . "' and mo.act ='R' ";
$rsRact  = $db->query($rActSql);
if ($rsRact) {
    $rRact                = $db->fetch_array($rsRact);
    $rRact['fulladdress'] = $county[$rRact['county']] . $rRact['city'] . $rRact['address'];
}
//承辦機關(承辦人部門)
$OrgSql = "select * from organization where id ='" . $r['sOid'] . "'";
$rsO    = $db->query($OrgSql);
if ($rsO) {
    $rO = $db->fetch_array($rsO);

}

//領據
$recSql = "select * from receipt where  edisid ='" . $_REQUEST['id'] . "'";
$rsRec  = $db->query($recSql);
if ($rsRec) {
    $count =0;
    while ($rRec = $db->fetch_array($rsRec)) {
        $count++;
        if($count==1) $receiptStr =' 領據：';
        $receiptStr .=' '.$rRec['receiptNo'];
    }
}


//主文附件
$mfSql = "select * from map_files where aType='M' and eid ='" . $_REQUEST['id'] . "'";
$rsMf  = $db->query($mfSql);
if ($rsMf) {
    $rMf = $db->fetch_array($rsMf);
}
//其他附件
$ofSql = "select * from map_files where aType='O' and eid ='" . $_REQUEST['id'] . "'";
$rsOf  = $db->query($ofSql);
if ($rsOf) {
    while ($rOf = $db->fetch_array($rsOf)) {
        $ofInfo[$rOf['fName']] = $rOf['title'];
    }
}
//正本
$nActSql = "select o.title from map_orgs mo,organization o where mo.oid=o.id  and mo.act ='N' and mo.eid ='" . $_REQUEST['id'] . "' order by mo.id";
$rsNact  = $db->query($nActSql);
if ($rsNact) {
    $count = 0;
    while ($rNact = $db->fetch_array($rsNact)) {
        $count++;
        if ($count == 1) {
            $nActInfo .= $rNact['title'];
        } else {
            $nActInfo .= '、' . $rNact['title'];
        }

    }
}

//副本
$cActSql = "select o.* from map_orgs mo,organization o where mo.oid=o.id  and mo.act ='C' and mo.eid ='" . $_REQUEST['id'] . "' order by mo.id";
$rsCact  = $db->query($cActSql);
if ($rsCact) {
    $count = 0;
    while ($rCact = $db->fetch_array($rsCact)) {
        $count++;
        if ($rSOact['oid'] == $rCact['id']) {
            $title = $rCact['selfTitle'];
        } else {
            $title = $rCact['title'];
        }
        if ($count == 1) {

            $cActInfo .= $title;

        } else {
            $cActInfo .= '、' . $title;
        }

    }
}

//所有正本副本
$allActSql = "select o.title from map_orgs mo,organization o where mo.oid=o.id  and mo.act in('N','C') and mo.eid ='" . $_REQUEST['id'] . "'";
$rsAllact  = $db->query($allActSql);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<link href="sod.css" rel="stylesheet" type="text/css">

<?
$checkResult = checkAttView($_REQUEST['id'],$_REQUEST['empid']);
$formContent = '<table class="printPage" width="100%" border="0">
  <tr style="visibility: hidden;">
        <td width="40%">
        <td width="10%">
        <td width="10%">
        <td width="10%">
        <td width="30%">
  </tr>
  <tr>' .
    '<td class ="twelve" colspan="4" >' . $r['pType'] . '</td>' .
    '<td class ="twelve" colspan="1" ><p>檔號：' . $r['filingNo'] . '</p>保存年限：' . $r['rYear'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twenty" colspan="5" align="center" ><p>' . $rSOact['title'] . '&nbsp;' .  $dtypeName . '</p></td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">' . $rRact['PosCode'] . '<br>' . $rRact['fulladdress'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="eighteen" colspan="5" height="45px">受文者：' . $rRact['title'];
if ($_REQUEST['ptype'] == 'combine') {
    $formContent .= '(詳正本/副本)';
}

$formContent .= '</td>' .
'</tr>' .
'<tr>' .
'<td class ="twelve" colspan="5">發文日期：中華民國' . getROCDate($r['sDate'], 'Y') . '年' . getROCDate($r['sDate'], 'm') . '月' . getROCDate($r['sDate'], 'd') . '日</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">發文字號：' . $r['did'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">速別：' . $r['dSpeed'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">密等及解密條件或保密期限：' . $r['dSecret'] . '</td>' .
    '</tr>' .
    '<tr>' .
    //'<td class ="twelve" colspan="5">附件：</td>';
    '<td class ="twelve" colspan="5"></td>';

/*$attpath = '../data/edis/' . $_REQUEST['id'] . '\/main\/';
if (file_exists($attpath)) {
    $formContent .= '<tr>';
    $formContent .= '<td class ="twelve" colspan="5">主文：';
    $dh = opendir($attpath);
    while (false !== ($filename = readdir($dh))) {
        if ($filename == '.') {
            continue;
        }

        if ($filename == '..') {
            continue;
        }

        $file_path = $attpath . $filename;
        $formContent .= '
              <a target="_blank" href="' . $file_path . '">' . $rMf['title'] . '</a>
            ';
    }
    $formContent .= '</td></tr>';
}*/
    $formContent .= '<tr>';
    //$formContent .= '<td class ="twelve" colspan="5">其他附件：';
    $formContent .= '<td class ="twelve" colspan="5">附件：'.$receiptStr;

$attpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
if (file_exists($attpath)) {

    $dh = opendir($attpath);

    while (false !== ($filename = readdir($dh))) {

        if ($filename == '.') {
            continue;
        }

        if ($filename == '..') {
            continue;
        }

        $file_path = $attpath . $filename;

if( $checkResult || $_REQUEST['loginType']=='cxo'){         
        $formContent .= '  <a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename] . '</a>
            ';
}else{
        $formContent .= '  '.$ofInfo[$filename];
}             

    }
    
}
$formContent .= '</td></tr>';
$formContent .=
    '</tr>' .
    '<tr>' .
    '<td class ="sixteen paddingLeftSubject" colspan="5">主旨：' . $r['subjects'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td colspan="5">' . $r['contents'] . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">正本：' . $nActInfo . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twelve" colspan="5">副本：' . $cActInfo . '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twenty" colspan="5" align="left">(條戳)</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="twenty" colspan="5" align="middle" height="50px">';
if ($rSOact['attach'] && $rSignCount=='1') {
    $formContent .= '<img src="' . $ulpath . str_replace(",", "", $rSOact['attach']) . '" width="310" align="middle">';
}

$formContent .= '</td></tr>' .
    '</table>';

?>


</head>

<body>

  <center>

<?

if ($_REQUEST['ptype'] == 'combine') {
    //統一列印
    echo $formContent;

} else if ($_REQUEST['ptype'] == 'alone') {
    //個別列印
    //受文者
    echo $formContent;
    while ($rAllact = $db->fetch_array($rsAllact)) {
        if ($rAllact['title'] !== $rRact['title']) {
            //正本/副本
            echo '<table class="printPage" width="100%" border="0" >
  <tr style="visibility: hidden;">
        <td width="40%">
        <td width="10%">
        <td width="10%">
        <td width="10%">
        <td width="30%">
  </tr>
  <tr>' .
            '<td class ="twelve" colspan="4" >' . $r['pType'] . '</td>' .
            '<td class ="twelve" colspan="1" ><p>檔號：' . '' . '</p>保存年限：' . $r['rYear'] . '</td>' .
            '</tr>' .
            '<tr>' .
            '<td class ="twenty" colspan="5" align="center" ><p>' . $rSOact['title'] . '&nbsp;' . $r['dType'] . '</p></td>' .
            '</tr>' .
            '<tr>' .
            '<td class ="twelve" colspan="5">' . $rRact['PosCode'] . '<br>' . $rRact['fulladdress'] . '</td>' .
            '</tr>' .
            '<tr>' .
            '<td class ="eighteen" colspan="5" height="45px">受文者：' . $rAllact['title'] . '</td>' .
            '</tr>' .
            '<tr>' .
            '<td class ="twelve" colspan="5">發文日期：中華民國' . getROCDate($r['sDate'], 'Y') . '年' . getROCDate($r['sDate'], 'm') . '月' . getROCDate($r['sDate'], 'd') . '日</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twelve" colspan="5">發文字號：' . $r['did'] . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twelve" colspan="5">速別：' . $r['dSpeed'] . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twelve" colspan="5">密等及解密條件或保密期限：' . $r['dSecret'] . '</td>' .
                '</tr>' .
                '<tr>' .
                //'<td class ="twelve" colspan="5">附件：</td>';
                '<td class ="twelve" colspan="5"></td>';

            /*$attpath = '../data/edis/' . $_REQUEST['id'] . '\/main\/';
            if (file_exists($attpath)) {
                echo '<tr>';
                echo '<td class ="twelve" colspan="5">主文：';
                $dh = opendir($attpath);
                while (false !== ($filename = readdir($dh))) {
                    if ($filename == '.') {
                        continue;
                    }

                    if ($filename == '..') {
                        continue;
                    }

                    $file_path = $attpath . $filename;
                    echo '
              <a target="_blank" href="' . $file_path . '">' . $rMf['title'] . '</a>
            ';
                }
                echo '</td></tr>';
            }
             */

                echo '<tr>';
                //echo '<td class ="twelve" colspan="5">其他附件：';
                echo '<td class ="twelve" colspan="5">附件：'.$receiptStr;            
            $attpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
            if (file_exists($attpath)) {

                $dh = opendir($attpath);

                while (false !== ($filename = readdir($dh))) {

                    if ($filename == '.') {
                        continue;
                    }

                    if ($filename == '..') {
                        continue;
                    }

                    $file_path = $attpath . $filename;

if( $checkResult || $_REQUEST['loginType']=='cxo'){         
        echo '  <a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename] . '</a>';
}else{
        echo '  '.$ofInfo[$filename];
}

                }
                
            }
            echo '</td></tr>';
            echo '</tr>' .
                '<tr>' .
                '<td class ="sixteen paddingLeftSubject" colspan="5">主旨：' . $r['subjects'] . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td colspan="5">' . $r['contents'] . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twelve" colspan="5">正本：' . $nActInfo . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twelve" colspan="5">副本：' . $cActInfo . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twenty" colspan="5" align="left">(條戳)</td>' .
                '</tr>' .
                '<tr>' .
                '<td class ="twenty" colspan="5" align="middle" height="50px">';
            if ($rSOact['attach'] && $rSignCount=='1') {
                echo '<img src="' . $ulpath . str_replace(",", "", $rSOact['attach']) . '" width="310" align="middle">';
            }

            echo '</td></tr>' .
                '</table>';
        }
    }
}
?>

</center>
</body>
</html>