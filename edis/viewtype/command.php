<?
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once '../../organization/init.php';
//echo $_REQUEST['ptype'];
function getROCDate($date, $dateType)
{
    if ($dateType == 'Y') {
        return date_format(date_create($date), 'Y') - 1911;
    } else if ($dateType == 'm') {
        return date_format(date_create($date), 'm');
    } else if ($dateType == 'd') {
        return date_format(date_create($date), 'd');
    }

}

$db = new db();
//公文
$id      = $_REQUEST['id'];
$edisSql = "select * from edis where id=$id";
$rs      = $db->query($edisSql);
$r       = $db->fetch_array($rs);


//承辦主管是否同意
$sginSql ="select * from map_orgs_sign where edisid =".$id." and `signLevel`='1' and `isSign` ='1'";
$rsSign      = $db->query($sginSql);
$rSignCount       = $db->num_rows($rsSign);

if($rSignCount=='1'){
    $dtypeName =$r['dType'];

}else{
    $dtypeName =$r['dType'].'(稿)';
}

//發文機關
$soActSql = "select mo.oid,o.title,o.PosCode,o.county,o.city,o.address,o.attach  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $_REQUEST['id'] . "' and mo.act ='SO'";
$rsSOact  = $db->query($soActSql);
if ($rsSOact) {
    $rSOact                = $db->fetch_array($rsSOact);
    $rSOact['fulladdress'] = $rSOact['PosCode'] . $county[$rSOact['county']] . $rSOact['city'] . $rSOact['address'];
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<link href="sod.css" rel="stylesheet" type="text/css">
</head>

<body>
  <center>
<table class="printPage"  width="100%" border="0">
  <tr style="visibility: hidden;">
        <td width="40%">
        <td width="10%">
        <td width="10%">
        <td width="10%">
        <td width="30%">
  </tr>
  <tr>
    <td class ="twelve" colspan="4" ><?=$r['pType']?></td>
    <td class ="twelve" colspan="1" ><p>檔號：<?=$r['filingNo']?></p>保存年限：<?=$r['rYear']?></td>
  </tr>
  <tr>
    <td class ="twenty" colspan="5" align="center" ><p><?=$rSOact['title']?>&nbsp;<?=$dtypeName?></p></td>
  </tr>

  <tr>
  <td class ="twelve" colspan="5">發文日期：中華民國<?=getROCDate($r['sDate'], 'Y')?>年<?=getROCDate($r['sDate'], 'm')?>月<?=getROCDate($r['sDate'], 'd')?>日</td>
  </tr>

  <tr>
    <td class ="twelve" colspan="5">發文字號：<?=$r['did']?></td>
  </tr>
  <tr>
    <td colspan="5" align="right"><p><div style="border-width:1px;border-style:dashed;padding:30px; width: 3cm;height:3cm; "></div></p>
    </td>
  </tr>
  <tr>
    <td class ="sixteen" colspan="5"><?=$r['subjects']?></td>
  </tr>
  <tr>
    <td class ="sixteenS" colspan="5"><?=$r['contents']?></td>
  </tr>
  <tr>
  <td class ="twenty" colspan="5" align="middle" height="50px">
  <?
if ($rSOact['attach'] && $rSignCount=='1' ) {
    echo "<img src='" . $ulpath . str_replace(",", "", $rSOact['attach']) . "' width='310'>";
}
?>
  </td>
  </tr>
</table></center>
</body>
</html>