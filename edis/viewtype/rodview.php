<?
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once '../../organization/init.php';

include_once "$root/edis/func.php";
//echo $_REQUEST['ptype'];
function getROCDate($date, $dateType)
{
    if ($dateType == 'Y') {
        return date_format(date_create($date), 'Y') - 1911;
    } else if ($dateType == 'm') {
        return date_format(date_create($date), 'm');
    } else if ($dateType == 'd') {
        return date_format(date_create($date), 'd');
    }

}

$db = new db();
//公文
$id      = $_REQUEST['id'];
$edisSql = "select * from edis where id=$id";
$rs      = $db->query($edisSql);
$r       = $db->fetch_array($rs);

//來文機關
$sActSql = "select mo.oid,mo.title,o.PosCode,o.county,o.city,o.address,o.attach  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $id . "' and mo.act ='S' ";
$rsSact  = $db->query($sActSql);
if ($rsSact) {
    $rSact                = $db->fetch_array($rsSact);
    $rSact['fulladdress'] = $rSact['PosCode'] . $county[$rSact['county']] . $rSact['city'] . $rSact['address'];
}

//收文單位
$rActSql = "select mo.oid,mo.title,o.PosCode,o.county,o.city,o.address from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $id . "' and mo.act ='R' ";
$rsRact  = $db->query($rActSql);
if ($rsRact) {
    $rRact                = $db->fetch_array($rsRact);
    $rRact['fulladdress'] = $county[$rRact['county']] . $rRact['city'] . $rRact['address'];
}

//主文附件
$mfSql = "select * from map_files where aType='M' and eid ='" . $id . "'";
$rsMf  = $db->query($mfSql);
if ($rsMf) {
    $rMf = $db->fetch_array($rsMf);
}
//其他附件
$ofSql = "select fName, title from map_files where aType='O' and eid ='" . $id . "'";
$rsOf  = $db->query($ofSql);
if ($rsOf) {
    while ($rOf = $db->fetch_array($rsOf)) {
        $ofInfo[$rOf['fName']] = $rOf['title'];
    }
}
//承辦人
$sidSql = "select signMan from map_orgs_sign where edisid ='" . $id . "' and isOriginal='1' and signLevel='1'";
$rsSid = $db->query($sidSql);
if ($rsSid) {
    while ($rSid = $db->fetch_array($rsSid)) {
        $sidLsit .= $emplyeeinfo[$rSid['signMan']].' ';
    }
}

$depSql = "select title from map_dep where eid ='" . $id . "'";
$rsdep  = $db->query($depSql);
if ($rsdep) {
    while ($rdep = $db->fetch_array($rsdep)) {
        $depName[] = $rdep['title'];
    }
}

$dep = join("<br>　　　",$depName);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>表單列印</title>
<style>

/* 預設字型 */
@font-face {
  font-family: MyOdFont;
  src: local(Heiti TC),
       local("標楷體");
}


/* 英文 */
@font-face {
  font-family: MyOdFont;
  unicode-range: U+00-024F;
  src:  local(Times New Roman),local(Times) ;
}


td.ten {
    font-size: 10pt;　　　　　          /* 設定文字大小 */
    line-height: 10pt;　　　          　/* 設定文字行距 */

}

td.twelve {
    font-size: 12pt;　　　　　          /* 設定文字大小 */
    line-height: 15pt;　　　          　/* 設定文字行距 */

}

td.sixteen {
    font-size: 16pt;　　　　　          /* 設定文字大小 */
    line-height: 28pt;　　　          　/* 設定文字行距 */


}
td.sixteenS {
    font-size: 16pt;　　　　　          /* 設定文字大小 */
    line-height: 28pt;　　　          　/* 設定文字行距 */
    margin-left:16pt;
    text-indent : 16pt;


}

td.twenty {
    font-size: 20pt;　　　　　          /* 設定文字大小 */
    line-height: 36pt;　　　          　/* 設定文字行距 */


}

body {
  font-family: MyOdFont, sans-serif;
}

@page {
 margin-top: 0.6cm;    
 margin-left: 1.5cm;
 margin-right: 1.5cm;
 /*margin-bottom: 1.5cm;*/
 size:'A4'; /*列印紙張大小*/
    }

.printPage {
page-break-after:always;
}


</style>



<?
$checkResult = checkAttView($_REQUEST['id'],$_REQUEST['empid']);
$formContent = '<table class="printPage" width="100%" border="0" >
  <tr style="visibility: hidden;">
        <td width="40%">
        <td width="10%">
        <td width="10%">
        <td width="10%">
        <td width="30%">
  </tr>
  <tr >
    ' .
'<td class ="sixteen" colspan="2" >收文單位：' . $rRact['title'] .
'<td class ="sixteen" colspan="5" >收文字號：' . $r['did'] . '</td>' .
'</tr>' .

'<tr>' .
'<td class ="sixteen" colspan="2" >來文機關:' . $rSact['title'] . '&nbsp;' . $r['dType'] . '</td>' .
'<td class ="sixteen" colspan="5">文書承辦日期：中華民國' . getROCDate($r['rDate'], 'Y') . '年' . getROCDate($r['rDate'], 'm') . '月' . getROCDate($r['rDate'], 'd') . '日</td>' .
'</tr>';

$formContent .= 
'<td class ="sixteen" colspan="2">來文字號：' . $r['dNo'] . '</td>' .
'<td class ="sixteen" colspan="5">速別：' . $r['dSpeed'] . '</td>' .
'</tr>' .
'<tr>' .
'<td class ="sixteen" colspan="2">來文日期：中華民國' . getROCDate($r['sDate'], 'Y') . '年' . getROCDate($r['sDate'], 'm') . '月' . getROCDate($r['sDate'], 'd') . '日</td>' .
'<td class ="sixteen" colspan="5">期限：中華民國' . getROCDate($r['deadline'],'Y') .'年'. getROCDate($r['deadline'], 'm') . '月' . getROCDate($r['deadline'], 'd') . '日</td>' .
    '</tr>' ;


$attpath = '../../data/edis/' . $_REQUEST['id'] . '\/main\/';
if (file_exists($attpath)) {
    $formContent .= '<tr>';
    $formContent .= '<td class ="sixteen" colspan="2">主文：';
    $dh = opendir($attpath);
    while (false !== ($filename = readdir($dh))) {
        if ($filename == '.') {
            continue;
        }

        if ($filename == '..') {
            continue;
        }

        $file_path = $attpath . $filename;
        if( $checkResult || $_REQUEST['loginType']=='cxo'){         
            $formContent .= '<a target="_blank" href="' . $file_path . '">' . $rMf['title'] . '</a>  ';
        }else{
            $formContent .= '  '.$rMf['title'];
        }         
        
    }
    $formContent .= '</td>';
}
    $formContent .= '<td class ="sixteen" colspan="5">計畫編號：' . $emplyeeinfo[$r['planNo']] . '</td></tr>';
    $formContent .= '<tr>';
    $formContent .= '<td class ="sixteen" colspan="2">其他附件：';
$attpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
if (file_exists($attpath)) {
    $dh = opendir($attpath);

    while (false !== ($filename = readdir($dh))) {

        if ($filename == '.') {
            continue;
        }

        if ($filename == '..') {
            continue;
        }

        $file_path = $attpath . $filename;

        if( $checkResult || $_REQUEST['loginType']=='cxo'){         
            $formContent .= '<a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename] . '</a>  ';
        }else{
            $formContent .= '  '.$ofInfo[$filename];
        }    



    }
    $formContent .= '</td></tr>';
}

$formContent .= '<tr><td class ="sixteen" colspan="2">文書：' . $emplyeeinfo[$r['creator']] . '</td>' .'<td class ="sixteen" colspan="5">歸檔編號：' . $r['filingNo'] . '</td>' .
    '</tr><tr>' .
    '<td class ="sixteen" colspan="5">主旨：' . $r['subjects']. '</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="sixteen" colspan="5">備註：'.$r['note'] .'</td>' .
    '</tr>' .
    '<tr>' .
    '<td class ="sixteenS" colspan="5">' . $r['contents'] . '</td>' .
    '</tr>';
/*if ($rSact['attach']) {
    $formContent .= '<img src="' . $ulpath . str_replace(",", "", $rSact['attach']) . '" width="310">';
}*/

$formContent .= '<tr>'.
'<td class ="sixteen" colspan="2">承辦人：' . $sidLsit . '</td>'.
'<td class ="sixteen" colspan="5">部門：' . $dep . '</td>'.
'</tr>'.'</table>';
?>

</head>

  <center>

<?
if ($_REQUEST['ptype'] == 'combine') {
    //統一列印
    echo $formContent;

} else if ($_REQUEST['ptype'] == 'alone') {
    //分別列印
    echo $formContent;

}

?>

</center>
</body>
</html>
