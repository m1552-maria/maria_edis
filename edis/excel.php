<?
  if($_REQUEST['sqltx']) {
    $filename = '財團法人瑪利亞社會福利基金會'.$_REQUEST['dateRange'].'統計報表'.'.xlsx';
    //echo $_REQUEST['sqltx'];exit;
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:attachment;filename=$filename");
    include_once "../system/db.php";
    include_once "../config.php";
    include_once "../getEmplyeeInfo.php";
    include_once "../inc_vars.php";
    include '../receipt/func.php';
    require_once('../Classes/PHPExcel.php');
    require_once('../Classes/PHPExcel/IOFactory.php');
    
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0);
    
    
    //設定字型大小
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    
    //設定欄位背景色(方法1)
    // $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray(
    //     array(
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '969696')
    //         ),
    //         'font'   => array('bold' => true,
    //             'size' => '12',
    //             'color' => array('argb' => 'FFFFFF')
    //         )
    //     )
    // );


  
if($_REQUEST['odTypeName']=='收文'){
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );  
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
  
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:I1");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
   // VERTICAL_CENTER 垂直置中
   // VERTICAL_TOP
   // HORIZONTAL_CENTER
   // HORIZONTAL_RIGHT
   // HORIZONTAL_LEFT
   // HORIZONTAL_JUSTIFY
       
    $objPHPExcel->getActiveSheet()->setCellValue('A1','財團法人瑪利亞社會福利基金會'.$_REQUEST['dateRange'].'統計報表');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','收文字號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C2','來文日期'); 
    $objPHPExcel->getActiveSheet()->setCellValue('D2','來文機關'); 
    $objPHPExcel->getActiveSheet()->setCellValue('E2','主旨');    
    $objPHPExcel->getActiveSheet()->setCellValue('F2','承辦');
    $objPHPExcel->getActiveSheet()->setCellValue('G2','備註');
    $objPHPExcel->getActiveSheet()->setCellValue('H2','類別');
    $objPHPExcel->getActiveSheet()->setCellValue('I2','歸檔編號');
    $objPHPExcel->getActiveSheet()->setCellValue('B2','文書承辦日期');


    $sql = substr($_REQUEST['sqltx'],0,strpos($_REQUEST['sqltx'],"limit"));
    $DB = new db();
    $rsX    = $DB->query($sql); 
    $i = 3;
   

    while($rsX && $r=$DB->fetch_array($rsX)) {
      
        $did = getDid($r['edisid']);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$r['did']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$r['odDeadlineType']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$r['sDate']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$r['sActName']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$r['subjects']);   
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,date('Y-m-d',strtotime($r['rDate'])));

        $sql = "select signMan from map_orgs_sign where signLevel = '0' and edisid = '".$r['id']."'";
        $rs  = $DB->query($sql);
        $rSid = $DB->fetch_all($rs);
        $empStr = array();
        foreach ($rSid as $key => $value) {
            $empStr[] = $emplyeeinfo[$value['signMan']];
        }

        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, join(',', $empStr));

        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$r['note']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$r['filingNo']);


        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
      
    }

    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);    
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);


}else if($_REQUEST['odTypeName']=='發文'){

    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );  
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C0C0C0')
            )
        )
    );
  
    //合併儲存隔 
    $objPHPExcel->getActiveSheet()->mergeCells("A1:I1");
    //對齊方式
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
   // VERTICAL_CENTER 垂直置中
   // VERTICAL_TOP
   // HORIZONTAL_CENTER
   // HORIZONTAL_RIGHT
   // HORIZONTAL_LEFT
   // HORIZONTAL_JUSTIFY
       
    $objPHPExcel->getActiveSheet()->setCellValue('A1','財團法人瑪利亞社會福利基金會'.$_REQUEST['dateRange'].'統計報表');
    $objPHPExcel->getActiveSheet()->setCellValue('A2','發文字號'); 
    $objPHPExcel->getActiveSheet()->setCellValue('B2','發文日期'); 
    $objPHPExcel->getActiveSheet()->setCellValue('C2','受文者');
    $objPHPExcel->getActiveSheet()->setCellValue('D2','主旨');     
    $objPHPExcel->getActiveSheet()->setCellValue('E2','承辦');
    // $objPHPExcel->getActiveSheet()->setCellValue('F2','總表數量');
    $objPHPExcel->getActiveSheet()->setCellValue('F2','承辦部門');
    $objPHPExcel->getActiveSheet()->setCellValue('G2','備註');
    $objPHPExcel->getActiveSheet()->setCellValue('H2','類別');
    $objPHPExcel->getActiveSheet()->setCellValue('I2','歸檔編號');

    $sql = substr($_REQUEST['sqltx'],0,strpos($_REQUEST['sqltx'],"limit"));
    $DB = new db();
    $rsX    = $DB->query($sql);
    $i = 3;
   

    while($rsX && $r=$DB->fetch_array($rsX)) {
      
        $sql = "select * from edis where id =".$r['id'];    
        $rs = $DB->query($sql);
        while( $numr = $DB->fetch_array($rs) ){ 
            $budget = '核銷冊:'.(empty($numr['budgetNum'])? 0: $numr['budgetNum']);
            $result = '成果冊:'.(empty($numr['resultNum'])? 0: $numr['resultNum']);
            $plan   = '計畫書:'.(empty($numr['planNum'])? 0: $numr['planNum']);
            $sumNum = $budget.' '.$result.' '.$plan;
            $dep = explode(',', $numr['depID']);
            $depArr = array();
            foreach ($dep as $key => $value) {
              $depArr[] = $depInfo[$value];
            }
         }   
      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$r['did']);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$r['sDate']);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$r['rActName']);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$r['subjects']);      
      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$emplyeeinfo[$r['sId']]);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$r['note']);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,join(',',$depArr));
      // $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$sumNum);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$r['odDeadlineType']);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$r['filingNo']);    
      

      $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
      
    }

    //設定欄寬(自動欄寬)
    // $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
    // $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
    //設定欄寬
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);    
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);    
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
    // $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
}

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
  ob_clean();
  $objWriter->save('php://output');

  }

?>