<?php 
include_once 'inc_vars.php';
include_once "$root/system/db.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/group/groupDiv.php";

$empID = $_SESSION['empID'];
$depID = $_SESSION["depID"];
$odTypeName = '收文';

$db    = new db();
//公文主檔
if($_GET['mode'] == 'copy'){
    $sql = "select * from edis_temp where id=" . $_REQUEST['id'];
    $rs  = $db->query($sql);
    if ($rs) {
        $r = $db->fetch_array($rs);
    }
    $edisID = $r['sourceID'];
}else{
    $sql = "select * from edis where id=" . $_REQUEST['id'];
    $rs  = $db->query($sql);
    if ($rs) {
        $r = $db->fetch_array($rs);
    }
    $edisID = $_REQUEST['id'];
}

//承辦部門
$mdepSql = "SELECT * FROM map_dep WHERE eid = '" . $edisID . "'";
$rsDep   = $db->query($mdepSql);

//承辦人(多人)
$signSql = "SELECT 
                id as sMgrid,
                signMan 
            FROM map_orgs_sign  
            where edisid = '".$edisID."' and moid is null and signLevel='1' order by id";
$rsSign  = $db->query($signSql);

//承辦主管
$signMgrSql = "SELECT 
                id as sMgrid,
                signMan 
            FROM map_orgs_sign  
            where edisid = '".$edisID."' and moid is null and signLevel='2' order by id";
$rsSignMgr  = $db->query($signMgrSql);

// 機關
$ActSql = "select 
                mo.oid,
                mo.act,
                if((length(o.sTitle) > 0), o.sTitle, o.title) as stitle
            from map_orgs mo LEFT JOIN organization o ON(mo.oid =o.id) where mo.eid = '" . $edisID . "'";
$rsAct  = $db->query($ActSql);

if ($rsAct) {
    while ($rAct = $db->fetch_array($rsAct)) {
        switch ($rAct['act']) {
            case 'S':
                $rSact = $rAct;
                break;
            case 'R':
                $rRact = $rAct;
                break;
            case 'RO':
                $rROact = $rAct;
                break;
        } 
    }
}
//來文機關
// $rActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
//                      `o`.`sTitle`,
//                      `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='S' ";
// $rsRact  = $db->query($rActSql);
// if ($rsRact) {
//     $rSact = $db->fetch_array($rsRact);
// }

//收文機關
// $rActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
//                      `o`.`sTitle`,
//                      `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='R' ";
// $rsRact  = $db->query($rActSql);
// if ($rsRact) {
//     $rRact = $db->fetch_array($rsRact);
// }

// //收文單位
// $roActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
//                      `o`.`sTitle`,
//                      `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='RO' ";
// $rsROact  = $db->query($roActSql);
// if ($rsROact) {
//     $rROact = $db->fetch_array($rsROact);
// }

//主文附件
$mFileSql = "select * from map_files mf where mf.eid = '" . $edisID . "' and aType='M'";
$rsMfile  = $db->query($mFileSql);
if ($rsMfile) {
    $rMfile = $db->fetch_array($rsMfile);
}

//其他附件
$oFileSql = "select * from map_files mf where mf.eid = '" . $edisID . "' and aType='O'";
$rsOfile  = $db->query($oFileSql);

?>

<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<link href="group/group.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="group/group.js"></script>
<script>
function returnValues(data){
    if(data[0]['type'] == 'depID'){
        var depData = new Array();
        if($('input[name="depID[]"]').first().val() != ''){
            $.each( $('input[name="depID[]"]'), function(kk, vv){
                if($(vv).val() == '') return;
                if(!depData[kk]) depData[kk] = new Array();

                depData[kk]['id'] = $(vv).val();
                depData[kk]['title'] = $(vv).attr('title');
                depData[kk]['type'] = 'depID';
            });
        }
        var testArray;
        testArray = depData.concat(data);
        data = testArray;

        var DepsDiv = '<div><input name="depID[]" class="queryID" required="true" readonly id="admDeps" type="text" size="6" onkeypress="return checkInput(event,\'depID\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/> <span class="formTxS"></span><span class="sBtn" id="sidRemove" onclick=\'removeItem(this)\'> -移除</span></div>';
        $('#admDepsDiv').empty();
        $('#admDepsDiv').html(DepsDiv);
    }

    if(data[0]['type'] == 'empID'){
        var empData = new Array();
        if($('input[name="sId[]"]').first().val() != ''){
            $.each( $('input[name="sId[]"]'), function(kk, vv){
                if($(vv).val() == '') return;
                if(!empData[kk]) empData[kk] = new Array();

                empData[kk]['id'] = $(vv).val();
                empData[kk]['title'] = $(vv).attr('title');
                empData[kk]['type'] = 'empID';
            });
        }
        var testArray;
        testArray = empData.concat(data);
        data = testArray;
        console.log(data);
        var sIdDiv = '<div><input name="sId[]" type="text" class="queryID" id="sIds" required ="true" size="6" value="<?php  echo $empID?>" title="<?php  echo $emplyeeinfo[$empID]?>" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidRemove" onclick=\'removeItem(this)\'> -移除</span></div>';
        $('#sIdsDiv').empty();
        $('#sIdsDiv').html(sIdDiv);
    }
    
    $.each( data, function( k, v ) {
        if(v['type'] == 'depID'){
            var depInputLen = $('input[name="depID[]"]').length;
            var depInputVal = true;
            if($('input[name="depID[]"]').first().val() == '') var depInputVal = false; 

            if((depInputLen != 1 || depInputVal) ) addItem('','depID','admDeps');
            $("input[name='depID[]']").last().val(v['id']);
            $("input[name='depID[]']").last().attr('title',v['title']);
            $("input[name='depID[]']").last().after(' '+v['title']);
        }

        if(v['type'] == 'empID'){
            if(k == 0){
                $.ajax({
                    url: 'edis/api.php?act=leadID&empID='+v['id'],
                    type:"GET",
                    dataType:'json',
                    success: function(response){
                        if (response == 0){
                            var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="" title="" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                            $('#sidMgrDiv').empty();
                            $('#sidMgrDiv').html(sIdDiv);
                        }else{
                            var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="'+response['leadID']+'" title="'+response['empName']+'" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="formTxS">'+response['empName']+'</span><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                            $('#sidMgrDiv').empty();
                            $('#sidMgrDiv').html(sIdDiv);

                            $("input[name='sidMgr[]']").first().val(response['leadID']);
                            $("input[name='sidMgr[]']").first().value = response['leadID'];
                            $("input[name='sidMgr[]']").first().attr('title',response['empName']);
                        }
                    }
                });
            }
            if(k != 0) addItem('','sId','sIds');
            $("input[name='sId[]']").last().val(v['id']);
            $("input[name='sId[]']").last().attr('title',v['title']);
            $("input[name='sId[]']").last().after(' '+v['title']);
        }
    });
}
function addItem(tbtn,fld,qid) {
    if(fld =='oTitle'){
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' placeholder='請輸入名稱' type='text'/>"
        + " <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'/>"
        + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }else if(fld=='sidMgr'){
        var newone = "<div>"
        +"<input name='"+fld+"[]' id='"+qid+"' type='text' size='6'/>"
        +"<img src='/scripts/form_images/search.png'  align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
        +"<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }else if(fld=='sId'){
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\" >"
        + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
        + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }else if(fld=='depID'){
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' required ='true' readonly>"
        + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
        + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }else{
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
        + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
        + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }

    var divTitle ='#'+qid+'Div';
    $(divTitle).append(newone);
}

function removeItem(tbtn) {
    $(tbtn).parent().remove();
}

$(function(){
    $('#sDate').datepicker();
    $('#deadline').datepicker();
    $('#rDate').datepicker();
    id2proc['rAct'] = '/organization/query_inner_id';
});

function checkOnly(act){//檢核檔號及發文字號是否唯一
    var checkNull;
    var checkValue;
    var InputFiled;
    if(act =='did'){
        if($("[name='did']").val() == ""){ 
            return false;
        }else{
            checkNull  = 'true';
            checkValue = $("[name='did']").val();
            InputFiled = document.querySelector('#did');
        }
    }else if(act =='dNo'){
        if($("[name='dNo']").val()==""){
            return false;
        }else{
            checkNull  = 'true';
            checkValue = $("[name='dNo']").val();
            InputFiled = document.querySelector('#dNo');
        }
    }
    if(checkNull =='true'){
        $.ajax({
            url: 'edis/api.php?act='+act+'&inValue='+checkValue,
            type:"GET",
            dataType:'text',
            success: function(response){
                if (response == 'valid') {
                    InputFiled.setCustomValidity('');
                     console.log('valid');
                }else if (response == 'invalid') {
                    InputFiled.setCustomValidity('需為唯一值');
                     console.log('invalid');
                }
            }
        });
    }
}

function getDeadline(){//檢核收文及發文字號是否唯一
    var act='deadline';
    var checkNull;
    var rDate;
    var checkValue;
    var InputFiled;
    var errMsg;

    if($("[name='rDate']").val() == "" && $("[name='dSpeed']").val() == ""){
        return false;
    }else{
        checkNull ='true';
        rDate =$("[name='rDate']").val();
        checkValue =$("[name='dSpeed']").val(); 
        InputFiled = document.querySelector('#deadline');
        errMsg ='取得期限發生異常';
    }

    if(checkNull =='true'){
        $.ajax({
            url: 'edis/api.php?act='+act+'&rDate='+rDate+'&inValue='+checkValue,
            type:"GET",
            dataType:'text',
            success: function(response){
                if (response !== '') {
                    document.getElementById('deadline').value =response.trim();
                   // InputFiled.setCustomValidity('');
                }else if (response == '') {
                   // InputFiled.setCustomValidity(errMsg);
                }
            }

        });
    }
}

function getodNum(){//取得文件字號    
    var did =$("[name='did']").val();
    if(did.length<=0){
        var fv =document.getElementById('mFile').value;
        var fvid =document.getElementById('mFileId').value;
        var otValue= document.getElementById('mTitle').value;
        var act='odNum';
        var checkNull='';
        var orgid;
        var InputFiled;
        var odType =$("[name='odType']").val();
        var result =false;
        var errMsg ;
        if($("[name='rAct']").val() == "" || $("[name='odType']").val() == "" || $("[name='subjects']").val() == "" || $("[name='rDate']").val() == "" || $("[name='dNo']").val() == "" || $("[name='dSpeed']").val() == "" || $("[name='sDate']").val() == "" || $("[name='deadline']").val() == "" || (fv.length<=0 && fvid.length<=0) || otValue.length == 0 || $("[name='sAct']").val() == "" || $("[name='sId[]']").length == 0 || $("[name='depID[]']").length == 0){
            checkNull ='false';             
            errMsg ='尚有必填欄位未填入。'; 
            alert(errMsg);
        }else{
            checkNull = 'true';
            orgid     = $("[name='rAct']").val();
            orgidRo   = $("[name='roAct']").val(); 
        }

        if(checkNull =='true'){
            result=true;
        }
        return result;
    }
}

//選擇上傳檔案(多個)需檢核有無輸入名稱
function checkMutiAtt(u,f,t){
    //f:input type=file id
    //t:title id
    var fileName =f+'[]';
    var titleName =t+'[]';
    var titleId ='#'+t;
    var uName =u+'[]';
    var fl=document.getElementsByName(fileName);

    for(var i=0;i<fl.length;i++){
        var uv= document.getElementsByName(uName)[i].innerHTML;
        var fv = fl[i].value;
        var otValue= document.getElementsByName(titleName)[i].value;

        if(fv.length>0 || uv.length>0){
            if(otValue.length==0){
                document.querySelectorAll(titleId)[i].setCustomValidity('請填入附件名稱');
            }else{
                document.querySelectorAll(titleId)[i].setCustomValidity('');
            }
        }else{
            document.querySelectorAll(titleId)[i].setCustomValidity('');
        }
    }
}
//選擇上傳檔案(單一)需檢核有無輸入名稱
function checkAloneAtt(u,f,t){
    //u:已上傳檔案名稱span id
    var titleName =t;
    var titleId ='#'+f;
    var uv=document.getElementById(u).innerHTML;
    var fv =document.getElementById(f).value;
    var otValue= document.getElementById(titleName).value;
    if(fv.length>0 || uv.length>0){
        if(otValue.length==0){
            document.querySelector(titleId).setCustomValidity('請填入附件名稱');
        }else{
            document.querySelector(titleId).setCustomValidity('');
        }
    }else{
        document.querySelector(titleId).setCustomValidity('請上傳主文');
    }
}


$("#oFile").live('change', function(){
    checkMutiAtt('oUpload','oFile','oTitle');
})

$("#oTitle").live('input', function(){
    checkMutiAtt('oUpload','oFile','oTitle');
})

$("#mFile").live('change', function(){
    checkAloneAtt('mUpload','mFile','mTitle');
})

$("#mTitle").live('input', function(){
    checkAloneAtt('mUpload','mFile','mTitle');
})

</script>




<style>
    .require{
 color:red;
 }

 </style>



<div class="row-fluid">
    <div class="span12">
        <table width="100%" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; padding:5px">
            <tr><th bgcolor="#e0e0e0">修改<?php  echo $odTypeName?></th></tr>
        </table>
    </div>
</div>
<?php  echo genGroupDom('sIds');  ?>  
<div style="height:5px"></div><div style="border-top:1px #999999 dashed; height=1px;"></div><div style="height:5px"></div>
<?php 
    if($_GET['mode'] == 'revEdit') $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/odfind.php&muID=0&isMenu=1";
    else $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/myod.php&muID=0";
?>

<form id="prjform" action="<?php  echo "edis/rod/editDo.php?odMenu=" . $odMenu?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="<?php  echo $_GET['mode']?>">
<input type="hidden" name="loginType" value="<?php  echo $r['loginType']?>">
<input type="hidden" name="sourceID" value="<?php  echo $edisID?>">
<table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
<tr>
    <td align="right"><span class="require">*</span>收文機關：</td>
    <td align="left">
        <input name="rAct" type="text" class="queryID" id="rAct" data-odtype="<?php echo  $odMenu;?>" value="<?php  echo $rRact['oid']?>" size="6" required ="true" readonly title="<?php  echo $rRact['stitle']?>" onkeypress="return checkInput(event,'depName',this)" />
        <span></span>
        收文單位：
        <input name="roAct" type="text" class="queryID" id="roAct" data-odtype="<?php echo  $odMenu;?>" value="<?php  echo $rROact['oid']?>" size="6" readonly title="<?php  echo $rROact['stitle']?>" onkeypress="return checkInput(event,'depName',this)" />               
    </td>    
    <td align="right"><?php if($_GET['mode']!='copy'){?><span class="require">*</span><?php }?>收文字號：</td>
    <td>
        <input type="text" name="did" id="did" readonly="true" required ="true" oninput="checkOnly('did')" value="<?php  echo $r['did']?>"/>
        <input type="hidden" name="edisId" id="edisId" value="<?php  echo $_REQUEST['id']?>"/>
        <input type="hidden" name="odType" id="odType" value="<?php  echo $r['odType']?>" required ="true" size="6"/>         
        <?php  if ($mobile) { echo '</tr><tr>'; } ?>
</tr>
<tr>
    <td align="right"><span class="require">*</span>來文機關：</td>
    <td align="left"><input name="sAct" type="text" class="queryID" id="sAct" value="<?php  echo $rSact['oid']?>" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true"  readonly title="<?php  echo $rSact['stitle']?>"  onkeypress="return checkInput(event,'depName',this)" /><input name="depID" type="hidden" id="depID" value="<?php  echo $depID?>"/></td>
    <td align="right"><span class="require">*</span>文書承辦日期：</td>
    <td><input type="text" name="rDate" id="rDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" required ="true" value="<?php  echo date_format(date_create($r['rDate']), 'Y/m/d')?>" onchange="getDeadline()" /></td>        
</tr>
<tr>
    <td align="right"><span class="require">*</span>來文字號：</td>
    <td align="left"><input type="text"  name="dNo" id="dNo" value="<?php  echo $r['dNo']?>" required ="true" oninput="checkOnly('dNo')" /></td>        
    <td align="right"><span class="require">*</span>速別：</td>
    <td><select name="dSpeed" id="dSpeed" required ="true"  onchange="getDeadline()"/>
        <?php foreach ($odSpeed as $v) {
        if ($r['dSpeed'] == $v) {
            echo "<option selected='selected'>$v</option>";
        } else {
            echo "<option>$v</option>";
        }
    }?>
    </select>
    </td>

</tr>
<tr>
    <td align="right"><span class="require">*</span>來文日期：</td>
    <td><input type="text" name="sDate" id="sDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" value="<?php  echo date_format(date_create($r['sDate']), 'Y/m/d')?>" required ="true" /></td>        

    <td align="right"><span class="require">*</span>期限：</td>
    <td><input type="text" name="deadline" id="deadline" value="<?php  echo date_format(date_create($r['deadline']), 'Y/m/d')?>" required ="true" /></td>
</tr>
<tr>
    <td align="right">文書：</td>
    <td><input  type="text" name="creatorName" id="creatorName" readonly="true" value="<?php  echo $emplyeeinfo[$r['creator']]?>"/><input type="hidden" name="creator" id="creator" value="<?php  echo $r['creator']?>"></td>         
    <td align="right">計畫編號：</td>
    <td><input type="text" name="planNo" id="planNo" value="<?php  echo $r['planNo']?>" /></td>        
</tr>
</tr>
<tr>
    <td align="right"><span class="require">*</span>主文：</td>
    <td colspan="3"><input name="mTitle" id="mTitle" value="主文" type="hidden" placeholder='請輸入名稱' required ="true" value="主文" /><span id="mUpload" name="mUpload[]"><?php  echo $rMfile['fName']?></span><input name='mFileId' type='hidden' id='mFileId' value="<?php  echo $rMfile['id']?>"/> <input type="file" name="mFile" id="mFile" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf" <?php  echo $rMfile['fName']==''?required:''?> >
    </td>
    
</tr>
<tr>
    <td align="right">其他附件：</td>
    <td colspan="3">
        <div>
            <button type="button" id='addAttBtn'  data-toggle="collapse"  data-target="#demo" >展開</button>
            <div id="demo" class="collapse.show">
            <?php 
            $oCount = 0;
            while ($rOfile = $db->fetch_array($rsOfile)) {
                $oCount++;
                if ($oCount == 1) {
                    if($_GET['mode'] == 'copy'){
                        echo "<input name='oTitle[]' id='oTitle' value='" . $rOfile['title'] . "'type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span><input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span>　<span><input name='delFile' type='checkbox' value='" . $rOfile['id'] . "'>刪除</span><div id='oTitleDiv'>";
                    }else{
                        echo "<input name='oTitle[]' id='oTitle' value='" . $rOfile['title'] . "'type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span><input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span> <div id='oTitleDiv'>";
                    }
                    

                } else if ($oCount > 1) {

                    echo "<div><input name='oTitle[]' id='oTitle'  value ='" . $rOfile['title'] . "' placeholder='請輸入名稱' type='text'><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span><input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
                }

            }

            if ($oCount == 0) {
                echo "<input name='oTitle[]' id='oTitle' type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'></span>
                    <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span>
                        <div id='oTitleDiv'>
                        </div>";
                //add by tina 180420 修改公文時如已有其他附件則展開div
                echo '<script type="text/javascript">',
                'document.addEventListener("DOMContentLoaded", function() {',
                    'document.getElementById("demo").setAttribute("class", "collapse");
                    });</script>';
            }else{
                echo "</div>";
            }
            ?>
            </div>
        </div>
    </td>
</tr>
<tr>
    <td align="right"><span class="require">*</span>主旨：</td>
    <td colspan="3"><textarea name="subjects" id="subjects" rows="3" style="width:85%" required ="true"><?php  echo $r['subjects']?></textarea></td>
</tr>
<tr>
    <td align="right">備註：</td>
    <td colspan="3"><textarea name="note" id="note" rows="3" style="width:85%" ><?php  echo $r['note']?></textarea>
    </td>
</tr>
<tr>

<td align="right"><span class="require">*</span>承辦人：</td>
<td>
    <?php  if($_GET['mode'] !== 'revEdit'){ ?>
        <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_sIds');" value="群組">
        <span class='sBtn' onclick="addItem(this,'sId','sIds')"> +新增承辦</span>
    <?php  } ?>
    <?php 
    $sCount = 0;
    while ($rSign = $db->fetch_array($rsSign)) {
        // var_dump($rSign);exit;
        $sCount++;

        if ($sCount == 1) {
            if($_GET['mode'] === 'revEdit'){
                echo "<input name='sId[]' type='text' id='sIds' required value='".$rSign['signMan']."' size='6' readonly>".$emplyeeinfo[$rSign['signMan']];
            } else{
                echo "<div id='sIdsDiv'><div><input name='sId[]' type='text' class='queryID' id='sIds' required value='".$rSign['signMan']."' size='6' title='".$emplyeeinfo[$rSign['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\" ><input name='sidId[]' type='hidden' id='sidId' value='" . $rSign['id'] . "'/><span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
            }
        } else if ($sCount > 1) {
            if($_GET['mode'] === 'revEdit'){
                echo "<div><input name='sId[]' type='text' id='sIds' required value='".$rSign['signMan']."' size='6' readonly>".$emplyeeinfo[$rSign['signMan']]."</div>";
            } else{
                echo "<div><input name='sId[]' type='text' class='queryID' id='sIds' required value='".$rSign['signMan']."' size='6' title='".$emplyeeinfo[$rSign['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\"> <input name='sidId[]' type='hidden' id='sidId' value='" . $rSign['id'] . "'/> ". "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
            }
        }
    }
    echo '</div>';
    if ($sCount == 0) {
        echo "<div id='sIdsDiv'><div><input name='sId[]' type='text' class='queryID' id='sIds' value='".$empID."' title='".$emplyeeinfo[$empID]."' required onkeypress=\"return checkInput(event,'empName',this)\" size='6'> <input name='sidId[]' type='hidden' id='sidId' value=''><span class='sBtn' onClick='removeItem(this)'> -移除</span>
            </div></div>";

    }

    ?>

    </td>            
    <td align="right"><span class="require">*</span>承辦部門：</td>
        <td align="left">
            <span class="sBtn" onclick="addItem(this,'depID','admDeps')"> +新增承辦部門</span>
            <?php 
                $i = 1;
                while($rDep = $db->fetch_array($rsDep)){
                    if($i==1){
                        echo '<div id="admDepsDiv" style="margin-top: 7px;"><div><input name="depID[]" required ="true" readonly class="queryID" id="admDeps" type="text" size="6" onkeypress="return checkInput(event,\'depID\',this)" value="'.$rDep['depID'].'" title="'.$depAdminfo[$rDep['depID']].'" />';
                        // echo '<span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'depID\',\'admDeps\')"> +新增承辦部門</span>';
                        echo '<span class="sBtn" id="admDepRemove" onclick="removeItem(this)"> -移除</span></div>';
                    }else{
                        echo '<div><input name="depID[]" required ="true" readonly class="queryID" id="admDeps" type="text" size="6" onkeypress="return checkInput(event,\'depID\',this)" value="'.$rDep['depID'].'" title="'.$depAdminfo[$rDep['depID']].'" />';
                        echo '<span class="sBtn" id="admDepRemove" onclick="removeItem(this)"> -移除</span></div>';
                    }
                    
                    if($i == $count) echo '</div>';
		    $i++;
                }
            ?>
            
        </td>
</tr>    
<tr>
    <td align="right">承辦人主管：</td>
    <td align="left" colspan="3">
        <div id='sidMgrDiv'>
        <?php 
        $sCount = 0;
        while ($rSignMgr = $db->fetch_array($rsSignMgr)) {
            // var_dump($rSignMgr);exit;
            $sCount++;
            if ($sCount == 1) {
                if($_GET['mode'] === 'revEdit'){
                    echo "<input name='sidMgr[]' type='text' required id='sidMgr' value='".$rSignMgr['signMan']."' size='6' readonly>".$emplyeeinfo[$rSignMgr['signMan']];
                } else{
                    echo "<input name='sidMgr[]' type='text' class='queryID' id='sidMgr' value='".$rSignMgr['signMan']."' size='6' title='".$emplyeeinfo[$rSignMgr['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\" ><span class='sBtn' onclick=\"addItem(this,'sidMgr','sidMgr')\"> +新增</span>";
                }
            } else if ($sCount > 1) {
                if($_GET['mode'] === 'revEdit'){
                    echo "<div><input name='sidMgr[]' type='text' required id='sidMgr' value='".$rSignMgr['signMan']."' size='6' readonly>".$emplyeeinfo[$rSignMgr['signMan']]."</div>";
                } else{
                    echo "<div><input name='sidMgr[]' type='text' class='queryID' id='sidMgr' value='".$rSignMgr['signMan']."' size='6' title='".$emplyeeinfo[$rSignMgr['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\"><span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
                }
            }
        }
        if ($sCount == 0) {
            echo "<input name='sidMgr[]' type='text' class='queryID' id='sidMgr' value='' size='6' title='' onkeypress=\"return checkInput(event,'empName',this)\"><span class='sBtn' onclick=\"addItem(this,'sidMgr','sidMgr')\"> +新增</span>";
        }
        ?>
        </div>
    </td>
</tr>
<tr>
    <td colspan="4" align="center">
        <?php  if($_GET['mode'] != 'copy'){ ?>
            <input type="submit" name="Submit" id="Submit" onclick="return getodNum();"  value="送出" />
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>'">
        <?php  }else{ ?>
            <input type="hidden" name="mode" value="copy">
            <input type="submit" name="Submit" id="Submit" onclick="return getodNum();"  value="送出" />
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>&copy=cancel&id=<?php  echo $_GET['id']?>'">
        <?php  } ?>
    </td>
</tr>
</table>
</form>
