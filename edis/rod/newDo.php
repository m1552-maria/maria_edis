<?php 
@session_start();
if ($_REQUEST['Submit']) {

    include_once "../../config.php";
    include_once "../../system/db.php";
    include_once "../../getEmplyeeInfo.php";
    include_once "../inc_vars.php";
    include_once "../func.php";

    //insert 公文主檔
    $did = getodNum('收文',$_REQUEST['rAct'],$_REQUEST['roAct'],$_REQUEST['sDate']);

    $db                        = new db('edis');
    $db->row['did']            = "'" . $did . "'";
    $db->row['odType']         = "'" . $_REQUEST['odType'] . "'";
    $db->row['creator']        = "'" . $_REQUEST['creator'] . "'";
    $db->row['rDate']          = "'" . $_REQUEST['rDate'] . "'";
    $db->row['dNo']            = "'" . $_REQUEST['dNo'] . "'";
    $db->row['dSpeed']         = "'" . $_REQUEST['dSpeed'] . "'";
    $db->row['sDate']          = "'" . $_REQUEST['sDate'] . "'";
    $db->row['deadline']       = "'" . $_REQUEST['deadline'] . "'";
    $db->row['subjects']       = "'" . addslashes($_REQUEST['subjects']) . "'";
    $db->row['note']           = "'" . $_REQUEST['note'] . "'";
    $db->row['planNo']         = "'" . $_REQUEST['planNo'] . "'";
    $db->row['loginType']      = "'" . $_REQUEST['loginType'] . "'";
    $db->row['signStage']      = "'1'";
    $db->insert();
    $edisId = $db->lastInsertId;

    //insert DepID 2021/03/31
    foreach ($_REQUEST['depID'] as $key => $value) {
        $op = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );

        $db = new db($op);
        $sql2  = "select depTitle from department where depID = '".$value."'";
        $rs2   = $db->query($sql2);
        $r2    = $db->fetch_array($rs2);
        $title = $r2['depTitle'];
        $db->close();

        $db               = new db('map_dep');
        $db->row['eid']   = $edisId;
        $db->row['depID'] = "'" . $value . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->insert();
        $db->close();
    }
    
    //insert 收文機關
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();

    $db             = new db('map_orgs');
    $db->row['eid'] = $edisId;
    $db->row['oid'] = "'" . $_REQUEST['rAct'] . "'";
    $db->row['act'] = "'" . 'R' . "'";
    $db->row['title'] = "'" .  $r['title'] . "'";
    $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
    $db->insert();
    $rActId = $db->lastInsertId;

    //insert 收文單位
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['roAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();

    if(strlen($_REQUEST['roAct'])>0){
        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['roAct'] . "'";
        $db->row['act'] = "'" . 'RO' . "'";
        $db->row['title'] = "'" .  $r['title'] . "'";
        $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
        $db->insert();
        $roActId = $db->lastInsertId; 
    }   

    //insert 來文機關
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();
    
    $db             = new db('map_orgs');
    $db->row['eid'] = $edisId;
    $db->row['oid'] = "'" . $_REQUEST['sAct'] . "'";
    $db->row['act'] = "'" . 'S' . "'";
    $db->row['title'] = "'" .  $r['title'] . "'";
    $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
    $db->insert();

    //insert承辦人(單或多組)
    $pcount = -1;
    foreach ($_REQUEST['sId'] as $k => $v) {
        $pcount++;
        //承辦人
        $db                    = new db('map_orgs_sign');
        $db->row['edisid']     = $edisId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '0' . "'";
        $db->row['signLevel']  = "'" . '1' . "'";
        $db->row['signMan']    = "'" . str_pad(trim($_REQUEST['sId'][$k]), 5, "0", STR_PAD_LEFT) . "'";
        $db->insert();
        $sidManId = $db->lastInsertId;
    }

    foreach ($_REQUEST['sidMgr'] as $k => $v) {
        $pcount++;
        if(empty($_REQUEST['sidMgr'][$k]) || $_REQUEST['sidMgr'][$k] == '') continue;
        //承辦人主管
        $db                    = new db('map_orgs_sign');
        $db->row['edisid']     = $edisId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '0' . "'";
        $db->row['signLevel']  = "'" . '2' . "'";
        $db->row['signMan']    = "'" . str_pad(trim($_REQUEST['sidMgr'][$k]), 5, "0", STR_PAD_LEFT) . "'";
        $db->insert();
        $sidManId = $db->lastInsertId;
        // //簽核階層
    }
    //insert 歸檔
    //取得收文機關之總文書
    if($_REQUEST['rAct'] == $_REQUEST['roAct'] || empty($_REQUEST['roAct'])) $rActMain = $_REQUEST['rAct'];
    else $rActMain = $_REQUEST['roAct'];

    $rRevSql = "select *  from  organization where id=" . "'" . $rActMain . "' and orgType='SELF' ";
    $rsRrev  = $db->query($rRevSql);
    if ($rsRrev) {
        $rRrev = $db->fetch_array($rsRrev);
    }
    if (!empty($rRrev['mainRevMan'])) {
        //insert收文機關之總文書
        $db                    = new db('map_orgs_sign');
        $db->row['edisid']     = $edisId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '1' . "'";
        $db->row['signLevel']  = "'" . '4' . "'";
        $db->row['signMan']    = "'" . str_pad($rRrev['mainRevMan'], 5, "0", STR_PAD_LEFT) . "'";
        $db->insert();
        $revManId = $db->lastInsertId;
    }

    //處理主文附件
    if ($_FILES['mFile']['tmp_name'] != "") {
        if (!empty($_REQUEST['mTitle'])) {
            //名稱已填寫insert 主文附件
            $ulpath = '../../data/edis/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $ulpath = '../../data/edis/' . $edisId . '/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $ulpath = '../../data/edis/' . $edisId . '/' . 'main/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $srcfn = '';
            $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);

            $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);
            if ($rzt) {
                $sLocation = strripos($srcfn, '/') + 1;
                $length    = strlen($srcfn) - strripos($srcfn, '/');
                $fileName  = substr($srcfn, $sLocation, $length);

                //insert 主文附件
                $db               = new db('map_files');
                $db->row['eid']   = $edisId;
                $db->row['aType'] = "'" . 'M' . "'";
                $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                $db->row['fName'] = "'" . $fileName . "'";
                $db->insert();

            } else {
                echo '主文上傳作業失敗！';exit;
            }
        }
    }

    //處理其他附件
    foreach ($_FILES['oFile']['name'] as $k => $v) {
        if ($_FILES['oFile']['tmp_name'][$k] != "") {
            if (!empty($_REQUEST['oTitle'][$k])) {
                //名稱已填寫 新增上傳資料
                $ulpath = '../../data/edis/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $ulpath = '../../data/edis/' . $edisId . '/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $ulpath = '../../data/edis/' . $edisId . '/' . 'others/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $srcfn = '';
                $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);

                if ($rzt) {
                    //insert 其他附件
                    $sLocation = strripos($srcfn, '/') + 1;
                    $length    = strlen($srcfn) - strripos($srcfn, '/');
                    $fileName  = substr($srcfn, $sLocation, $length);

                    $db               = new db('map_files');
                    $db->row['eid']   = $edisId;
                    $db->row['aType'] = "'" . 'O' . "'";
                    $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                    $db->row['fName'] = "'" . $fileName . "'";
                    $db->insert();
                } else {
                    echo '其他附件上傳作業失敗！';exit;
                }
            }
        }
    }

    
    $loc="Location: /index.php?funcUrl=edis/rod/voldone.php&muID=0&did=".$did;
    if($_SESSION["loginType"]=='vol'){
        header($loc);
    }else{
        header("Location: /index.php?funcUrl=edis/rod/new.php&muID=0&did=".$did);    
    }

} else {
    if($_SESSION["loginType"]=='vol'){
        header( $loc);  
    }else{
        header("Location: /index.php?funcUrl=edis/rod/new.php&muID=0&did=".$did);    
    }

}
