<?php 
@session_start();

if ($_REQUEST['Submit']) {
    $edit_edis_id = $_REQUEST['edisId'];
    include "../../config.php";
    include "../../system/db.php";
    include "../../getEmplyeeInfo.php";
    include "../../setting.php";
    include_once "../../inc_vars.php";
    
    include_once "../func.php";

    $db = new db();
    //如果編輯 就直接update 不跑其他東西
    if($_REQUEST['mode'] == 'revEdit'){
        if(is_array($_REQUEST['depID'])){
            $depID = join(',', $_REQUEST['depID']);
        }else{
            $depID = $_REQUEST['depID'];
        }

        $sql = "UPDATE edis SET 
                `dNo` = '" . $_REQUEST['dNo'] . "',
                `sDate`    = '" . $_REQUEST['sDate'] . "',
                `rDate`    = '" . $_REQUEST['rDate'] . "',
                `dSpeed`   = '" . $_REQUEST['dSpeed'] . "',
                `deadline` = '" . $_REQUEST['deadline'] . "',
                `planNo`   = '" . $_REQUEST['planNo'] . "',
                `subjects` = '" . addslashes($_REQUEST['subjects']) . "',
                `note`     = '" . $_REQUEST['note'] . "'
                WHERE `id` = '" . $edit_edis_id . "'";

        $db->query($sql);

        //刪除公文承辦部門
        $sql = "delete from map_dep where eid = $edit_edis_id";
        $db->query($sql);

        //insert DepID 2021/03/31
        foreach ($_REQUEST['depID'] as $key => $value) {
            $op = array(
                'dbSource'   => DB_ADMSOURCE,
                'dbAccount'  => DB_ACCOUNT,
                'dbPassword' => DB_PASSWORD,
                'tableName'  => '',
            );

            $db = new db($op);
            $sql2 = "select depTitle from department where depID = '".$value."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['depTitle'];
            $db->close();

            $db               = new db('map_dep');
            $db->row['eid']   = $edit_edis_id;
            $db->row['depID'] = "'" . $value . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->insert();
            $db->close();
        }
        
        $db = new db();
        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['rAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "' 
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'R'";
        $db->query($sql);

        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['roAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['roAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "' 
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'RO'";   
        $db->query($sql);

        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['sAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "'  
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'S'";
        $db->query($sql);

        //主文附件
        if ($_FILES['mFile']['tmp_name'] != "") {
            //有map_file.id
            if (!empty($_REQUEST['mFileId'])) {
                $mSql = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='M'";
                $rsM  = $db->query($mSql);
                $rM   = $db->fetch_array($rsM);
                var_dump($rM);
                $fileOldName = substr($rM['fName'], 0, strripos($rM['fName'], '.'));

                $fMain = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/main/';
                $fn    = $fMain . $rM['fName'];
                //名稱已填寫
                if (!empty($_REQUEST['mTitle'])) {
                //update 主文附件

                    //刪除原上傳資料
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }
                    //更新上傳資料
                    $fileNewName = $fileOldName . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                    $srcfn       = $fMain . $fileNewName;
                    echo $srcfn;
                    $rzt         = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);
                    if ($rzt) {
                    //更新資料表 map_files
                        $mUSql = "update  map_files set "
                            . "title='" . $_REQUEST['mTitle'] . "',"
                            . "fName='" . $fileNewName . "'"
                            . "where id ='" . $_REQUEST['mFileId'] . "'";

                        $db->query($mUSql);
                    } else {
                        echo '主文附件上傳作業失敗！';exit;
                    }
                } else {
                // 名稱未填寫 delete 主文附件
                    //刪除原上傳資料
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }
                    //刪除資料表 map_files
                    $mDSql = "delete from map_files "
                        . "where id ='" . $_REQUEST['mFileId'] . "'";
                    $db->query($mDSql);
                }

            } else {
                //無map_file.id
                if (!empty($_REQUEST['mTitle'])) {
                    //名稱已填寫insert 主文附件
                    $ulpath = '../../data/edis/';
                    if (!file_exists($ulpath)) {
                        mkdir($ulpath);
                    }

                    $ulpath = '../../data/edis/' . $edit_edis_id . '/';
                    if (!file_exists($ulpath)) {
                        mkdir($ulpath);
                    }

                    $ulpath = '../../data/edis/' . $edit_edis_id . '/' . 'main/';
                    if (!file_exists($ulpath)) {
                        mkdir($ulpath);
                    }

                    $srcfn = '';
                    $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                    $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

                    if ($rzt) {
                        $sLocation = strripos($srcfn, '/') + 1;
                        $length    = strlen($srcfn) - strripos($srcfn, '/');
                        $fileName  = substr($srcfn, $sLocation, $length);

                        //insert 主文附件
                        $db               = new db('map_files');
                        $db->row['eid']   = $edit_edis_id;
                        $db->row['aType'] = "'" . 'M' . "'";
                        $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                        $db->row['fName'] = "'" . $fileName . "'";
                        $db->insert();

                    } else {
                        echo '主文附件上傳作業失敗！';exit;
                    }
                } else {
                    //名稱未填寫
                    echo '名稱未填寫,主文附件上傳作業失敗！';exit;
                }
            }
        } else {
            //無上傳新檔案
            if (!empty($_REQUEST['mFileId'])) {
                if (!empty($_REQUEST['mTitle'])) {
                    //名稱非空值則更新名稱
                    $mUSql = "update  map_files set "
                        . "title='" . $_REQUEST['mTitle'] . "'"
                        . "where id ='" . $_REQUEST['mFileId'] . "'";

                    $db->query($mUSql);

                } else {
                    //名稱為空值則刪除上傳資料
                    $mSql  = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='M'";
                    $rsM   = $db->query($mSql);
                    $rM    = $db->fetch_array($rsM);
                    $fMain = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/main/';
                    $fn    = $fMain . $rM['fName'];

                    //刪除原上傳資料
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                    //刪除資料表 map_files
                    $mDSql = "delete from map_files "
                        . "where id ='" . $_REQUEST['mFileId'] . "'";
                    $db->query($mDSql);

                }
            }
        }

        //其他附件
        $oFileOldSql = "select mf.*  from map_files mf where  mf.eid = " . $edit_edis_id . " and mf.aType ='O' ";
        $rsFileOld   = $db->query($oFileOldSql);
        //delete
        while ($rFileOld = $db->fetch_array($rsFileOld)) {
            $exist = 0;
            foreach ($_REQUEST['oFileId'] as $k => $v) {

                if ($rFileOld['id'] == $v) {

                    $exist = 1;
                    break;

                }

            }
            if ($exist == 0) {
                //delete 畫面上已移除資料

                $fOthers = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                $fn      = $fOthers . $rFileOld['fName'];

                //刪除原上傳資料
                if (file_exists($fn)) {
                    @unlink($fn);
                }
                //刪除資料表 map_files
                $oFileDelSql = "delete from  map_files "
                    . "where id ='" . $rFileOld['id'] . "'";
                $db->query($oFileDelSql);

            }
        }

        foreach ($_FILES['oFile']['name'] as $k => $v) {
            if ($_FILES['oFile']['tmp_name'][$k] != "") {
                //有上傳新資料
                if (!empty($_REQUEST['oFileId'][$k])) {
                    //有map_files.id
                    $ofID        = $_REQUEST['oFileId'][$k];
                    $oSql        = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='O' and id ='" . $ofID . "' ";
                    $rsO         = $db->query($oSql);
                    $rO          = $db->fetch_array($rsO);
                    $fOthers     = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                    $fn          = $fOthers . $rO['fName'];
                    $fileOldName = substr($rO['fName'], 0, strripos($rO['fName'], '.'));
                    if (!empty($_REQUEST['oTitle'][$k])) {

                    //名稱已填寫 更新上傳資料
                        // echo '刪除 更新上傳資料:' . $fn . ',name:' . $ofID;
                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //更新上傳資料
                        $fileNewName = $fileOldName . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                        $srcfn       = $fOthers . $fileNewName;
                        //echo '更新上傳資料路徑:' . $srcfn;
                        $rzt = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                        if ($rzt) {
                            //更新資料表 map_files
                            $oUSql = "update  map_files set "
                                . "title='" . $_REQUEST['oTitle'][$k] . "',"
                                . "fName='" . $fileNewName . "'"
                                . "where id ='" . $ofID . "'";

                            $db->query($oUSql);
                        } else {
                            echo '其他附件上傳作業失敗1！';exit;
                        }
                    } else {
                        //名稱未填寫 刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }
                        //刪除資料表 map_files
                        $oDSql = "delete from map_files "
                            . "where id ='" . $ofID . "'";
                        $db->query($oDSql);
                    }
                } else {
                    //無map_files.id
                    if (!empty($_REQUEST['oTitle'][$k])) {
                        //名稱已填寫 新增上傳資料
                        $ulpath = '../../data/edis/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edit_edis_id . '/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edit_edis_id . '/' . 'others/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $srcfn = '';
                        $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                        $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                        if ($rzt) {
                            //insert 其他附件
                            // echo ' ofile_name:' . $_FILES['oFile']['name'][$k];
                            // echo ' ofile_title:' . $_REQUEST['oTitle'][$k];

                            $sLocation = strripos($srcfn, '/') + 1;
                            $length    = strlen($srcfn) - strripos($srcfn, '/');
                            $fileName  = substr($srcfn, $sLocation, $length);

                            $db               = new db('map_files');
                            $db->row['eid']   = $edit_edis_id;
                            $db->row['aType'] = "'" . 'O' . "'";
                            $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                            $db->row['fName'] = "'" . $fileName . "'";
                            $db->insert();
                        } else {
                            echo '其他附件上傳作業失敗2！';exit;
                        }
                    } else {
                    //名稱未填寫 none
                    }
                }

            } else {
                //無上傳新資料
                if (!empty($_REQUEST['oFileId'][$k])) {
                    //有map_files.id
                    $ofID = $_REQUEST['oFileId'][$k];

                    if (!empty($_REQUEST['oTitle'][$k])) {
                        //名稱已填寫 更新map_files 名稱
                        $oUSql = "update  map_files set "
                            . "title='" . $_REQUEST['oTitle'][$k] . "'"
                            . "where id ='" . $ofID . "'";
                        $db->query($oUSql);

                    } else {
                        //名稱未填寫 刪除上傳資料
                        $oSql    = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='O' and id ='" . $ofID . "' ";
                        $rsO     = $db->query($oSql);
                        $rO      = $db->fetch_array($rsO);
                        $fOthers = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                        $fn      = $fOthers . $rO['fName'];

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $oDSql = "delete from map_files "
                            . "where id ='" . $ofID . "'";
                        $db->query($oDSql);
                    }
                }
            }
        }

        if ($_REQUEST['odMenu'] == 'rod') {
            header("Location: /index.php?funcUrl=edis/rod/odfind.php&muID=0&isMenu=1");
        }
    }else{
        //簽核否決後修改,複製簽收狀況資料至失效歷程資料 add 20180514
        //簽核否決數量
        if($_REQUEST['mode'] != 'copy'){
            $signSql = "select count(1) num from map_orgs_sign where edisid ='" . $edit_edis_id . "' and  isSign='0'";
            $rsSign  = $db->query($signSql);
            if ($rsSign) {
                $rSign = $db->fetch_array($rsSign);
            }
            if ($rSign['num'] > 0) {
                $versionSql = "select ifnull(max(version),0) num from sign_history where  edisid ='" . $edit_edis_id . "'";
                $rsVersion  = $db->query($versionSql);
                if ($rsVersion) {
                    $rVersion   = $db->fetch_array($rsVersion);
                    $nowVersion = $rVersion['num'] + 1;
                }
                if (!empty($rVersion)) {
                    //複製至失效歷程資料
                    $copySignSql = "set @row_num = 0; insert into sign_history(
                                        `recno`,
                                        `signid`,
                                        `edisid`,
                                        `version`,
                                        `disableTime`,
                                        `moid`,
                                        `signLevel`,
                                        `isOriginal`,
                                        `isWait`,
                                        `signMan`,
                                        `signTime`,
                                        `signState`,
                                        `signContent`,
                                        `isSign`,
                                        `agent`
                                    )
                                SELECT @row_num := @row_num + 1 as recno ,
                                    al.`id`,
                                    al.`edisid`," .
                                    $nowVersion . ",
                                    now(),
                                    al.`moid`,
                                    al.`signLevel`,
                                    al.`isOriginal`,
                                    al.`isWait`,
                                    al.`signMan`,
                                    al.`signTime`,
                                    al.`signState`,
                                    al.`signContent`,
                                    al.`isSign`,
                                    al.`agent`
                                FROM (SELECT *
                                    FROM (  SELECT mos.*
                                        FROM map_orgs_sign mos
                                            WHERE edisid = '" . $edit_edis_id . "' AND ifnull(signlevel, 0) != '".$rodLastLevel."'
                                            ORDER BY FIND_IN_SET(id, getEdisChildList(edisid)) DESC) d1
                                        UNION ALL
                                    SELECT *
                                    FROM (  SELECT mos.*
                                        FROM map_orgs_sign mos
                                        WHERE edisid = '" . $edit_edis_id . "' AND ifnull(signlevel, 0) = '".$rodLastLevel."'
                                        ORDER BY FIND_IN_SET(id, getEdisChildList(edisid))) d2) al";
                    $db->query($copySignSql);
                }
            }
        
            //刪除舊收文資料
            //公文主檔
            $sql = "select * from edis where id=" . $edit_edis_id;
            $rs  = $db->query($sql);
            if ($rs) {
                $r = $db->fetch_array($rs);
            }
            $otype = $r['odType'];
            $id    = $edit_edis_id;

            //刪除公文承辦部門
            $sql = "delete from map_dep where eid =$id";
            $db->query($sql);
            //刪除公文組織機關關連檔
            $sql = "delete from map_orgs where eid =$id";
            $db->query($sql);     
            //刪除公文簽核關連檔
            $sql = "delete from map_orgs_sign where edisid =$id";
            $db->query($sql);
            //刪除設定機密之公文,可查看人員
            $sql = "delete from secret_members where edisid =$id";
            $db->query($sql);    
            //刪除公文主檔
            $sql = "delete from edis where id=$id";
            $db->query($sql);
            //刪除舊收文資料 end
        } 

        //新增修改後收文資料
        //insert 公文主檔
        $db                   = new db('edis');
        if($_REQUEST['mode'] != 'copy'){
            $db->row['id']    = "'" . $edit_edis_id . "'";
            $db->row['did']   = "'" . $_REQUEST['did'] . "'";
        }else{
            $did = getodNum('收文',$_REQUEST['rAct'],$_REQUEST['roAct'],$_REQUEST['sDate']);
            $db->row['did']   = "'" . $did . "'";
        }

        $db->row['odType']    = "'" . $_REQUEST['odType'] . "'";
        //$db->row['rSignType']   = "'" . $_REQUEST['rSignType'] . "'";
        //$db->row['sId']            = "'" . $_REQUEST['sId'] . "'";     
        //$db->row['odDeadlineType']    = "'" . $_REQUEST['odDeadlineType'] . "'";
        $db->row['creator']  = "'" . $_REQUEST['creator'] . "'"; 
        $db->row['rDate']  = "'" . $_REQUEST['rDate'] . "'";
        $db->row['dNo']      = "'" . $_REQUEST['dNo'] . "'"; 
        $db->row['dSpeed']   = "'" . $_REQUEST['dSpeed'] . "'";
        $db->row['sDate']    = "'" . $_REQUEST['sDate'] . "'";                  
        $db->row['deadline'] = "'" . $_REQUEST['deadline'] . "'"; 
        $db->row['subjects'] = "'" . addslashes($_REQUEST['subjects']) . "'";      
        $db->row['note'] = "'" . $_REQUEST['note'] . "'";
        $db->row['planNo'] = "'" . $_REQUEST['planNo'] . "'";
        $db->row['loginType']      = "'" . $_REQUEST['loginType'] . "'";
        $db->row['signStage']      = "'1'";
        $db->insert();
        
        if($_REQUEST['mode'] != 'copy'){
            $edisId = $edit_edis_id;
        }else{
            $edisId = $db->lastInsertId;
        }

        //insert DepID 2021/03/31
        foreach ($_REQUEST['depID'] as $key => $value) {
            $op = array(
                'dbSource'   => DB_ADMSOURCE,
                'dbAccount'  => DB_ACCOUNT,
                'dbPassword' => DB_PASSWORD,
                'tableName'  => '',
            );

            $db = new db($op);
            $sql2 = "select depTitle from department where depID = '".$value."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['depTitle'];
            $db->close();

            $db               = new db('map_dep');
            $db->row['eid']   = $edisId;
            $db->row['depID'] = "'" . $value . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->insert();
            $db->close();
        }


        //insert 收文機關
        $db = new db();
        $sql = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $title = $r['title'];
        $sTitle = $r['sTitle'];
        $db->close();

        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['rAct'] . "'";
        $db->row['act'] = "'" . 'R' . "'";
        $db->row['title'] = "'" .  $r['title'] . "'";
        $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
        $db->insert();
        $rActId = $db->lastInsertId;


        //insert 收文單位
        if(strlen($_REQUEST['roAct'])>0){
            $db = new db();
            $sql = "select title, sTitle from organization where id = '".$_REQUEST['roAct']."'";
            $rs  = $db->query($sql);
            $r   = $db->fetch_array($rs);
            $title = $r['title'];
            $sTitle = $r['sTitle'];
            $db->close();

            $db             = new db('map_orgs');
            $db->row['eid'] = $edisId;
            $db->row['oid'] = "'" . $_REQUEST['roAct'] . "'";
            $db->row['act'] = "'" . 'RO' . "'";
            $db->row['title'] = "'" .  $r['title'] . "'";
            $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
            $db->insert();
            $roActId = $db->lastInsertId; 
        }       

        //insert 來文機關
        $db = new db();
        $sql = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $title = $r['title'];
        $sTitle = $r['sTitle'];
        $db->close();

        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['sAct'] . "'";
        $db->row['act'] = "'" . 'S' . "'";
        $db->row['title'] = "'" .  $r['title'] . "'";
        $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
        $db->insert();

        //insert承辦人(單或多組)
        $pcount = -1;
        foreach ($_REQUEST['sId'] as $k => $v) {
            $pcount++;
            //承辦人
            $db                    = new db('map_orgs_sign');
            $db->row['edisid']     = $edisId;
            $db->row['isOriginal'] = "'" . '1' . "'";
            $db->row['isWait']     = "'" . '1' . "'";
            $db->row['signLevel']  = "'" . '1' . "'";
            $db->row['signMan']    = "'" . trim($_REQUEST['sId'][$k]) . "'";
            $db->insert();
            $sidManId = $db->lastInsertId;
        }

        //承辦人主管
        foreach ($_REQUEST['sidMgr'] as $k => $v) {
            $pcount++;
            if(empty($_REQUEST['sidMgr'][$k]) || $_REQUEST['sidMgr'][$k] == '') continue;
            
            $db                    = new db('map_orgs_sign');
            $db->row['edisid']     = $edisId;
            $db->row['isOriginal'] = "'" . '1' . "'";
            $db->row['isWait']     = "'" . '0' . "'";
            $db->row['signLevel']  = "'" . '2' . "'";
            $db->row['signMan']    = "'" . trim($_REQUEST['sidMgr'][$k]) . "'";
            $db->insert();
            $sidManId = $db->lastInsertId;
        }
        //insert 歸檔
        //取得收文機關之總文書
        if($_REQUEST['rAct'] == $_REQUEST['roAct'] || empty($_REQUEST['roAct'])) $rActMain = $_REQUEST['rAct'];
        else $rActMain = $_REQUEST['roAct'];

        $rRevSql = "select *  from  organization where id=" . "'" . $rActMain . "' and orgType='SELF' ";
        $rsRrev  = $db->query($rRevSql);
        if ($rsRrev) {
            $rRrev = $db->fetch_array($rsRrev);
        }
        if (!empty($rRrev['mainRevMan'])) {
            //insert收文機關之總文書
            $db                    = new db('map_orgs_sign');
            $db->row['edisid']     = $edisId;
            $db->row['moid']       = $rActId;
            $db->row['isOriginal'] = "'" . '1' . "'";
            $db->row['isWait']     = "'" . '1' . "'";
            $db->row['signLevel']  = "'" . '4' . "'";
            $db->row['signMan']    = "'" . $rRrev['mainRevMan'] . "'";
            $db->insert();
            $revManId = $db->lastInsertId;
        } 

        if($_REQUEST['mode'] == 'copy'){
            $ulpath1 = '../../data/edis/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $ulpath1 = '../../data/edis/' . $edisId . '/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $ulpath1 = '../../data/edis/' . $edisId . '/main/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }
            //主文 複製後 如果沒上傳新檔案
            if ($_FILES['mFile']['tmp_name'] == "") {
                if (!empty($_REQUEST['mFileId'])) {
                    $mSql = "select * from map_files where id ='" . $_REQUEST['mFileId'] . "' and aType ='M'";
                    $rsM  = $db->query($mSql);
                    $rM   = $db->fetch_array($rsM);

                    $extension = explode('.', $rM['fName']);

                    $fMain = $root . $ulpath . '/data/edis/' . $rM['eid'] . '/main/';
                    $fn    = $fMain . $rM['fName'];

                    $CopyFileName = time() . '.' . $extension[1];
                    $fileName = $root . $ulpath . '/data/edis/' . $edisId . '/main/' . $CopyFileName;

                    if (!empty($_REQUEST['mTitle'])) {
                        if (file_exists($fn)) {
                            copy($fn, $fileName);
                        }

                        $db               = new db('map_files');
                        $db->row['eid']   = $edisId;
                        $db->row['aType'] = "'" . 'M' . "'";
                        $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                        $db->row['fName'] = "'" . $CopyFileName . "'";
                        $db->insert();
                    }
                }else {
                    echo '名稱未填寫,主文附件上傳作業失敗！';exit;
                }
            }else{
                $srcfn = $ulpath1 . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

                if ($rzt) {
                    $sLocation = strripos($srcfn, '/') + 1;
                    $length    = strlen($srcfn) - strripos($srcfn, '/');
                    $fileName  = substr($srcfn, $sLocation, $length);
                
                    $db               = new db('map_files');
                    $db->row['eid']   = $edisId;
                    $db->row['aType'] = "'" . 'M' . "'";
                    $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                    $db->row['fName'] = "'" . $fileName . "'";
                    $db->insert();
                }
            }

            //其他附件
            $mSql = "select * from map_files where eid ='" . $_REQUEST['sourceID'] . "' and aType ='O'";
            $rsM  = $db->query($mSql);

            $ulpath1 = '../../data/edis/' . $edisId . '/others/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $rM   = $db->fetch_all($rsM);
            foreach ($rM as $key => $value) {
                if(empty($_FILES['oFile']['tmp_name'][$key]) && $_REQUEST['oFileId'][$key] == $rM[$key]['id'] && $_REQUEST['delFile'] != $rM[$key]['id']){
                    $fMain = $root . $ulpath . '/data/edis/' . $value['eid'] . '/others/';
                    $fn    = $fMain . $value['fName'];

                    $fCopy = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                    $extension = explode('.', $value['fName']);
                    $CopyFileName = time() . "_" . $key . '.' . $extension[1];
                    $fnCopy = $fCopy . $CopyFileName;

                    if (file_exists($fn)) {
                        copy($fn, $fnCopy);
                    }

                    $db               = new db('map_files');
                    $db->row['eid']   = $edisId;
                    $db->row['aType'] = "'" . 'O' . "'";
                    $db->row['title'] = "'" . $_REQUEST['oTitle'][$key] . "'";
                    $db->row['fName'] = "'" . $CopyFileName . "'";
                    $db->insert();
                }
            }

            foreach ($_FILES['oFile']['name'] as $k => $v) {
                if ($_FILES['oFile']['tmp_name'][$k] != "") {
                    $srcfn = $ulpath1 . time() . "_" . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                    $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                    if($rzt){
                        $sLocation = strripos($srcfn, '/') + 1;
                        $length    = strlen($srcfn) - strripos($srcfn, '/');
                        $fileName  = substr($srcfn, $sLocation, $length);

                        $db               = new db('map_files');
                        $db->row['eid']   = $edisId;
                        $db->row['aType'] = "'" . 'O' . "'";
                        $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                        $db->row['fName'] = "'" . $fileName . "'";
                        $db->insert();
                    }
                }
            }
        }else{
            //主文附件
            if ($_FILES['mFile']['tmp_name'] != "") {
                //有map_file.id
                if (!empty($_REQUEST['mFileId'])) {
                    $mSql = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='M'";
                    $rsM  = $db->query($mSql);
                    $rM   = $db->fetch_array($rsM);

                    $fileOldName = substr($rM['fName'], 0, strripos($rM['fName'], '.'));

                    $fMain = $root . $ulpath . '/data/edis/' . $edisId . '/main/';
                    $fn    = $fMain . $rM['fName'];
                    //名稱已填寫
                    if (!empty($_REQUEST['mTitle'])) {
                    //update 主文附件

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }
                        //更新上傳資料
                        $fileNewName = $fileOldName . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                        $srcfn       = $fMain . $fileNewName;
                        $rzt         = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);
                        if ($rzt) {
                        //更新資料表 map_files
                            $mUSql = "update  map_files set "
                                . "title='" . $_REQUEST['mTitle'] . "',"
                                . "fName='" . $fileNewName . "'"
                                . "where id ='" . $_REQUEST['mFileId'] . "'";

                            $db->query($mUSql);
                        } else {
                            echo '主文附件上傳作業失敗！';exit;
                        }
                    } else {
                    // 名稱未填寫 delete 主文附件
                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }
                        //刪除資料表 map_files
                        $mDSql = "delete from map_files "
                            . "where id ='" . $_REQUEST['mFileId'] . "'";
                        $db->query($mDSql);
                    }

                } else {
                    //無map_file.id
                    if (!empty($_REQUEST['mTitle'])) {
                        //名稱已填寫insert 主文附件
                        $ulpath = '../../data/edis/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edisId . '/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edisId . '/' . 'main/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $srcfn = '';
                        $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                        $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

                        if ($rzt) {
                            $sLocation = strripos($srcfn, '/') + 1;
                            $length    = strlen($srcfn) - strripos($srcfn, '/');
                            $fileName  = substr($srcfn, $sLocation, $length);

                            //insert 主文附件
                            $db               = new db('map_files');
                            $db->row['eid']   = $edisId;
                            $db->row['aType'] = "'" . 'M' . "'";
                            $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                            $db->row['fName'] = "'" . $fileName . "'";
                            $db->insert();

                        } else {
                            echo '主文附件上傳作業失敗！';exit;
                        }
                    } else {
                        //名稱未填寫
                        echo '名稱未填寫,主文附件上傳作業失敗！';exit;
                    }
                }
            } else {
                //無上傳新檔案
                if (!empty($_REQUEST['mFileId'])) {
                    if (!empty($_REQUEST['mTitle'])) {
                        //名稱非空值則更新名稱
                        $mUSql = "update  map_files set "
                            . "title='" . $_REQUEST['mTitle'] . "'"
                            . "where id ='" . $_REQUEST['mFileId'] . "'";

                        $db->query($mUSql);

                    } else {
                        //名稱為空值則刪除上傳資料
                        $mSql  = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='M'";
                        $rsM   = $db->query($mSql);
                        $rM    = $db->fetch_array($rsM);
                        $fMain = $root . $ulpath . '/data/edis/' . $edisId . '/main/';
                        $fn    = $fMain . $rM['fName'];

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $mDSql = "delete from map_files "
                            . "where id ='" . $_REQUEST['mFileId'] . "'";
                        $db->query($mDSql);

                    }
                }
            }

            //其他附件
            $oFileOldSql = "select mf.*  from map_files mf where  mf.eid = " . $edisId . " and mf.aType ='O' ";
            $rsFileOld   = $db->query($oFileOldSql);
            //delete
            while ($rFileOld = $db->fetch_array($rsFileOld)) {
                $exist = 0;
                foreach ($_REQUEST['oFileId'] as $k => $v) {

                    if ($rFileOld['id'] == $v) {

                        $exist = 1;
                        break;

                    }

                }
                if ($exist == 0) {
                    //delete 畫面上已移除資料

                    $fOthers = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                    $fn      = $fOthers . $rFileOld['fName'];

                    //刪除原上傳資料
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }
                    //刪除資料表 map_files
                    $oFileDelSql = "delete from  map_files "
                        . "where id ='" . $rFileOld['id'] . "'";
                    $db->query($oFileDelSql);

                }
            }

            foreach ($_FILES['oFile']['name'] as $k => $v) {
                if ($_FILES['oFile']['tmp_name'][$k] != "") {
                    //有上傳新資料
                    if (!empty($_REQUEST['oFileId'][$k])) {
                        //有map_files.id
                        $ofID        = $_REQUEST['oFileId'][$k];
                        $oSql        = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='O' and id ='" . $ofID . "' ";
                        $rsO         = $db->query($oSql);
                        $rO          = $db->fetch_array($rsO);
                        $fOthers     = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                        $fn          = $fOthers . $rO['fName'];
                        $fileOldName = substr($rO['fName'], 0, strripos($rO['fName'], '.'));
                        if (!empty($_REQUEST['oTitle'][$k])) {

                        //名稱已填寫 更新上傳資料
                            // echo '刪除 更新上傳資料:' . $fn . ',name:' . $ofID;
                            //刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }

                            //更新上傳資料
                            $fileNewName = $fileOldName . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                            $srcfn       = $fOthers . $fileNewName;
                            //echo '更新上傳資料路徑:' . $srcfn;
                            $rzt = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                            if ($rzt) {
                                //更新資料表 map_files
                                $oUSql = "update  map_files set "
                                    . "title='" . $_REQUEST['oTitle'][$k] . "',"
                                    . "fName='" . $fileNewName . "'"
                                    . "where id ='" . $ofID . "'";

                                $db->query($oUSql);
                            } else {
                                echo '其他附件上傳作業失敗1！';exit;
                            }
                        } else {
                            //名稱未填寫 刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }
                            //刪除資料表 map_files
                            $oDSql = "delete from map_files "
                                . "where id ='" . $ofID . "'";
                            $db->query($oDSql);
                        }
                    } else {
                        //無map_files.id
                        if (!empty($_REQUEST['oTitle'][$k])) {
                            //名稱已填寫 新增上傳資料
                            $ulpath = '../../data/edis/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $ulpath = '../../data/edis/' . $edisId . '/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $ulpath = '../../data/edis/' . $edisId . '/' . 'others/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $srcfn = '';
                            $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                            $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                            if ($rzt) {
                                //insert 其他附件
                                // echo ' ofile_name:' . $_FILES['oFile']['name'][$k];
                                // echo ' ofile_title:' . $_REQUEST['oTitle'][$k];

                                $sLocation = strripos($srcfn, '/') + 1;
                                $length    = strlen($srcfn) - strripos($srcfn, '/');
                                $fileName  = substr($srcfn, $sLocation, $length);

                                $db               = new db('map_files');
                                $db->row['eid']   = $edisId;
                                $db->row['aType'] = "'" . 'O' . "'";
                                $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                                $db->row['fName'] = "'" . $fileName . "'";
                                $db->insert();
                            } else {
                                echo '其他附件上傳作業失敗2！';exit;
                            }
                        } else {
                        //名稱未填寫 none
                        }
                    }

                } else {
                    //無上傳新資料
                    if (!empty($_REQUEST['oFileId'][$k])) {
                        //有map_files.id
                        $ofID = $_REQUEST['oFileId'][$k];

                        if (!empty($_REQUEST['oTitle'][$k])) {
                            //名稱已填寫 更新map_files 名稱
                            $oUSql = "update  map_files set "
                                . "title='" . $_REQUEST['oTitle'][$k] . "'"
                                . "where id ='" . $ofID . "'";
                            $db->query($oUSql);

                        } else {
                            //名稱未填寫 刪除上傳資料
                            $oSql    = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='O' and id ='" . $ofID . "' ";
                            $rsO     = $db->query($oSql);
                            $rO      = $db->fetch_array($rsO);
                            $fOthers = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                            $fn      = $fOthers . $rO['fName'];

                            //刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }

                            //刪除資料表 map_files
                            $oDSql = "delete from map_files "
                                . "where id ='" . $ofID . "'";
                            $db->query($oDSql);
                        }
                    }
                }
            }
        }

        if($_REQUEST['mgmt']=='1'){
            if($_REQUEST['mode'] == 'copy') header("Location: /index.php?funcUrl=edis/rod/odmgmt.php&muID=0&did=$did");
            else header("Location: /index.php?funcUrl=edis/rod/odmgmt.php&muID=0");
        }else{
            if($_REQUEST['mode'] == 'copy') header("Location: /index.php?funcUrl=edis/rod/myod.php&muID=0&did=$did");
            else header("Location: /index.php?funcUrl=edis/rod/myod.php&muID=0");   
        }
    }
} else {
    if($_REQUEST['mgmt']=='1'){
        header("Location: /index.php?funcUrl=edis/rod/odmgmt.php&muID=0");
    }else{
        header("Location: /index.php?funcUrl=edis/rod/myod.php&muID=0");
    }
}
