<?
include_once 'inc_vars.php';
include_once "$root/system/db.php";
include_once 'getEmplyeeInfo.php';


$empID = $_SESSION['empID'];
$depID = $_SESSION["depID"];
if ($odMenu == 'rod') {
    $odTypeName = '收文';
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';

}
$db    = new db();
//公文主檔
$sql = "select * from edis where id=" . $_REQUEST['id'];
$rs  = $db->query($sql);
if ($rs) {
    $r = $db->fetch_array($rs);
}

//承辦人+承辦主管
$signMgrSql = "SELECT mos.id sMgrid,mos.signMan sMgrMan,sp.parentid id,(select signMan from map_orgs_sign where id =sp.parentid ) signMan FROM map_orgs_sign mos
LEFT JOIN sign_parents sp ON(mos.edisid = sp.edisid  and mos.id = sp.mosid)
where mos.edisid='".$_REQUEST['id']."' and sp.mosid is not null and mos.moid is null and mos.signLevel='1' order by mos.id";

//$rsSignMgr  = $db->query($signMgrSql);

//echo $signMgrSql;

//承辦人(多人)
$signSql = "SELECT mos.id sMgrid,mos.signMan FROM map_orgs_sign mos where mos.edisid='".$_REQUEST['id']."'  and mos.moid is null and mos.signLevel='0' order by mos.id";

$rsSign  = $db->query($signSql);
//echo $signSql;


//來文機關
$sActSql = "select mo.oid,o.title  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $_REQUEST['id'] . "' and mo.act ='S' ";
$rsSact  = $db->query($sActSql);
if ($rsSact) {
    $rSact = $db->fetch_array($rsSact);
}
//收文單位
$rActSql = "select mo.oid,o.title  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $_REQUEST['id'] . "' and mo.act ='R' ";
$rsRact  = $db->query($rActSql);
if ($rsRact) {
    $rRact = $db->fetch_array($rsRact);
}


//主文附件
$mFileSql = "select * from map_files mf where mf.eid = '" . $_REQUEST['id'] . "' and aType='M'";
$rsMfile  = $db->query($mFileSql);
if ($rsMfile) {
    $rMfile = $db->fetch_array($rsMfile);
}

//其他附件
$oFileSql = "select * from map_files mf where mf.eid = '" . $_REQUEST['id'] . "' and aType='O'";
$rsOfile  = $db->query($oFileSql);

?>

<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script>
function addItem(tbtn,fld,qid) {
 if(fld =='oTitle'){
   var newone = "<div><input name='"+fld+"[]' id='"+qid+"' placeholder='請輸入名稱' type='text'/>"
   + " <input type='file' name='oFile[]' id='oFile'/>"
   + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
}else if(fld=='sidMgr'){
    var newone = "<div><input name='sId[]' id='sId' type='text' size='6'/>"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
    +"<input name='"+fld+"[]' id='"+qid+"' type='text' size='6'/>"
    +"<img src='/scripts/form_images/search.png'  align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
    + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";  
     
}else if(fld=='sId'){
    var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\" >"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
    + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
}else{
    var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
    + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
}

 var divTitle ='#'+qid+'Div';
  $(divTitle).append(newone);
}

function removeItem(tbtn) {
      $(tbtn).parent().remove();
   }



$(function(){
    $('#sDate').datepicker();
    $('#deadline').datepicker();
    $('#rDate').datepicker();

    
 });



function checkOnly(act){//檢核檔號及發文字號是否唯一
    var checkNull;
    var checkValue;
    var InputFiled;
    if(act =='did'){
        if($("[name='did']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='did']").val();
        InputFiled = document.querySelector('#did');
    };
}else if(act =='dNo'){
    if($("[name='dNo']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='dNo']").val();
      InputFiled = document.querySelector('#dNo');
   };
}
if(checkNull =='true'){
    $.ajax({
        url: 'edis/api.php?act='+act+'&inValue='+checkValue,
        type:"GET",
        dataType:'text',
        success: function(response){
            //alert(response);
            if (response == 'valid') {
                InputFiled.setCustomValidity('');
                 console.log('valid');
            }else if (response == 'invalid') {
                InputFiled.setCustomValidity('需為唯一值');
                 console.log('invalid');
            }
        }

    });
}
}



function getDeadline(){//檢核收文及發文字號是否唯一
    var act='deadline';
    var checkNull;
    var rDate;
    var checkValue;
    var InputFiled;
    var errMsg;

    if($("[name='rDate']").val()=="" && $("[name='dSpeed']").val()==""){return}else{checkNull ='true'; rDate =$("[name='rDate']").val(); checkValue =$("[name='dSpeed']").val(); 
    InputFiled = document.querySelector('#deadline');
    errMsg ='取得期限發生異常';
    };


if(checkNull =='true'){
    $.ajax({
        url: 'edis/api.php?act='+act+'&rDate='+rDate+'&inValue='+checkValue,
        type:"GET",
        dataType:'text',
        success: function(response){
            if (response !== '') {
                document.getElementById('deadline').value =response.trim();
               // InputFiled.setCustomValidity('');
            }else if (response == '') {
               // InputFiled.setCustomValidity(errMsg);
            }
        }

    });
}
}

//選擇上傳檔案(多個)需檢核有無輸入名稱
function checkMutiAtt(u,f,t){
    //f:input type=file id
    //t:title id
    var fileName =f+'[]';
    var titleName =t+'[]';
    var titleId ='#'+t;
    var uName =u+'[]';
    var fl=document.getElementsByName(fileName);

    for(var i=0;i<fl.length;i++){
     var uv= document.getElementsByName(uName)[i].innerHTML;
     var fv = fl[i].value;
     var otValue= document.getElementsByName(titleName)[i].value;

     if(fv.length>0 || uv.length>0){
     if(otValue.length==0){
      document.querySelectorAll(titleId)[i].setCustomValidity('請填入附件名稱');
     }else{
      document.querySelectorAll(titleId)[i].setCustomValidity('');
     }
   }else{
     document.querySelectorAll(titleId)[i].setCustomValidity('');
   }

  }
}
//選擇上傳檔案(單一)需檢核有無輸入名稱
function checkAloneAtt(u,f,t){
    //u:已上傳檔案名稱span id
    //f:input type=file id
    //t:title id
    var titleName =t;
    var titleId ='#'+f;
    var uv=document.getElementById(u).innerHTML;
    var fv =document.getElementById(f).value;
    var otValue= document.getElementById(titleName).value;
    if(fv.length>0 || uv.length>0){
     if(otValue.length==0){
      document.querySelector(titleId).setCustomValidity('請填入附件名稱');
     }else{
      document.querySelector(titleId).setCustomValidity('');
     }
  }else{
    document.querySelector(titleId).setCustomValidity('請上傳主文');
  }
}





$("#oFile").live('change', function(){

    checkMutiAtt('oUpload','oFile','oTitle');

    })

$("#oTitle").live('input', function(){

    checkMutiAtt('oUpload','oFile','oTitle');

    })

$("#mFile").live('change', function(){

    checkAloneAtt('mUpload','mFile','mTitle');

    })

$("#mTitle").live('input', function(){

    checkAloneAtt('mUpload','mFile','mTitle');

    })

</script>




<style>
    .require{
 color:red;
 }

 </style>



<div class="row-fluid">
  <div class="span12">
      <table width="100%" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; padding:5px">
        <tr><th bgcolor="#e0e0e0">修改部門<?=$odTypeName?>承辦人</th></tr>
    <!--
    <tr><td bgcolor="#f8f8f8"><div style="overflow:auto; height:100px; width:100%;"><?=nl2br(file_get_contents('data/absence.txt'))?></div></td></tr>
-->
</table>
</div>
</div>

<div style="height:5px"></div><div style="border-top:1px #999999 dashed; height=1px;"></div><div style="height:5px"></div>
<form id="prjform" action="<?="edis/rod/editDo.php?odMenu=" . $odMenu."&mgmt=1"?>" method="post" enctype="multipart/form-data">


 <table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
 <tr>
            <td align="right"><span class="require">*</span>收文單位：</td>
            <td align="left"><input name="rAct" type="text"  id="rAct" value="<?=$rRact['oid']?>" size="6" required ="true" title="<?=$rRact['title']?>" onkeypress="return checkInput(event,'depName',this)" readonly="true"/> <?=$rRact['title']?>
            </td>    
            <td align="right"><span class="require">*</span>收文字號：</td>
            <td>
            <input type="text" name="did" id="did" readonly="true" required ="true" oninput="checkOnly('did')"
              value="<?=$r['did']?>"/><input name="edisId" type="hidden" id="edisId" value="<?=$_REQUEST['id']?>"/>
            <input type="hidden" name="odType"  id="odType" required ="true" size="6" value="<?=$r['odType']?>"/>         
            <?if ($mobile) {
            echo '</tr><tr>';
            }
            ?>
        </tr>
        <tr>
            <td align="right">類別：</td>
            <td align="left"><select name="odDeadlineType" id="odDeadlineType" required ="true" readonly="true"/>
                <?foreach ($odDeadlineType as $v) {
    if ($r['odDeadlineType'] == $v) {
        echo "<option selected='selected'>$v</option>";
    } else {
        //echo "<option>$v</option>";
    }
}?>
            </select>
        </td>
        <td align="right">文書：</td>
        <td><input  type="text" name="creatorName" id="creatorName" readonly="true" value="<?=$emplyeeinfo[$r['creator']]?>"/><input type="hidden" name="creator" id="creator" value="<?=$r['creator']?>"></td>          
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>來文機關：</td>
        <td align="left"><input name="sAct" type="text"  id="sAct" value="<?=$rSact['oid']?>" size="6" required ="true" title="<?=$rSact['title']?>"  onkeypress="return checkInput(event,'depName',this)" readonly="true" /> <?=$rSact['title']?><input name="depID" type="hidden" id="depID" value="<?=$depID?>"/></td>
        <td align="right"><span class="require">*</span>文書承辦日期：</td>
        <td><input type="text" name="rDate" id="rDate" required ="true" value="<?=date_format(date_create($r['rDate']), 'Y/m/d')?>" onchange="getDeadline()" readonly="true"/></td>        
        
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>來文字號：</td>
        <td align="left"><input type="text"  name="dNo" id="dNo" value="<?=$r['dNo']?>" required ="true" oninput="checkOnly('dNo')" readonly="true" /></td>        
        <td align="right"><span class="require">*</span>速別：</td>
        <td><select name="dSpeed" id="dSpeed" required ="true"  onchange="getDeadline()" readonly="true"/>
            <?foreach ($odSpeed as $v) {
            if ($r['dSpeed'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                //echo "<option>$v</option>";
            }
        }?>
        </select>
        </td>

    </tr>
    <tr>
        <td align="right"><span class="require">*</span>來文日期：</td>
        <td><input type="text" name="sDate" id="sDate" value="<?=date_format(date_create($r['sDate']), 'Y/m/d')?>" required ="true" readonly="true"/></td>        

        <td align="right"><span class="require">*</span>期限：</td>
        <td><input type="text" name="deadline" id="deadline" value="<?=date_format(date_create($r['deadline']), 'Y/m/d')?>" required ="true" readonly="true" /></td>
    </tr>
    <tr>
        <td align="right">計畫編號：</td>
        <td><input type="text" name="planNo" id="planNo" value="<?=$r['planNo']?>" readonly="true" /></td>        
    </tr>
    </tr>
    <tr>
   
<tr>
    <td align="right"><span class="require">*</span>主旨：</td>
    <td colspan="3"><textarea name="subjects" id="subjects" rows="3" style="width:85%" required ="true" readonly="true"><?=$r['subjects']?></textarea></td>
</tr>
<tr>
    <td align="right">備註：</td>
    <td colspan="3"><textarea name="contents" id="contents" rows="1" style="width:85%" readonly="true" ><?=$r['contents']?></textarea>
    </td>
</tr>
<tr>
        <!-- <td align="right">收文簽核類型：</td>
         <td align="left"><select name="rSignType" id="rSignType" required ="true"/>
            <?foreach ($RsignType as $v) {
            if ($r['rSignType'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                echo "<option>$v</option>";
            }
        }?>
        </select></td>
        -->

<td align="right"><span class="require">*</span>承辦人：</td>
    <?
    $sCount = 0;
    while ($rSign = $db->fetch_array($rsSign)) {
       //var_dump($rSign);exit;
        $sCount++;

        if ($sCount == 1) {
            echo "<td align='left'><input name='sId[]' type='text' class='queryID' id='sId' required value='".$rSign['signMan']."' size='6' title='".$emplyeeinfo[$rSign['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\" ><input name='sidId[]' type='hidden' id='sidId' value='" . $rSign['id'] . "'/> ". "<span class='sBtn' onclick=\"addItem(this,'sId','sId')\"> +新增承辦</span><div id='sIdDiv'> ";
        } else if ($sCount > 1) {
            echo "<div><input name='sId[]' type='text' class='queryID' id='sId' required value='".$rSign['signMan']."' size='6' title='".$emplyeeinfo[$rSign['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\"> <input name='sidId[]' type='hidden' id='sidId' value='" . $rSign['id'] . "'/> ". "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
        }
    }
    echo '</div>';
    if ($sCount == 0) {
        echo "<div><input name='sId[]' type='text' class='queryID' id='sId' required onkeypress=\"return checkInput(event,'empName',this)\"> <input name='sidId[]' type='hidden' id='sidId' value=''". "<span class='sBtn' onclick=\"addItem(this,'sId','sId')\"> +新增承辦</span>
            <div id='sIdDiv'></div>";

    }

    ?>

    </td>            

</tr>    
<?
if ($odMenu == 'rod') {
    $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/odmgmt.php&muID=0";
} else if ($odMenu == 'sod') {
    $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/odmgmt.php&muID=1";
}
?>
  <tr>
    <td colspan="4" align="center">
      <input type="submit" name="Submit" id="Submit" onclick="return checkReport();"  value="送出" />
      <input type="reset" name="button2" id="button2" value="重設" />
      <input type="button" value="取消" onclick="location.href='<?=$locUrl?>'">
  </td>
  </tr>
</table>
</form>
