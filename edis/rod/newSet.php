<?php 
if($_GET['did']){
    echo '<script>alert("取得收文字號:'.$_GET['did'].'");</script>';
}

@session_start();
$odTypeName = '收文';

include_once "$root/inc_vars.php";
include_once "$root/system/db.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/group/groupDiv.php";

$empID = $_SESSION['empID'];
$depID = $_SESSION['depID'];

//抓取承辦人主管

if($_SESSION["loginType"] == 'lead'){
    $leadEmpID = '';
}else{
    $op = array(
        'dbSource'   => "mysql:host=localhost;dbname=mariaadm",
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db = new db($op);
    $rs = $db->query("select d.leadID from department d JOIN emplyee e ON (e.depID = d.depID) WHERE e.empID = '".$empID."'");
    $r  = $db->fetch_array($rs);

    $leadEmpID = $r['leadID'];
}

?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<link href="group/group.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="group/group.js"></script>

<script>

    function returnValues(data){
        if(data[0]['type'] == 'depID'){
            var depData = new Array();
            if($('input[name="depID[]"]').first().val() != ''){
                $.each( $('input[name="depID[]"]'), function(kk, vv){
                    if($(vv).val() == '') return;
                    if(!depData[kk]) depData[kk] = new Array();

                    depData[kk]['id'] = $(vv).val();
                    depData[kk]['title'] = $(vv).attr('title');
                    depData[kk]['type'] = 'depID';
                });
            }
            var testArray;
            testArray = depData.concat(data);
            data = testArray;

            var DepsDiv = '<div><input name="depID[]" class="queryID" required="true" readonly id="admDeps" type="text" size="6" onkeypress="return checkInput(event,\'depID\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidRemove" onclick=\'removeItem(this)\'> -移除</span></div>';
            $('#admDepsDiv').empty();
            $('#admDepsDiv').html(DepsDiv);
        }

        if(data[0]['type'] == 'empID'){
            var empData = new Array();
            if($('input[name="sId[]"]').first().val() != ''){
                $.each( $('input[name="sId[]"]'), function(kk, vv){
                    if($(vv).val() == '') return;
                    if(!empData[kk]) empData[kk] = new Array();

                    empData[kk]['id'] = $(vv).val();
                    empData[kk]['title'] = $(vv).attr('title');
                    empData[kk]['type'] = 'empID';
                });
            }
            var testArray;
            testArray = empData.concat(data);
            data = testArray;

            var sIdDiv = '<div><input name="sId[]" type="text" class="queryID" id="sIds" required ="true" size="6" value="<?php  echo $empID?>" title="<?php  echo $emplyeeinfo[$empID]?>" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidRemove" onclick=\'removeItem(this)\'> -移除</span></div>';
            $('#sIdsDiv').empty();
            $('#sIdsDiv').html(sIdDiv);
        }
        
        $.each( data, function( k, v ) {
            if(v['type'] == 'depID'){
                var depInputLen = $('input[name="depID[]"]').length;
                var depInputVal = true;
                if($('input[name="depID[]"]').first().val() == '') var depInputVal = false; 

                if((depInputLen != 1 || depInputVal) ) addItem('','depID','admDeps');
                $("input[name='depID[]']").last().val(v['id']);
                $("input[name='depID[]']").last().attr('title',v['title']);
                $("input[name='depID[]']").last().after(' '+v['title']);
            }

            if(v['type'] == 'empID'){
                if(k == 0){
                    $.ajax({
                        url: 'edis/api.php?act=leadID&empID='+v['id'],
                        type:"GET",
                        dataType:'json',
                        success: function(response){
                            if (response == 0){
                                var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="" title="" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                                $('#sidMgrDiv').empty();
                                $('#sidMgrDiv').html(sIdDiv);
                            }else{
                                var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="'+response['leadID']+'" title="'+response['empName']+'" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="formTxS">'+response['empName']+'</span><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                                $('#sidMgrDiv').empty();
                                $('#sidMgrDiv').html(sIdDiv);

                                $("input[name='sidMgr[]']").first().val(response['leadID']);
                                $("input[name='sidMgr[]']").first().value = response['leadID'];
                                $("input[name='sidMgr[]']").first().attr('title',response['empName']);
                            }
                        }
                    });
                }
                if(k != 0) addItem('','sId','sIds');
                $("input[name='sId[]']").last().val(v['id']);
                $("input[name='sId[]']").last().attr('title',v['title']);
                $("input[name='sId[]']").last().after(' '+v['title']);
            }
        });
    }
    function addItem(tbtn,fld,qid) {
        if(fld =='oTitle'){
            var newone = "<div><input name='"+fld+"[]' id='"+qid+"' placeholder='請輸入名稱' type='text'/>"
            + " <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'/>"
            + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
        }else if(fld=='sidMgr'){
            var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'/>"
            +"<img src='/scripts/form_images/search.png'  align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
            + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";  

        }else if(fld=='sId'){
            var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\" >"
            + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
            + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
        }else{
            var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' readonly>"
            + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'></span> "
            + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
        }

        var divTitle ='#'+qid+'Div';
        $(divTitle).append(newone);
    }

    function removeItem(tbtn) {
        $(tbtn).parent().remove();
    }

    $(function(){
        $('#sDate').datepicker();
        $('#deadline').datepicker();
        $('#rDate').datepicker();
        
        id2proc['rAct'] = '/organization/query_inner_id.php';
        $('#button2').on('click',function(){
            alert("重設重整畫面，請稍等畫面跑完。");
            window.location.reload();
            // $('#sIdsDiv input[name="sIds[]"]').removeAttr('value');
            // $('#odform').trigger("reset");
        });
    });

    function checkOnly(act){//檢核收文及發文字號是否唯一
        var checkNull;
        var checkValue;
        var InputFiled;
        var errMsg;
        if(act =='did'){
            if($("[name='did']").val()==""){   
                return;
            }else{
                checkNull ='true'; 
                checkValue =$("[name='did']").val();
                InputFiled = document.querySelector('#did');
                errMsg =<?php  echo $odTypeName?>+'字號已存在';
            }
        }else if(act =='dNo'){
            if($("[name='dNo']").val()==""){
                return
            }else{
                checkNull ='true'; 
                checkValue =$("[name='dNo']").val();
                InputFiled = document.querySelector('#dNo');
                errMsg ='來文字號已存在';
            }
        }
        if(checkNull =='true'){
            $.ajax({
                url: 'edis/api.php?act='+act+'&inValue='+checkValue,
                type:"GET",
                dataType:'text',
                success: function(response){
                    if (response == 'valid') {
                        InputFiled.setCustomValidity('');
                        console.log('valid');
                    }else if (response == 'invalid') {
                        InputFiled.setCustomValidity(errMsg);
                        console.log('invalid');
                    }
                }

            });
        }
    }

    function getodNum(){//取得文件字號    
        $("#Submit").attr('hidden','true');
        var fv =document.getElementById('mFile').value;
        var otValue= document.getElementById('mTitle').value;
        var act='odNum';
        var checkNull='true';
        var orgid;
        var InputFiled;
        var odType =$("[name='odType']").val();
        var result =false;
        var errMsg = '';

        $.each($("[name='sId[]']"), function(k, v){
            if(v.value == ''){
                errMsg = '承辦人不能有空白';
                checkNull ='false'; 
            }
        });

        $.each($("[name='depID[]']"), function(k, v){
            if(v.value == ''){
                errMsg = '承辦部門不能有空白';
                checkNull ='false'; 
            }
        });

        if($("[name='depID[]']").val()=="" || $("[name='depID[]']").length == 0){
            $("[name='depID[]']").first().focus();
            checkNull ='false'; 
            errMsg ='承辦部門未填入。'; 
        }
        if($("[name='rAct']").val()==""){
            $("[name='rAct']").focus();
            checkNull ='false'; 
            errMsg ='收文機關未填入。'; 
        }
        if($("[name='sAct']").val()==""){
            $("[name='sAct']").focus();
            checkNull ='false'; 
            errMsg ='來文機關未填入。'; 
        }
        if($("[name='dSpeed']").val()==""){
            $("[name='dSpeed']").focus();
            checkNull ='false'; 
            errMsg ='速別未填入。'; 
        }
        if($("[name='dNo']").val()==""){
            $("[name='dNo']").focus();
            checkNull ='false'; 
            errMsg ='來文字號未填入。'; 
        }
        if(fv.length<=0){
            checkNull ='false'; 
            errMsg ='主文未上傳未填入。'; 
        }
        if($("[name='sId[]']").length == 0){
            $("[name='sId']").focus();
            checkNull ='false'; 
            errMsg ='承辦人未填入。'; 
        }
        if($("[name='deadline']").val()==""){
            $("[name='deadline']").focus();
            checkNull ='false'; 
            errMsg ='期限未填入。'; 
        }
        if($("[name='rDate']").val()==""){
            $("[name='rDate']").focus();
            checkNull ='false'; 
            errMsg ='文書承辦日期未填入。'; 
        }
        if($("[name='sDate']").val()==""){
            $("[name='sDate']").focus();
            checkNull ='false'; 
            errMsg ='來文日期未填入。'; 
        }
        if($("[name='subjects']").val()==""){
            $("[name='subjects']").focus();
            checkNull ='false'; 
            errMsg ='主旨未填入。'; 
        }
        if(errMsg != '') alert(errMsg);

        if(checkNull =='true'){
            result=true;
        }
        if(checkNull =='false') $("#Submit").removeAttr('hidden');

        return result;
    }


    function getDeadline(){//檢核收文及發文字號是否唯一
        var act='deadline';
        var checkNull;
        var rDate;
        var checkValue;
        var InputFiled;
        var errMsg;

        if($("[name='rDate']").val()=="" && $("[name='dSpeed']").val()==""){
            return
        }
        else{
            checkNull ='true'; rDate =$("[name='rDate']").val(); 
            checkValue =$("[name='dSpeed']").val(); 
            InputFiled = document.querySelector('#deadline');
            errMsg ='取得期限發生異常';
        };


        if(checkNull =='true'){
            $.ajax({
                url: 'edis/api.php?act='+act+'&rDate='+rDate+'&inValue='+checkValue,
                type:"GET",
                dataType:'text',
                success: function(response){
                    if (response !== '') {
                        document.getElementById('deadline').value =response.trim();
                       // InputFiled.setCustomValidity('');
                   }else if (response == '') {
                       // InputFiled.setCustomValidity(errMsg);
                   }
               }

           });
        }
    }

    //選擇上傳檔案(多個)需檢核有無輸入名稱
    function checkMutiAtt(f,t){
        //f:input type=file id
        //t:title id
        var fileName =f+'[]';
        var titleName =t+'[]';
        var titleId ='#'+t;
        var fl=document.getElementsByName(fileName);

        for(var i=0;i<fl.length;i++){
            var fv = fl[i].value;
            var otValue= document.getElementsByName(titleName)[i].value;
            if(fv.length>0){
                if(otValue.length==0){
                    document.querySelectorAll(titleId)[i].setCustomValidity('請填入附件名稱');
                }else{
                    document.querySelectorAll(titleId)[i].setCustomValidity('');
                }
            }else{
                document.querySelectorAll(titleId)[i].setCustomValidity('');
            }
        }
    }
    //選擇上傳檔案(單一)需檢核有無輸入名稱
    function checkAloneAtt(f,t){
        //f:input type=file id
        //t:title id
        var titleName =t;
        var titleId ='#'+f;
        var fv =document.getElementById(f).value;
        var otValue= document.getElementById(titleName).value;
        if(fv.length>0){
           if(otValue.length==0){
              document.querySelector(titleId).setCustomValidity('請填入附件名稱');
          }else{
              document.querySelector(titleId).setCustomValidity('');
          }
      }else{
        document.querySelector(titleId).setCustomValidity('請上傳主文');
    }
    }


    $("#oFile").live('change', function(){

        checkMutiAtt('oFile','oTitle');

    })

    $("#oTitle").live('input', function(){

        checkMutiAtt('oFile','oTitle');
        $("#Submit").removeAttr('hidden');
    })

    $("#mFile").live('change', function(){

        checkAloneAtt('mFile','mTitle');

    })

    $("#mTitle").live('input', function(){

        checkAloneAtt('mFile','mTitle');

    })


    $(function(){
      //預設文書承辦日期為當日並帶出期限
      var date = $.datepicker.formatDate('yy/mm/dd', new Date());
      $("[name='rDate']").val(date);
      getDeadline();
    });

</script>

<style>
.require{
 color:red;
}

#group{
  position:absolute;
  left: 20%; top:40%;
  background-color:#F19851;
  padding: 5px;
  display:none;
}

#groupTable{
  background-color:#FFFFFF;
}

</style>

<div class="row-fluid">
  <div class="span12">
      <table width="100%" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; padding:5px">
        <tr><th bgcolor="#e0e0e0">建立<?php  echo $odTypeName?></th></tr>
    <!--
    <tr><td bgcolor="#f8f8f8"><div style="overflow:auto; height:100px; width:100%;"><?php  echo nl2br(file_get_contents('data/absence.txt'))?></div></td></tr>
-->
</table>
</div>
</div>
<?php  echo genGroupDom('sIds');  ?>  
<div style="height:5px"></div><div style="border-top:1px #999999 dashed; height=1px;"></div><div style="height:5px"></div>

<form id="odform" action="<?php  echo "edis/rod/newDo.php?odMenu=".$odMenu?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="loginType" value="<?php  echo $_SESSION['loginType']?>">
    <table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
        <tr>
            <td align="right"><span class="require">*</span>收文機關：</td>
            <td align="left">
                <input name="rAct" type="text" class="queryID" id="rAct" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true" readonly onkeypress="return checkInput(event,'depName',this)"/>   
                <span>

                </span>
                收文單位：
                <input name="roAct" type="text" class="queryID" id="roAct" data-odtype="<?php echo  $odMenu;?>" size="6" readonly onkeypress="return checkInput(event,'depName',this)"/>
            </td> 
            <td align="right"><?php  echo $odTypeName?>字號：</td>
            <td>
                <input type="text" name="did" id="did" readonly="true" placeholder='系統取號' />
                <input type="hidden" name="odType"  id="odType" required ="true" size="6" value="<?php  echo $odTypeName?>"/>
            </td>
            <?php  if ($mobile) {
                echo '</tr><tr>';
            }
            ?>

        </tr>

        <tr>       
            <td align="right"><span class="require">*</span>來文機關：</td>
            <td align="left"><input name="sAct" type="text" class="queryID" id="sAct" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true" readonly onkeypress="return checkInput(event,'depName',this)"/>
                <!-- <input name="depID" type="hidden" id="depID" value="<?php  echo $depID?>"/> -->
            </td>           
            <td align="right"><span class="require">*</span>文書承辦日期：</td>
            <td><input type="text" name="rDate" id="rDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" required ="true"  onchange="getDeadline()" /></td>                
        </tr>
        <tr>
            <td align="right"><span class="require">*</span>來文字號：</td>
            <td align="left"><input type="text"  name="dNo" id="dNo" required ="true" oninput="checkOnly('dNo')" /></td>  <td align="right"><span class="require">*</span>速別：</td>
            <td><select name="dSpeed" id="dSpeed" required ="true" onchange="getDeadline()"/>
                <?php foreach ($odSpeed as $v) {
                    if ($_REQUEST['dSpeed'] == $v) {
                        echo "<option selected='selected'>$v</option>";
                    } else {
                        echo "<option>$v</option>";
                    }
                }?>
            </select>        
        </tr>
        <tr>
            <td align="right"><span class="require">*</span>來文日期：</td>
            <td><input type="text" name="sDate" id="sDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" required ="true" /></td> 
            <td align="right"><span class="require">*</span>期限：</td>
            <td><input type="text" name="deadline" id="deadline" required ="true" /></td>        
        </tr>
        <tr>

            <td align="right">文書：</td>
            <td><input  type="text" name="creator" id="creatorName" readonly="true" value="<?php  echo $emplyeeinfo[$empID] ?>"/>
                <input  type="hidden" name="creator" id="creator"  value="<?php  echo $empID?>"/>
            </td>    
            <td align="right">計畫編號：</td>
            <td><input type="text" name="planNo" id="planNo" /></td>       
        </tr>
        <tr>

            <td align="right"><span class="require">*</span>主文：</td>
            <td colspan="3"><input name="mTitle" id="mTitle" type="hidden"  placeholder='請輸入名稱'  required ="true" value="主文" />
             <input type="file" name="mFile" id="mFile" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf" required ="true">
         </td>
     </tr>
     <tr>
        <td align="right">其他附件：</td>
        <td colspan="3">
            <button type="button" data-toggle="collapse" data-target="#demo">展開</button>
            <div id="demo" class="collapse">
                <input name="oTitle[]" id="oTitle" type="text"  placeholder='請輸入名稱' />
                <input type="file" name="oFile[]" id="oFile" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text"><span class="sBtn" id="oTitleAdd" onclick="addItem(this,'oTitle','oTitle')"> +新增其他附件</span>
                <div id="oTitleDiv">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>主旨：</td>
        <td colspan="3"><textarea name="subjects" id="subjects" rows="3" style="width:85%" required ="true"></textarea></td>
    </tr>
    <tr>
        <td align="right">備註：</td>
        <td colspan="3"><textarea name="note" id="note" rows="3" style="width:85%" ></textarea>
        </td>
    </tr>

<tr> 
<td align="right"><span class="require">*</span>承辦人：</td>
<td align="left">
    <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_sIds');" value="群組"> 
    <span class="sBtn" id="sidAdd" onclick="addItem(this,'sId','sIds')"> +新增承辦</span>
    <div id="sIdsDiv">
        <div>
            <!-- <input name="sId[]" type="text" class="queryID" id="sIds" required ="true" size="6" value="<?php  echo $empID?>" title="<?php  echo $emplyeeinfo[$empID]?>" onkeypress="return checkInput(event,'empName',this)"/>
            <span class="sBtn" id="sidRemove" onclick='removeItem(this)'> -移除</span> -->
        </div>
    </div>
</td>
<td align="right"><span class="require">*</span>承辦部門：</td>
<td align="left">
    <span class="sBtn" id="sidMgrAdd" onclick="addItem(this,'depID','admDeps')"> +新增承辦部門</span>
    <div id="admDepsDiv" style="margin-top: 7px;">
        <!-- <input name="depID[]" class="queryID" required="true" readonly id="admDeps" type="text" size="6" onkeypress="return checkInput(event,'depID',this)"/> -->
    </div>
</td>
</tr>

<tr>
    <td align="right">承辦人主管：</td>
    <td align="left" colspan="3">
        <div id="sidMgrDiv">
            <input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="" title="" onkeypress="return checkInput(event,'empName',this)"/>
            <span class="sBtn" id="sidMgrAdd" onclick="addItem(this,'sidMgr','sidMgr')"> +新增</span>
        </div>
    </td>
</tr>

    <?php 
    if($odMenu=='rod'){
        if($_SESSION["loginType"]=='vol'){
            $locUrl="/index.php?funcUrl=edis/".$odMenu."/newSet.php&muID=0";    
        }else{    
            $locUrl="/index.php?funcUrl=edis/".$odMenu."/myod.php&muID=0";
        }
    }else if($odMenu=='sod'){
        $locUrl="/index.php?funcUrl=edis/".$odMenu."/myod.php&muID=1";
    }
    ?>

    <tr>
        <td td colspan="4" align="center">
            <input type="submit" name="Submit" id="Submit" onclick="return getodNum();" value="送出" />
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>'">
        </td>
    </tr>

</table>
</form>
