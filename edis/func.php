<?php 
// include_once "../receipt/func.php";
// include_once "../system/db.php";
// include_once "../config.php";
// include_once "../getEmplyeeInfo.php";
// include_once "../inc_vars.php";
// include_once "../mpdf/mpdf.php";

function findLeadID($empID)
{
    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => ''
    );
    $db = new db($op);
    $rs = $db->query("select count(leadID) as _count from department WHERE leadID = '".$empID."'");
    $r  = $db->fetch_array($rs);

    if($r['_count'] > 0){
        return 0;
    }else{
        $rs2 = $db->query("select d.leadID, ee.empName from department d JOIN emplyee e ON (e.depID = d.depID) JOIN emplyee ee ON (ee.empID = d.leadID) WHERE e.empID = '".$empID."'");
        $r2  = $db->fetch_array($rs2);

        return json_encode($r2);
    }
}

function eheckDid($inValue)
{
    $db  = new db();
    $sql = "select count(1) rec from edis where did ='$inValue'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);

    if ($r['rec'] > 0) {
        $result = 'invalid';

    } else {

        $result = 'valid';
    }

    return $result;
}

function checkDno($inValue)
{
    $db  = new db();
    $sql = "select count(1) rec from edis where did ='$inValue'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);

    if ($r['rec'] > 0) {
        $result = 'invalid';

    } else {

        $result = 'valid';
    }

    return $result;
    //echo $sql;
}

function getodNum($odType, $orgid, $orgidRo=null, $sDate)
{
    $db   = new db();
    $sql  = "select *  from organization where id ='$orgid'";
    $rs   = $db->query($sql);
    $r    = $db->fetch_array($rs);

    $Rosql  = "select *  from organization where id ='$orgidRo' and (numberTitle !='' and numberTitle is not null)";
    $rsRo   = $db->query($Rosql);
    $rRo    = $db->fetch_array($rsRo);   

    $char01   = '瑪'; //前置碼
    // $char02   = '第'; //中間碼
    $char03   = '號'; //後置碼
    
    if ($odType == '收文') {
        //收文
        if($db->num_rows($rsRo)>0){
            $numTitle = $char01.$r['numberTitle'].$rRo['numberTitle'].'字第'; //收文機關+收文單位編碼簡稱
            $dateYear = substr(date("Y",strtotime($sDate)),-2);

            //取號(找出符合編碼規則最後一筆資料)
            $esql = "select * from edis where odType ='$odType' and id =(select id from edis where odType='$odType' and did like '%$numTitle$dateYear%' order by did desc limit 1)";
            $ers  = $db->query($esql);
            $er   = $db->fetch_array($ers); 
            $noNum    = ((int) mb_substr($er['did'], -5, 4, 'utf8')) + 1;          
            $num      = sprintf("%04d", $noNum);        
        }else{
            $numTitle = $char01.$r['numberTitle'].'字第'; //收文機關編碼簡稱 
            //取號(找出符合編碼規則最後一筆資料)
            // $esql = "select * from edis where odType ='$odType' and id =(select max(id) from edis where odType ='$odType' and SUBSTRING(did,2,1)='$numTitle')";
            $dateYear = substr(date("Y",strtotime($sDate)),-2);
	        $esql = "select * from edis where odType ='$odType' and id =(select id from edis where odType='$odType' and did like '%$numTitle$dateYear%' order by did desc limit 1)";
            $ers  = $db->query($esql);
            $er   = $db->fetch_array($ers); 
            $noNum    = ((int) mb_substr($er['did'], -5, 4, 'utf8')) + 1;          
            $num      = sprintf("%04d", $noNum);           
        }
        // $dateYear = date("y"); //西元年後兩碼

    } else if ($odType == '發文') {
        //發文
        $numTitle = $char01.$r['numberTitle'].'字第'; //組織編碼簡稱
        $dateYear = date("Y",strtotime($sDate)) - 1911; //民國年
        //取號(找出符合編碼規則最後一筆資料)
        $esql = "select * from edis where odType ='$odType' and id =(select id from edis where odType ='$odType' and did like '%$numTitle$dateYear%' order by did desc limit 1)";
	$ers  = $db->query($esql);
        $er   = $db->fetch_array($ers);
        if($er){
            $noNum = ((int) mb_substr($er['did'], -5, 4, 'utf8')) + 1;       
            $num   = sprintf("%04d", $noNum);
        }else{
            $num   = sprintf("%04d", '1');
        }
    }

    $odNum = $numTitle . $dateYear . $num . $char03;
    return $odNum;
}

function getDeadline($rDate, $inValue)
{
    global $odSpeedDay;

    //依速別設定的天數取得初始期限日期
    if ($inValue == '普通件') {
        $time = date('Y/m/d', strtotime($rDate . ' + ' . $odSpeedDay['普通件'] . ' days'));
    } elseif ($inValue == '速件') {
        $time = date('Y/m/d', strtotime($rDate . ' + ' . $odSpeedDay['速件'] . ' days'));
    } elseif ($inValue == '最速件') {
        $time = date('Y/m/d', strtotime($rDate . ' + ' . $odSpeedDay['最速件'] . ' days'));
    }
   
    //計算期限,傳入文書承辦日期/初始期限日期
   // $deadlineDate = 
    countDeadline($rDate,$odSpeedDay[$inValue]);
    return $deadlineDate;
}

//計算期限(需排除六日及班表設定之休假/補班日)
function countDeadline($start,$limitDay)
{
    $forCount =0;
    $resultDate=$start;
    for ($i=1; $i <= $limitDay ; $i++) {
        $forCount++; 
        //檢核日期
        $forDate = date('Y/m/d', strtotime($start . ' + ' . $forCount . ' days'));
        $result = checkDay($forDate);
        
        if($result){//不上班
          $i--;
        }else{//上班
          $resultDate = $forDate;
        }
        if($forCount>=60){
             break;
        }
    }
    echo  $resultDate;
}

//檢查日期是否為休假/六日(且無補班)
function checkDay($day)
{
    global $DeadlineCalendar;    
    //取得設定班表id
    $hDateID =$DeadlineCalendar['放假'];
    $wDateID =$DeadlineCalendar['補班'];
    //計算天數參數
    $hCount =0;
    $wCount =0;

    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db  = new db($op);   

     //休假
     $hsql = "select distinct startTime from duty_month where dutyID=$hDateID and '$day' between startTime and endTime ;";
     $rh = $db->query($hsql);
     $hCount = $db->num_rows($rh);
     if($hCount>0){
         return true;
     }else{
        //六日(且無補班)
         $wsql = "select distinct startTime from duty_month where dutyID=$wDateID and '$day' between startTime and IFNULL(startTime,endTime) ;";
         $rw = $db->query($wsql);
         $wCount = $db->num_rows($rw);
         if($wCount==0){ //無補班
            //是否為六日
           if((date('w',strtotime($day))==6) || (date('w',strtotime($day)) == 0)){
            return true;
           }else{
            return false;
           }
         }else{//有補班
            return false;
         }
    }
}

//取得職員管理之部門單位 $empID = 員工檔id $downFlag = 0:不包含子部門 1:包含子部門
function getManagerDept($empId, $downFlag)
{
    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db  = new db($op);
    $sql = "select depID from department where leadid =$empId ;";

    $rs = $db->query($sql);

    $aa = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'$r[0]'";
        if ($downFlag) {
            $subSql = "select depID from department where depID like '" . $r[0] . "%' and depID !='" . $r[0] . "' ;";
            //echo $subSql;
            $rsSub = $db->query($subSql);
            while ($rSub = $db->fetch_array($rsSub)) {
                $aa[] = "'$rSub[0]'";
            }
        }
    }

    if (count($aa) > 0) {
        $lstss = join(',', $aa);
    }
    if (empty($lstss)) {
        $lstss = 'null';
    }
    return $lstss;
}

//取得內部組織文書
function getrevManID($act)
{
    global $emplyeeDept;
    $db      = new db();
    $rRevSql = "select *  from  organization o where o.id=" . "'" . $act . "' and orgType='SELF'";
    $rsRrev  = $db->query($rRevSql);
    if ($rsRrev) {
        $rRrev = $db->fetch_array($rsRrev);
    }
    if (!empty($rRrev['revMan'])) {
        $return_val = $rRrev['revMan'];
    }
    return $return_val;
}

//取得內部組織文書
function getrevMan($act)
{
    $return_val = array();
    global $emplyeeDept;
    global $emplyeeinfo;
    $db      = new db();
    $rRevSql = "select *  from  organization o where o.id=" . "'" . $emplyeeDept[$act] . "' and orgType='SELF'";
    $rsRrev  = $db->query($rRevSql);
    if ($rsRrev) {
        $rRrev = $db->fetch_array($rsRrev);
    }
    if (!empty($rRrev['revMan'])) {
        $return_val[] = array('id' => $rRrev['revMan'], 'name' => $emplyeeinfo[$rRrev['revMan']]);
    }
    return json_encode($return_val);
}

//登入者所在之部門(包含子部門) $empID = 員工檔id $downFlag = 0:不包含子部門 1:包含子部門
function getEmpDept($empID, $downFlag, $depArr = array())
{
    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db  = new db($op);
    $sql = "select depID from emplyee where empID = $empID ;";
    $rs  = $db->query($sql);

    $aa = array();

    if(empty($depArr)){
        while ($r = $db->fetch_array($rs)) {
            if ($downFlag == 'Son') {
                $subSql = "select depID from department where depID like '" . $r[0] . "%'";
                $rsSub = $db->query($subSql);
                while ($rSub = $db->fetch_array($rsSub)) {
                    $aa[] = "'$rSub[0]'";
                }
            }elseif ($downFlag == 'noSon') {
                $aa[] = "'$r[0]'";
            }
        }
    }else{
        foreach ($depArr as $key => $value) {
            if ($downFlag == 'Son') {
                $subSql = "select depID from department where depID like '" . $value . "%'";
                $rsSub = $db->query($subSql);
                while ($rSub = $db->fetch_array($rsSub)) {
                    $aa[] = "'$rSub[0]'";
                }
            }elseif ($downFlag == 'noSon') {
                $aa[] = "'$value'";
            }
        }
    }

    //文書所負責小組
    $revMan = getAllRevMan();
    if(in_array($empID, $revMan)){
        $sql = "select id from organization where revMan = '$empID' or proxyRevMan = '$empID'";
        $db  = new db();
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)){
            $aa[] = "'$r[0]'";
        }        
    }

    //總文書可查詢負責的機構
    $isMainRevMan = isMainRevMan($empID);
    if($downFlag == 'Org'){
        $aa = array();
        if ($isMainRevMan) {
            $db  = new db();
            $sql = "select distinct id from organization where mainRevMan = '$empID' AND numberTitle !='';";
            $rs  = $db->query($sql);
            while ($r = $db->fetch_array($rs)) {
                $aa[] = "'$r[0]'";
            }
        }
    }
    $lstss = "''";
    if (count($aa) > 0) {
        $lstss = join(',', array_unique($aa));
    }

    return $lstss;
}

//職員本人及其下屬人員
function getViewEmp($empId)
{
    global $emplyeeDept;
    global $departmentlead;
    //組合字串: $empId = 建立者/承辦人/會簽人,及$empId為主管之人員
    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $lstss = "'" . $empId . "'";
    $db    = new db($op);
    $sql   = "select distinct empID id from emplyee e where EXISTS (select depID from department where leadID = $empId AND depID = e.depID) and empID != $empId;";
    $rs    = $db->query($sql);
    $aa    = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'$r[0]'";
    }
    if (count($aa) > 0) {
        if (empty($lstss)) {
            $lstss = join(',', $aa);
        } else {
            $lstss = $lstss . ',' . join(',', $aa);
        }
    }
     
     //與登入者同部門之職員
    $sql   = "select distinct empID id from emplyee e where depID =(SELECT depID FROM emplyee WHERE empID='".$empId."')";
    $rs    = $db->query($sql);
    $aa    = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'$r[0]'";
    }
    if (count($aa) > 0) {
        if (empty($lstss)) {
            $lstss = join(',', $aa);
        } else {
            $lstss = $lstss . ',' . join(',', $aa);
        }
    }    

    if (empty($lstss)) {
        $lstss = 'null';
    }
    return $lstss;
}

function getRevEmp($empId)
{
    $db    = new db();
    $sql = "select id from organization where (revMan ='$empId' or proxyRevMan='$empId')";
    $rs  = $db->query($sql);
    $aa    = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'".$r['id']."'";
    }


    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db    = new db($op);
    $sql   = "select distinct empID id from emplyee e where depID IN(".join(',', $aa).")";
    $rs    = $db->query($sql);
    $aa    = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'$r[0]'";
    }

    if (count($aa) > 0) {
        $lstss = join(',', $aa);
    }else{
        $lstss = 'null';
    }
    
    return $lstss;
}

//登入者為誰的代理人及已離職職員之主管
function getAgent($empId)
{

    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );

    $db = new db($op);

    $sql = "SELECT DISTINCT e.`empId` empid FROM emplyee e, department d
           WHERE e.`depID` = e.`depID` AND d.`leadID` = '$empId' AND e.`isOnduty` = '0' UNION ALL
           SELECT DISTINCT a.empId empid FROM absence a WHERE a.agent = '$empId' AND a.`isDel` = '0' AND (now() BETWEEN a.bDate AND a.eDate);";

    $rs = $db->query($sql);

    $aa = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = "'$r[0]'";
    }

    $lstss = join(',', $aa);
    if (empty($lstss)) {
        $lstss = 'null';
    }
    return $lstss;
}
//登入者有權限查閱之規則
function getViewRule($empId)
{
    $isAssistant = isAssistant($empId);
    //跨單位管理公文人員:依設定(跨單位管理公文設定)可查詢之發文編號前兩碼
    if (!empty($isAssistant)) {
        $db  = new db();
        $sql = "select distinct odNumCode from assistant_ods where empID ='$empId';";
        $rs  = $db->query($sql);
        $aa  = array();
        while ($r = $db->fetch_array($rs)) {
            $aa[] = "'$r[0]'";
        }

        if (count($aa) > 0) {
            if (empty($lstss)) {
                $lstss = join(',', $aa);
            } else {
                $lstss = $lstss . ',' . join(',', $aa);
            }
        }

    } else {
        //非跨單位管理公文人員
        //登入者所在之部門(包含子部門)
        $lstss = getEmpDept($empId, 1);
    }
    

    if (empty($lstss)) {
        $lstss = 'null';
    }
    return $lstss;
}

//判斷登入者是否為跨單位管理公文人員
function isAssistant($empId)
{
    $db = new db();
    $sql = "select count(1) no from assistant_ods where (empID ='$empId');";
    $rs  = $db->query($sql);
    $aa  = array();
    $r   = $db->fetch_array($rs);
    if ($r[0] > 0) {
        return true;
    } else {
        return false;
    }
}

//判斷登入者是否為總文書
function isMainRevMan($empId)
{
    $db  = new db();
    $sql = "select count(1) no from organization where (mainRevMan ='$empId' or proxyMainRevMan='$empId')";
    $rs  = $db->query($sql);
    $aa  = array();
    $r   = $db->fetch_array($rs);
    if ($r[0] > 0) {
        return true;
    } else {
        return false;
    }
}

//判斷代理登入者是否為總文書
function isMainRevManProxy($empId,$empId2)
{
    $db = new db();
    $sql = "select count(1) no from organization where (mainRevMan ='$empId' and proxyMainRevMan='$empId2')";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    if ($r[0] > 0) {
        return 1;
    } else {
        $sql = "select count(1) no from organization where (revMan ='$empId' and proxyRevMan='$empId2')";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);

        if ($r[0] > 0) {
            return 2;
        }else{
            return 0;
        }
    }
}

//取得領據人員設定之負責機構
function getreceiptOrgs($empId,$empReceiptType)
{
    global $orgUse;

    if($empReceiptType=='A'){
       $empType='S';
    }elseif($empReceiptType=='B'){
       $empType='F';
    }

    $db      = new db();
    $Sql = "select *  from  seals s where s.empid=" . "'" . $empId . "' and empType='".$empType."'";
    $rs  = $db->query($Sql);
    $lists ='';
     while ($r = $db->fetch_array($rs)) {
        if(strlen($lists)>0){
            $lists .=','.$r['depts'];
        }else{
            $lists .=$r['depts'];
        }
        
     }
    $orgAry=explode(",", $lists);
    $Sql = "select *  from  organization where id in('".implode("','",$orgUse)."')";
    $rs  = $db->query($Sql);
    while ($r = $db->fetch_array($rs)) {
        if(in_array($r['id'],$orgAry)){
            $return[]=$r['numberTitle'];
        }
    }
    $final ='\'';
    $final .= implode('\',\'',$return);
    $final .='\'';

    return $final;
}

function getAllRevMan()
{
    $db = new db();
    $sql = "select revMan,mainRevMan,proxyRevMan,proxyMainRevMan from organization";
    $rs  = $db->query($sql);
    $aa  = array();
    while ($r = $db->fetch_array($rs)) {
        $aa[] = $r['revMan'];
        $aa[] = $r['proxyRevMan'];
        // $aa[] = $r['proxyMainRevMan'];
        // $aa[] = "'".$r['mainRevMan']."'";
    }
    return array_values(array_unique(array_filter($aa)));
}

function findRevMan($viewemp,$revman,$m)
{
    $arr1 = explode(",", $viewemp);
    switch ($m){
        case 'diff': return array_diff($arr1,$revman); break;
        case 'same': return array_intersect($arr1,$revman); break;
    }
}

function getResponDept($empID)
{
    $op  = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );
    $db  = new db($op);
    $sql = "select depID from department where leadID='".$empID."'";
    $rs  = $db->query($sql);
    while ($r   = $db->fetch_array($rs)) $row[] = $r['depID'];
    $db->close();

    return $row;
}

function isProxyMan($empID)
{
    $db = new db();
    $sql = "select count(1) no from organization where (proxyRevMan = '$empID' or proxyMainRevMan = '$empID')";
    $rs  = $db->query($sql);
    $aa  = array();
    $r   = $db->fetch_array($rs);
    if ($r[0] > 0) {
        return true;
    } else {
        return false;
    }
}

function getProxyPeople($empID)
{
    $row[] = $empID;
    $db = new db();

    $sql = "select revMan from organization where (proxyRevMan = '$empID')";
    $rs  = $db->query($sql);
    while ($r = $db->fetch_array($rs)) {
        if(!empty($r)){
            $row[] = $r['revMan'];
        }
    }

    $sql = "select mainRevMan from organization where (proxyMainRevMan = '$empID')";
    $rs  = $db->query($sql);
    while ($r = $db->fetch_array($rs)) {
        if(!empty($r)){
            $row[] = $r['mainRevMan'];
        }
    }

    return array_values(array_unique($row));
}

function getProxyDep($empID)
{
    $db = new db();

    $sql = "select id from organization where (proxyMainRevMan = '$empID' or proxyRevMan = '$empID')";
    $rs  = $db->query($sql);
    while ($r = $db->fetch_array($rs)) {
        if(!empty($r)){
            $row[] = $r['id'];
        }
    }

    return $row;
}

function getProxyDeps($empID, $proxyEmpID)
{
    $db = new db();
    $sql = "select id from organization where (proxyMainRevMan = '$proxyEmpID' or proxyRevMan = '$proxyEmpID') and (mainRevMan = '$empID' or revMan = '$empID')";
    $rs  = $db->query($sql);
    while ($r = $db->fetch_array($rs)) {
        if(!empty($r)){
            $row[] = $r['id'];
        }
    }

    return $row;
}

//組合查詢公文條件
function getOdFindFilter($odMenu, $empID, $loginType = null, $searchDep = null)
{
    //如果是執行長直接回傳 也不用判斷其他條件
    if($loginType == 'cxo'){
        return '1=1';
    }

    //登入者是否為總文書
    $isMainRevMan = isMainRevMan($empID);
    //登入者是否為跨單位管理公文人員
    $isAssistant = isAssistant($empID);
    //所有文書清單
    $revMan = getAllRevMan();
    //跨單位管理人員:公文編碼前2碼
    $viewrule = getViewRule($empID);
    //職員本人及其下屬人員
    $viewemp = getViewEmp($empID);
    //登入者所在之部門(包含子部門)
    $empDept = getEmpDept($empID, 'Son');
    //登入者所在之部門(不包含子部門)
    $empNowDept = getEmpDept($empID, 'noSon');
    //登入者所能負責的機構
    $revOrg = getEmpDept($empID, 'Org');
    //是否為代理人
    $isProxyMan = isProxyMan($empID); 

    if($isProxyMan){
        $proxy = getProxyPeople($empID); //抓取代理的員編

        foreach ($proxy as $key => $value) {
            $proxyDep[] = getEmpDept($value, 'Son', getProxyDeps($value, $empID));
            $proxyEmp[] = $value;
        }

        $proxyDep[] = $empDept;
        $proxyEmp[] = $empID;

        if($isMainRevMan){ //總文書
            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $revOrg . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $revOrg . ")";
            }
        }else if($loginType == 'lead'){ //主管
            $t = array();
            $respon = getResponDept($empID);
            foreach ($respon as $key => $value) $t[] = "'$value'";
            $aa = join(',',$t);

            $empDept .= ','.$aa;

            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $empDept . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $empDept . ")";
            }

            $empID = $viewemp;
        }else{
            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $empDept . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $empDept . ")";
            }
        }

        if($isAssistant){ //跨單位
            if ($odMenu == 'rod') {
                $wherefStr .= " or (substr(ed.did, 1, instr(ed.did, '字')-1) in(" . $viewrule . "))";
            }else if ($odMenu == 'sod') {
                $wherefStr .= " or (substr(ed.did, 1, instr(ed.did, '字')-1) in(" . $viewrule . "))";
            }
        }

        // if($searchDep){
        // //     unset($proxyDep);
        // //     $proxyDep[] = "'".$searchDep."'";
        // // }
        //     $wherefStr .= " or EXISTS(select distinct edisid from map_orgs_sign where signMan in(" . join(',', array_unique($proxyEmp)) . ") and edisid = ed.id))";
        // }else{
        $wherefStr .= " or EXISTS(select distinct depID from map_dep where depID IN(" . join(',', array_unique($proxyDep)) . ") and eid = ed.id) or EXISTS(select distinct edisid from map_orgs_sign where signMan in(" . join(',', array_unique($proxyEmp)) . ") and edisid = ed.id))";
        // }
    }else{
        if($isMainRevMan){ //總文書
            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $revOrg . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $revOrg . ")";
            }
        }else if($loginType == 'lead'){ //主管
            $t = array();
            $respon = getResponDept($empID);
            foreach ($respon as $key => $value) $t[] = "'$value'";
            $aa = join(',',$t);

            $empDept .= ','.$aa;

            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $empDept . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $empDept . ")";
            }

            $empID = $viewemp;
        }else if(in_array($empID, $revMan)){
            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $empDept . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $empDept . ")";
            }
            $empID = getRevEmp($empID);
        }else{
            if ($odMenu == 'rod') {
                $wherefStr = "(ogR.oid in (" . $empDept . ")";
            }else if ($odMenu == 'sod') {
                $wherefStr = "(ogS.oid in (" . $empDept . ")";
            }
        }

        if($isAssistant){ //跨單位
            if ($odMenu == 'rod') {
                $wherefStr .= " or (substr(ed.did, 1, instr(ed.did, '字')-1) in(" . $viewrule . "))";
            }else if ($odMenu == 'sod') {
                $wherefStr .= " or (substr(ed.did, 1, instr(ed.did, '字')-1) in(" . $viewrule . "))";
            }
        }

        // if($searchDep){
        //     // $empDept = "'".$searchDep."'";
        //     $wherefStr .= " or EXISTS(select distinct edisid from map_orgs_sign where signMan in(" . $empID . ") and edisid = ed.id))";
        // }else{
        $wherefStr .= " or EXISTS(select distinct depID from map_dep where depID IN(" . $empDept . ") and eid = ed.id) or EXISTS(select distinct edisid from map_orgs_sign where signMan in(" . $empID . ") and edisid = ed.id))";
        // }
    }

    return $wherefStr;
}

//文件儲存PDF檔至指定路徑
function odSavePDF($Urlpath, $edisid)
{

    $mpdf              = new mPDF('UTF-8');
    $mpdf->useAdobeCJK = true;
    $mpdf->SetAutoFont(AUTOFONT_ALL);
    $mpdf->SetDisplayMode('fullpage');
    $strContent              = file_get_contents($Urlpath);
    $start                   = strpos($strContent, '<center>');
    $end                     = strripos($strContent, '</center>');
    $content                 = substr($strContent, $start, $end - $start);
    $mpdf->showWatermarkText = true;
    $mpdf->SetAutoFont();
    $mpdf->WriteHTML($content);

    $ulpath = '../data/edis/' . $edisid . '/';
    if (!file_exists($ulpath)) {
        mkdir($ulpath);
    }
    $ulpath = '../data/edis/' . $edisid . '/' . 'main/';

    if (!file_exists($ulpath)) {

        mkdir($ulpath);
    }
    $fileName = time() . '_' . $edisid . '.pdf';
    $mpdf->Output($ulpath . $fileName, 'F');
    return $fileName;
}

function copyOd($edisid)
{
    @session_start();
    global $root;

    $temp_array = array();
    //公文主檔
    $db      = new db();
    $edisSql = "select * from edis e where e.id=" . "'" . $edisid . "'";
    $rEdis   = $db->query($edisSql);

    if ($rEdis) {
        $rE = $db->fetch_array($rEdis);
    }
    
    $db                        = new db('edis_temp');
    $db->row['odDeadlineType'] = (is_null($rE['odDeadlineType'])) ? ('null') : ("'" . $rE['odDeadlineType'] . "'");
    $db->row['rSignType']      = (is_null($rE['rSignType']))    ? ('null') : ("'" . $rE['rSignType'] . "'");
    $db->row['odType']         = (is_null($rE['odType']))       ? ('null') : ("'" . $rE['odType'] . "'");
    $db->row['pType']          = (is_null($rE['pType']))        ? ('null') : ("'" . $rE['pType'] . "'");
    $db->row['sOid']           = (is_null($rE['sOid']))         ? ('null') : ("'" . $rE['sOid'] . "'");
    $db->row['sId']            = (is_null($rE['sId']))          ? ('null') : ("'" . $rE['sId'] . "'");
    $db->row['ext']            = (is_null($rE['ext']))          ? ('null') : ("'" . $rE['ext'] . "'");
    $db->row['dType']          = (is_null($rE['dType']))        ? ('null') : ("'" . $rE['dType'] . "'");
    $db->row['rDate']          = (is_null($rE['rDate']))        ? ('null') : ("'" . $rE['rDate'] . "'");
    $db->row['sDate']          = (is_null($rE['sDate']))        ? ('null') : ("'" . $rE['sDate'] . "'");
    $db->row['rYear']          = (is_null($rE['rYear']))        ? ('null') : ("'" . $rE['rYear'] . "'");
    $db->row['dNo']            = (is_null($rE['dNo']))          ? ('null') : ("'" . $rE['dNo'] . "'");
    $db->row['dSpeed']         = (is_null($rE['dSpeed']))       ? ('null') : ("'" . $rE['dSpeed'] . "'");
    $db->row['dSecret']        = (is_null($rE['dSecret']))      ? ('null') : ("'" . $rE['dSecret'] . "'");
    $db->row['subjects']       = (is_null($rE['subjects']))     ? ('null') : ("'" . $rE['subjects'] . "'");
    $db->row['contents']       = (is_null($rE['contents']))     ? ('null') : ("'" . $rE['contents'] . "'");
    $db->row['signTitle']      = (is_null($rE['signTitle']))    ? ('null') : ("'" . $rE['signTitle'] . "'");
    $db->row['signName']       = (is_null($rE['signName']))     ? ('null') : ("'" . $rE['signName'] . "'");
    $db->row['proofreader']    = (is_null($rE['proofreader']))  ? ('null') : ("'" . $rE['proofreader'] . "'");
    $db->row['creator']        = (is_null($rE['creator']))      ? ('null') : ("'" . $rE['creator'] . "'");
    $db->row['deadline']       = (is_null($rE['deadline']))     ? ('null') : ("'" . $rE['deadline'] . "'");
    $db->row['planNo']         = (is_null($rE['planNo']))       ? ('null') : ("'" . $rE['planNo'] . "'");
    $db->row['sodid']          = (is_null($rE['sodid']))        ? ('null') : ("'" . $rE['sodid'] . "'");
    $db->row['letterAddr']     = (is_null($rE['letterAddr']))   ? ('null') : ("'" . $rE['letterAddr'] . "'");
    $db->row['loginType']      = "'" . $_SESSION['loginType'] . "'";
    $db->row['sourceID']       = "'" . $edisid . "'";
    $db->insert();
    $newEdisId = $db->lastInsertId;

    if ($newEdisId) {
        return $newEdisId;
    }

}

//新增或刪除我的收藏
function changeCollection($obj)
{
    if ($obj['type'] == 'add') {
        $db                   = new db('collections');
        $db->row['edisid']    = $obj['id'];
        $db->row['odType']    = "'" . $obj['odTypeName'] . "'";
        $db->row['creatorid'] = "'" . $obj['empid'] . "'";
        $db->insert();
        if ($db->lastInsertId) {
            return 'insert collections success.';
        } else {
            return 'insert collections fail.';
        }
    } else if ($obj['type'] == 'del') {
        $db  = new db();
        $sql = "delete from collections where edisid='" . $obj['id'] . "' and creatorid='" . $obj['empid'] . "'";
        $db->query($sql);
        return 'delete collections edisid:' . $obj['id'];
    }
}
//執行長/董事長已讀取收文log
function addlog($obj)
{
    $tempDB = new db();
    $ofSql  = "select * from read_logs where edisid ='" . $obj['id'] . "' and empid ='" . $obj["empid"] . "'";
    $rsOf   = $tempDB->query($ofSql);

    if ($tempDB->num_rows($rsOf) > 0) {
        return 'already insert read_logs.';
    } else {
        $db                  = new db('read_logs');
        $db->row['edisid']   = $obj['id'];
        $db->row['odType']   = "'" . $obj['odTypeName'] . "'";
        $db->row['empid']    = "'" . $obj['empid'] . "'";
        $db->row['readTime'] = "'" . date("Y-m-d") . "'";
        $db->insert();
        if ($db->lastInsertId) {
            return 'insert read_logs success.';
        } else {
            return 'insert read_logs fail.';
        }
    }
    $tempDB->close();
}

function checkAttView($edisid,$empid)
{
    $db = new db();
    //指定公文是否設定為機密
    $Sql ="select * FROM edis WHERE id =".$edisid." AND isSecret ='1'";
    $rs  =  $db->query($Sql);
    $count = $db->num_rows($rs); 
    //已設定為機密  
    if($count>0){     
        //指定公文中職員是否有權限
        $smSql ="select * FROM secret_members WHERE edisid =".$edisid." AND empid ='".$empid."'";
        $rsSm =  $db->query($smSql);
        $Rcount = $db->num_rows($rsSm);

        if($Rcount >0 ){
            return true;
        }else{
            return false;
        }

    }else{ //未設定機密
        return true;
    }
}

//歸檔後發送最後通知至訊息中心
function sendmsg($empid,$edisid)
{
    $db = new db();
    //指定公文是否設定為機密
    $Sql ="select * FROM edis WHERE id =".$edisid;
    $rs =  $db->query($Sql);
    $edisInfo = $db->fetch_array($rs);


    $tempDB = new db();
    $sql ="select DISTINCT signMan empid FROM map_orgs_sign WHERE edisid =".$edisid." AND isFinalNotify=1";
    $rsX = $tempDB->query($sql);
    $empPMStr='';
    $empcount=0;
    while ($rX = $tempDB->fetch_array($rsX)) {
        $empcount++;
        if($empcount==1){
            $empPMStr = $rX['empid'].":R";
        }else{
            $empPMStr .= ";".$rX['empid'].":R";        
        }
    }
    if(strlen($empPMStr)>0){
        //建立查詢訊息應讀取人員,取得permits_emplyee id
        $op = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => 'permits_emplyee',
        );

        $db = new db($op);         
        $db->row['title']         = "'" . $edisInfo['odType'].'最後通知:'.$edisInfo['did']. "'";
        $db->row['empPM']         = "'" . $empPMStr. "'";
        $db->row['crman']         = "'" . $empid . "'";
        $db->row['isDel']         = "'" . '0' . "'";   
        $db->insert();
        $peid = $db->lastInsertId;

        //訊息內容設定
        $notifies =3;
        $ClassID =0;
        $title = $edisInfo['odType'].'最後通知:'.$edisInfo['did'];
        $Content = $edisInfo['odType'].':'.$edisInfo['did'].',已完成歸檔,請至公文系統查看,謝謝。';
        $unValidDate =date('Y-m-d',strtotime('+1 day'));
        $announcer = $empid;
        $srcfn = null;

        //新增發佈的訊息(瑪利亞)      
        $sql = "insert into contents(ClassID,title,Content,unValidDate,announcer,filepath,notifies,PID)"
             . " values('$ClassID','$title','$Content','$unValidDate','$announcer','$srcfn',$notifies,$peid)";
        $op = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );

        $tempDB = new db($op);     
        $tempDB->query($sql);
        $content_id =$tempDB->lastInsertId;
        
        if($content_id){
            //新增發佈的訊息(edis)
            $edisDB = new db();     
            $sql = "insert into contents(ID,ClassID,title,Content,unValidDate,announcer,filepath,notifies,PID,empID)"
                 . " values('$content_id','$ClassID','$title','$Content','$unValidDate','$announcer','$srcfn','$notifies','$peid','$empid')";
             //echo $sql;exit;
            $edisDB->query($sql); 
        }  

    } 
}