<?php 
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
error_reporting(E_ALL);

include_once "$root/config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/inc_vars.php";

include_once "$root/edis/func.php";
include_once "$root/group/groupDiv.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "ListControl.php";
include "$root/system/ViewControl.php";

if ($odMenu == 'rod') {
    $odTypeName = '收文';
    $actArr = $rodRact;
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';
    $actArr = $sodSact;
}

$pageTitle = "應簽" . $odTypeName;
$tableName = "edis";
// for Upload files and Delete Record use
$ulpath = "/data/edis/";
if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}

$delField = 'Ahead';
$delFlag  = true;

//: Select field Prepared -----------------------------------------------------
$orgs = array();
$orgs['']='-全部組織-'; //空字串:全部
foreach ($actArr as $k=>$v) {
    $orgs[$v] = $v;
}
$deps = array();
$deps[''] = '-全部部門';
foreach ($depAdminfo as $key => $value) {
    if(strlen($key)>1){
        $aa = preg_split('//', $key, -1, PREG_SPLIT_NO_EMPTY);
        $deps[$key] = $depAdminfo[$aa[0]].' - '.$value;
    }else $deps[$key] = $value;
}

//篩選時間設定
$year = date("Y",time());
$halfMonth = date("Y-m-d", strtotime("-3 month"));
$first = $halfMonth;
$end = date("Y-m-d",strtotime("+6 day"));

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, true, SearchKeyword),
    "newBtn"    => array("＋新增", false, false, doAppend),
    "editBtn"   => array("－修改", false, false, doEdit),
    "delBtn"    => array("Ｘ刪除", false, false, doDel),
);

//:ListControl  ---------------------------------------------------------------
//收發文之欄位名稱不同
if ($odMenu == 'rod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '來文日期';
    $FsActName = '來文機關';
    $FrActName = '收文機關';
    $FName     = 'rActName'; 
    $querySql = $unsignedRod;

    $opList = array(
        "alterColor"    => true, //交換色
        "canScroll"     => true,
        "curOrderField" => 'did', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('did', 'rActName', 'sOidName', 'subjects'), //要搜尋的欄位
        "filterOption"=>array(
            'autochange'=>false,
            'rDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'rDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filterCondi"=>array(   //比對條件
            'depID'=>"%s like '%s%%'"
        ),
        "filters"=>array(
            $FName =>$orgs,
            'depID'=>$deps,
            'rDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            //array("統計報表",false,true,'receipt_report'),
        )      
    );

    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"       => array($Fdid, "20px", true, array('Id', "gorec(%d)")),
        "rDate"     => array('承辦日期', "20px", true, array('Date')),
        "deadline"  => array("期限", "20px", true, array('Text')),        
        "sActName"  => array($FsActName, "30px", true, array('Text')),
        //"rActName"  => array($FrActName, "30px", true, array('Text')),
        "subjects"  => array("主旨", "120px", false, array('Text')),
        "signStage" => array("階段", "20px", true, array('Define', getLevelName)),
        "id"        => array("內容", "20px", false, array('Text')),
        "signMan"   => array("簽核人", "20px", true, array('Define', getMan)),
        "mosid"     => array("簽核", "10px", false, array('Define', goSign)),
    );
} else if ($odMenu == 'sod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '發文日期';
    $FsActName = '發文機關';
    $FrActName = '受文者';
    $FName     = 'sActName';
    $querySql = $unsignedSod;

    $opList = array(
        "alterColor"    => true, //交換色
        "canScroll"     => true,
        "curOrderField" => 'did', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('did', 'sActName', 'sOidName', 'subjects'), //要搜尋的欄位
        "filterOption"=>array(
            'autochange'=>false,
            'sDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'sDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filterCondi"=>array(   //比對條件
            'depID'=>"%s like '%s%%'"
        ),
        "filters"=>array(
            $FName =>$orgs,
            'depID'=>$deps,
            'sDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            //array("統計報表",false,true,'receipt_report'),
        )      
    );

    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"       => array($Fdid, "20px", true, array('Id', "gorec(%d)")),
        "sDate"     => array($FsDate, "20px", true, array('Date')),
        //"sActName"  => array($FsActName, "30px", true, array('Text')),
        "rActName"  => array($FrActName, "30px", true, array('Text')),
        "subjects"  => array("主旨", "120px", false, array('Text')),
        "signStage" => array("階段", "20px", true, array('Define', getLevelName)),
        "id"        => array("內容", "20px", false, array('Text')),
        "signMan"   => array("簽核人", "20px", true, array('Define', getMan)),
        "mosid"     => array("簽核", "10px", false, array('Define', goSign)),
    );
}