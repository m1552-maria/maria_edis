<?php 
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */

include_once 'unsign_init.php';

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'new' || $act == 'edit') {
    $_SESSION['myod_tab'] = 0;
}
//登入者為誰的代理人及已離職職員之主管
// $agentList = getAgent($_SESSION['empID']);
$agentList = 'null';
error_log(date('Y-m-d H:i:s')." unsign_list.php started.\n", 3, 'C:/wamp/logs/haotung-debug.log');

$tabidx = $_SESSION['myod_tab'] ? $_SESSION['myod_tab'] : 1;
if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        error_log('check are there files to be deleted:'.$sql);
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }
                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);
}
error_log(date('Y-m-d H:i:s')." unsign_list.php check point 1.\n", 3, 'C:/wamp/logs/haotung-debug.log');

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_Tel($value){
    return "TEL:" . $value;
}

function func_Time($value){
    return "<input type'text' value='$value.:00'>";
}

function func_note($value){
    return "defined:" . $value;
}

?>
<!-- ::Stand-alone -->
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="/media/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/media/js/jquery-ui-1.10.1.custom.min.js"></script>
<!-- -->

<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">
<link href="group/group.css" rel="stylesheet" type="text/css" />

<!--日期區間css-->
<link href="/Scripts/daterangepicker/daterangepicker.css" rel="stylesheet"></link>
<link href="/Scripts/daterangepicker/MonthPicker.css" rel="stylesheet"/>

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>
<script src="group/group.js"></script>
<script src="/Scripts/form.js"></script>
<!--日期區間js-->
<script src="/Scripts/daterangepicker/moment.min.js"></script>
<script src="/Scripts/daterangepicker/daterangepicker.min.js"></script>
<script src="/Scripts/daterangepicker/MonthPicker.js"></script>
<!--開啟其他附件-->
<script src="/edis/edis.js"></script>
<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script>
    $(document).ready(function() {
        $("#addSign").change(function() {
            if($("#addSign").attr('checked')){
                document.getElementById('filNo').style.display = 'none';
                document.getElementById('odDeadline').style.display = 'none';
                document.getElementById('isSign').style.display = 'none';
                document.getElementById('isFinalNotify').style.display = 'none';
                document.getElementById('signContent').style.display = 'none';
                document.getElementById('filingNo').removeAttribute('required'); 
                document.getElementById('newSign').style.display = 'table-row';
            }else{
                document.getElementById('filNo').style.display = 'table-row';
                document.getElementById('odDeadline').style.display = 'table-row';
                document.getElementById('isSign').style.display = 'table-row';
                document.getElementById('isFinalNotify').style.display = 'table-row';
                document.getElementById('signContent').style.display = 'table-row';
                document.getElementById('filingNo').setAttribute('required','true');
                document.getElementById('newSign').style.display = 'none';
            }
        });
        $('input[type=radio][name=isSign]').change(function() {
            if (this.value == '0') {
                document.getElementById('filingNo').removeAttribute('required');     
            }
            else if (this.value == '1') {
                document.getElementById('filingNo').setAttribute('required','true');
            }
        });
        $('#Submit').on('click',function(e){
            var error = false;
            $('[name="newSigner[]"]').each(function(){
                if($(this).val() != '' && $(this).val().length != 5){
                    error = true;
                }
            });
            if(error){
                alert('員工編號錯誤');
                return false;
            }
            var form = $('#statefm');
            var url = form.attr('action');
            if($('#filingNo').attr('required')){
                if($('#filingNo').val()!=''){
                    $('.tdButton').html('');
                    $('.tdButton').append('<p style="color:red;">簽核作業中，請稍候。</p>');
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: form.serialize(),
                        dataType: 'json',
                        success: function(d){
                            if(d == 1){
                                location.reload();
                            }
                        }
                    });
                }else{
                    $('#filingNo').focus();
                }
            }else{
                $('.tdButton').html('');
                $('.tdButton').append('<p style="color:red;">簽核作業中，請稍候。</p>');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: form.serialize(),
                    dataType: 'json',
                    success: function(d){
                        if(d == 1){
                            location.reload();
                        }
                    }
                });
            }
        });
    });
    function doDel(id){
        if(!confirm("確定刪除?")){ return;}
        location.href='edis/del.php?id='+id;
    }

    function setnail(n) {
        $('.tabimg').attr('src','../images/unnail.png');
        $('#tabg'+n).attr('src','../images/nail.png');
        $.get('edis/setnail.php',{'idx':n});
    }

    function stateitgform(id,did,signLev,isSecret,isFirstLead) {
        var odmenu = "<?php global $odMenu;echo $odMenu;?>";
        var rodLastLevel ="<?php  echo $rodLastLevel; ?>";
        var sodLastLevel ="<?php  echo $sodLastLevel; ?>";    
        var newSign_style = document.getElementById('newSign').style;
        var filNo_style = document.getElementById('filNo').style;
        var odDeadline_style = document.getElementById('odDeadline').style;
        var secret_style = document.getElementById('secret').style;
        var odDeadlineType = '<?php  echo json_encode($odDeadlineType); ?>';
        if(odmenu == 'rod' && signLev == rodLastLevel ){
            newSign_style.display = 'none';
            filNo_style.display = 'table-row';
            document.getElementById('signTD').style.display = 'table-row';

            $("#odDeadlineType").empty();
            $.each(JSON.parse(odDeadlineType), function( key, value ) {
                $("#odDeadlineType").append('<option value="'+key+'">'+value+'</option>');
            });

            odDeadline_style.display = 'table-row';
            document.getElementById('filingNo').setAttribute('required','true');

        }else if(odmenu =='sod' && signLev == sodLastLevel ){
            newSign_style.display = 'none';
            filNo_style.display = 'table-row';
            document.getElementById('signTD').style.display = 'table-row';

            $("#odDeadlineType").empty();
            $.each(JSON.parse(odDeadlineType), function( key, value ) {
                $("#odDeadlineType").append('<option value="'+key+'">'+value+'</option>');
            });

            odDeadline_style.display = 'table-row';
            document.getElementById('filingNo').setAttribute('required','true');
        }else{
            newSign_style.display = 'table-row';
            filNo_style.display = 'none';
            odDeadline_style.display = 'none';
            document.getElementById('filingNo').removeAttribute('required');      
        }
        if(isFirstLead==1){
            secret_style.display ='table-row';
        }else{
            secret_style.display ='none';
        }

        $("input[name*='isSecret'][value='0']").attr("checked", true);
        $("input[name*='isSecret'][value='1']").attr("checked", false);
        $("input[name*='isFinalNotify'][value='0']").attr("checked", true);
        $("input[name*='isFinalNotify'][value='1']").attr("checked", false);
        statefm.mosid.value = id;
        $("#odDid").html(did);
        statefm.signLev.value = signLev;
        $('#div1').show();
    }

    function setSecretViewer(){
        var isSecret =$("input[name='isSecret']:checked").val();
        var secretViewer_style = document.getElementById('secretView').style;
        if(isSecret==1){
            secretViewer_style.display = 'table-row';
            document.getElementById('secretViewer').setAttribute('required','true');
        }else{
            secretViewer_style.display = 'none';
            document.getElementById('secretViewer').removeAttribute('required');
        }

    }  
    function addItem(tbtn,fld,qid) {
        if(fld=='newSigner'){
            var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' class='queryID' size='6' onkeypress=\"return checkInput(event,'empName',this)\" callback='checkSigner'>"
            + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
            +" <input name='isWait[]' id='isWait' type='checkbox' >"
            + " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
           var divTitle ='#'+fld+'Div';
        }else{
           var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text'class='queryID' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
            + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
            + " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
           var divTitle ='#'+fld+'Div';
        }
        $(divTitle).append(newone);
    }
    function removeItem(tbtn) {
        $(tbtn).parent().remove();
    }
    function checkSigner(empId){
        $.ajax({
            url: '/edis/newSignerCheck.php',
            type: 'POST',
            data: { 
                empId : empId,
                id : $("#mosid").val()
            },
            dataType: 'json',
            success: function(d){
                if(d == 1) alert(empId+'已重複加簽');
            }
        });
    }
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<style>
    #div1,#div2 {
        position:absolute;
        left: 50%; top:50%;
        background-color:#888;
        padding: 20px;
        display:none;
        margin-left: -153px;
        margin-top: -150px;
    }
    .date{width:50px;}
    .spanbtn {
        font-size:smaller;
        float:right;
        cursor: pointer;
    }

    #isWait {
        display: none;
    }
</style>
<div id="div1">
    <form id="statefm" action="edis/req.php">
        <table bgcolor="#FFFFFF" cellpadding="4">      
            <tr>
                <td align="right"><?php  echo $odTypeName?>字號</td>
                <td id="odDid" /></td>
                <input name="mosid" type="hidden" id="mosid" value=""/>
                <input name="signLev" type="hidden" id="signLev" value=""/>
                <input name="empID" type="hidden" id="empID" value="<?php  echo $_SESSION['empID']?>"/>
            </tr>
            <tr id="signTD" style="display: none;">
                <td align="right">增加加簽：</td>
                <td><input type="checkbox" id="addSign" name="addSign"></td>
            </tr>
            <tr id="isSign">
                <td align="right">簽核：</td>
                <td>
                    <input type="radio" name="isSign" value="1" checked="checked" /> 是 
                    <input type="radio" name="isSign" value="0" /> 否 
                </td>
            </tr>
            <tr id="isFinalNotify">
                <td align="right">最後通知：</td>
                <td>
                    <input type="radio" name="isFinalNotify" value="1"  /> 是 
                    <input type="radio" name="isFinalNotify" value="0" checked="checked"/> 否 
                </td>
            </tr>      
            <tr hidden>
                <td align="right">簽核用語：</td>
                <td><input type="text" name="signState" id="signState" value=""></td>
            </tr>
            <tr id="signContent">
                <td align="right">簽核意見/內容：</td>
                <td>
                    <input type="text" name="signContent" list="signText" size="50" />
                    <datalist id="signText">
                        <option value="知悉">
                        <option value="依文辦理">
                        <option value="如擬">
                        <option value="准">
                        <option value="文存">
                        <option value="已辦理">
                        <option value="不報名">
                    </datalist>
                </td>
            </tr>
            <tr id="filNo" style="display: none;" >
                <td align="right">歸檔編號：</td>
                <td><input type="text" name="filingNo" id="filingNo"></td>
            </tr>
            <tr id="odDeadline" style="display: none;" >
                <td align="right">類別：</td>
                <td>
                    <select name="odDeadlineType" id="odDeadlineType" required ="true" />
                    <?php  foreach ($odDeadlineType as $v) { echo "<option>$v</option>"; } ?>
                    </select>
                </td>
            </tr>
            <tr id="newSign" style="display: none;" >
                <td align="right">會簽人：</td>
                <td>
                    <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_newSigner');" value="群組">
                    <input name="newSigner[]" type="text" class="queryID" id="sId" size="6" onkeypress="return checkInput(event,'empName',this)" callback='checkSigner'/>
                    <input name='isWait[]' id='isWait' type='checkbox'>
                    <span class="sBtn" onclick="addItem(this,'newSigner','sId')"> +新增</span>
                    <div id="newSignerDiv">
                    </div>
                </td>
            </tr>
            <tr id ="secret" style="display: none;">
                <td align="right">機密：</td>
                <td>
                    <input type="radio" name="isSecret" value="1" onChange="setSecretViewer();"/> 是 
                    <input type="radio" name="isSecret" value="0" checked="checked" onChange="setSecretViewer();"/> 否 
                </td>
            </tr>        
            <tr id="secretView" style="display: none;" >
                <td align="right">機密查看附件：</td>
                <td>
                    <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_secretViewer');" value="群組">
                    <input name="secretViewer[]" type="text" class="queryID" id="sId" size="6" onkeypress="return checkInput(event,'empName',this)"/>
                    <span class="sBtn" onclick="addItem(this,'secretViewer','sId')"> +新增</span>
                    <div id="secretViewerDiv">
                    </div>
                </td>
            </tr>        
            <tr>
                <td colspan="2" align="center" class="tdButton">
                    <input type="button" name="Submit" id="Submit" value="確定" />
                    <button type="button" onclick="$('#div1').hide()">取消</button><br>
                    <span style="color: red;font-size: 14px;">請確認是否需要增加簽核人員</span>
                </td>
            </tr>
        </table>
    </form>
</div>

<?php 
    echo genGroupDom('newSigner');
    echo genGroupDom('secretViewer');
    $whereStr = ''; //initial where string
    $wherekStrSave = ''; //add by Tina 20190508 
    //:filter handle  ---------------------------------------------------
    $whereAry = array();
    //收發文之基本where條件不同
    if(isProxyMan($_SESSION['empID'])){
        $agentArr = getProxyPeople($_SESSION['empID']);
        $agentList = join(',',$agentArr);
        $sLevel = array();
        foreach ($agentArr as $key => $value) {
            $isMainRev = isMainRevManProxy($value,$_SESSION['empID']);
            if($isMainRev == '2'){
                $sLevel[]= "(signMan = '$value' and signLevel <> '4')";
            }else{
                $sLevel[] = "(signMan = '$value')";
            }
        }        
    }

    if($sLevel) $agentStr = join(' or ',$sLevel);
    else $agentStr = '1=2';

    $wherefStr = "odType='" . $odTypeName . "' and (signMan='".$_SESSION['empID']."' or $agentStr) ";

    if($opList['filters']) {
        foreach($opList['filters'] as $k=>$v) {
            $wType = $opList['filterType'][$k];
            switch($wType) {
                case 'Month':   
                    if($_REQUEST[$k]) $curMh = $_REQUEST[$k];
                    elseif($opList['filterOption'][$k]) $curMh=$opList['filterOption'][$k][0];
                    $Days  = date('t',strtotime("$curMh/01"));
                    $bDate = date($curMh.'/01');
                    $eDate = date($curMh.'/'.$Days);    
                    if($curMh) $whereAry[]="($k between '$bDate' and '$eDate 23:59:59')";
                    break;
                case 'DateRange':
                    $aa = array();
                    if($_REQUEST[$k]) {
                        $aa = explode('~',$_REQUEST[$k]);
                    } elseif($opList['filterOption'][$k]) {
                        $aa = explode('~',$opList['filterOption'][$k][0]);
                    }
                    if(count($aa)) $whereAry[] = "($k between '$aa[0]' AND '$aa[1] 23:59:59')";
                    break;
                default:
                    if($opList['filterOption'][$k]) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);  
            }
        }
        if(count($whereAry)) $wherefStr .= "and ".join(' and ',$whereAry);
    }

    //:Search filter handle  ---------------------------------------------
    $whereAry  = array();
    $wherekStr = '';
    if ($opList['keyword']) {
        $key = $opList['keyword'];
        foreach ($opList['searchField'] as $v) {
            $whereAry[] = "$v like '%$key%'";
        }

        $n = count($whereAry);
        if ($n > 1) {
            $wherekStr = '(' . join(' or ', $whereAry) . ')';
        } else if ($n == 1) {
            $wherekStr = join(' or ', $whereAry);
        }

    }

    //add by Tina 20190508 篩選部門非實際欄位,需修改sql
    if($_REQUEST[$FName]){
        if($_REQUEST[$FName]) $fdepid=sprintf("%s = '%s'",$FName,$_REQUEST[$FName]);    
        if(strlen($wherekStr)>0){
            $wherekStr .=' and ('.$fdepid.')';
        }else{
            $wherekStr .='('.$fdepid.')';
        }
    }

    //:Merge where
    if ($wherefStr || $wherekStr) {
        $flag     = false;
        if ($wherefStr) {
            $whereStr .= $wherefStr;
            $flag = true;}
        if ($wherekStr) {
            $wherekStrSave = $wherekStr;
        }
    }

    if($_REQUEST['depID']) $whereStr .= " AND dep.depID IN ('".$_REQUEST['depID']."')";

    $db     = new db();
    //:PageInfo -----------------------------------------------------------------------

    /*update by Tina 20190508 如有執行搜尋按鈕,因搜尋欄位非實際欄位,需修改sql*/
    if ($wherekStrSave) {
        $osql = sprintf('%s and %s',
            $querySql, $whereStr. $unsignWhere 
        );
        $sql = sprintf('select * from(%s) otable where %s order by %s %s',$osql,$wherekStrSave,
            $opList['curOrderField'], $opList['sortMark']);
    }else{
        $sql = sprintf('%s and %s order by %s %s',
            $querySql, $whereStr. $unsignWhere ,
            $opList['curOrderField'], $opList['sortMark']
        );
    }
    // echo $sql;
    $rs    = $db->query($sql);
    $pdata = $db->fetch_all($rs);
    $rCount = count($pdata);

    if (!$pageSize) {
        $pageSize = 10;
    }

    $pinf = new PageInfo($rCount, $pageSize);
    if ($recno > 1) {
        $pinf->setRecord($recno);
    } else if ($page > 1) {
        $pinf->setPage($page);
        $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;
    }
    $n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
    $rData = array();
    for($i = $n*10; $i<$pinf->curPage*10; $i++){
        $rData[] = $pdata[$i];
    }
    $rData = array_filter($rData);

    //: Header -------------------------------------------------------------------------
    echo "<h1 align='center'>$pageTitle ";
    if ($opList['keyword']) {
        echo " - 搜尋:" . $opList['keyword'];
    }

    echo "</h1>";

    //:PageControl ---------------------------------------------------------------------
    $obj = new EditPageControl($opPage, $pinf);
    echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

    //:Defined this page PHP Function -------------------------------------------------------------
    function getContent($v){ return null; }

    function goSign($v){
        global $Gjob;
        global $emplyeeJobID;
        $isFirstLead = 0;
        $db  = new db();
        $sql = "SELECT mos.id , mos.signMan, mos.signLevel, mos.isSign, ed.did, ed.isSecret FROM map_orgs_sign mos, edis ed WHERE mos.edisid = ed.id AND mos.id = '" . $v . "' order by mos.signLevel asc";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        if(in_array($emplyeeJobID[$r['signMan']],$Gjob)){//簽核流程第一位副主任級主管
            $isFirstLead =1;
        }else{
            $isFirstLead =0;
        }

        if (empty($r['isSign'])) {
            return "<img title='簽名' src='/images/sign.png' onclick=stateitgform('".trim($r['id'])."','".trim($r['did'])."','".trim($r['signLevel'])."','".trim($r['isSecret'])."','".$isFirstLead."')>";
        }
    }

    function getLevelName($v){
        global $odMenu;
        if ($odMenu == 'rod') {
            global $RsignLevel;
            if (empty($RsignLevel[$v])) {
                return '加簽';
            } else {
                return $RsignLevel[$v];
            }

        } elseif ($odMenu == 'sod') {
            global $SsignLevel;
            if (empty($SsignLevel[$v])) {
                return '加簽';
            } else {
                return $SsignLevel[$v];
            }
        }
    }

    function getMan($v){ global $emplyeeinfo;return $emplyeeinfo[$v]; }

    //:ListControl Object --------------------------------------------------------------------
    $listPos = $pinf->curRecord % $pinf->pageSize;
    $edisID = $rData[$listPos]['id'];
    $empid = $_SESSION['empID'];
    $loginType = $_SESSION["loginType"];
    //add by tina 20180326 List:顯示管理內容
    foreach ($rData as &$pdataInfo) {
        $par = "id=".$pdataInfo['id']."&empid=".$_SESSION['empID']."&loginType=".$_SESSION["loginType"];
        $id = $pdataInfo['id'];
        global $odMenu;
        //列印
        if ($odMenu == 'rod') {
            $odtype = "edis/printform.php?/edis/viewtype/rodview.php?" .$par;
        } elseif ($odMenu == 'sod') {
            if ($pdataInfo['dType'] == '令') {
                $odtype = "edis/printform.php?/edis/viewtype/command.php?" . $par;
            } else if ($pdataInfo['dType'] == '函') {
                $odtype = "edis/printform.php?/edis/viewtype/letter.php?" . $par;
            } else if ($pdataInfo['dType'] == '公告') {
                $odtype = "edis/printform.php?/edis/viewtype/declaration.php?" . $par;
            } else if ($pdataInfo['dType'] == '開會通知單') {
                $odtype = "edis/printform.php?/edis/viewtype/notice.php?" . $par;
            }
        }

        if ($odMenu == 'sod' && $pdataInfo['signStage'] == '1') {
            $edit    = "index.php?funcUrl=edis/" . $odMenu . "/edit.php&amp;muID=1&id=" . $pdataInfo['id'] . "&signLevel=" . $pdataInfo['signStage'];
            $mtPrint = '<a href="' . $odtype . '" target="_blank"><img src="/images/detail.gif" title="公文內容" align="absmiddle" border="0"/></a>' . '<br><a href="' . $edit . '" target="_blank">修改</a>';
        } else {
            $mtPrint = '<a href="' . $odtype . '" target="_blank"><img src="/images/detail.gif" title="公文內容" align="absmiddle" border="0"/></a>';
        }

        /*新增附件連結*/
        if ($odMenu == 'rod') {
            //主文附件        
           $mainUlpath = 'data/edis/' . $pdataInfo['id'] . '\/main\/';
            if (file_exists($mainUlpath)) {
                $dh = opendir($mainUlpath);
                while (false !== ($mfilename = readdir($dh))) {
                    $test .='read';
                    if ($mfilename == '.') {
                        continue;
                    }

                    if ($mfilename == '..') {
                        continue;
                    }

                    $mfile_path = $mainUlpath . $mfilename;
                }
                  $mtMainAtt = '<a href="' . $mfile_path . '" target="_blank"><img src="images/mainAtt.png" title="主文附件" align="absmiddle" border="0"/></a>'; 
            }

            //其他附件
            $db = new db();
            $ofSql = "select * from map_files where aType='O' and eid ='" . $pdataInfo['id'] . "'";
            $rsOf  = $db->query($ofSql);
            $aa = $db->num_rows($rsOf);
            $mtOtherAtt = '';
            if($db->num_rows($rsOf)>0){ 
              $path ='edis/tabpage/att_other.php';
              $par = json_encode(array('path'=>$path,'id'=>$pdataInfo['id'],'odMenu'=>$odMenu));

              $mtOtherAtt = '<img src="images/oAtt.png" style="width:16px;height:16px" onclick=\'showOtherAtt('.$par.')\' align="absmiddle" title="其他附件"/>';  

            }  
            if(!checkAttView($pdataInfo['id'],$_SESSION['empID']) && $_SESSION['loginType'] != 'cxo'){
                $mtMainAtt = '';
                $mtOtherAtt =''; 
            }  

             $pdataInfo['id'] = $mtMainAtt.' '.$mtOtherAtt.' '.$mtPrint;
        }elseif ($odMenu == 'sod') {
            $db = new db();
            $ofSql = "select * from map_files where aType='O' and eid ='" . $pdataInfo['id'] . "'";
            $rsOf  = $db->query($ofSql);
            $aa = $db->num_rows($rsOf);
            $mtOtherAtt = '';
            if($db->num_rows($rsOf)>0){ 
                $path ='edis/tabpage/att_other.php';
                $par = json_encode(array('path'=>$path,'id'=>$pdataInfo['id'],'odMenu'=>$odMenu));

                $mtOtherAtt = '<img src="images/oAtt.png" style="width:16px;height:16px" onclick=\'showOtherAtt('.$par.')\' align="absmiddle" title="其他附件"/>';  

            }
             $pdataInfo['id'] = $mtOtherAtt.' '.$mtPrint;
        }

        if (isset($id)) {
            global $odTypeName;
            $mtCollection='';   
            $tempDB = new db();
            $sql = "select * from edis where id =".$id;    
            $rs = $tempDB->query($sql);
            while( $r = $tempDB->fetch_array($rs) ){ 
                $budget = '核銷冊:'.(empty($r['budgetNum'])? 0: $r['budgetNum']);
                $result = '成果冊:'.(empty($r['resultNum'])? 0: $r['resultNum']);
                $plan   = '計畫書:'.(empty($r['planNum'])? 0: $r['planNum']);
                $pdataInfo['sumNum'] = $budget.' '.$result.' '.$plan;
                if($r['loginType'] == 'vol'){
                    $pdataInfo['did'] = "<img src='../images/vol.png' style='width:5%'> ".$pdataInfo['did'];
                }
                if($r['isSecret'] == '1'){
                    $pdataInfo['did'] = "<img src='../images/lock.png' style='width:3%'> ".$pdataInfo['did'];
                }
             }

            //我的收藏
            $ofSql = "select * from collections where edisid ='" . $id . "' and creatorid ='".$_SESSION["empID"]."'";
            $rsOf  = $tempDB->query($ofSql);
            $par = json_encode(array('empid'=>$_SESSION["empID"],'id'=>$id,'odTypeName'=>$odTypeName));
            if($tempDB->num_rows($rsOf)>0){
                $mtCollection = '<img src="images/heart_red.png" id="heart'.$id.'" onclick=\'cancelCollection('.$par.')\' align="absmiddle" title="取消收藏"/>'; 
            }else{
                $mtCollection = '<img src="images/heart.png" id="heart'.$id.'" onclick=\'addCollection('.$par.')\' align="absmiddle" title="新增收藏"/>'; 
            }
            $pdataInfo['collection'] = $mtCollection;

            $tempDB->close();
        }
        unset($pdataInfo);
    }
    $obj   = new BaseListControl($rData, $fieldsList, $opList, $pinf);

    //:ViewControl Object ------------------------------------------------------------------------
    switch ($act) {
        case 'edit': //修改
            $op3 = array(
                "type"      => "edit",
                "form"      => array('form1', "$tableName/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
                "submitBtn" => array("確定修改", false, ''),
                "cancilBtn" => array("取消修改", false, ''));
            //$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);
            break;
        case 'new': //新增
            $op3 = array(
                "type"      => "append",
                "form"      => array('form1', "$tableName/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
                "submitBtn" => array("確定新增", false, ''),
                "cancilBtn" => array("取消新增", false, ''));
            //$ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
            break;
        default: //View
    }
    
    error_log(date('Y-m-d H:i:s')." unsign_list.php before including public/inc_listjs.php.\n", 3, 'C:/wamp/logs/haotung-debug.log');
?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li <?php  echo ($tabidx == 0 ? "class='active'" : '')?> onclick="setnail(0)"><a href="#tab0" data-toggle="tab" >附件<img id="tabg0" class="tabimg" src="/images/<?php  echo $tabidx == 0 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
        <li <?php  echo ($tabidx == 1 ? "class='active'" : '')?> onclick="setnail(1)"><a href="#tab1" data-toggle="tab">簽收狀況<img id="tabg1" class="tabimg" src="/images/<?php  echo $tabidx == 1 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
        <li <?php  echo ($tabidx == 2 ? "class='active'" : '')?> onclick="setnail(2)"><a href="#tab2" data-toggle="tab">簽收失效歷程<img id="tabg2" class="tabimg" src="/images/<?php  echo $tabidx == 2 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane<?php  echo ($tabidx == 0 ? ' active' : '')?>" id="tab0">
            <iframe id="tab0if" width="100%" height="320" frameborder="0" src="edis/tabpage/att.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>&empid=<?php  echo $empid ?>&loginType=<?php  echo $loginType?>"></iframe>
        </div>
        <div  class="tab-pane<?php  echo ($tabidx == 1 ? ' active' : '')?>" id="tab1">
            <iframe id="tab1if" width="100%" height="320" frameborder="0" src="edis/tabpage/odsign.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>"></iframe>
        </div>
        <div  class="tab-pane<?php  echo ($tabidx == 2 ? ' active' : '')?>" id="tab2">
            <iframe id="tab2if" width="100%" height="320" frameborder="0" src="edis/tabpage/history.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>"></iframe>
        </div>
    </div>
</div>

<?php  include 'public/inc_listjs.php'; ?>