<?
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */

include_once 'odmgmt_init.php';

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'new' || $act == 'edit') {
    $_SESSION['myod_tab'] = 0;
}

$now = DateTime::createFromFormat('U.u', microtime(true));
error_log($now->format('Y-m-d H:i:s.u')." odmgmt_list.php started.\n", 3, 'C:/wamp/logs/haotung-debug.log');
$tabidx = $_SESSION['myod_tab'] ? $_SESSION['myod_tab'] : 0;
if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    //echo $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);

}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_Tel($value)
{
    return "TEL:" . $value;
}

function func_Time($value)
{
    return "<input type'text' value='$value.:00'>";
}

function func_note($value)
{
    return "defined:" . $value;
}

?>
<!-- ::Stand-alone -->
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="/media/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/media/js/jquery-ui-1.10.1.custom.min.js"></script>      
<!-- -->
<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">


<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>

<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script>
  function delRec(id){
    if(!confirm("確定刪除?")){ return;}
    location.href='edis/del.php?id='+id;
}

function setnail(n) {
    $('.tabimg').attr('src','../images/unnail.png');
    $('#tabg'+n).attr('src','../images/nail.png');
    $.get('edis/setnail.php',{'idx':n});
}

var rzt = false;
function doSearchAdv(){
    if(rzt){//處理不要一直開視窗
        rzt.close();
        rzt = false;
    }
    rzt = openBrWindow('edis/od/advSearch.php?odMenu=<?echo $odMenu;?>','',550,400);    
}
function openBrWindow(theURL,inParam,win_width,win_height) { 
    var PosX = (document.body.clientWidth-win_width)/2; 
    var PosY = (document.body.clientHeight-win_height)/2; 
    features = "width="+win_width+",height="+win_height+",top="+PosY+",left=400,scrollbars=yes"; 
    var newwin = window.open(theURL,'',features);
    return newwin;
}
function returnAdv(data) {//傳回來的值
    var odMenu ="<?echo $odMenu;?>";
    if(data) {

        if(odMenu=='rod'){
            var aForm = $("<form action='/index.php?funcUrl=edis/rod/odmgmt.php&muID=0' method='POST'></form>");
        }else if(odMenu=='sod'){
            var aForm = $("<form action='/index.php?funcUrl=edis/sod/odmgmt.php&muID=1' method='POST'></form>");
        }
        aForm.append("<input type='hidden' name='sqlSelect' value=\""+data['select']+"\">");
        aForm.append("<input type='hidden' name='sqlTable' value=\""+data['tableName']+"\">");
        aForm.append("<input type='hidden' name='sqlWhere' value=\""+data['where']+"\">");
        $('body').append(aForm);
        aForm.submit();     
    }
}
</script>
<?
$whereStr = ''; //initial where string
$wherekStrSave = ''; //add by Tina 20190508 
//:filter handle  ---------------------------------------------------
$whereAry  = array();
    //收發文之基本where條件不同
$mgmtDept = getManagerDept($_SESSION['empID'],1);
if ($odMenu == 'rod') {
  $wherefStr = "odType='" . $odTypeName ."' and (rAct in (".$mgmtDept."))";
} else if ($odMenu == 'sod') {
  $wherefStr = "odType='" . $odTypeName ."' and (sAct in (".$mgmtDept."))";    
}

if ($opList['filters']) {
    foreach ($opList['filters'] as $k => $v) {
        if ($_REQUEST[$k]) {
            $whereAry[] = "$k = '" . $_REQUEST[$k] . "'";
        } else if ($opList['filterOption'][$k]) {
            $whereAry[] = "$k = '" . $opList['filterOption'][$k][0] . "'";
        }

    }
    if (count($whereAry)) {
        $wherefStr = join(' and ', $whereAry);
    }

}

//:Search filter handle  ---------------------------------------------
$whereAry  = array();
$wherekStr = '';
if ($opList['keyword']) {
    $key = $opList['keyword'];
    foreach ($opList['searchField'] as $v) {
        $whereAry[] = "$v like '%$key%'";
    }

    $n = count($whereAry);
    if ($n > 1) {
        $wherekStr = '(' . join(' or ', $whereAry) . ')';
    } else if ($n == 1) {
        $wherekStr = join(' or ', $whereAry);
    }

}

//:Merge where
if ($wherefStr || $wherekStr) {
    $whereStr = 'where ';
    $flag     = false;
    if ($wherefStr) {
        $whereStr .= $wherefStr;
        $flag = true;}
    /* update by Tina 20190508 針對list搜尋按鈕,搜尋之欄位為子查詢得來,非實際欄位,需修改sql查詢
    if ($wherekStr) {
        if ($flag) {
            $whereStr .= ' and ' . $wherekStr;
        } else {
            $whereStr .= $wherekStr;
        }

    }*/
    if ($wherekStr) {
        $wherekStrSave = $wherekStr;
    }
}
/*
$db     = new db();
$sql    = sprintf('select 1 from %s %s', $tableName, $whereStr);
$rs     = $db->query($sql);
$rCount = $db->num_rows($rs);
*/


$db = new db();
// update by tina 20190508不使用view查詢故修改sql

//取得登入者角色可查詢之規則             
$viewRule = getOdFindFilter($odMenu, $_SESSION['empID']);

//檢查並取得進階搜尋的條件
$tbName = $querySql;
//$fieldsStr = $_REQUEST['sqlSelect']?$_REQUEST['sqlSelect']:'*';
if($_REQUEST['sqlWhere']){
    $_SESSION['sqlWhere'] = $_REQUEST['sqlWhere'];
    $whereStr = $_SESSION['sqlWhere'];
}else{
    if($_SESSION['sqlWhere']) $whereStr = $_SESSION['sqlWhere'];
    else $whereStr = " ed.rDate>='".$first."' AND ed.rDate<='".$end."' AND ".$viewRule;

}

// echo $_SESSION['sqlWhere'];

/*update by Tina 20190508 如有執行搜尋按鈕,因搜尋欄位非實際欄位,需修改sql*/

if($wherekStrSave){
  $sql     = sprintf('%s where %s', $tbName, $wherekStrSave." and ".$whereStr);
}else{
  $sql     = sprintf('%s where %s', $tbName,$whereStr);
}
// echo $sql;
$rs = $db->query($sql);
$rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
if (!$pageSize) {
    $pageSize = 10;
}

$pinf = new PageInfo($rCount, $pageSize);
if ($recno > 1) {
    $pinf->setRecord($recno);
} else if ($page > 1) {
    $pinf->setPage($page);
    $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;}
    ;

//: Header -------------------------------------------------------------------------
    echo "<h1 align='center'>$pageTitle ";
    if ($opList['keyword']) {
        echo " - 搜尋:" . $opList['keyword'];
    }

    echo "</h1>";

//:PageControl ---------------------------------------------------------------------
    $obj = new EditPageControl($opPage, $pinf);
    echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//:Defined this page PHP Function -------------------------------------------------------------
    function getMaintain($v)
    {
        global $tableName;
        global $odMenu;
        global $rodLastLevel;
        global $sodLastLevel;    
        $now = DateTime::createFromFormat('U.u', microtime(true));
        error_log($now->format('Y-m-d H:i:s.u')." odmgmt_list.php execute getMaintain(".$tableName.', '.$v.").\n", 3, 'C:/wamp/logs/haotung-debug.log');
        $db  = new db();
        $sql = 'select ed.*, (SELECT count(1)
        FROM `maria_edis`.`map_orgs_sign`
        WHERE (    (`maria_edis`.`map_orgs_sign`.`edisid` = `ed`.`id`)
        AND (`maria_edis`.`map_orgs_sign`.`isSign` IS NOT NULL)))
        AS `signNum`,
        (SELECT if((count(`maria_edis`.`map_orgs_sign`.`id`) = 0), 0, 1)
        FROM `maria_edis`.`map_orgs_sign`
        WHERE (    (`maria_edis`.`map_orgs_sign`.`edisid` = `ed`.`id`)
        AND (`maria_edis`.`map_orgs_sign`.`isSign` = 0)))
        AS `isReject` from '.$tableName.' ed where ed.id=' . $v;
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $par = "id=".$r['id']."&empid=".$_SESSION['empID']."&loginType=".$_SESSION["loginType"];
    //列印
        if ($odMenu == 'rod') {
            $odtype = "edis/printform.php?/edis/viewtype/rodview.php?" . $par;
            $LastLevel = $rodLastLevel;
        } elseif ($odMenu == 'sod') {
            $LastLevel = $sodLastLevel;
            if ($r['dType'] == '令') {
                $odtype = "edis/printform.php?/edis/viewtype/command.php?" . $par;
            } else if ($r['dType'] == '函') {
                $odtype = "edis/printform.php?/edis/viewtype/letter.php?" . $par;
            } else if ($r['dType'] == '公告') {
                $odtype = "edis/printform.php?/edis/viewtype/declaration.php?" . $par;
            } else if ($r['dType'] == '開會通知單') {
                $odtype = "edis/printform.php?/edis/viewtype/notice.php?" . $par;
            }
        }

        $mtPrint = '<a href="' . $odtype . '" target="_blank"><img src="images/print.png" title="列印" align="absmiddle" border="0"/></a><br>';

        if ($odMenu == 'rod') {
            $edUrl = "<a class='sBtn' href='index.php?funcUrl=edis/" . $odMenu . "/editMgmt.php&muID=0&id=" . $r['id'] . "'>修改承辦人</a>";

        } else if ($odMenu == 'sod') {
            $edUrl = "<a class='sBtn' href='index.php?funcUrl=edis/" . $odMenu . "/editMgmt.php&muID=1&id=" . $r['id'] . "'>修改承辦人</a>";

        }
        $deUrl = ' <a class="sBtn" onclick="delRec(' . $r['id'] . ')" >刪除</a>';

        if ($r['signNum'] == 0) {
        //add by Tina 20190508舊資料未產生簽核流程,故判斷有無歸檔編號
            if(!empty($r['filingMan'])){
                $mtMatain = "已歸檔";
            }else{
        //尚未進行簽核流程
                $mtMatain = $edUrl . $deUrl;
            }
        } else {
            if ($r['isReject'] == 1) {
            //簽核流程中有被否決
                $mtMatain = $edUrl . $deUrl;
            } else {
         //檢查是否已歸檔
                $sql = 'select * from map_orgs_sign where edisid=' . $v.' and signLevel='.$LastLevel;
                $rs  = $db->query($sql);
                $r   = $db->fetch_array($rs);
                if($r['isSign']){
                   $mtMatain = "已歸檔";
               }else{
                   $mtMatain = "進行簽核中";
               }           
           }

       }

       return $mtPrint . ' ' . $mtMatain;

   }


   function getMan($v)
   {
    global $emplyeeinfo;return $emplyeeinfo[$v];
}

//:ListControl Object --------------------------------------------------------------------
$n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
/*update by Tina 20190508 如有執行搜尋按鈕,因搜尋欄位非實際欄位,需修改sql*/

if ($wherekStrSave) {
    $sql = sprintf('%s where %s order by %s %s limit %d,%d',$querySql,$whereStr.' and '.$wherekStrSave,$opList['curOrderField'], $opList['sortMark'],$n * $pinf->pageSize, $pinf->pageSize);

}else{
    $sql = sprintf('%s where %s order by %s %s limit %d,%d', $querySql,$whereStr,$opList['curOrderField'], $opList['sortMark'],
    $n * $pinf->pageSize, $pinf->pageSize);
}
// echo $sql;
$rs    = $db->query($sql);
$pdata = $db->fetch_all($rs);
$obj   = new BaseListControl($pdata, $fieldsList, $opList, $pinf);

//:ViewControl Object ------------------------------------------------------------------------
$listPos = $pinf->curRecord % $pinf->pageSize;
switch ($act) {
    case 'edit': //修改
    $op3 = array(
        "type"      => "edit",
        "form"      => array('form1', "$tableName/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定修改", false, ''),
        "cancilBtn" => array("取消修改", false, ''));
        //$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);
    break;
    case 'new': //新增
    $op3 = array(
        "type"      => "append",
        "form"      => array('form1', "$tableName/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定新增", false, ''),
        "cancilBtn" => array("取消新增", false, ''));
        //$ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
    break;
    default: //View
        /* $op3 = array(
"type"=>"view",
"submitBtn"=>array("確定變更",true,''),
"resetBtn"=>array("重新輸入",true,''),
"cancilBtn"=>array("關閉",true,''));
$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);    */
}
$edisID = $pdata[$listPos]['id'];
$empid = $_SESSION['empID'];
$loginType = $_SESSION["loginType"];
?>

<div class="tabbable tabbable-custom">
  <ul class="nav nav-tabs">
    <li <?=($tabidx == 0 ? "class='active'" : '')?> onclick="setnail(0)"><a href="#tab0" data-toggle="tab" >附件<img id="tabg0" class="tabimg" src="/images/<?=$tabidx == 0 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
    <li <?=($tabidx == 1 ? "class='active'" : '')?> onclick="setnail(1)"><a href="#tab1" data-toggle="tab">簽收狀況<img id="tabg1" class="tabimg" src="/images/<?=$tabidx == 1 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
    <li <?=($tabidx == 2 ? "class='active'" : '')?> onclick="setnail(2)"><a href="#tab2" data-toggle="tab">簽收失效歷程<img id="tabg2" class="tabimg" src="/images/<?=$tabidx == 2 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane<?=($tabidx == 0 ? ' active' : '')?>" id="tab0">
      <iframe id="tab0if" width="100%" height="320" frameborder="0" src="edis/tabpage/att.php?id=<?=$edisID?>&odMenu=<?=$odMenu?>&empid=<?=$empid ?>&loginType=<?=$loginType?>"></iframe> 
  </div>
  <div  class="tab-pane<?=($tabidx == 1 ? ' active' : '')?>" id="tab1">
      <iframe id="tab1if" width="100%" height="320" frameborder="0" src="edis/tabpage/odsign.php?id=<?=$edisID?>&odMenu=<?=$odMenu?>"></iframe>
  </div>
  <div  class="tab-pane<?=($tabidx == 2 ? ' active' : '')?>" id="tab2">
      <iframe id="tab2if" width="100%" height="320" frameborder="0" src="edis/tabpage/history.php?id=<?=$edisID?>&odMenu=<?=$odMenu?>"></iframe>
  </div>
</div>
<?
include 'public/inc_listjs.php';
?>
