<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
error_reporting(E_ALL);
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once('../../config.php');  
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/inc_vars.php";

include_once "$root/edis/func.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "ListControl.php";
include "$root/system/ViewControl.php";
include "$root/system/TreeControl.php";

if ($odMenu == 'rod') {
    $odTypeName = '收文';
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';

}
$pageTitle = "查詢交辦";
//$pageSize=5; //使用預設
$tableName = "(SELECT c.*,SUBSTR(title,8) did, SUBSTR(title,1,2) odType FROM contents c WHERE c.title LIKE '%收文交辦%') ta";
// for Upload files and Delete Record use
$ulpath = "/data/edis/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}

$delField = 'Ahead';
$delFlag  = true;

//: Select field Prepared -----------------------------------------------------
$OrderEmps = array();
$OrderEmps['']='-全部交辦人員-'; //空字串:全部

$db  = new db();
$sql = "select distinct empID from sys_menu_permission where menuID in(select id from sys_menu where title='查詢交辦') ;";
$rs = $db->query($sql);
 while ($r = $db->fetch_array($rs)) {
     $OrderEmps[$r['empID']] = $emplyeeinfo[$r['empID']];
 }

/*
//當前時間前六個月
$year=date("Y",time());
$halfMonth= date("Y-m-d", strtotime("-6 month"));
$first=$halfMonth;
//當年最後一天
$end=$year."-12-31";
*/
$date=date('Y-m-d');  //當前日期

$wfirst=1; //$wfirst =1 表示每周星期一為開始日期 0表示每周日為開始日期

$w=date('w',strtotime($date));  //取得當前周的第幾天 周日是 0 周一到周六是 1 - 6 

$first=date('Y-m-d',strtotime("$date -".($w ? $w - $wfirst : 6).' days')); //取得本周開始日期，如果$w是0，則表示周日，減去 6 天

$end=date('Y-m-d',strtotime("$now_start +6 days"));  //本周结束日期

//$last_start=date('Y-m-d',strtotime("$now_start - 7 days"));  //上周开始日期

//$last_end=date('Y-m-d',strtotime("$now_start - 1 days"));  //上周结束日期

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, true, SearchKeyword),
    "newBtn"    => array("＋新增", false, false, doAppend),
    "editBtn"   => array("－修改", false, false, doEdit),
    "delBtn"    => array("Ｘ刪除", false, false, doDel),
);

//:ListControl  ---------------------------------------------------------------


    $opList = array(
        "alterColor"=>true,         //交換色
        "canScroll"     => true,
        "curOrderField" => 'R_Date', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('title', 'empID'), //要搜尋的欄位   
        "filterOption"=>array(
            'autochange'=>false,
            'R_Date'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'R_Date'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filterCondi"=>array(   //比對條件
        ),
        "filters"=>array(
            'empID' =>$OrderEmps,
            'R_Date'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            //array("統計報表",false,true,'receipt_report'),
        )

    );

    $fieldsList = array(
        "ID"      => array("編號", "30px", true, array('Id', "gorec(%d)")),
        "R_Date"    => array("交辦日期", "30px", true, array('DateTime')),
        "title" => array("交辦文號", "30px", true, array('Text')),
        "Content" => array("交辦內容", "30px", true, array('Text')),
        "empID"  => array("交辦人員", "30px", true, array('Define', getMan))
    );



//:ViewControl -----------------------------------------------------------------
/*    $fieldA = array(
"SimpleText"=>array("簡述<span class='need'>*</span>","text",60,'','',array(true,'','',8)),
"Content"=>array("內文","textarea",60,6,'',array(true,'','',8)),
"unValidDate"=>array("失效日期","datetime",20,'','',array(true,PTN_DATETIME,'請輸入日期時間')),
'Link'=>array("說明","textarea",60,5,),
'isReady'=>array("上架","checkbox",20)
);
$fieldA_Data = array("unValidDate"=>date('Y/m/d',strtotime("+30 days")),'isReady'=>1);

$fieldE = array(
"ID"=>array("編號","id",20,),
"SimpleText"=>array("簡述","text",60,),
"unValidDate"=>array("失效日期","date",10,),
'Link'=>array("連結","text",60,'',''),
"Content"=>array("內文","textarea",60,6),
'isReady'=>array("上架","checkbox",60,5,'')
);
 */
