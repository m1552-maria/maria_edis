<?php 
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
error_reporting(E_ALL);
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once "$root/config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/inc_vars.php";

include_once "$root/edis/func.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "ListControl.php";
include "$root/system/ViewControl.php";

if ($odMenu == 'rod') {
    $odTypeName = '收文';
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';
}

$pageTitle = "查詢" . $odTypeName;
//$pageSize=5; //使用預設
$tableName = "edis ed";
// for Upload files and Delete Record use
$ulpath = "/data/edis/";
if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}

//預設查詢區間 當前時間前6個月至下禮拜
$year = date("Y",time());
$lastYearMonth = date("Y-m-d", strtotime("-6 month"));
$first = $lastYearMonth;
$end = date("Y-m-d", strtotime("+6 day"));

/*query sql add by Tina 20190508 優化效能不使用view,將view的select 搬出來*/

$delField = 'Ahead';
$delFlag  = true;

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, false, SearchKeyword),
    "newBtn"    => array("＋新增", false, false, doAppend),
    "editBtn"   => array("－修改", false, false, doEdit),
    "delBtn"    => array("Ｘ刪除", false, false, doDel),
    "extraButton" => array(
        "SearchAdv" => array("進階搜尋",false,true,doSearchAdv)
    )
);

//:ListControl  ---------------------------------------------------------------
if ($odMenu == 'rod') {
    $actArr = $rodRact;
} else if ($odMenu == 'sod') { 
    $actArr = $sodSact;
}   

$orgs = array();
$orgs['']='-全部組織-'; //空字串:全部
foreach ($actArr as $k=>$v) {
    $orgs[$v] = $v;
}
$deps = array();
$deps[''] = '-全部部門-';
foreach ($depAdminfo as $key => $value) {
    if(strlen($key)>1){
        $aa = preg_split('//', $key, -1, PREG_SPLIT_NO_EMPTY);
        $deps[$key] = $depAdminfo[$aa[0]].' - '.$value;
    }else $deps[$key] = $value;
}

$isSign = array(''=>'-全部簽核狀態-','1'=>'簽核未通過','0'=>'簽核通過');
$otherArray = array(''=>'-全部附件類型-','budgetNum'=>'核銷冊','resultNum'=>'成果冊','planNum'=>'計畫書');

if ($odMenu == 'rod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '來文日期';
    $FsActName = '來文機關';
    $FrActName = '收文單位';
    $FName     = 'ractname';
    $querySql  = $myRod;

    $opList = array(
        "alterColor"    => true, //交換色
        "curOrderField" => 'did', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "searchField"   => array('did', 'sActName', 'rActName', 'creator', 'subjects'), //要搜尋的欄位
        "canScroll"     => true,
        "filterOption"  =>array(
            'autochange'=>false,
            'rDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"    =>array(
            'rDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filters"=>array(
            'did'=>array_merge(array("" => "-全部文號-"), $odCodeList),
            $FName =>$orgs,
            'depID'=>$deps,
            'isReject'=>$isSign,
            'rDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            array("統計報表",false,true,'odfind_report'),
        )

    );

    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"      => array($Fdid, "20px", true, array('Id', "gorec(%d)")),
        "rDate"    => array('承辦日期', "20px", true, array('Date')),
        "sActName" => array($FsActName, "50px", true, array('Text')),        
        "subjects" => array("主旨", "200px", false, array('Text')),
        "note" => array("備註", "40px", false, array('Text')),
        "odDeadlineType"=>array('類別', "20px", true, array('Text')),
        "id"       => array("內容", "20px", false, array('Define', getMaintain)),
        "isReject" => array("簽核未通過", "10px", true, array('Bool')),
    );

} else if ($odMenu == 'sod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '發文日期';
    $FsActName = '文號選取';
    $FrActName = '受文者';
    $FName     = 'soname';
    $querySql  = $mySod;

    $opList = array(
        "alterColor"    => true, //交換色
        "curOrderField" => 'did', //主排序的欄位
        "sortMark"      => 'desc', //升降序 (ASC | DES)
        "canScroll"     => true,
        "searchField"   => array('did', 'sActName', 'rActName', 'creator', 'subjects'), //要搜尋的欄位
        "filterOption"=>array(
            'autochange'=>false,
            'sDate'=>array($first.' ~ '.$end,false),    //預設Key值,是否隱藏
            'goBtn'=>array("確定送出",false,true,'')
        ),
        "filterType"=>array(
            'sDate'=>'DateRange'    //不定義就是 <select>, Month:月選取， DateRange:日期範圍
        ),
        "filters"=>array(
            'did'=>array_merge(array("" => "-全部文號-"), $odCodeList),
            $FName =>$orgs,
            'depID'=>$deps,
            'oFileAttr'=>$otherArray,
            'sDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
        ),
        "extraButton"=>array(
            array("統計報表",false,true,'odfind_report'),
        )

    );

    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"      => array($Fdid, "20px", true, array('Id', "gorec(%d)")),
        "sDate"    => array($FsDate, "20px", true, array('Date')),
        "rActName" => array($FrActName, "30px", true, array('Text')),
        "subjects" => array("主旨", "150px", false, array('Text')),        
        "sId"  => array("承辦", "20px", true, array('Define', getMan)),
        "odDeadlineType"=>array('類別', "20px", true, array('Text')),
        "id"       => array("內容", "20px", false, array('Define', getMaintain)),
        "isReject" => array("簽核未通過", "10px", true, array('Bool')),

    );
    
}
