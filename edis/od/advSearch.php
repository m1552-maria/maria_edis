<?php
	@session_start();
	include_once "../../config.php";
	include_once "$root/system/db.php";
	include_once "$root/setting.php";
	include_once "$root/inc_vars.php";
	include_once "$root/getEmplyeeInfo.php";
	include_once "$root/edis/func.php";

	if ($_REQUEST['odMenu'] == 'rod') {
		$odTypeName      = '收文';
		$odOrg           = '來文';
		$odNumHint       = '採西元後兩碼+四碼流水號';
		$dateTitle       = '文書承辦日期：';
		$singleDateTitle = '來文';
	} elseif ($_REQUEST['odMenu'] == 'sod') {
		$odTypeName      = '發文';
		$odOrg           = '受文';
		$odNumHint       = '採民國年+四碼流水號';
		$dateTitle       = '發文日期：';
		$singleDateTitle = '發文';
	}

	//$odCodeList   = array('瑪基' => '瑪基', '瑪啟' => '瑪啟', '瑪愛' => '瑪愛', '瑪霧' => '瑪霧');
	$odStatusList = array('1' => '未歸檔', '2' => '已歸檔');

	//發文機關選項
	$sql = "select id,sTitle from organization where id in('".implode("','",$orgUse)."')";
	$db = new db();
	$rs = $db->query($sql);
	while ($r = $db->fetch_array($rs)) {
		$soOrgs[$r[0]] = $r[1];
	}

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>進階搜尋</title>
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">

<script src="/Scripts/jquery-1.3.2.min.js"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/form.js"></script>
<form name="form1" method="post" action="">
	<table width="90%" align="center" style="padding-top:10px; font-family:'微軟正黑體',Verdana; font-size:13px">
		<? if ($_REQUEST['odMenu'] == 'sod') { ?>
			<tr>
				<td align="right"><? echo $odTypeName; ?>機關：</td>
				<td>
					<select name="soAct" id="soAct">
						<? foreach ($soOrgs as $k => $v) echo "<option value='$k'>$v</option>"; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">附件類型：</td>
				<td>
					核銷冊>1：<input name="budgetCount" type="checkbox"  id="budgetCount" />
					成果冊>1：<input name="resultCount" type="checkbox"  id="resultCount" />
					計畫書>1：<input name="planCount" type="checkbox"  id="planCount" />
				</td>
			</tr>
		<? } ?>
		<? if ($_REQUEST['odMenu'] == 'rod') { ?>
			<tr>
				<td align="right">來文字號：</td>
				<td align="left">
					<input type="text" name="dNo" id="dNo">
				</td>
			</tr>
			<tr>
				<td align="right"><?echo $singleDateTitle; ?>日期：</td>
				<td><input name="single_date" type="text" class="queryDate" id="single_date" size="7" maxlength="10" /></td>
			</tr>
		<? } ?>

		<tr>
			<td align="right"><?echo $odTypeName; ?>編碼類型：</td>
			<td>
				<select name="odcode" id="odcode">
					<? foreach ($odCodeList as $k => $v) echo "<option value='$k'>$v</option>"; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right"><?echo $odTypeName; ?>字號：</td>			
			<td><input type="text" name="odNum" id="odNum" placeholder='<?echo $odNumHint ?>'></td>
		</tr>		
		<tr>
			<td align="right"><span style="color: red;">*</span>公文狀態：</td>
			<td>
				<select name="odLevel" id="odLevel">
					<option value=''>-全部-</option>
					<? foreach ($odStatusList as $k => $v) echo "<option value='$k'>$v</option>"; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right"><span style="color: red;">*</span><?echo $dateTitle; ?></td>
			<td>
				起始日期:<input name="date_start" type="text" class="queryDate" id="date_start" size="7" maxlength="10" /> ~
				結束日期:<input name="date_end" type="text" class="queryDate" id="date_end" size="7" maxlength="10" />
			</td>
		</tr>
		<tr>
			<td align="right">承辦部門：</td>
			<td align="left">
				<input name="depID" class="queryID"  id="admDep" type="text" size="6" onkeypress="return checkInput(event,'depID',this)"/>
			</td>
		</tr>
		<tr>
			<td align="right"><?echo $odOrg; ?>機構：</td>
			<td><input type="text" name="org" id="org"></td>
		</tr>
		<tr>
			<td align="right">主旨：</td>
			<td><input type="text" name="subject" id="subject"></td>
		</tr>
		<tr>
			<td align="right">承辦人：</td>
			<td align="left"><input name="sId" type="text" class="queryID" id="sId" required ="true" size="6" value="<?=$empID?>" title="<?=$emplyeeinfo[$empID]?>" onkeypress="return checkInput(event,'empName',this)"/>
			</td>
		</tr>
		<tr>
			<td align="right"><?echo $odTypeName; ?>單位：</td>
			<td><input name="sAct" type="text" id="sAct"  size="6" required ="true"/></td>
		</tr>
		<tr>
			<td align="right">類別：</td>
			<td>
				<select name="odDeadlineType" id="odDeadlineType">
					<option value=''>-全部-</option>
					<? foreach ($odDeadlineType as $k => $v) echo "<option value='$k'>$v</option>"; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right">&nbsp;</td>
			<td>
				<input type="button" name="btnOK" id="btnOK" value="確定" onClick="doConfirm()">
				<input type="button" name="btnCancel" id="btnCancel" value="取消" onClick="window.close()">
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	var odMenu = '<?php  echo $_REQUEST['odMenu']; ?>';
	var rzt = '';	//where String
	var sRzt = '';

	function doConfirm(){
		rzt = '';
		// 公文字號
		var odcode = false;
		var odNum = false;
		if($("#odcode").val()) var odcode = true;
		if($("#odNum").val()) var odNum = true;
		if(odcode){
			if(odNum){
				var v = $("#odcode").val()+'字第' + $("#odNum").val() + '號';
				if(rzt) rzt += " AND did = '"+v+"'"; 
				else rzt += "did = '"+v+"'"; 
			}else{
				var v =  $("#odcode").val()+'字第';
				if(rzt) rzt += " AND did like '"+v+"%'";
				else rzt += "did like '"+v+"%'";
			}
		}

		//公文狀態
		var odLevel = $("#odLevel").val();
		if(odLevel) {
			if(odLevel == '1'){//未歸檔
				var levelsql = " (filingNo IS NULL)";
			}else if(odLevel == '2'){//已歸檔
				var levelsql = " (filingNo IS NOT NULL)";
			}

			if(rzt) rzt += " AND " + levelsql;
			else rzt += levelsql; 
		}

		//類別
		var odDeadlineType = $("#odDeadlineType").val(); 
		if(odDeadlineType) { 
			if(rzt) rzt += " AND odDeadlineType = '"+odDeadlineType+"'";
			else rzt += "odDeadlineType = '"+odDeadlineType+"'";
		}

		//主旨
		var v = $("#subject").val();
		if(v) { 
			if(rzt) rzt += " AND subjects like '%"+v+"%'"; 
			else rzt += "subjects like '%"+v+"%'"; 
		}

		var alertText = '請輸入完整'+'<?php  echo mb_substr($dateTitle,0,mb_strpos($dateTitle,'：')); ?>';
		var sDay = $("#date_start").val();
		var eDay = $("#date_end").val();

		if(odMenu=='rod'){
			//文書承辦日期
			if(sDay != '' && eDay != ''){ 		//判斷都有值
				if(rzt) rzt += " AND rDate >= '"+sDay+"' AND rDate <= '"+eDay+"'";
				else rzt += "rDate >= '"+sDay+"' AND rDate <= '"+eDay+"'";
			}else if(sDay == '' || eDay == ''){
				alert(alertText);
				return false;
			}

    		var v = $("#dNo").val();//來文字號
			if(v) { 
				if(rzt) rzt += " AND dNo like '%"+v+"%'";
				else rzt += "dNo like '%"+v+"%'"; 
			}

			var v = $("#org").val();//來文機構
			if(v) { 
				if(rzt) rzt += " AND sActName like '%"+v+"%'"; 
				else rzt += "sActName like '%"+v+"%'"; 
			}

            var v = $("#sAct").val();//收文單位
            if(v) { 
            	if(rzt) rzt += " AND rActName like '%"+v+"%'"; 
            	else rzt += "rActName like '%"+v+"%'";
            }

            var v = $("#single_date").val();//來文日期/發文日期
			if(v) { 
				if(rzt) rzt += " AND sDate = '"+v+"' "; 
				else rzt += "sDate = '"+v+"' "; 
			}

			var v = $("#sId").val();//承辦人
			if(v) { 
				if(rzt) rzt += " AND EXISTS(SELECT 1 FROM map_orgs_sign WHERE signlevel=1 AND signMan='"+v+"' AND edisid = otable.id)"; 
				else rzt = "EXISTS(SELECT 1 FROM map_orgs_sign WHERE signlevel=1 AND signMan='"+v+"' AND edisid = otable.id)"; 
			}
		}else if(odMenu=='sod'){
			//發文日期
			if(sDay != '' && eDay != ''){ 		//判斷都有值
				if(rzt) rzt += " AND sDate >= '"+sDay+"' AND sDate <= '"+eDay+"'";
				else rzt += "sDate >= '"+sDay+"' AND sDate <= '"+eDay+"'";
			}else if(sDay == '' || eDay == ''){
				alert(alertText);
				return false;
			}

			var v = $("#soAct").val(); //發文機關
			if(v.length>0) {
				if(rzt) rzt += " AND soAct='"+v+"'"; 
				else rzt += "soAct='"+v+"'"; 
			}

			var v = $("#org").val();//受文機構
			if(v) { 
				if(rzt) rzt += " AND rActName like '%"+v+"%'"; 
				else rzt += "rActName like '%"+v+"%'"; 
			}

            var v = $("#sAct").val();//文號選取
            if(v) { 
            	if(rzt) rzt += " AND sActName like '%"+v+"%'"; 
            	else rzt += "sActName like '%"+v+"%'";
            }

		    var budgetCheck = $("input[name=budgetCount]").is(":checked");//核銷冊
		    var resultCheck = $("input[name=resultCount]").is(":checked");//成果冊
		    var planCheck = $("input[name=planCount]").is(":checked");//計畫書

		    if(budgetCheck || resultCheck || planCheck){ 
		    	var checkCombin =''; 	
		    	if(budgetCheck){
		    		checkCombin = 'budgetNum>0';
		    	}
		    	if(resultCheck){
		    		if(checkCombin.length>0){
		    			checkCombin = checkCombin+' or resultNum>0';
		    		}else{
		    			checkCombin = checkCombin+' resultNum>0';
		    		}
		    	}
		    	if(planCheck){
		    		if(checkCombin.length>0){
		    			checkCombin = checkCombin+'  or planNum>0';
		    		}else{
		    			checkCombin = checkCombin+' planNum>0';
		    		}		    		
		    	}
		    	if(rzt) rzt += " AND "+checkCombin+""; 
		    	else rzt += checkCombin;
		    }

		    var v = $("#sId").val();//承辦人
		    if(v) { 
		    	if(rzt) rzt += " AND sId='"+v+"'"; 
		    	else rzt = "sId='"+v+"'"; 
		    }
		}

		var admDep = $("#admDep").val()

		if(!rzt) { 
			alert('您尚未設定任何條件!');  
			return false; 
		}
        //add by Tina 20190418 取得文書承辦日期查詢區間
        var dateRange =  $("#date_start").val()+'-'+$("#date_end").val() ;

        var rztObj = { 'where':rzt, 'DateRange':dateRange, 'admDep':admDep }

        window.opener.returnAdv(rztObj);
		window.close(); //關掉
	}
</script>