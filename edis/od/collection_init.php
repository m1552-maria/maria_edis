<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
error_reporting(E_ALL);
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once "$root/config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
include_once "$root/inc_vars.php";
include_once "$root/getEmplyeeInfo.php";

include_once "$root/edis/func.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "ListControl.php";
include "$root/system/ViewControl.php";
if ($odMenu == 'rod') {
    $odTypeName = '收文';
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';

}
$pageTitle = "我的收藏";
//$pageSize=5; //使用預設
$tableName = "edis ed";
// for Upload files and Delete Record use
$ulpath = "/data/edis/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}


/*query sql add by Tina 20190508 優化效能不使用view,將view的select 搬出來*/

// $querySql =$myod;



$delField = 'Ahead';
$delFlag  = true;

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, true, SearchKeyword),
    "newBtn"    => array("＋新增", false, false, doAppend),
    "editBtn"   => array("－修改", false, false, doEdit),
    "delBtn"    => array("Ｘ刪除", false, false, doDel),
   /* "extraButton" => array(
            "SearchAdv" => array("進階搜尋",false,true,doSearchAdv)
        )*/
);

//:ListControl  ---------------------------------------------------------------

//收發文之欄位名稱不同
if ($odMenu == 'rod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '來文日期';
    $FsActName = '來文機關';
    $FrActName = '收文機關';
    $querySql  = $myRod;
} else if ($odMenu == 'sod') {
    $Fdid      = $odTypeName . '字號';
    $FsDate    = '發文日期';
    $FsActName = '發文機關';
    $FrActName = '受文者';
    $querySql  = $mySod;
}
$querySql .= ' LEFT JOIN collections c ON( c.edisid = ed.id)';

if ($odMenu == 'rod') {
$opList = array(
    "alterColor"    => true, //交換色
    "canScroll"     => true,
    "curOrderField" => 'did', //主排序的欄位
    "sortMark"      => 'desc', //升降序 (ASC | DES)
    "searchField"   => array('did', 'sActName', 'creator', 'subjects'), //要搜尋的欄位
);    
    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"      => array($Fdid, "30px", true, array('Id', "gorec(%d)")),
        "rDate"    => array('承辦日期', "30px", true, array('Date')),
        //"deadline" => array("期限", "30px", true, array('Text')),        
        "sActName" => array($FsActName, "30px", true, array('Text')),
        //"rActName" => array($FrActName, "30px", true, array('Text')),
        //"creator"  => array("文書", "30px", true, array('Define', getMan)),
        //"sId"  => array("承辦", "30px", true, array('Define', getMan)),
        "subjects" => array("主旨", "80px", false, array('Text')),
        "id"       => array("內容", "50px", false, array('Define', getMaintain)),
        "isReject" => array("簽核未通過", "30px", true, array('Bool')),

    );
} else if ($odMenu == 'sod') {
$opList = array(
    "alterColor"    => true, //交換色
    "canScroll"     => true,
    "curOrderField" => 'did', //主排序的欄位
    "sortMark"      => 'desc', //升降序 (ASC | DES)
    "searchField"   => array('did', 'sActName', 'creator', 'subjects'), //要搜尋的欄位
);    
    $fieldsList = array(
        "collection"  => array("收藏", "10px", true, array('Text')),
        "did"      => array($Fdid, "30px", true, array('Id', "gorec(%d)")),
        "sDate"    => array($FsDate, "30px", true, array('Date')),
        //"sActName" => array($FsActName, "30px", true, array('Text')),
        "rActName" => array($FrActName, "30px", true, array('Text')),
        "creator"  => array("文書", "30px", true, array('Define', getMan)),
        "sId"  => array("承辦", "30px", true, array('Define', getMan)),
        "subjects" => array("主旨", "80px", false, array('Text')),
        "sumNum"  => array("總表數量", "30px", false, array('Text')),
        "id"       => array("內容", "50px", false, array('Define', getMaintain)),
        "isReject" => array("簽核未通過", "30px", true, array('Bool')),

    );
}

//:ViewControl -----------------------------------------------------------------
/*    $fieldA = array(
"SimpleText"=>array("簡述<span class='need'>*</span>","text",60,'','',array(true,'','',8)),
"Content"=>array("內文","textarea",60,6,'',array(true,'','',8)),
"unValidDate"=>array("失效日期","datetime",20,'','',array(true,PTN_DATETIME,'請輸入日期時間')),
'Link'=>array("說明","textarea",60,5,),
'isReady'=>array("上架","checkbox",20)
);
$fieldA_Data = array("unValidDate"=>date('Y/m/d',strtotime("+30 days")),'isReady'=>1);

$fieldE = array(
"ID"=>array("編號","id",20,),
"SimpleText"=>array("簡述","text",60,),
"unValidDate"=>array("失效日期","date",10,),
'Link'=>array("連結","text",60,'',''),
"Content"=>array("內文","textarea",60,6),
'isReady'=>array("上架","checkbox",60,5,'')
);
 */
