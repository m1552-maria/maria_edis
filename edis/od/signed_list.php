<?php 
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */

include_once 'signed_init.php';

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'new' || $act == 'edit') {
    $_SESSION['myod_tab'] = 0;
}

//登入者為誰的代理人及已離職職員之主管
// $agentList = getAgent($_SESSION['empID']);
$agentList = 'null';

$tabidx = $_SESSION['myod_tab'] ? $_SESSION['myod_tab'] : 0;
if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $db    = new db();
    //檢查是否要順便刪除檔案
    if (isset($delFlag)) {
        $sql = "select * from $tableName where $aa[0] in ($lstss)";
        $rs  = $db->query($sql);
        while ($r = $db->fetch_array($rs)) {
            $fns = $r[$delField];
            if ($fns) {
                $ba = explode(',', $fns);
                foreach ($ba as $fn) {
                    $fn = $root . $ulpath . $fn;
                    //echo $fn;
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }

                }
            }
        }
    }
    $sql = "delete from $tableName where $aa[0] in ($lstss)";
    //echo $sql; exit;
    $db->query($sql);

}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_Tel($value)
{
    return "TEL:" . $value;
}

function func_Time($value)
{
    return "<input type'text' value='$value.:00'>";
}

function func_note($value)
{
    return "defined:" . $value;
}

?>
<!-- ::Stand-alone -->
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="/media/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/media/js/jquery-ui-1.10.1.custom.min.js"></script>      
<!-- -->

<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">

<!--日期區間css-->
<link href="/Scripts/daterangepicker/daterangepicker.css" rel="stylesheet"></link>
<link href="/Scripts/daterangepicker/MonthPicker.css" rel="stylesheet"/>

<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>

<script src="/Scripts/form.js"></script>

<script src="/edis/edis.js"></script>

<!--日期區間js-->
<script src="/Scripts/daterangepicker/moment.min.js"></script>
<script src="/Scripts/daterangepicker/daterangepicker.min.js"></script>
<script src="/Scripts/daterangepicker/MonthPicker.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script>
    function doDel(id){
        if(!confirm("確定刪除?")){ return;}
        location.href='edis/del.php?id='+id;
    }

    function setnail(n) {
     $('.tabimg').attr('src','../images/unnail.png');
     $('#tabg'+n).attr('src','../images/nail.png');
     $.get('edis/setnail.php',{'idx':n});
 }

 function stateitgform(id,did,signLev) {
    statefm.mosid.value =id;
    $("#odDid").html(did);
    statefm.signLev.value =signLev;
    $('#div1').show();

}
function addItem(tbtn,fld,qid) {

    var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6'>"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
    + " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
    var divTitle ='#'+qid+'Div';
    $(divTitle).append(newone);
}
function removeItem(tbtn) {
    $(tbtn).parent().remove();
}  
</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<style>
#div1 {
  position:absolute;
  left: 40%; top:30%;
  background-color:#888;
  padding: 20px;
  display:none;
}
.date{width:50px;}
.spanbtn {
  font-size:smaller;
  float:right;
  cursor: pointer;
}
</style>


<?php 
$whereStr = ''; //initial where string
$wherekStrSave = ''; //add by Tina 20190508 
//:filter handle  ---------------------------------------------------
$whereAry  = array();
    //收發文之基本where條件不同
if(isProxyMan($_SESSION['empID'])){
    $agentList = join(',',getProxyPeople($_SESSION['empID']));
}

if ($odMenu == 'rod') {
    $wherefStr = "odType='" . $odTypeName . "' and (signMan='".$_SESSION['empID']."' or agent in($agentList))";
} else if ($odMenu == 'sod') {
    $wherefStr = "odType='" . $odTypeName . "' and (signMan='".$_SESSION['empID']."' or agent in($agentList))";
}
//$wherefStr = "signMan='$_SESSION[empID]' and odType='" . $odTypeName . "'";
if($opList['filters']) {
    foreach($opList['filters'] as $k=>$v) {
        $wType = $opList['filterType'][$k];
        switch($wType) {
            case 'Month':   
            if($_REQUEST[$k]) $curMh = $_REQUEST[$k];
            elseif($opList['filterOption'][$k]) $curMh=$opList['filterOption'][$k][0];
            $Days  = date('t',strtotime("$curMh/01"));
            $bDate = date($curMh.'/01');
            $eDate = date($curMh.'/'.$Days);    
            if($curMh) $whereAry[]="($k between '$bDate' and '$eDate 23:59:59')";
                break;
                case 'DateRange':
                $aa = array();
                if($_REQUEST[$k]) {
                    $aa = explode('~',$_REQUEST[$k]);
                } elseif($opList['filterOption'][$k]) {
                    $aa = explode('~',$opList['filterOption'][$k][0]);
                }
                if(count($aa)) $whereAry[] = "($k between '$aa[0]' AND '$aa[1] 23:59:59')";
                    break;
                    default:        
                     //update by Tina 20190508 篩選部門非實際欄位,需修改sql       
                    /*$oprator = $opList['filterCondi'][$k]?$opList['filterCondi'][$k]:"%s = '%s'"; 
                    if($_REQUEST[$k] || ($_REQUEST[$k]=='0' && $k!='depID')) $whereAry[]=sprintf($oprator,$k,$_REQUEST[$k]);
                    else if($opList['filterOption'][$k]) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);
                    */
                    if($opList['filterOption'][$k]) $whereAry[]=sprintf($oprator,$k,$opList['filterOption'][$k][0]);  
                }
            }
            if(count($whereAry)) $wherefStr .= "and ".join(' and ',$whereAry);
        }

//:Search filter handle  ---------------------------------------------
        $whereAry  = array();
        $wherekStr = '';
        if ($opList['keyword']) {
            $key = $opList['keyword'];
            foreach ($opList['searchField'] as $v) {
                $whereAry[] = "$v like '%$key%'";
            }

            $n = count($whereAry);
            if ($n > 1) {
                $wherekStr = '(' . join(' or ', $whereAry) . ')';
            } else if ($n == 1) {
                $wherekStr = join(' or ', $whereAry);
            }

        }

//add by Tina 20190508 篩選部門非實際欄位,需修改sql
        if($_REQUEST[$FName]){
         if($_REQUEST[$FName]) $fdepid=sprintf("%s = '%s'",$FName,$_REQUEST[$FName]);    
         if(strlen($wherekStr)>0){
          $wherekStr .=' and ('.$fdepid.')';
      }else{
          $wherekStr .='('.$fdepid.')';
      }
  }
// if($_REQUEST['depID']){
//     $fdepid=sprintf("%s = '%s'",'ed.depID',$_REQUEST['depID']);  
//     if(strlen($wherekStr)>0){
//         $wherekStr .=' and ('.$fdepid.')';
//     }else{
//         $wherekStr .='('.$fdepid.')';
//     }  
// }


//:Merge where
  if ($wherefStr || $wherekStr) {
    //update by Tina 20190805 $signed 已有where 條件
    //$whereStr = 'and ';
    $flag     = false;
    if ($wherefStr) {
        $whereStr .= $wherefStr;
        $flag = true;}
    /* update by Tina 針對list搜尋按鈕,搜尋之欄位為子查詢得來,非實際欄位,需修改sql查詢
    if ($wherekStr) {
        if ($flag) {
            $whereStr .= ' and ' . $wherekStr;
        } else {
            $whereStr .= $wherekStr;
        }

    }*/
    if ($wherekStr) {
        $wherekStrSave = $wherekStr;
    }
}

if($_REQUEST['depID']) $whereStr .= " AND dep.depID IN ('".$_REQUEST['depID']."') GROUP BY ed.id";
else $whereStr .= " GROUP BY ed.id ";

$db     = new db();
/*update by Tina 20190508 如有執行搜尋按鈕,因搜尋欄位非實際欄位,需修改sql*/

if($wherekStrSave){
    $osql    = sprintf('%s and %s', $querySql, $whereStr);
    $sql     = sprintf('select 1 from (%s) otable where %s ', $osql, $wherekStrSave);
}else{
    $sql    = sprintf('%s and %s', $querySql, $whereStr);
}
// echo $sql;
$rs     = $db->query($sql);
$rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
if (!$pageSize) {
    $pageSize = 10;
}

$pinf = new PageInfo($rCount, $pageSize);
if ($recno > 1) {
    $pinf->setRecord($recno);
} else if ($page > 1) {
    $pinf->setPage($page);
    $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;
}

//: Header -------------------------------------------------------------------------
    echo "<h1 align='center'>$pageTitle ";
    if ($opList['keyword']) {
        echo " - 搜尋:" . $opList['keyword'];
    }

    echo "</h1>";

//:PageControl ---------------------------------------------------------------------
    $obj = new EditPageControl($opPage, $pinf);
    echo "<div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//:Defined this page PHP Function -------------------------------------------------------------
    function getContent($v)
    {
        global $tableName;
        global $odMenu;
        $db  = new db();
        $sql = 'select * from ' . $tableName . ' where id=' . $v;
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $par = "id=".$r['id']."&empid=".$_SESSION['empID']."&loginType=".$_SESSION["loginType"];
        //列印
        if ($odMenu == 'rod') {
            $odtype = "edis/printform.php?/edis/viewtype/rodview.php?" . $par;
            $LastLevel = $rodLastLevel;
        } elseif ($odMenu == 'sod') {
            $LastLevel = $sodLastLevel;
            if ($r['dType'] == '令') {
                $odtype = "edis/printform.php?/edis/viewtype/command.php?" . $par;
            } else if ($r['dType'] == '函') {
                $odtype = "edis/printform.php?/edis/viewtype/letter.php?" . $par;
            } else if ($r['dType'] == '公告') {
                $odtype = "edis/printform.php?/edis/viewtype/declaration.php?" . $par;
            } else if ($r['dType'] == '開會通知單') {
                $odtype = "edis/printform.php?/edis/viewtype/notice.php?" . $par;
            }
        }
        $mtPrint = '<a href="' . $odtype . '" target="_blank"><img src="/images/detail.gif" title="公文內容" align="absmiddle" border="0"/></a>';

        /*新增附件連結*/
        if ($odMenu == 'rod') {
        //主文附件        
            $mainUlpath = 'data/edis/' . $r['id'] . '\/main\/';
            if (file_exists($mainUlpath)) {
                $dh = opendir($mainUlpath);
                while (false !== ($mfilename = readdir($dh))) {
                    $test .='read';
                    if ($mfilename == '.') {
                        continue;
                    }

                    if ($mfilename == '..') {
                        continue;
                    }

                    $mfile_path = $mainUlpath . $mfilename;
                }
                $mtMainAtt = '<a href="' . $mfile_path . '" target="_blank"><img src="images/mainAtt.png" title="主文附件" align="absmiddle" border="0"/></a>'; 
            }

            //其他附件
            $db = new db();
            $ofSql = "select * from map_files where aType='O' and eid ='" . $r['id'] . "'";
            $rsOf  = $db->query($ofSql);
            if($db->num_rows($rsOf)>0){ 
                $path ='edis/tabpage/att_other.php';
                $par = json_encode(array('path'=>$path,'id'=>$r['id'],'odMenu'=>$odMenu));

                $mtOtherAtt = '<img src="images/oAtt.png" style="width:16px;height:16px" onclick=\'showOtherAtt('.$par.')\' align="absmiddle" title="其他附件"/>';  

            }  
            if(!checkAttView($r['id'],$_SESSION['empID']) && $_SESSION['loginType'] != 'cxo'){
                $mtMainAtt = '';
                $mtOtherAtt =''; 
            }  

            return $mtMainAtt.' '.$mtOtherAtt.' '.$mtPrint;
        }elseif ($odMenu == 'sod') {
            $db = new db();
            $ofSql = "select * from map_files where aType='O' and eid ='" . $r['id'] . "'";
            $rsOf  = $db->query($ofSql);
            if($db->num_rows($rsOf)>0){ 
                $path ='edis/tabpage/att_other.php';
                $par = json_encode(array('path'=>$path,'id'=>$r['id'],'odMenu'=>$odMenu));

                $mtOtherAtt = '<img src="images/oAtt.png" style="width:16px;height:16px" onclick=\'showOtherAtt('.$par.')\' align="absmiddle" title="其他附件"/>';  

            }
            return $mtOtherAtt . ' ' . $mtPrint;
        }
    }

    function getLevelName($v){
        global $odMenu;
        if ($odMenu == 'rod') {
            global $RsignLevel;
            if (empty($RsignLevel[$v])) {
                return '加簽';
            } else {
                return $RsignLevel[$v];
            }

        } elseif ($odMenu == 'sod') {
            global $SsignLevel;
            if (empty($SsignLevel[$v])) {
                return '加簽';
            } else {
                return $SsignLevel[$v];
            }
        }
    }

    function getMan($v)
    {
        global $emplyeeinfo;return $emplyeeinfo[$v];
    }

//:ListControl Object --------------------------------------------------------------------
$n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
/*update by Tina 20190508 如有執行搜尋按鈕,因搜尋欄位非實際欄位,需修改sql*/
if ($wherekStrSave) {
    $osql = sprintf('%s and %s',
        $querySql,
        $whereStr
    );
    $sql = sprintf('select * from(%s) otable where %s order by %s %s limit %d,%d',
        $osql,
        $wherekStrSave,
        $opList['curOrderField'],
        $opList['sortMark'],
        $n * $pinf->pageSize,
        $pinf->pageSize
    );
}else{

    $sql = sprintf('%s and  %s order by %s %s limit %d,%d',
    $querySql, $whereStr,
    $opList['curOrderField'], $opList['sortMark'],
    $n * $pinf->pageSize, $pinf->pageSize
);

}
// echo $sql;
$rs    = $db->query($sql);
$pdata = $db->fetch_all($rs);

foreach ($pdata as &$pdataInfo) {
    $id = $pdataInfo['id'];
    if (isset($id)) {
        global $odTypeName;
        $mtCollection='';   
        $tempDB = new db();
        $sql = "select * from edis where id =".$id;    
        $rs = $tempDB->query($sql);
        while( $r = $tempDB->fetch_array($rs) ){ 
            $budget = '核銷冊:'.(empty($r['budgetNum'])? 0: $r['budgetNum']);
            $result = '成果冊:'.(empty($r['resultNum'])? 0: $r['resultNum']);
            $plan   = '計畫書:'.(empty($r['planNum'])? 0: $r['planNum']);
            $pdataInfo['sumNum'] = $budget.' '.$result.' '.$plan;
            if($r['isSecret'] == '1'){
                $pdataInfo['did'] = "<img src='../images/lock.png' style='width:3%'> ".$pdataInfo['did'];
            }
        }

        //我的收藏
        $ofSql = "select * from collections where edisid ='" . $id . "' and creatorid ='".$_SESSION["empID"]."'";
        $rsOf  = $tempDB->query($ofSql);
        $par = json_encode(array('empid'=>$_SESSION["empID"],'id'=>$id,'odTypeName'=>$odTypeName));
        if($tempDB->num_rows($rsOf)>0){ 
            $mtCollection = '<img src="images/heart_red.png" id="heart'.$id.'" onclick=\'cancelCollection('.$par.')\' align="absmiddle" title="取消收藏"/>'; 
        }else{
            $mtCollection = '<img src="images/heart.png" id="heart'.$id.'" onclick=\'addCollection('.$par.')\' align="absmiddle" title="新增收藏"/>'; 
        }

        $pdataInfo['collection'] = $mtCollection;

        $tempDB->close();
    }
    unset($pdataInfo);
}
$obj   = new BaseListControl($pdata, $fieldsList, $opList, $pinf);

//:ViewControl Object ------------------------------------------------------------------------
$listPos = $pinf->curRecord % $pinf->pageSize;
switch ($act) {
    case 'edit': //修改
    $op3 = array(
        "type"      => "edit",
        "form"      => array('form1', "$tableName/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定修改", false, ''),
        "cancilBtn" => array("取消修改", false, ''));
        //$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);
    break;
    case 'new': //新增
    $op3 = array(
        "type"      => "append",
        "form"      => array('form1', "$tableName/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
        "submitBtn" => array("確定新增", false, ''),
        "cancilBtn" => array("取消新增", false, ''));
        //$ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
    break;
    default: //View
        /* $op3 = array(
"type"=>"view",
"submitBtn"=>array("確定變更",true,''),
"resetBtn"=>array("重新輸入",true,''),
"cancilBtn"=>array("關閉",true,''));
$ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);    */
}

$edisID = $pdata[$listPos]['id'];
$empid = $_SESSION['empID'];
$loginType = $_SESSION["loginType"];
?>

<div class="tabbable tabbable-custom">
  <ul class="nav nav-tabs">
    <li <?php  echo ($tabidx == 0 ? "class='active'" : '')?> onclick="setnail(0)"><a href="#tab0" data-toggle="tab" >附件<img id="tabg0" class="tabimg" src="/images/<?php  echo $tabidx == 0 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
    <li <?php  echo ($tabidx == 1 ? "class='active'" : '')?> onclick="setnail(1)"><a href="#tab1" data-toggle="tab">簽收狀況<img id="tabg1" class="tabimg" src="/images/<?php  echo $tabidx == 1 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
    <li <?php  echo ($tabidx == 2 ? "class='active'" : '')?> onclick="setnail(2)"><a href="#tab2" data-toggle="tab">簽收失效歷程<img id="tabg2" class="tabimg" src="/images/<?php  echo $tabidx == 2 ? 'nail.png' : 'unnail.png'?>" align="absmiddle" /></a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane<?php  echo ($tabidx == 0 ? ' active' : '')?>" id="tab0">
      <iframe id="tab0if" width="100%" height="320" frameborder="0" src="edis/tabpage/att.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>&empid=<?php  echo $empid ?>&loginType=<?php  echo $loginType?>"></iframe>
  </div>
  <div  class="tab-pane<?php  echo ($tabidx == 1 ? ' active' : '')?>" id="tab1">
      <iframe id="tab1if" width="100%" height="320" frameborder="0" src="edis/tabpage/odsign.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>"></iframe>
  </div>
  <div  class="tab-pane<?php  echo ($tabidx == 2 ? ' active' : '')?>" id="tab2">
      <iframe id="tab2if" width="100%" height="320" frameborder="0" src="edis/tabpage/history.php?id=<?php  echo $edisID?>&odMenu=<?php  echo $odMenu?>"></iframe>
  </div>
</div>
<?php 
include 'public/inc_listjs.php';
?>

