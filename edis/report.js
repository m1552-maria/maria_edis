function cancelReport(reportType){
 var insert_type_flag ='#'+reportType+'Flag';
 var divName ='#'+reportType;
  $(insert_type_flag).val('F');
    changeReportRequired();
  $(divName).hide();

}

function insertReport(reportType){
 var insert_type_flag ='#'+reportType+'Flag';
 var divName ='#'+reportType;
 $(insert_type_flag).val('T');
   changeReportRequired();
 $(divName).hide();

}

function closeReport(reportType){
 var divName ='#'+reportType;
 $(divName).hide();

}

//出現總表DIV畫面
function setReport(divName){
  $("#receiptFlag").val('T');
  changeReportRequired();
      var div = '#'+divName;
      if(divName=='receipt'){
      $("#rOid").val($("#rAct").val());
      $("#rOid").next().next().html($("#rAct").next().next().html());
      $("#sOid").val($("#sAct").val());
      $("#sOid").next().next().html($("#sAct").next().next().html());
      $("#sid").val($("#sId").val());
      $("#sid").next().next().html($("#sId").next().next().html());
      }

      $(div).show();
};    

function checkReport(){
var receipt_val =$("#receiptFlag").val();

 if(receipt_val=='F'){
    changeReportRequired();
   return true;
 }else if(receipt_val=='T'){
    $("#applyDate").attr('required', 'required');
    $("#rOid").attr('required', 'required');
    $("#content").attr('required', 'required');
    $("#sOid").attr('required', 'required');
    $("#sid").attr('required', 'required');
    $("#amount").attr('required', 'required');
    if($("#applyDate").val().length > 0 && $("#rOid").val().length > 0 && $("#content").val().length > 0 && $("#sOid").val().length > 0 && $("#sid").val().length > 0 && $("#amount").val().length > 0 ){
      return true;
    }else{
      alert('領據內容請填寫完整,否則請按刪除/取消');
      return false;
    }

    
 }

}


function changeReportRequired(){

var receipt_val =$("#receiptFlag").val();
 if(receipt_val=='F'){

$("#applyDate").removeAttr('required');
$("#rOid").removeAttr('required');
$("#content").removeAttr('required');
$("#sOid").removeAttr('required');
$("#sid").removeAttr('required');
$("#amount").removeAttr('required');

 }else if(receipt_val=='T'){
$("#applyDate").attr('required', 'required');
$("#rOid").attr('required', 'required');
$("#content").attr('required', 'required');
$("#sOid").attr('required', 'required');
$("#sid").attr('required', 'required');
$("#amount").attr('required', 'required');
 }

}