<?
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once '../../setting.php';
$db    = new db();

$rSql  = "select  mos.*,(select o.title from map_orgs mo,organization o where mo.id =mos.moid and mo.oid=o.id)orgTitle from map_orgs_sign mos where edisid =" . $_REQUEST['id'] . " and ifnull(signlevel,0) !='".$rodLastLevel."' order by FIND_IN_SET(id, getEdisChildList(edisid)) desc";
$rr    = $db->query($rSql);
$rLast = "select  mos.*,(select o.title from map_orgs mo,organization o where mo.id =mos.moid and mo.oid=o.id)orgTitle from map_orgs_sign mos where edisid =" . $_REQUEST['id'] . " and ifnull(signlevel,0) ='".$rodLastLevel."' order by FIND_IN_SET(id, getEdisChildList(edisid))";
$rlr   = $db->query($rLast);

$sSql  = "select  mos.*,(select o.title from map_orgs mo,organization o where mo.id =mos.moid and mo.oid=o.id)orgTitle from map_orgs_sign mos where edisid =" . $_REQUEST['id'] . " and ifnull(signlevel,0) !='".$sodLastLevel."' order by FIND_IN_SET(id, getEdisChildList(edisid)) desc";
$rs    = $db->query($sSql);
$sLast = "select  mos.*,(select o.title from map_orgs mo,organization o where mo.id =mos.moid and mo.oid=o.id)orgTitle from map_orgs_sign mos where edisid =" . $_REQUEST['id'] . " and ifnull(signlevel,0) ='".$sodLastLevel."' order by FIND_IN_SET(id, getEdisChildList(edisid))";
$rls   = $db->query($sLast);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>公文簽收狀況</title>
</head>
<body>
    <table width="100%" border="1" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
        <tr bgcolor="#D8DBD6">
            <td >階段</td>
            <td >等簽</td>
            <td >簽核人</td>
            <td >代理簽核者</td>
            <td >簽核時間</td>
            <td >核准</td>
            <td >簽核用語</td>
            <td >簽核意見/內容</td>
        </tr>
<?
$k      = 0;
$rodArr = array();
$sodArr = array();
if ($_REQUEST['odMenu'] == 'rod') {

    while ($r = $db->fetch_array($rr)) {
        $rodArr[$k] = $r;
        $k++;
    }
    while ($r = $db->fetch_array($rlr)) {
        $rodArr[$k] = $r;
        $k++;
    }

    $levelCount = 0;
    foreach ($rodArr as $k => $v) {
        if (empty($RsignLevel[$rodArr[$k]['signLevel']])) {
            $signlevel = '加簽';
        } else {

            $signlevel = $RsignLevel[$rodArr[$k]['signLevel']];
        }
        echo "<tr>";
        if ($rodArr[$k]['signLevel'] == '0') {
            $levelCount++;

            echo "<td>" . $signlevel . "(" . $levelCount . ")</td>";

        } else if ($rodArr[$k]['signLevel'] == '1') {
            echo "<td>" . $signlevel . "(" . $levelCount . ")</td>";
        } else {
            echo "<td>" . $signlevel . "</td>";
        }

        if ($rodArr[$k]['isWait'] == '1') {
            echo "<td>" . "是" . "</td>";
        } else if ($rodArr[$k]['isWait'] == '0') {
            echo "<td>" . "否" . "</td>";

        }

        echo "<td>" . $emplyeeinfo[$rodArr[$k]['signMan']] . "</td>";
        echo "<td>" . $emplyeeinfo[$rodArr[$k]['agent']] . "</td>";
        echo "<td>" . $rodArr[$k]['signTime'] . "</td>";
        if (empty($rodArr[$k]['signTime'])) {
            echo "<td>" . "尚未簽核" . "</td>";
        } else {
            if ($rodArr[$k]['isSign'] == '1') {
                echo "<td>" . "是" . "</td>";
            } else if ($rodArr[$k]['isSign'] == '0') {
                echo "<td>" . "否" . "</td>";

            }

        }
        echo "<td>" . $rodArr[$k]['signState'] . "</td>";
        echo "<td>" . $rodArr[$k]['signContent'] . "</td>";
        echo "</tr>";

    }

} elseif ($_REQUEST['odMenu'] == 'sod') {
//發文
    while ($r = $db->fetch_array($rs)) {
        $sodArr[$k] = $r;
        $k++;
    }
    while ($r = $db->fetch_array($rls)) {
        $sodArr[$k] = $r;
        $k++;
    }
    foreach ($sodArr as $k => $v) {
        if (empty($SsignLevel[$sodArr[$k]['signLevel']])) {
            $signlevel = '加簽';
        } else {

            $signlevel = $SsignLevel[$sodArr[$k]['signLevel']];
        }
        echo "<tr>";
        echo "<td>" . $signlevel . "</td>";
        if ($sodArr[$k]['isWait'] == '1') {
            echo "<td>" . "是" . "</td>";
        } else if ($sodArr[$k]['isWait'] == '0') {
            echo "<td>" . "否" . "</td>";

        }
        echo "<td>" . $emplyeeinfo[$sodArr[$k]['signMan']] . "</td>";
        echo "<td>" . $emplyeeinfo[$rodArr[$k]['agent']] . "</td>";
        echo "<td>" . $sodArr[$k]['signTime'] . "</td>";
        if (empty($sodArr[$k]['signTime'])) {
            echo "<td>" . "尚未簽核" . "</td>";
        } else {
            if ($sodArr[$k]['isSign'] == '1') {
                echo "<td>" . "是" . "</td>";
            } else if ($sodArr[$k]['isSign'] == '0') {
                echo "<td>" . "否" . "</td>";

            }

        }
        echo "<td>" . $sodArr[$k]['signState'] . "</td>";
        echo "<td>" . $sodArr[$k]['signContent'] . "</td>";
        echo "</tr>";

    }
}
?>
    </table>
</body>
</html>
