<?php 
/*
===== For 1 List Table Basic use =====
Handle parameter and user operation.
Create By Michael Rou from 2017/9/21
 */
include_once "init.php";

if (isset($_REQUEST['act'])) {
    $act = $_REQUEST['act'];
}

if ($act == 'new' || $act == 'edit') {
    $_SESSION['myod_tab'] = 0;
}

$tabidx = $_SESSION['myod_tab'] ? $_SESSION['myod_tab'] : 0;
if ($act == 'del') {
    //:: Delete record -----------------------------------------
    $aa = array();
    foreach ($_POST['ID'] as $v) {
        $aa[] = "'$v'";
    }

    $lstss = join(',', $aa);
    $db    = new db();
    $sql = "delete from map_orgs_sign where id in ($lstss)";
    $db->query($sql);

    $sql = "select count(1) as Num from map_orgs_sign where edisid = '" . $_GET['id'] . "'  AND isSign IS NULL AND signLevel = '3'";
    $rs  = $db->query($sql);
    $c   = $db->fetch_array($rs);
    if($c['Num'] == 0){
        $updatSignState = "update edis set signStage = '4' where id = '" . $_GET['id'] . "'";
        $db->query($updatSignState);
    }
}

//::處理排序 ------------------------------------------------------------------------
if (isset($_REQUEST["curOrderField"])) {
    $opList['curOrderField'] = $_REQUEST["curOrderField"];
}

if (isset($_REQUEST["sortMark"])) {
    $opList['sortMark'] = $_REQUEST["sortMark"];
}

if (isset($_REQUEST["keyword"])) {
    $opList['keyword'] = $_REQUEST["keyword"];
}

//::處理跳頁 -------------------------------------------------------------------------
$page  = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$recno = $_REQUEST['recno'] ? $_REQUEST['recno'] : 1;

//:Defined PHP Function -------------------------------------------------------------
function func_Tel($value)
{
    return "TEL:" . $value;
}

function func_Time($value)
{
    return "<input type'text' value='$value.:00'>";
}

function func_note($value)
{
    return "defined:" . $value;
}

?>

<!-- ::Stand-alone -->
<script src="/Scripts/jquery-1.12.3.min.js"></script>
<script src="/media/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/media/js/jquery-ui-1.10.1.custom.min.js"></script>
<!-- -->
<link href="/system/PageControl.css" rel="stylesheet" type="text/css">
<link href="/system/ListControl.css" rel="stylesheet" type="text/css">
<link href="/system/ViewControl.css" rel="stylesheet" type="text/css">
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet">
<link href="/Scripts/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link href="/Scripts/form.css" rel="stylesheet">
<link href="/css/list.css" rel="stylesheet">


<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/jquery.treeview.js"></script>
<script src="/ckeditor/ckeditor.js"></script>

<script src="/Scripts/form.js"></script>

<script src="/system/utilities/system.js"></script>
<script src="/system/PageControl.js"></script>
<script src="/system/ListControl.js"></script>
<script src="/system/ViewControl.js"></script>
<script src="/system/TreeControl.js"></script>
<script>

function setnail(n) {
    $('.tabimg').attr('src','../images/unnail.png');
    $('#tabg'+n).attr('src','../images/nail.png');
    $.get('edis/setnail.php',{'idx':n});
}

function returnValue(data) {

    if(data) {
        param.val(data[0]);
        param.trigger("focus");
        param.next().next('span').html(data[1]); 
        var cbFunc = param.attr('callback');
        if(cbFunc) setTimeout(cbFunc+'("'+data[0]+'")',0);

        $.ajax({
            url: '/edis/api.php',
            type: 'POST',
            data: { 
                act: 'checkSign',
                empId : $("[name='signMan']").val(),
                id : '<?php echo $_GET["id"];?>'
            },
            dataType: 'json',
            success: function(d){
                if(d == 1) alert($("[name='signMan']").val()+'已重複加簽');
            }
        });
    }
}

function checkInput(evt,act,elm) {
    var v = elm.value;
    function handleRezult(data) { //Closure
        if(data) {
            if(data=='無此編號！') {
                $(elm).val('');
                $(elm).next().next().text(v+' '+data); 
            } else{
                $(elm).next().next().text(data); 
                $.ajax({
                    url: '/edis/api.php',
                    type: 'POST',
                    data: { 
                        act: 'checkSign',
                        empId : $("[name='signMan']").val(),
                        id : '<?php echo $_GET["id"];?>'
                    },
                    dataType: 'json',
                    success: function(d){
                        if(d == 1) alert($("[name='signMan']").val()+'已重複加簽');
                    }
                });
            }
        }       
    }
    
    if( typeof evt === 'undefined' ) { //with ViewControl
        $.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
        return false;
    } else {  //without ViewControl
        if(evt.keyCode==13) {
            $.get('/getdbInfo.php',{'act':act,'ID':v},handleRezult);
            return false;
        }
    }
}

</script>
<link href="/css/cms.css" rel="stylesheet" type="text/css">
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<style>
#div1 {
  position:absolute;
  left: 40%; top:30%;
  background-color:#888;
  padding: 20px;
  display:none;
}
.date{width:50px;}
.spanbtn {
  font-size:smaller;
  float:right;
  cursor: pointer;
}
</style>

<?php 
$whereStr = ''; //initial where string
//:filter handle  ---------------------------------------------------
$whereAry = array();
$wherefStr = '';
if ($opList['filters']) {
    foreach ($opList['filters'] as $k => $v) {
        if ($_REQUEST[$k]) {
            $whereAry[] = "$k = '" . $_REQUEST[$k] . "'";
        } else if ($opList['filterOption'][$k]) {
            $whereAry[] = "$k = '" . $opList['filterOption'][$k][0] . "'";
        }

    }
    if (count($whereAry)) {
        $wherefStr = join(' and ', $whereAry);
    }

}

//:Search filter handle  ---------------------------------------------
$whereAry  = array();
$wherekStr = '';
if ($opList['keyword']) {
    $key = $opList['keyword'];
    foreach ($opList['searchField'] as $v) {
        $whereAry[] = "$v like '%$key%'";
    }

    $n = count($whereAry);
    if ($n > 1) {
        $wherekStr = '(' . join(' or ', $whereAry) . ')';
    } else if ($n == 1) {
        $wherekStr = join(' or ', $whereAry);
    }

}

//:Merge where
if ($wherefStr || $wherekStr) {
    $whereStr = ' where ';
    $flag     = false;
    if ($wherefStr) {
        $whereStr .= $wherefStr;
        $flag = true;}
    if ($wherekStr) {
        if ($flag) {
            $whereStr .= ' and ' . $wherekStr;
        } else {
            $whereStr .= $wherekStr;
        }

    }
}

$db     = new db();
$sql    = $tableName;
$rs     = $db->query($sql);
$rCount = $db->num_rows($rs);
//:PageInfo -----------------------------------------------------------------------
if (!$pageSize) {
    $pageSize = 10;
}

$pinf = new PageInfo($rCount, $pageSize);
if ($recno > 1) {
    $pinf->setRecord($recno);
} else if ($page > 1) {
    $pinf->setPage($page);
    $pinf->curRecord = ($pinf->curPage - 1) * $pinf->pageSize;}
;

//: Header -------------------------------------------------------------------------
//echo "<h1 align='center'>$pageTitle ";
if ($opList['keyword']) {
    echo " - 搜尋:" . $opList['keyword'];
}

echo "</h1>";

//:PageControl ---------------------------------------------------------------------
$obj = new EditPageControl($opPage, $pinf);
echo "<span style='font-size:larger'>【".$pageTitle."】</span><div style='float:right'>頁面：" . $pinf->curPage . "/" . $pinf->pages . " 筆數：" . $pinf->records . " 記錄：" . $pinf->curRecord . "</div><hr>";

//:Defined this page PHP Function -------------------------------------------------------------

function getLevelName($v)
{
    global $odMenu;
    if ($odMenu == 'rod') {
        global $RsignLevel;
        if (empty($RsignLevel[$v])) {
            return '加簽';
        } else {
            return $RsignLevel[$v];
        }

    } elseif ($odMenu == 'sod') {
        global $SsignLevel;
        if (empty($SsignLevel[$v])) {
            return '加簽';
        } else {
            return $SsignLevel[$v];
        }

    }

}

function getMan($v)
{
    global $emplyeeinfo;return $emplyeeinfo[$v];
}

function getWait($v)
{
    if ($v == '0') {
        return '否';
    } elseif ($v == '1') {
        return '是';
    } else {
        return '';
    }
}

function getSign($v)
{
    if ($v == '0') {
        return '否';
    } elseif ($v == '1') {
        return '是';
    } else {
        return '尚未簽核';
    }
}

//:ListControl Object --------------------------------------------------------------------
$n   = ($pinf->curPage - 1 < 0) ? 0 : $pinf->curPage - 1;
$sql = sprintf('%s %s order by %s %s limit %d,%d',
    $tableName, $whereStr,
    $opList['curOrderField'], $opList['sortMark'],
    $n * $pinf->pageSize, $pinf->pageSize
);
//echo $sql;
$rs    = $db->query($sql);
$pdata = $db->fetch_all($rs);
$obj   = new BaseListControl($pdata, $fieldsList, $opList, $pinf);
//:ViewControl Object ------------------------------------------------------------------------
$listPos = $pinf->curRecord % $pinf->pageSize;
switch ($act) {
    case 'edit': //修改
        $op3 = array(
            "type"      => "edit",
            "form"      => array('form1', "/edis/tabpage/odsign/doedit.php", 'post', 'multipart/form-data', 'form1Valid'),
            "submitBtn" => array("確定修改", false, ''),
            "cancilBtn" => array("取消修改", false, ''));
        $ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);
        break;
    case 'new': //新增
        $op3 = array(
            "type"      => "append",
            "form"      => array('form1', "/edis/tabpage/odsign/doappend.php", 'post', 'multipart/form-data', 'form1Valid'),
            "submitBtn" => array("確定新增", false, ''),
            "cancilBtn" => array("取消新增", false, ''));
        $ctx = new BaseViewControl($fieldA_Data, $fieldA, $op3);
        break;
    default: //View
         $op3 = array(
            "type"=>"view",
            "submitBtn"=>array("確定變更",true,''),
            "resetBtn"=>array("重新輸入",true,''),
            "cancilBtn"=>array("關閉",true,''));
            $ctx = new BaseViewControl($pdata[$listPos], $fieldE, $op3);    

}

$edisID = $pdata[$listPos]['id'];
?>

<?php 
include "$root/public/inc_listjs.php";
?>

