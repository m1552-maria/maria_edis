<?
	class BaseListControl {
		const SORTMARK_ASC="<span class='sortmark'>▲</span>";
		const SORTMARK_DEC="<span class='sortmark'>▼</span>";
		
		protected $defaultFields = array(
			"id"=>array("編號","80px",true,'Link')	//key : title,width,sortable,欄位設定[]
		);
		
		/* 欄位設定說明
		 * Id : array('Id',paramFmt)
		 * Text: array('Text',單位)
		 * Link : array('Link',url)
		 * Image: array('Image',width)
		 * Number: array('Number',N'digit)
		 * Select: array('Select',optionArray())
		 * Define: array('Define',funcName)
		 * */
		 
		protected $defaultOption = array(
			"alterColor"=>false,			//交換色
			"dataKey"=>true,					//get data with Key
			"checkbox"=>true,					//show checkbox
			"hideControl"=>false,			//隱藏控件
			"postUrl"=>'',						//POST to URL
			"curOrderField"=>'id',		//主排序的欄位
			"sortMark"=>'ASC',				//升降序 (ASC | DESC)
			"groupUp"=>false,					//分群
			"groupTitle"=>array('Text'),	//目前只支援 Text,Define
			"groupField"=>null,
			"filterOption" => array(			//過濾器選項 
				'autochange'=>TRUE,					//'depID'=>array('',false), //預設Key值,是否隱藏
				'goBtn' => array("確定",false,true,'')		
			 )
															//Options(defined by use time): 
															//  searchField" => array()
															//  filters => array($k=>array(),...)
															//  extraButton => array(array(),...)
															// 	filterType"=>array(field=>Type) 不定義就是 <select>, Month:月選取， DateRange:日期範圍	
		);												
		
		public $goBtn = "<button type='button' class='goBtn'";
		public $etraBtn = "<button type='button' class='etraBtn'";
		public $etraInp = "<input type='text' class='MonthInp' size='6'";
		public $dateRgInp = "<input type='text' class='dateRange form-control' size='20'";
				
		protected $mData  = array();	//List's Data
		protected $mFields = array();
		protected $mPageInf = array();
		public $curRow = array(); 	
			
		function __construct($data=array(), $fields=array(), $option=array(),$pgInfo=null)	{
			if(!empty($option)) {
				if($option['groupUp']) $this->defaultOption['groupUp'] = $option['groupUp'];
				if($option['groupTitle']) $this->defaultOption['groupTitle'] = $option['groupTitle'];
				if($option['groupField']) $this->defaultOption['groupField'] = $option['groupField'];
				if($option['sortMark']) $this->defaultOption['sortMark'] = $option['sortMark'];
				if($option['alterColor']) $this->defaultOption['alterColor'] = $option['alterColor'];
				if(isset($option['dataKey'])) $this->defaultOption['dataKey'] = $option['dataKey'];
				if($option['curOrderField']) $this->defaultOption['curOrderField'] = $option['curOrderField'];
				if($option['keyword']) $this->defaultOption['keyword'] = $option['keyword'];
				if(isset($option['checkbox'])) $this->defaultOption['checkbox'] = $option['checkbox'];
				if($option['filterOption']) $this->defaultOption['filterOption'] = $option['filterOption'];
				if($option['extraButton']) $this->defaultOption['extraButton'] = $option['extraButton'];
				if($option['filterType']) $this->defaultOption['filterType'] = $option['filterType'];
				if($option['filters']) $this->defaultOption['filters'] = $option['filters'];
				if(isset($option['hideControl'])) $this->defaultOption['hideControl'] = $option['hideControl'];
			}
			$this->mFields = $fields;
			$this->mData = $data;
			$this->mPageInf = $pgInfo;
			if($this->defaultOption['hideControl']) echo "<div id='ListControl' style='display:none'>"; else echo "<div id='ListControl'>"; 
			echo "<form name='ListControlForm' method='POST' action='".$this->defaultOption['postUrl']."'>";
			//:: for keep status
			echo "<input type='hidden' name='curOrderField' value='".$this->defaultOption['curOrderField']."'>";
			echo "<input type='hidden' name='sortMark' value='".$this->defaultOption['sortMark']."'>";
			echo "<input type='hidden' name='keyword' value='".$this->defaultOption['keyword']."'>";
			echo "<input type='hidden' name='act'>";
			echo "<input type='hidden' name='page'>";
			echo "<input type='hidden' name='recno'>";
			echo "<input type='hidden' name='cname' value='".((!empty($_REQUEST['cname'])) ? $_REQUEST['cname'] : '') ."'>";
			echo "<table>";
			$this->genFilter($this->defaultOption['filters']);
			$this->genHeader($this->mFields);
			$this->genRow($this->mData);
			
			if(get_class($this)=='BaseListControl') echo "</table></form></div>";
		}
		
		function __destruct() {
			//reserved
    }
		
		protected function genButton($btn,$setting) {
			echo $btn;
			if($setting[1]) echo " disabled";
			if(!$setting[2]) echo " style='display:none'";
			if($setting[3]) echo " data-handler='".$setting[3]."'";
			echo ">".$setting[0]."</button>";
		}
		
		protected function genInput($inp,$fld, $setting) {
			echo $inp;
			echo " id='$fld' name='$fld'";
			echo " placeholder='".$setting['placeholder']."'";
			if($setting['MinMonth']) echo " data-MinMonth='".$setting['MinMonth']."'";
			if($setting['MaxMonth']) echo " data-MaxMonth='".$setting['MaxMonth']."'";
			if($setting['value']) echo " value='".$setting['value']."'";
			if($this->defaultOption['filterOption']['autochange']) echo "onblur='this.form.submit()'";
			echo "/>";
		}	

		protected function genDateRgInput($inp,$fld, $setting) {
			echo $inp;
			echo " id='$fld' name='$fld'";
			echo " placeholder='".$setting['placeholder']."'";
			if($setting['value']) echo " value='".$setting['value']."'";
			//if($this->defaultOption['filterOption']['autochange']) echo "onblur='this.form.submit()'";
			echo "/>";
		}
				
		protected function genFilter($filters) {
			echo "<caption>";
			if( isset($this->defaultOption['groupField']) ) { //設定分群
				echo "<div class='divSelgroup'><select name='listGroup' id='listGroup' onchange='this.form.submit()'>";
				$listGroup = $this->defaultOption['groupField'];
				foreach ($listGroup as $k => $v) {
					if($_REQUEST['listGroup']==$k && !$flag1) {
						$selectedStr='selected';
					} else $selectedStr=''; 
					echo "<option value='$k' $selectedStr>$v</option>";
				}
				echo "</select></div>";
			}
			
			foreach ($filters as $key => $item) {
				$dfKey = $this->defaultOption['filterOption'][$key][0];													//filter預設值
				$fltV  = $this->defaultOption['filterOption'][$key][2]?'style=display:none':'';	//filter是否顯示	
				$wType = $this->defaultOption['filterType'][$key];
				$fixK = str_replace('.','_',$key);	//修正 JOIN TABLE 的問題
				switch($wType) {
					case 'Month':
						if( isset($_REQUEST[$fixK])) $item['value']=$_REQUEST[$fixK];	else if($dfKey) $item['value']=$dfKey;
						$this->genInput($this->etraInp, $key, $item);
						break;
					case 'DateRange':
						if( isset($_REQUEST[$fixK])) $item['value']=$_REQUEST[$fixK];	else if($dfKey) $item['value']=$dfKey;
						$this->genDateRgInput($this->dateRgInp, $key, $item);
						break;	
					default:
						if($this->defaultOption['filterOption']['autochange']) echo "<select name='$key' id='$key' onchange='this.form.submit()' $fltV>";
						else echo "<select name='$key' id='$key' $fltV>";				
						$flag1 = false;				
						if(isset($_REQUEST[$fixK])) { //傳值優先
							foreach ($item as $k => $v) {
								if($_REQUEST[$fixK]==$k && !$flag1) {
									$selectedStr='selected';
									$flag1 = true;	//防止重覆設定
								} else $selectedStr=''; 
								echo "<option value='$k' $selectedStr>$v</option>";
							}
							$flag1 = true;
						} 
						if(!$flag1) {	//其次預設
							foreach ($item as $k => $v) {
								if($dfKey===$k) $selectedStr='selected';else $selectedStr=''; 
								echo "<option value='$k' $selectedStr>$v</option>";
							}
						} 
						echo "</select>";
				}
			}
			
			if($this->defaultOption['extraButton']) {
				foreach ($this->defaultOption['extraButton'] as $item) {
					$this->genButton($this->etraBtn, $item);				
				}
			}		
			if(!$this->defaultOption['filterOption']['autochange']) $this->genButton($this->goBtn, $this->defaultOption['filterOption']['goBtn']);
			
			echo "</caption>";
		}
		
		protected function genHeader($fields) {
			echo "<tr class='columnRow'>";
			foreach ($fields as $key => $value) {
				$text = $value[0];
				if($key==$this->defaultOption['curOrderField']) $text .= ($this->defaultOption['sortMark']=='ASC'?$this::SORTMARK_ASC:$this::SORTMARK_DEC); 
				if($value[2]) echo "<th data-id='$key' class='columnHead sortable' width='$value[1]'>$text</th>";
				else echo "<th data-id='$key' class='columnHead' width='$value[1]'>$text</th>";
			}
			echo "</tr>";
		}
		
		protected function genRow($data) {
			global $ulpath;
			$count = 1;
			$mod = ($this->mPageInf->curRecord) % $this->mPageInf->pageSize; $mod++;

			if($this->defaultOption['groupUp']) {
				$groupFld = $this->defaultOption['curOrderField'];
				$rowClass = 'ListRowHide';
			} else {
				$groupFld = '';
				$rowClass = 'ListRow';
			} 
			 
			$curGpV = '';
			$n = count($this->mFields);
			foreach ($data as $item) {
				if($groupFld && $curGpV !== $item[$groupFld]) {  //Initial GroupUp
					$curGpV = $item[$groupFld];
					//::Gen adition group Row
					echo "<tr class='GroupRow'>";
					$fType = $this->defaultOption['groupTitle'][0];
					$tmpStr = '';
					switch ($fType) {
						case 'Define': $func=$this->defaultOption['groupTitle'][1]; $tmpStr=$func($curGpV,$this); break;
						default: $tmpStr = $curGpV;
					}		
					echo "<td colspan='$n'><i class='icon-folder-close'></i> $tmpStr &nbsp;</td>";
					echo "</tr>";
				} 
				
				$this->curRow = $item;	//for outside use 
				if($this->defaultOption['alterColor']) {
					if($count%2==1)	$color="columnDataEven"; else $color="columnDataOdd";
				}
				if($mod == $count) $color="columnCurrent";
				
				echo "<tr class='$rowClass'>";
				$key = array_keys($this->mFields);
				for($idx=0; $idx<$n; $idx++) {
					echo "<td class='columnData $color'>";					
					$ii = $key[$idx];
					$value = $this->defaultOption['dataKey']?$item[$ii]:$item[$idx];
					$fType = $this->mFields[$ii][3][0];
					/*if($idx==0 && $this->defaultOption['checkbox']) echo "<input type='checkbox' value='$value' name='ID[]'>"; *///update by Tina 20180907 no checkbox
					switch ($fType) {
						case 'Id':
							$fmt=$this->mFields[$ii][3][1];
							$recIdx = ($this->mPageInf->curPage-1)*$this->mPageInf->pageSize+$count;
							$onclick = sprintf($fmt,$recIdx);  
							echo "<a href='#' onClick='$onclick'>$value</a>";	break;	
						case 'Link': $link=$this->mFields[$ii][3][1];	echo "<a href='$link$value'>$value</a>"; break;
						
						case "Email": echo "<a href='mailto:$value'>$value</a>"; break;
						case "Bool" : $pic=($value?'/images/checked.gif':'/images/uncheck.gif'); echo "<img src='$pic'>"; break;	
						
						case 'Date': echo $value&&$value!='0000-00-00'?date('Y-m-d',strtotime($value)):'&nbsp;'; break;
						case 'DateTime': echo $value&&$value!='0000-00-00'?date('Y-m-d H:i:s',strtotime($value)):'&nbsp;'; break;
						case 'DateTime2': echo $value&&$value!='0000-00-00'?date('Y-m-d H:i',strtotime($value)):'&nbsp;'; break;
						case 'Image': if($value) {$w=$this->mFields[$ii][3][1]; echo "<img src='$ulpath$value' width='$w'>";} break;
						case 'Select': $aa=$this->mFields[$ii][3][1]; echo $aa[$value];	break;
						case 'File': if($value) $this->genFileview($value,$ii); break;
						case 'MultiFile': 
							if($value) {
								$aa = explode(',',$value);
								$this->genFileview($aa[0],$ii);
								if(count($aa)>1) echo ' 更多...';
							} 
							break;
						case 'Number': echo number_format($value,$this->mFields[$ii][3][1]);	break;
						case 'Define': $func=$this->mFields[$ii][3][1]; echo $func($value,$this); break;
						case 'Text': echo $value.$this->mFields[$ii][3][1];	break;
						default: echo $value;	break;
					}
					echo "</td>";
				}
				
				echo "</tr>";
				$count++;
			}
		}
	
		private function genFileview($value,$ii) { 
			global $ulpath,$icons;
			$ext = pathinfo($value,PATHINFO_EXTENSION); 
			if(isImage($ext)) {
				$w=$this->mFields[$ii][3][1]; 
				echo "<img src='$ulpath$value' width='$w'>";
			} else {
		 		$fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
		 		echo "<img src='/images/$fn' align='absmiddle'/><a href='$ulpath$value' target='_new'>下載</a>";
			}
		}
		
	}
	/* -------------------------------------------------------------------------------------------------------------------- */
	
?>