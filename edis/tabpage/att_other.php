<?
//echo $_REQUEST['id'];
session_start();
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once "$root/edis/func.php";

$db = new db();
//其他附件
$ofSql = "select * from map_files where aType='O' and eid ='" . $_REQUEST['id'] . "'";
$rsOf  = $db->query($ofSql);
if ($rsOf) {
    while ($rOf = $db->fetch_array($rsOf)) {
        $ofInfo[$rOf['fName']] = $rOf['title'];
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>公文附件檢視</title>
</head>
<body leftmargin="0" topmargin="0" style="overflow:auto">   
<table border="0" cellpadding="4" cellspacing="0" bgcolor="#333333" width="100%">
    <tr>
    <td id="tblHead" bgcolor="#CCCCCC" align="center">其他附件檢視
    </td>
    </tr>
    <tr><td bgcolor="#FFFFFF">
    <div id="markup" class="whitebg"><ul id="browser">
    <? 
        $ulpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
        if(checkAttView($_REQUEST['id'], $_SESSION['empID'])){
            if (file_exists($ulpath)) {
                
                $dh = opendir($ulpath);

                while (false !== ($filename = readdir($dh))) {

                    if ($filename == '.') {
                        continue;
                    }

                    if ($filename == '..') {
                        continue;
                    }

                    $file_path = $ulpath . $filename;
                    echo '<li><a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename] . '</a></li>';
                }
            }
        }else{
            echo '已設為機密,您無查看附件之權限';   
        }
    ?> 

    </ul></div>
    </td></tr>
</table>
</body>
</html>