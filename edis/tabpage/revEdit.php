<?
	@session_start();
	include '../../config.php';
	include '../../system/db.php';
	include '../../inc_vars.php';
	function genGroupDom($fld){
	    $db = new db();
	    //group主檔
	    $sql = "SELECT g.* FROM groups g where g.creator_id = " . $_SESSION['empID'];
	    $rs  = $db->query($sql);

	    $html_code = '<div id="group_'.$fld.'"> <table id="groupTable" cellpadding="4">
	        <caption>選擇群組</caption>
	      <tr>
	         <td align="right">群組名稱：</td>
	        <td><select name="grupName" id="grupName"/>';
	    while ($r = $db->fetch_array($rs)) {
	       $html_code .= '<option value="'.$r['group_id'].'">'.$r['name'].'</option>';
	    }
	    $html_code .='</select>  
	      </tr>
	      <tr>
	        <td colspan="2" align="center">
	        <button type="button" onclick="return insertGroup(\'group\',\''.$fld.'\')">確定</button>
	        <button type="button" onclick="return closeGroup(\'group\',\''.$fld.'\')">取消</button></td></tr></table> </div>';
	    $entity = html_entity_decode($html_code);

	    echo $entity;
	}

	$db = new db();
	$sql = "select * from edis where id='".$_GET['id']."'";
	$rs  = $db->query($sql);
	$r = $db->fetch_array($rs);

	// var_dump($r);

?>
<link href="../../css/all.css" rel="stylesheet" type="text/css"/>
<link href="../../Scripts/form.css" rel="stylesheet">
<link href="../../css/list.css" rel="stylesheet">
<link href="../../group/group.css" type="text/css">
<style type="text/css">
	#secretDiv {
	    position:absolute;
	    left: 50%; top:50%;
	    background-color:#888;
	    padding: 20px;
	    display:none;
	    margin-left: -262px;
	    margin-top: -150px;
	}
	#group_secretViewer{
		position:absolute;
		left: 50%; top:50%;
		background-color:#F19851;
		padding: 5px;
		display:none;
		margin-top: -150px;
    	margin-left: -184px;
	}

	#MainRev {
	    position:absolute;
	    left: 50%; top:50%;
	    background-color:#888;
	    padding: 20px;
	    display:none;
	    margin-left: -262px;
	    margin-top: -150px;
	}

</style>

<script src="../../Scripts/jquery-1.12.3.min.js"></script>
<script src="../../media/js/jquery-ui-1.10.1.custom.min.js"></script>
<script src="../../Scripts/form.js"></script>
<script src="../../group/group.js"></script>
<script src="../../edis/edis.js"></script>
<!--日期區間js-->
<script src="/Scripts/daterangepicker/moment.min.js"></script>
<script src="/Scripts/daterangepicker/daterangepicker.min.js"></script>
<script src="/Scripts/daterangepicker/MonthPicker.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#addSecret").on('click',function(){ $('#secretDiv').show(); });
		$("#filingEdit").on('click',function(){ $('#MainRev').show(); });
		$("#del").on('click',function(){
			if(!confirm("<?=$r['did']?>　確定刪除?")) return false;
			location.href='/edis/del.php?id='+$('input[name="id"]').val();
		});
	});
	function insertGroup(group_name,fld) {
	    var divName = '#' + group_name+'_'+fld;
	    var group_id = $('#grupName').val();
	    var count = 0;
	    var html = [];
	    if (group_id.length > 0) {
	        id_numbers = new Array();
	        $.ajax({
	            url: "/group/api.php?act=getGroupEmp&inValue=" + group_id,
	            type: "GET",
	            dataType: "json",
	            async: false,
	            success: function(msg) {
	                id_numbers = msg;
	            },
	        });
	        $.each(id_numbers, function(key, value) {
	        	var divTitle = '#' + fld + 'Div';
	         	$(divTitle).empty();
	            count++;
	            if (count == 1) {
	               $("#groupEnter").next().next().val(value['emp_id']);
	               $("#groupEnter").next().next().next().next().html(value['emp_name']);
	            } else if (count > 1) {
	               html.push(addItemGroup(fld, fld,value['emp_id'],value['emp_name']));
	            }
	             
	        });

	        var divTitle = '#' + fld + 'Div';
	        $(divTitle).append(html.join(''));
	    }
	     $(divName).hide();
	}
	function addItem(tbtn,fld,qid){
	   var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text'class='queryID' size='6' onkeypress=\"return checkInput(event,'empName',this)\" callback='checkSigner'>"
	    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
	    + " <span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
	   var divTitle ='#'+fld+'Div';
	
	   $(divTitle).append(newone);
	}
	function removeItem(tbtn) {
	    $(tbtn).parent().remove();
	}
	function checkVoid(){
		if(confirm('你確定作廢此公文嗎？')){
			input1 = $("<input type='hidden' name='void' value='1' />");
			$('#form1').append(input1);
			$('#form1').submit();
		}
	}
</script>
<form action="revEdit.php?id=<?=$_GET['id']?>&odMenu=<?=$_GET['odMenu']?>" method="post" id="form1">
	<input type="hidden" name="id" value="<?=$_GET['id']?>">
	<input type="hidden" name="odMenu" value="<?=$_GET['odMenu']?>">
	<? if(empty($r['filingDate'])){ ?>
		<input type="submit" name="edit" id="edit" value="公文編輯">
		<input type="button" name="del" id="del" value="公文刪除"><br><br>
	<? }else{ ?>
		<input type="button" name="filingEdit" id="filingEdit" value="歸檔修改"><br><br>
	<? } ?>
	<input type="text" name="voidNote" id="voidNote" size="100" placeholder="公文作廢原因">
	<input type="button" value="公文作廢" onclick="checkVoid()"><br><br>
	<? if($r['isSecret'] == '0'){ ?>
		<input type="button" name="addSecret" id="addSecret" value="公文加密">
	<? }elseif($r['isSecret'] == '1'){ ?>
		<input type="submit" name="disSecret" id="disSecret" value="公文解密">
	<? } ?>
	<div id="MainRev">
		<table bgcolor="#FFFFFF" cellpadding="4">
			<tr>
				<td align="right">歸檔編號：</td>
				<td><input type="text" name="filingNo" value="<?=$r['filingNo']?>"></td>
			</tr> 
			<tr>
				<td align="right">類別：</td>
				<td>
					<select name="odDeadlineType" id="odDeadlineType" required ="true" />
						<? 
							foreach ($odDeadlineType as $v){
								if($r['odDeadlineType'] == $v) echo "<option selected>$v</option>";
								else echo "<option>$v</option>"; 
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" class="tdButton">
					<input type="submit" name="Type" id="Type" value="確定" />
					<button type="button" onclick="$('#MainRev').hide()">取消</button>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="secretDiv">
		<table bgcolor="#FFFFFF" cellpadding="4">
			<tr id ="secret" hidden>
				<td align="right">機密：</td>
				<td><input type="hidden" name="isSecret" value="1" checked="checked"/></td>
			</tr> 
			<tr id="secretView">
				<td align="right">機密查看附件：</td>
				<td>
					<input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_secretViewer');" value="群組"><br>
					<input name="secretViewer[]" type="text" class="queryID" id="sId" size="6" onkeypress="return checkInput(event,'empName',this)"/>
					<span class="sBtn" onclick="addItem(this,'secretViewer','sId')"> +新增</span>
					<div id="secretViewerDiv">
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" class="tdButton">
					<input type="submit" name="secret" id="secret" value="確定" />
					<button type="button" onclick="$('#secretDiv').hide()">取消</button>
				</td>
			</tr>
		</table>
	</div>
</form>
<? 
	echo genGroupDom('secretViewer');

	if($_POST['edit']){
		$url = "/index.php?funcUrl=edis/" . $_POST['odMenu'] . "/edit.php&muID=0&mode=revEdit&id=" . $_POST['id'];
		echo "<script>";
		echo "window.open('".$url."')";
		echo "</script>";
	}

	if($_POST['disSecret']){
		$db = new db();
		$filSql = "update edis set "
                . "isSecret ='0' "
                . "where id='" . $_POST['id'] . "'";
        // echo $filSql;
        $db->query($filSql);

        $db = new db('secret_members');
        $db->delete("edisid='".$_POST['id']."'");
        echo "<script>parent.window.location.reload();</script>";
	}

	if($_POST['Type']){
		$db = new db();
		$filSql = "update edis set "
				. "filingNo = '" . $_POST['filingNo'] . "',"
                . "odDeadlineType = '" . $_POST['odDeadlineType'] . "',"
                . "filingDate = '" . date("Y/m/d") . "'"
                . "where id='" . $_POST['id'] . "'";
        // echo $filSql;
        $db->query($filSql);
        echo "<script>parent.window.location.reload();</script>";
	}

	if($_POST['secret']){
		$db = new db();
		if ($_POST['isSecret']=='1') {
            $filSql = "update edis set "
                . "isSecret ='" . $_POST['isSecret'] . "'"
                . "where id='" . $_POST['id'] . "'";
            // echo $filSql;
            $db->query($filSql);

            $db = new db();
            if ($_POST['odMenu'] == 'rod') {
                $sql = "select signMan as sId from map_orgs_sign where edisid='" . $_POST['id'] . "' and signLevel='0' and moid is null";
            }
            if($_POST['odMenu'] == 'sod'){
                $sql = "select sId from edis where id='" . $_POST['id'] . "'";
            }
            $rs = $db->query($sql);

            //新增機密公文可查看人員
            // 文書自己
            $db                = new db('secret_members');
            $db->row['edisid'] = "'" . $_POST['id'] . "'";
            $db->row['empid'] = "'" . $_SESSION['empID'] . "'";
            $db->row['createrid'] = "'" . $_SESSION['empID'] . "'";
            $db->insert();

            foreach ($_POST['secretViewer'] as $k => $v) {
                if (!empty($v)) {
                    $db                = new db('secret_members');
                    $db->row['edisid'] = "'" . $_POST['id'] . "'";
                    $db->row['empid']  = "'" . $v . "'";
                    $db->row['createrid'] = "'" . $_SESSION['empID'] . "'";
                    $db->insert();
                }
            }
        }
        echo "<script>parent.window.location.reload();</script>";
	}

	if($_POST['void']){
		$db = new db();
		$filSql = "update edis set "
                . "void ='1',"
                . "voidNote = '" . $_POST['voidNote'] . "' "
                . "where id='" . $_POST['id'] . "'";
        // echo $filSql;
        $db->query($filSql);
        echo "<script>parent.window.location.reload();</script>";
	}

?>