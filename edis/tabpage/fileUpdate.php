<?
	include_once "../../config.php";
    include_once "../../system/db.php";
    $eid = $_POST['mainID'];

    $dir = '../../data/edis/'.$eid;
    if(!is_dir($dir)) mkdir($dir,0700);

    //主附檔
    if ($_FILES['mFile']['tmp_name'] != "") {
    	$db = new db();
    	$sql = "select fName from map_files where eid='".$eid."' and aType='M'";
    	$rs  = $db->query($sql);
		$r = $db->fetch_array($rs);

		$ulpath = '../../data/edis/' . $eid . '/' . 'main/';
        $dir = '../../data/edis/' . $eid . '/' . 'main';
        if(!is_dir($dir)) mkdir($dir,0700);
		if(!empty($r)) unlink($ulpath.$r['fName']); //刪除修改前的附件

    	$srcfn = '';
        $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
        $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

        if ($rzt) {
            $sLocation = strripos($srcfn, '/') + 1;
            $length    = strlen($srcfn) - strripos($srcfn, '/');
            $fileName  = substr($srcfn, $sLocation, $length);

            //insert 主文附件
            if($r){
                $db               = new db('map_files');
                $db->row['fName'] = "'" . $fileName . "'";
                $db->update("eid='".$eid."' and aType='M'");
            }else{
                $db               = new db('map_files');
                $db->row['fName'] = "'" . $fileName . "'";
                $db->row['title'] = "'主文'";
                $db->row['eid']   = "'" . $eid . "'";                
                $db->row['aType'] = "'M'";
                $db->insert();
            }
        } else{
        	echo '主文上傳作業失敗！';exit;
    	}
    }

    if($_POST['delOFile']){
        foreach ($_POST['delOFile'] as $key => $value) {
            $db = new db();
            $sql = "select fName from map_files where id='".$value."' and aType='O'";
            $rs  = $db->query($sql);
            while($r = $db->fetch_array($rs)) if(!empty($r)) unlink('../../data/edis/' . $eid . '/' . 'others/'.$r['fName']);
            $db->table = 'map_files';
            $db->delete("id='".$value."' and aType='O'");
        }
    }

    //處理其他附件
    foreach ($_FILES['oFile']['name'] as $k => $v) {
        if ($_FILES['oFile']['tmp_name'][$k] != "") {
        	$ulpath = '../../data/edis/' . $eid . '/' . 'others/';
            $dir = '../../data/edis/' . $eid . '/' . 'others';
            if(!is_dir($dir)) mkdir($dir,0700);

            if (!empty($_REQUEST['oTitle'][$k])) {
                //名稱已填寫 新增上傳資料
                $srcfn = '';
                $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);

                if ($rzt) {
                    //insert 其他附件
                    $sLocation = strripos($srcfn, '/') + 1;
                    $length    = strlen($srcfn) - strripos($srcfn, '/');
                    $fileName  = substr($srcfn, $sLocation, $length);

                    $db               = new db('map_files');
                    $db->row['eid']   = $eid;
                    $db->row['aType'] = "'" . 'O' . "'";
                    $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                    $db->row['fName'] = "'" . $fileName . "'";
                    $db->insert();
                } else{
                	echo '其他附件上傳作業失敗！';exit;
            	}
            }
        }
    }
    echo "檔案修改成功";
 //    $loc="Location: attUpFile.php".$_POST['src'];
	// header($loc);
?>