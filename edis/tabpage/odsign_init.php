<?
/*
This page is for Definition
Create By Michael Rou on 2017/9/21
 */
/* ::Access Permission Control
session_start();
if ($_SESSION['privilege']<100) {
header('Content-type: text/html; charset=utf-8');
echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
exit;
} */
include_once "../../config.php";
include_once "$root/system/utilities/miclib.php";
include_once "$root/system/db.php";
include_once "$root/setting.php";
//:: include System Class ============================================================
include "$root/system/PageInfo.php";
include "$root/system/PageControl.php";
include "ListControl.php";
include "$root/system/ViewControl.php";
$odMenu = $_REQUEST['odMenu'];
if ($odMenu == 'rod') {
    $odTypeName = '收文';
    $lastLevel =$rodLastLevel;
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';
    $lastLevel =$sodLastLevel;
}

$pageTitle = "簽收狀況";
//$pageSize=5; //使用預設
$tableName = "SELECT *
              FROM map_orgs_sign
              WHERE edisid = '". $_REQUEST['id'] ."' AND (edisid IS NOT NULL OR edisid <> '0' OR edisid <> '')";
// for Upload files and Delete Record use
$ulpath = "/data/edis/";if (!file_exists($root . $ulpath)) {
    mkdir($root . $ulpath);
}

$delField = 'Ahead';
$delFlag  = true;

//:PageControl ------------------------------------------------------------------
$opPage = array(
    "firstBtn"  => array("第一頁", false, true, firstBtnClick),
    "prevBtn"   => array("前一頁", false, true, prevBtnClick),
    "nextBtn"   => array("下一頁", false, true, nextBtnClick),
    "lastBtn"   => array("最末頁", false, true, lastBtnClick),
    "goBtn"     => array("確定", false, true, goBtnClick), //NumPageControl
    "numEdit"   => array(false, true), //
    "searchBtn" => array("◎搜尋", false, false, SearchKeyword),
    "newBtn"    => array("＋新增", false, false, doAppend),
    "editBtn"   => array("－修改", false, false, doEdit),
    "delBtn"    => array("Ｘ刪除", false, false, doDel),
);

//:ListControl  ---------------------------------------------------------------
$opList = array(
    "alterColor"    => true, //交換色
    "curOrderField" => 'signLevel', //主排序的欄位
    "sortMark"      => 'ASC', //升降序 (ASC | DES)
    "searchField"   => array(), //要搜尋的欄位
);
$fieldsList = array(
    "id"       => array('編號', "30px", false, array('Id')),
    "signLevel" => array("階段", "50px", false, array('Define', getLevelName)),
    //"isWait" => array("等簽", "30px", false, array('Define', getWait)),
    "signMan"   => array("姓名", "50px", false, array('Define', getMan)),
    "agent"   => array("代理簽核   ", "60px", false, array('Define', getMan)),
    "signTime"     => array('簽核時間', "100px", false, array('DateTime')),
    "isSign"   => array("簽辦狀況", "60px", false, array('Define', getSign)),
    "signState"  => array("簽核用語", "100px", false, array('Text')),
    "signContent"  => array("簽核意見/內容", "", false, array('Text')),
);

//:ViewControl -----------------------------------------------------------------
/*    $fieldA = array(
"SimpleText"=>array("簡述<span class='need'>*</span>","text",60,'','',array(true,'','',8)),
"Content"=>array("內文","textarea",60,6,'',array(true,'','',8)),
"unValidDate"=>array("失效日期","datetime",20,'','',array(true,PTN_DATETIME,'請輸入日期時間')),
'Link'=>array("說明","textarea",60,5,),
'isReady'=>array("上架","checkbox",20)
);
$fieldA_Data = array("unValidDate"=>date('Y/m/d',strtotime("+30 days")),'isReady'=>1);

$fieldE = array(
"ID"=>array("編號","id",20,),
"SimpleText"=>array("簡述","text",60,),
"unValidDate"=>array("失效日期","date",10,),
'Link'=>array("連結","text",60,'',''),
"Content"=>array("內文","textarea",60,6),
'isReady'=>array("上架","checkbox",60,5,'')
);
 */
