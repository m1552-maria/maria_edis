<?
//echo $_REQUEST['id'];
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once "$root/edis/func.php";
$db = new db();
//主文附件
$mfSql = "select * from map_files where aType='M' and eid ='" . $_REQUEST['id'] . "'";
$rsMf  = $db->query($mfSql);
if ($rsMf) {
    $rMf = $db->fetch_array($rsMf);
}
//其他附件
$ofSql = "select * from map_files where aType='O' and eid ='" . $_REQUEST['id'] . "'";
$rsOf  = $db->query($ofSql);
if ($rsOf) {
    while ($rOf = $db->fetch_array($rsOf)) {
        $ofInfo[$rOf['fName']] = $rOf['title'];
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>公文附件檢視</title>
</head>
<body>	 
<?
    if(checkAttView($_REQUEST['id'], $_REQUEST['empid']) || $_REQUEST['loginType']=='cxo'){    
        echo'<table width="100%" border="1" cellpadding="4" cellspacing="0" style="font:normal 13px \'微軟正黑體\',Verdana">';
        echo '<tr>';
        echo '<td width=10% bgcolor="#D8DBD6">主文：</td><td>';
        $ulpath = '../../data/edis/' . $_REQUEST['id'] . '\/main\/';
        if (file_exists($ulpath)) {
            
            $dh = opendir($ulpath);
            while (false !== ($filename = readdir($dh))) {
                if ($filename == '.') {
                    continue;
                }

                if ($filename == '..') {
                    continue;
                }

                $file_path = $ulpath . $filename;
                echo '
                      <a target="_blank" href="' . $file_path . '">' . $rMf['title'] . '</a>
                    ';
            }
          
        }
        echo '</td></tr>';
        echo '<tr>';
        echo '<td bgcolor="#D8DBD6">其他附件：</td><td>';
        $ulpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
        if (file_exists($ulpath)) {
            
            $dh = opendir($ulpath);

            while (false !== ($filename = readdir($dh))) {

                if ($filename == '.') {
                    continue;
                }

                if ($filename == '..') {
                    continue;
                }

                $file_path = $ulpath . $filename;
                echo '<a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename] . '</a><br>';
            }
        }
        echo '</td></tr>';
        echo '</table>';
    }else{
        if(empty($_REQUEST['id'])){
            echo '無資料';
        }else{
            echo '已設為機密,您無查看附件之權限';   
        }
    }
?>
</body>
</html>