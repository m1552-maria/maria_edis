<?

// echo $_REQUEST['id'];
include_once '../../config.php';
include_once '../../system/db.php';
include_once '../../getEmplyeeInfo.php';
include_once '../../inc_vars.php';
include_once "$root/edis/func.php";
$db = new db();
//主文附件
$mfSql = "select * from map_files where aType='M' and eid ='" . $_REQUEST['id'] . "'";
$rsMf  = $db->query($mfSql);
if ($rsMf) {
    $rMf = $db->fetch_array($rsMf);
}
//其他附件
$ofSql = "select * from map_files where aType='O' and eid ='" . $_REQUEST['id'] . "'";
$rsOf  = $db->query($ofSql);
if ($rsOf) {
    while ($rOf = $db->fetch_array($rsOf)) {
        $ofInfo[$rOf['fName']]['title'] = $rOf['title'];
        $ofInfo[$rOf['fName']]['id'] = $rOf['id']; 
    }
}

$sql = "select creator, sId from edis where id='".$_REQUEST['id']."'";
$rs  = $db->query($sql);
$r = $db->fetch_array($rs);
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>公文附件檢視</title>
    <link href="/css/all.css" rel="stylesheet" type="text/css"/>
    <script src="/media/js/jquery-1.10.1.min.js"></script>
    <style type="text/css">
        #fader {
            opacity: 0.5;
            background: black;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            display: none;
        }
        #loadimg{
            width: 50px;
            height: 50px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -50px;
        }
        .progress {
            position:relative; 
            top: 50%;
            left: 50%;
            margin-top: 5px;
            margin-left: -230px;
            width: 400px; 
            border: 1px solid #ddd; 
            padding: 1px; 
            border-radius: 3px; 
        }
        .bar { 
            background-color: #99ccff; 
            width:0%; 
            height:20px; 
            border-radius: 3px; 
        }
        .percent { 
            position:absolute; 
            display:inline-block; 
            top:3px; 
            left:48%;
            color: #ff0000;
        }
    </style>
</head>
<body>
    <div id="fader">
        <img id="loadimg" src="../../images/load.gif">
        <div class='progress' id="progress-div">
            <div class='bar' id='bar'></div>
            <div class='percent' id='percent'>0%</div>
        </div>
    </div>
    <form id="fileForm" action="" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this)"  onkeydown="if(event.keyCode==13) return false;">
    <?
        if(!empty($_REQUEST['id'])){
            if(checkAttView($_REQUEST['id'],$_REQUEST['empid']) || $_REQUEST['loginType']=='cxo'){    
                echo'<table width="100%" border="1" cellpadding="4" cellspacing="0" style="font:normal 13px \'微軟正黑體\',Verdana">';
                echo '<tr>';
                echo '<td width=10% bgcolor="#D8DBD6">主文：</td><td>';

                $ulpath = '../../data/edis/' . $_REQUEST['id'] . '\/main\/';
                if (file_exists($ulpath)) {
                    
                    $dh = opendir($ulpath);
                    while (false !== ($filename = readdir($dh))) {
                        if ($filename == '.') {
                            continue;
                        }

                        if ($filename == '..') {
                            continue;
                        }

                        $file_path = $ulpath . $filename;
                        echo '
                              <a target="_blank" href="' . $file_path . '">' . $rMf['title'] . '</a><br>
                            ';
                    }
                  
                }
                if(($r['creator'] == $_GET['empid'] || $_GET['loginType'] == 'rev') && $_GET['odMenu'] != 'sod') echo '<input type="file" name="mFile" id="mFile" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf">';
                echo '</td></tr>';
                echo '<tr>';
                echo '<td bgcolor="#D8DBD6">其他附件：</td><td>';
                $ulpath = '../../data/edis/' . $_REQUEST['id'] . '\/others\/';
                if (file_exists($ulpath)) {
                    
                    $dh = opendir($ulpath);

                    while (false !== ($filename = readdir($dh))) {

                        if ($filename == '.') {
                            continue;
                        }

                        if ($filename == '..') {
                            continue;
                        }

                        $file_path = $ulpath . $filename;
                        $html = '<a target="_blank" href="' . $file_path . '">' . $ofInfo[$filename]['title'] . '</a>';
                        
                        if($_GET['loginType'] == 'rev' || $r['creator'] == $_GET['empid'] || $r['sId'] == $_GET['empid']){
                            $html .= '<label><input type="checkbox" name="delOFile[]" value="'.$ofInfo[$filename]['id'].'">刪除附件</label>';
                        }
                        $html .= '<br>';
                        
                        echo $html;
                    }
                }
                if($r['creator'] == $_GET['empid'] || $r['sId'] == $_GET['empid'] || $_GET['loginType'] == 'rev') echo '<input name="oTitle[]" id="oTitle" type="text"  placeholder="請輸入名稱"/><input type="file" name="oFile[]" id="oFile" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf"><span class="sBtn" id="oTitleAdd" onclick="addItem()"> +新增其他附件</span><div id="oTitleDiv"></div>';
                echo '</td></tr>';
                echo '</table>';
                if($r['creator'] == $_GET['empid'] || $r['sId'] == $_GET['empid'] || $_GET['loginType'] == 'rev'){
    ?>
        <input type="hidden" name="mainID" value="<?=$_REQUEST['id']?>">
        <input type="hidden" id="src" name="src" value="">
        <input type="submit" value="修改上傳">
    </form>
    <?     
                }
            }else{
                if(empty($_REQUEST['id'])){
                    echo '無資料';
                }else{
                    echo '已設為機密,您無查看附件之權限';   
                }
            }
        }else{
            echo '無資料';
        }
    ?>
</body>
</html>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#src').val(location.search);
        $("#fileForm").submit(function(e){
            $('#fader').css('display', 'block');
            e.preventDefault();
            var bol = false
            $("[name='oFile[]']").each(function(k,v){
                if($(this).val() != ''){
                    if($("[name='oTitle[]']")[k].value != ''){
                        bol = true;
                    }else{
                        $(this).prev().attr('required','true');
                        bol = false; 
                    }                
                }else{
                    $(this).prev().attr('required','false');
                    bol = true; 
                }
            });
            if(!bol){
                alert('請輸入檔案名稱');
                $('#fader').css('display', 'none');
                return bol;
            }
            
            if(bol){
                var formData = new FormData($("#fileForm")[0]);
                $.ajax({
                    type: "POST",
                    url: "fileUpdate.php",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'text',   // 回傳的資料格式                
                    success: function (data){
                        $('#fader').css('display', 'none');
                        alert(data);
                        location.reload();
                    },
                    xhr:function(){
                        var xhr = new window.XMLHttpRequest(); // 建立xhr(XMLHttpRequest)物件
                        xhr.upload.addEventListener("progress", function(progressEvent){ // 監聽ProgressEvent
                            if (progressEvent.lengthComputable) {
                                var percentComplete = progressEvent.loaded / progressEvent.total;
                                var percentVal = Math.round(percentComplete*100) + "%";
                                $("#percent").text(percentVal); // 進度條百分比文字
                                $("#bar").width(percentVal);    // 進度條顏色
                            }
                        }, false);
                        return xhr; // 注意必須將xhr(XMLHttpRequest)物件回傳
                    }
                }).fail(function(){
                    $("#percent").text("0%"); // 錯誤發生進度歸0%
                    $("#bar").width("0%");
                });
            }else{
                return bol;
            }
        });
    });
    function addItem() {
        var newone = "<div><input name='oTitle[]' id='oTitle' placeholder='請輸入名稱' type='text' />"
        + " <input type='file' name='oFile[]' id='oFile'/>"
        + "<span class='sBtn' id='oTitleRemove' onClick='removeItem(this)'> -移除</span></div>";
        $('#oTitleDiv').append(newone);
    }
    function removeItem(tbtn) {
        $(tbtn).parent().remove();
    }

    function checkForm(form){
        // var bol = false
        // $("[name='oFile[]']").each(function(k,v){
        //     if($(this).val() != ''){
        //         if($("[name='oTitle[]']")[k].value != ''){
        //             bol = true;
        //         }else{
        //             $(this).prev().attr('required','true');
        //             bol = false; 
        //         }                
        //     }else{
        //         $(this).prev().attr('required','false');
        //         bol = true; 
        //     }
        // });
        // if(!bol){
        //     alert('請輸入檔案名稱');
        // }else{
        //     $('#fader').css('display', 'block');
        // }
        // return bol;        
    }
</script>