<?php 
if ($odMenu == 'rod') {
    $odTypeName = '收文';
    $noteDefault ='';    
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';
    $noteDefault = '提醒您~
1.有附件卻無法上傳時，請在此欄說明；同時，請於發文後，另備一份副本（封面請註明「發文文號」）轉回各機構總文書存檔。
2.核銷冊僅需副本一份，請直接轉財務室稽核存檔。
以上，敬請合作！謝謝！';
}
include_once 'inc_vars.php';
include_once "$root/system/db.php";
include_once 'getEmplyeeInfo.php';
include_once "$root/receipt/receiptDiv.php";
include_once "$root/group/groupDiv.php";

$empID = $_SESSION['empID'];
$depID = $_SESSION['depID'];
$db    = new db();
//公文主檔
if($_GET['mode'] == 'copy'){
    $sql = "select * from edis_temp where id=" . $_REQUEST['id'];
    $rs  = $db->query($sql);
    if ($rs) {
        $r = $db->fetch_array($rs);
    }
    $edisID = $r['sourceID'];
}else{
    $sql = "select * from edis where id=" . $_REQUEST['id'];
    $rs  = $db->query($sql);
    if ($rs) {
        $r = $db->fetch_array($rs);
    }
    $edisID = $_REQUEST['id'];
}

//承辦部門
$mdepSql = "SELECT * FROM map_dep WHERE eid = '" . $r['id'] . "'";
$rsDep   = $db->query($mdepSql);
$rDep    = $db->fetch_array($rsDep);

//組織機關
$OrgSql = "select id,title from organization";
$rsX    = $db->query($OrgSql);
while ($rx = $db->fetch_array($rsX)) {
    $orgs[$rx[0]] = $rx[1];
}
//承辦主管
$signMgrSql = "select id,signMan from map_orgs_sign where edisid='" . $edisID . "' and signLevel='2' order by id";
$rsSignMgr  = $db->query($signMgrSql);

//校對
$PfSql = "select signMan from map_orgs_sign where edisid='" . $edisID . "' and signLevel='1'";
$rsPf  = $db->query($PfSql);
if ($rsPf) {
    $rPf = $db->fetch_array($rsPf);
}

//發文機關
$soActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
                     `o`.`sTitle`,
                     `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='SO' ";
//echo $soActSql;
$rsSOact  = $db->query($soActSql);
if ($rsSOact) {
    $rSOact = $db->fetch_array($rsSOact);
}

//文號選取
$sActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
                     `o`.`sTitle`,
                     `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='S' ";
//echo $sActSql;
$rsSact  = $db->query($sActSql);
if ($rsSact) {
    $rSact = $db->fetch_array($rsSact);
}
//受文者
$rActSql = "select mo.oid,if((length(`o`.`sTitle`) > 0),
                     `o`.`sTitle`,
                     `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='R' ";
$rsRact  = $db->query($rActSql);
if ($rsRact) {
    $rRact = $db->fetch_array($rsRact);
}

//正本
$nActSql = "select mo.id, mo.oid,if((length(`o`.`sTitle`) > 0),
                     `o`.`sTitle`,
                     `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='N' order by mo.id ";
$rsNact  = $db->query($nActSql);

//副本
$cActSql = "select mo.id, mo.oid,if((length(`o`.`sTitle`) > 0),
                     `o`.`sTitle`,
                     `o`.`title`) as stitle  from map_orgs mo , organization o where mo.oid =o.id  and mo.eid = '" . $edisID . "' and mo.act ='C' order by mo.id ";
$rsCact  = $db->query($cActSql);

//主文附件
$mFileSql = "select * from map_files mf where mf.eid = '" . $edisID . "' and aType='M'";
$rsMfile  = $db->query($mFileSql);
if ($rsMfile) {
    $rMfile = $db->fetch_array($rsMfile);
}

//其他附件
$oFileSql = "select * from map_files mf where mf.eid = '" . $edisID . "' and aType='O'";
$rsOfile  = $db->query($oFileSql);

?>

<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<link href="edis/report.css" rel="stylesheet" type="text/css" />
<link href="group/group.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    textarea::-webkit-input-placeholder{ color: red; }
</style>

<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="edis/report.js"></script>
<script src="group/group.js"></script>
<script>
  function returnValues(data){
    console.log(data);
    // var group_id = $("input[name='dt_group_id[]']").first().val();
    $.each( data, function( k, v ) {
        if(v['type']=='orgID'){
            if(k != 0) addItem('',data['act'],data['act']);
            $("input[name='"+data['act']+"[]']").last().val(v['id']);
            $("input[name='"+data['act']+"[]']").last().attr('title',v['title']);
            $("input[name='"+data['act']+"[]']").last().after(' '+v['title']);
            // $("input[name='dt_group_id[]']").last().val(group_id);
        }
    });
}
function addItem(tbtn,fld,qid) {
    if(fld !=='oTitle'){
        if(fld =='nAct' || fld =='cAct'){
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' class='queryID' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='"+fld+"'>"
        + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
        + " <span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
        }else{
        var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' class='queryID' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
        + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
        + " <span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
        }
    }else if(fld =='oTitle'){
       var newone = "<div><input name='"+fld+"[]' id='"+qid+"'  placeholder='請輸入名稱' type='text'>"
       + " <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'>"
       + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }
    var divTitle ='#'+fld+'Div';
    $(divTitle).append(newone);
}

function removeItem(tbtn) {
      $(tbtn).parent().remove();
   }

$(function(){
    $('#sDate').datepicker();
    $('#deadline').datepicker();
    $('#applyDate').datepicker();
});



function checkOnly(act){//檢核檔號及發文字號是否唯一
    var checkNull;
    var checkValue;
    var InputFiled;
    if(act =='did'){
        if($("[name='did']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='did']").val();
        InputFiled = document.querySelector('#did');
    };
}else if(act =='dNo'){
    if($("[name='dNo']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='dNo']").val();
      InputFiled = document.querySelector('#dNo');
   };
}
if(checkNull =='true'){
    $.ajax({
        url: 'edis/api.php?act='+act+'&inValue='+checkValue,
        type:"GET",
        dataType:'text',
        success: function(response){
            //alert(response);
            if (response == 'valid') {
                InputFiled.setCustomValidity('');
                 console.log('valid');
            }else if (response == 'invalid') {
                InputFiled.setCustomValidity('需為唯一值');
                 console.log('invalid');
            }
        }

    });
}
}



//選擇上傳檔案(多個)需檢核有無輸入名稱
function checkMutiAtt(u,f,t){
    //f:input type=file id
    //t:title id
    var fileName =f+'[]';
    var titleName =t+'[]';
    var titleId ='#'+t;
    var uName =u+'[]';
    var fl=document.getElementsByName(fileName);

    for(var i=0;i<fl.length;i++){
     var uv= document.getElementsByName(uName)[i].innerHTML;
     var fv = fl[i].value;
     var otValue= document.getElementsByName(titleName)[i].value;

     if(fv.length>0 || uv.length>0){
     if(otValue.length==0){
      document.querySelectorAll(titleId)[i].setCustomValidity('請填入附件名稱');
     }else{
      document.querySelectorAll(titleId)[i].setCustomValidity('');
     }
   }else{
     document.querySelectorAll(titleId)[i].setCustomValidity('');
   }

  }
}
//選擇上傳檔案(單一)需檢核有無輸入名稱
function checkAloneAtt(u,f,t){
    //u:已上傳檔案名稱span id
    //f:input type=file id
    //t:title id
    var titleName =t;
    var titleId ='#'+t;
    var uv=document.getElementById(u).innerHTML;
    var fv =document.getElementById(f).value;
    var otValue= document.getElementById(titleName).value;
    if(fv.length>0 || uv.length>0){
     if(otValue.length==0){
      document.querySelector(titleId).setCustomValidity('請填入附件名稱');
     }else{
      document.querySelector(titleId).setCustomValidity('');
     }
  }else{
    document.querySelector(titleId).setCustomValidity('');
  }
}

//文號選取變更,副本第一欄需連動
function setcAct(id,title){
 var params = $('input[name="cAct[]"]');
 $(params[0]).val(id);
 $(params[0]).next().next('span').html(title);


}


//受文單位變更,正本第一欄需連動
function setnAct(id,title){
 var params = $('input[name="nAct[]"]');
 $(params[0]).val(id);
 $(params[0]).next().next('span').html(title); 
}

$("#oFile").live('change', function(){

    checkMutiAtt('oUpload','oFile','oTitle');

    })

$("#oTitle").live('input', function(){

    checkMutiAtt('oUpload','oFile','oTitle');

    })

$("#mFile").live('change', function(){

    checkAloneAtt('mUpload','mFile','mTitle');

    })

$("#mTitle").live('input', function(){

    checkAloneAtt('mUpload','mFile','mTitle');

    })
function getodNum(){//取得文件字號
    var did =$("[name='did']").val();
    var reportResult = checkReport();//檢核總表
    if(did.length<=0){
        var act='odNum';
        var checkNull;
        var orgid;
        var InputFiled;
        var errMsg;
        var odType =$("[name='odType']").val();
        var result = false;
        var errMsg ='取號發生異常,無法送出。';

        if($("[name='resultNum']").attr('checked') || $("[name='planNum']").attr('checked')){
          $.each($('[name="oFile[]"]'), function(k,v){
              if(this.files.length <= 0){
                  checkNull ='false'; 
                  errMsg ='成果冊或計畫書請至少上傳一份。'; 
              }
          });
        }

        if($("[name='soAct']").val()==""||$("[name='sAct']").val()==""||$("[name='dSpeed']").val()==""||$("[name='dType']").val()==""||$("[name='rAct']").val()==""||$("[name='sId']").val()==""||$("[name='depID']").val()==""||$("[name='sDate']").val()==""||$("[name='sidMgr[]']").val()==""||$("[name='proofreader']").val()==""||$("[name='subjects']").val()==""){
            checkNull ='false';             
            errMsg ='尚有必填欄位未填入。'; 
            alert(errMsg);
        }else{
            checkNull ='true'; orgid =$("[name='sAct']").val(); orgidRo='';  
        }

      if(checkNull =='true'){
          if(reportResult){
              result=true;
            // $.ajax({
            //     url: 'edis/api.php?act='+act+'&orgid='+orgid+'&orgidRo='+orgidRo+'&odType='+odType,
            //     type:"GET",
            //     dataType:'text',
            //     async: false,
            //     success: function(response){
            //         if(response !==''){
            //         document.getElementById('did').value =response.trim();
            //         alert("取得"+odType+"字號:"+response.trim());
            //         result=true;
            //         }else{
            //         result=false;    
            //         alert(errMsg);
            //         }

            //     }
            // })
          }
      }
  }else{
        result=true;
  }
  return result;
}

</script>

<style>
    .require{
 color:red;
 }

 </style>

 <div class="row-fluid">
  <div class="span12">
      <table width="100%" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; padding:5px">
        <tr><th bgcolor="#e0e0e0">修改<?php  echo $odTypeName?></th></tr>
    <!--
    <tr><td bgcolor="#f8f8f8"><div style="overflow:auto; height:100px; width:100%;"><?php  echo nl2br(file_get_contents('data/absence.txt'))?></div></td></tr>
-->
</table>
</div>
</div>
<?php  echo genGroupDom('sidMgr');  ?>  
<?php  echo genGroupDom('nAct');  ?>
<?php  echo genGroupDom('cAct');  ?>
<div style="height:5px"></div><div style="border-top:1px #999999 dashed; height=1px;"></div><div style="height:5px"></div>

<form id="prjform" action="<?php  echo "edis/sod/editDo.php?odMenu=" . $odMenu?>" method="post" enctype="multipart/form-data">

      <!--flag 控制是否insert 總表:領據/成果冊/執行計畫報告 -->
        <input name="receiptFlag" type="hidden" id="receiptFlag" value="F"/>
        <input name="" type="hidden" id="" value="F"/>
        <input name="" type="hidden" id="" value="F"/>
        <?php  echo genReceiptDom($_REQUEST['id']);  ?>
    <input type="hidden" name="mode" value="<?php  echo $_GET['mode']?>">
    <input type="hidden" name="loginType" value="<?php  echo $r['loginType']?>">
    <input type="hidden" name="sourceID" value="<?php  echo $edisID?>">
    <input name="signLevel" type="hidden" id="signLevel" value="<?php  echo $_REQUEST['signLevel']?>"/>
    <table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">
 <tr>
            <td align="right"><span class="require">*</span>發文機關：</td>
            <td align="left"><input name="soAct" type="text" class="queryID" id="soAct" data-odtype="<?php echo  $odMenu;?>" value="<?php  echo $rSOact['oid']?>" size="6" required ="true" title="<?php  echo $rSOact['stitle']?>" onkeypress="return checkInput(event,'depName',this)" />   
            <span></span>文號選取：
            <input name="sAct" type="text" class="queryID" id="sAct" data-odtype="<?php echo  $odMenu;?>" value="<?php  echo $rSact['oid']?>" size="6" required ="true" title="<?php  echo $rSact['stitle']?>" onkeypress="return checkInput(event,'depName',this)" /><input name="depID" type="hidden" id="depID" value="<?php  echo $depID?>"/></td>
            <td align="right"><?php if($_GET['mode']!='copy'){?><span class="require">*</span><?php }?><?php  echo $odTypeName?>字號：</td>
            <td>
            <input type="text" name="did" id="did" required ="true" oninput="checkOnly('did')"
              value="<?php  echo $r['did']?>" readonly="readonly"/><input name="edisId" type="hidden" id="edisId" value="<?php  echo $_REQUEST['id']?>" />
            <input style="display: none" name="odType" type="text" id="odType" readonly="readonly" required ="true" size="6" value="<?php  echo $r['odType']?>"/>
           </td>
            <?php if ($mobile) {
    echo '</tr><tr>';
}
?>
        </tr>
        <tr>
            <td align="right"><span class="require">*</span>速別：</td>
            <td><select name="dSpeed" id="dSpeed" required ="true"/>
            <?php foreach ($odSpeed as $v) {
            if ($r['dSpeed'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                echo "<option>$v</option>";
            }
            }?>
            </select>
        </td>
        <td align="right">密等：</td>
        <td align="left"><select name="dSecret" id="dSecret" />
        <?php 
        foreach ($odSecret as $v) {
            if ($r['dSecret'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                echo "<option>$v</option>";
            }
        }?>
        </select>
    </td>
    </tr>

        <tr>
            <td align="right">建檔者：</td>
            <td><input  type="text" name="creatorName" id="creatorName" readonly="true" value="<?php  echo $emplyeeinfo[$r['creator']]?>"/></td><input name="creator" type="hidden" id="creator" value="<?php  echo $r['creator']?>"/>   
            <td align="right">公文類型：</td>
            <td align="left"><select name="dType" id="dType"  />
            <?php foreach ($dType as $v) {
                if ($r['dType'] == $v) {
                    echo "<option selected='selected'>$v</option>";
                } else {
                    echo "<option>$v</option>";
                }
            }?>
            </select>
            </td>             
        </tr>
        <tr>
            <td align="right"><span class="require">*</span>受文者：</td>
            <td align="left"><input name="rAct" type="text" class="queryID" id="rAct" data-odtype="<?php echo  $odMenu;?>" value="<?php  echo $rRact['oid']?>" size="6" required ="true" title="<?php  echo $rRact['stitle']?>" onkeypress="return checkInput(event,'depName',this)" />
            </td>
            <td align="right"><span class="require">*</span>承辦人：</td>
            <?php  if($_GET['mode'] === 'revEdit'){ ?>
                <td align="left"><input name="sId" type="text" id="sId" required ="true" value="<?php  echo $r['sId']?>" size="6" readonly/><?php  echo $emplyeeinfo[$r['sId']]?>
                  <input name="sOid" type="hidden" id="sOid" value="<?php  echo $emplyeeDept[$r['sId']]?>"/>
            <?php  }else{ ?>
                <td align="left"><input name="sId" type="text" class="queryID" id="sId" required ="true" value="<?php  echo $r['sId']?>" size="6" title="<?php  echo $emplyeeinfo[$r['sId']]?>" onkeypress="return checkInput(event,'empName',this)" /><input name="sOid" type="hidden" id="sOid" value="<?php  echo $emplyeeDept[$r['sId']]?>"/>
            <?php  } ?>
            分機：<input name="ext" type="text" id="ext" size="6" value="<?php  echo $r['ext']?>"/>
            </td>                
        </tr>
        <tr>
            <td align="right"><span class="require">*</span>發文日期：</td>
            <td><input type="text" name="sDate" id="sDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" value="<?php  echo date_format(date_create($r['sDate']), 'Y/m/d')?>" required ="true" /></td>
             <td align="right">承辦主管：</td>
                    <td>
                      <?php  if($_GET['mode'] !== 'revEdit'){ ?>
                        <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_sidMgr');" value="群組">
                    <?php  } ?>
                      
            <?php 

            $sCount = 0;
            while ($rSignMagr = $db->fetch_array($rsSignMgr)) {
                $sCount++;
                if ($sCount == 1) {
                  if($_GET['mode'] === 'revEdit'){ 
                    echo "<input name='sidMgr[]' type='text' id='sidMgr' value='".$rSignMagr['signMan'] . "' size='6' readonly/>" .$emplyeeinfo[$rSignMagr['signMan']];
                  }else{
                    echo "<input name='sidMgr[]' type='text' class='queryID' id='sidMgr' value='".$rSignMagr['signMan'] . "' size='6'"
                        . "title='" . $emplyeeinfo[$rSignMagr['signMan']] . "' onkeypress=\"return checkInput(event,'empName',this)\" />"
                        . "<span class='sBtn' onclick=\"addItem(this,'sidMgr','sidMgr')\"> +新增承辦主管</span><div id='sidMgrDiv'> ";
                  }
                } else if ($sCount > 1) {
                  if($_GET['mode'] === 'revEdit'){ 
                    echo "<div><input name='sidMgr[]' id='sidMgr' value='".$rSignMagr['signMan'] . "' type='text' size='6' readonly>".$emplyeeinfo[$rSignMagr['signMan']];
                  }else{
                    echo "<div><input name='sidMgr[]' class='queryID' id='sidMgr' value='".$rSignMagr['signMan'] . "' type='text' size='6' title='".$emplyeeinfo[$rSignMagr['signMan']]."' onkeypress=\"return checkInput(event,'empName',this)\" >"
                        . "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
                  }
                }

            }
            echo '</div>';
            if ($sCount == 0) {

                echo "<input name='sidMgr[]' type='text' class='queryID' id='sidMgr' size='6'/>"
                    . "<span class='sBtn' onclick=\"addItem(this,'sidMgr','sidMgr')\"  onkeypress=\"return checkInput(event,'depName',this)\" > +新增承辦主管</span>
                    <div id='sidMgrDiv'></div>";

            }

            ?>

            </td>        
        </tr>
    <tr>
        <!--<td align="right">類別：</td>
        <td align="left"><select name="odDeadlineType" id="odDeadlineType" required ="true" />
            <?php  /* foreach ($odDeadlineType as $v) {
                if ($_REQUEST['odDeadlineType'] == $v) {
                    echo "<option selected='selected'>$v</option>";
                } else {
                    echo "<option>$v</option>";
                }
            } */ ?>
        </select>
        </td>-->
        <td align="right"><span class="require">*</span>校對：</td>
        <td align="left"><input name="proofreader" type="text" class="queryID" id="proofreader" required ="true" size="6" value="<?php  echo $rPf['signMan']?>" title="<?php  echo $emplyeeinfo[$rPf['signMan']]?>" onkeypress="return checkInput(event,'empName',this)" /></td>
        <td align="right">計畫編號：</td>
        <td align="left"><input name="planNo" type="text" id="planNo" value="<?php  echo $r['planNo']?>" /></td>            
    </tr>    
   <tr>
            <td align="right"><span class="require">*</span>承辦部門：</td>
        <td align="left">
            <input name="depID" class="queryID"  id="admDep" type="text" readonly size="6" value="<?php  echo $rDep['depID']?>" onkeypress="return checkInput(event,'depID',this)" required ="true" title="<?php  echo $depAdminfo[$rDep['depID']]?>"/>
        </td>    
        <td align="right">發文機構地址：</td>
        <td>
           <input name="letterAddr" type="text" class="queryID" id="depInnerID" readonly value="<?php  echo $r['letterAddr']?>"  size="6" onkeypress="return checkInput(event,'org',this)" title="<?php  echo $depAdminfo[$r['letterAddr']]?>"/>    
        </td>        
    </tr>
    <tr>
        <td align="right">附件類型：</td>
        <td>
            <input name="budgetNum" type="checkbox" id="budgetNum"  size="6" <?php  if($r['budgetNum'] == 1) echo 'checked'; ?> />核銷冊
            <input name="resultNum" type="checkbox" id="resultNum"  size="6" <?php  if($r['resultNum'] == 1) echo 'checked'; ?> />成果冊
            <input name="planNum" type="checkbox" id="planNum"  size="6" <?php  if($r['planNum'] == 1) echo 'checked'; ?> />計畫書
        </td>
    </tr>
    <tr>
        <td align="right">附件：</td>
         <td colspan="3" align="left">
       <div>
        <button type="button" id='addAttBtn'  data-toggle="collapse"  data-target="#demo" >新增</button>
        <div id="demo" class="collapse.show">
    <?php 
    $oCount = 0;
    while ($rOfile = $db->fetch_array($rsOfile)) {
        $oCount++;
        if ($oCount == 1) {
            if($_GET['mode'] == 'copy'){
              echo "<input name='oTitle[]' id='oTitle'  value='" . $rOfile['title'] . "'type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span>
              <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span>　<span><input name='delFile' type='checkbox' value='" . $rOfile['id'] . "'>刪除</span><div id='oTitleDiv'>
                ";
            }else{
              echo "<input name='oTitle[]' id='oTitle'  value='" . $rOfile['title'] . "'type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span>
                <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span> <div id='oTitleDiv'>
                ";
            }
        } else if ($oCount > 1) {

            echo "<div><input name='oTitle[]' id='oTitle'  value ='" . $rOfile['title'] . "' placeholder='請輸入名稱' type='text'><span id='oUpload' name='oUpload[]'>" . $rOfile['fName'] . "</span><input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
        }

    }
    if ($oCount == 0) {
        echo "<input name='oTitle[]' id='oTitle'  type='text' placeholder='請輸入名稱' /><span id='oUpload' name='oUpload[]'></span>
            <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'><input name='oFileId[]' type='hidden' id='oFileId' value='" . $rOfile['id'] . "'/><span class='sBtn' onclick=\"addItem(this,'oTitle','oTitle')\"> +新增其他附件</span>
                <div id='oTitleDiv'>
                </div>";
        //add by tina 180420 修改公文時如已有其他附件則展開div
        echo '<script type="text/javascript">',
        'document.addEventListener("DOMContentLoaded", function() {',
            'document.getElementById("demo").setAttribute("class", "collapse");
            });</script>';

    } else {
        echo "</div>";
    }
    ?>
        </div>
       </div>
    </td>
    </tr>
    <tr>
        <td align="right">備註：</td>
        <td colspan="3"><textarea name="note" id="note" rows="4" style="width:75%" placeholder="<?php  echo $noteDefault?>"><?php  echo $r['note']?></textarea></td>
    </tr>    
    <tr>
        <td align="right"><span class="require">*</span>主旨：</td>
        <td colspan="3"><textarea name="subjects" id="subjects" rows="3" style="width:75%" required ="true"><?php  echo $r['subjects']?></textarea></td>
    </tr>
    <tr>
        <td align="right">說明：</td>
        <td colspan="3"><textarea name="contents" id="contents" rows="3" style="width:90%" ><?php  echo $r['contents']?></textarea>
        <script>
              CKEDITOR.replace( 'contents', {
        });

                </script>
        </td>
    </tr>
    <!--<tr>
        <td td colspan="4" align="center">
            <input type="button" name="receiptEnter" id="receiptEnter" onclick="setReport('receipt');" value="領據" />
            <input type="button" name="" id="" value="成果冊" />
            <input type="button" name="" id="" value="執行計畫報告" />
        </td>
    </tr>-->  
    <tr>
        <td align="right">正本：</td>
        <td>
          <input type="button" name="groupnAct" onclick="setGroup('group_nAct');" value="群組">
    <?php 
    $nCount = 0;
    while ($rNact = $db->fetch_array($rsNact)) {
        $nCount++;
        if ($nCount == 1) {
            echo "<input name='nAct[]' type='text' class='queryID' id='orgIDs' value='".trim($rNact['oid'])."'size='6'"
                . "title='" . $rNact['stitle'] . "' onkeypress=\"return checkInput(event,'org',this)\" data-act='nAct'/>"
                . "<span class='sBtn' onclick=\"addItem(this,'nAct','orgIDs')\"> +新增</span><input name='nActId[]' type='hidden' id='orgIDs' value='" . $rNact['id'] . "'/>
                <div id='nActDiv'> ";
        } else if ($nCount > 1) {
            echo "<div><input name='nAct[]' class='queryID' id='orgIDs' value='".trim($rNact['oid'])."'type='text' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='nAct'>"
                . "<span class='formTxS'>" . $rNact['stitle'] . "</span><input name='nActId[]' type='hidden' id='orgIDs' value='" . $rNact['id'] . "'/>"
                . "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
        }

    }
    echo '</div>';
    if ($nCount == 0) {

        echo "<input name='nAct[]' type='text' class='queryID' id='orgIDs' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='nAct'/><input name='nActId[]' type='hidden' id='orgIDs' value='" . $rNact['id'] . "' data-act='nAct'/>"
            . "<span class='sBtn' onclick=\"addItem(this,'nAct','orgIDs')\"> +新增</span>
            <div id='nActDiv'></div>";
    }

    ?>
    </td>
        <td align="right">副本：</td>
        <td>
          <input type="button" name="groupcAct" onclick="setGroup('group_cAct');" value="群組">
    <?php 

    $cCount = 0;
    while ($rCact = $db->fetch_array($rsCact)) {

        $cCount++;
        if ($cCount == 1) {
            echo "<input name='cAct[]' type='text' class='queryID' id='orgIDs' value='".trim($rCact['oid'])."'size='6'". "title='" . $rCact['stitle'] . "' onkeypress=\"return checkInput(event,'org',this)\" data-act='cAct'/><input name='cActId[]' type='hidden' id='orgIDs' value='" . $rCact['id'] . "' data-act='cAct'/>"
                . "<span class='sBtn' onclick=\"addItem(this,'cAct','orgIDs')\"> +新增</span>
                <div id='cActDiv'>";
        } else if ($cCount > 1) {
            echo "<div><input name='cAct[]' class='queryID' id='orgIDs' value='".trim($rCact['oid'])."'type='text' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='cAct'>"
                . "<span class='formTxS'>" . $rCact['stitle'] . "</span><input name='cActId[]' type='hidden' id='orgIDs' value='" . $rCact['id'] . "' data-act='cAct'/>"
                . "<span class='sBtn' onClick='removeItem(this)'> -移除</span></div>";
        }

    }
    echo '</div>';
    if ($cCount == 0) {

        echo "<input name='cAct[]' type='text' class='queryID' id='cAct' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='cAct'/><input name='cActId[]' type='hidden' id='cActId' value='" . $rCact['id'] . "' data-act='cAct'/>"
            . "<span class='sBtn' onclick=\"addItem(this,'cAct','orgIDs')\"> +新增</span>
            <div id='cActDiv'></div>";
    }
    ?>

        </td>
    </tr>
    <?php 
    if ($odMenu == 'rod') {
        $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/myod.php&muID=0";
    } else if ($odMenu == 'sod') {
        $locUrl = "/index.php?funcUrl=edis/" . $odMenu . "/myod.php&muID=1";
    }
    ?>
      <tr>
        <td colspan="4" align="center">
          <?php  if($_GET['mode'] != 'copy'){ ?>
            <input type="submit" name="Submit" id="Submit" value="送出" onclick="return getodNum();"/>
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>'">
          <?php  }else{ ?>
            <input type="hidden" name="mode" value="copy">
            <input type="submit" name="Submit" id="Submit" onclick="return getodNum();"  value="送出" />
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>&copy=cancel&id=<?php  echo $_GET['id']?>'">
          <?php  } ?>
      </td>
      </tr>
</table>
</form>
