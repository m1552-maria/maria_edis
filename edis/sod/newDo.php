<?php 
@session_start();
if ($_REQUEST['Submit']) {

    include_once "../../config.php";
    include_once "../../system/db.php";
    include_once "../../getEmplyeeInfo.php";
    include_once "$root/receipt/receiptDiv.php"; 
    include_once "../inc_vars.php";
    include_once "../func.php";

    //insert 公文主檔
    $did       = getodNum('發文',$_REQUEST['sAct'],'',$_REQUEST['sDate']);
    $budgetNum = isset($_REQUEST['budgetNum']) ? '1' : '0';
    $resultNum = isset($_REQUEST['resultNum']) ? '1' : '0';
    $planNum   = isset($_REQUEST['planNum']) ? '1' : '0';

    $db                     = new db('edis');
    $db->row['did']         = "'" . $did . "'";
    $db->row['proofreader'] = "'" . $_REQUEST['proofreader'] . "'";    
    $db->row['creator']     = "'" . $_REQUEST['creator'] . "'";
    $db->row['dType']       = "'" . $_REQUEST['dType'] . "'";
    $db->row['sId']         = "'" . $_REQUEST['sId'] . "'";
    $db->row['sOid']        = "'" . $_REQUEST['sOid'] . "'";
    $db->row['ext']         = "'" . $_REQUEST['ext'] . "'";
    $db->row['sDate']       = "'" . $_REQUEST['sDate'] . "'";
    $db->row['dSpeed']      = "'" . $_REQUEST['dSpeed'] . "'";
    $db->row['dSecret']     = "'" . $_REQUEST['dSecret'] . "'";
    $db->row['subjects']    = "'" . addslashes($_REQUEST['subjects']) . "'";  
    $db->row['contents']    = "'" . addslashes($_REQUEST['contents']) . "'";
    $db->row['odType']      = "'" . $_REQUEST['odType'] . "'";
    $db->row['planNo']      = "'" . $_REQUEST['planNo'] . "'";
    $db->row['budgetNum']   = "'" . $budgetNum . "'";
    $db->row['resultNum']   = "'" . $resultNum . "'";
    $db->row['planNum']     = "'" . $planNum . "'"; 
    $db->row['note']        = "'" . $_REQUEST['note'] . "'";
    $db->row['letterAddr']  = "'" . $_REQUEST['letterAddr'] . "'";  
    $db->row['signStage']   = "'1'";

    // var_dump($db);exit();
    $db->insert();
    $edisId = $db->lastInsertId;

    //insert DepID 2021/03/31
    $op = array(
        'dbSource'   => DB_ADMSOURCE,
        'dbAccount'  => DB_ACCOUNT,
        'dbPassword' => DB_PASSWORD,
        'tableName'  => '',
    );

    $db = new db($op);
    $sql2 = "select depTitle from department where depID = '".$_REQUEST['depID']."'";
    $rs2  = $db->query($sql2);
    $r2   = $db->fetch_array($rs2);
    $title = $r2['depTitle'];
    $db->close();

    $db               = new db('map_dep');
    $db->row['eid']   = $edisId;
    $db->row['depID'] = "'" . $_REQUEST['depID'] . "'";
    $db->row['title'] = "'" . $title . "'";
    $db->insert();
    $db->close();


    //insert 發文機關
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['soAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();

    $db             = new db('map_orgs');
    $db->row['eid'] = $edisId;
    $db->row['oid'] = "'" . $_REQUEST['soAct'] . "'";
    $db->row['act'] = "'" . 'SO' . "'";
    $db->row['title'] = "'" .  $r['title'] . "'";
    $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
    $db->insert();
    $soActId = $db->lastInsertId;

    //insert 文號選取
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();

    $db             = new db('map_orgs');
    $db->row['eid'] = $edisId;
    $db->row['oid'] = "'" . $_REQUEST['sAct'] . "'";
    $db->row['act'] = "'" . 'S' . "'";
    $db->row['title'] = "'" .  $r['title'] . "'";
    $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
    $db->insert();
    $sActId = $db->lastInsertId;

    //insert 受文者
    $db = new db();
    $sql = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
    $rs  = $db->query($sql);
    $r   = $db->fetch_array($rs);
    $title = $r['title'];
    $sTitle = $r['sTitle'];
    $db->close();

    $db             = new db('map_orgs');
    $db->row['eid'] = $edisId;
    $db->row['oid'] = "'" . $_REQUEST['rAct'] . "'";
    $db->row['act'] = "'" . 'R' . "'";
    $db->row['title'] = "'" .  $r['title'] . "'";
    $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
    $db->insert();
    $rActId = $db->lastInsertId;

    //insert 校對者
    $db                    = new db('map_orgs_sign');
    $db->row['edisid']     = $edisId;
    $db->row['isOriginal'] = "'" . '1' . "'";
    $db->row['isWait']     = "'" . '1' . "'";
    $db->row['signLevel']  = "'" . '1' . "'";
    $db->row['signMan']    = "'" . str_pad($_REQUEST['proofreader'], 5, "0", STR_PAD_LEFT) . "'";
    $db->insert();
    $pfId = $db->lastInsertId;
    //簽核階層

    $sidmgrAry = array();
    //insert承辦人主管(單或多個)
    $pcount = -1;
    foreach ($_REQUEST['sidMgr'] as $v) {
        $pcount++;
        $db                    = new db('map_orgs_sign');
        $db->row['edisid']     = $edisId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '1' . "'";
        $db->row['signLevel']  = "'" . '2' . "'";
        $db->row['signMan']    = "'" . str_pad(trim($v), 5, "0", STR_PAD_LEFT) . "'";
        $db->insert();
        $sidMgrId           = $db->lastInsertId;
        $sidmgrAry[$pcount] = $sidMgrId;
    }

    //insert 正本
    foreach ($_REQUEST['nAct'] as $v) {
        $db = new db();
        $sql = "select title, sTitle from organization where id = '".$v."'";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $title = $r['title'];
        $sTitle = $r['sTitle'];
        $db->close();

        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $v . "'";
        $db->row['act'] = "'" . 'N' . "'";
        $db->row['title'] = "'" .  $r['title'] . "'";
        $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
        $db->insert();
        $nActId = $db->lastInsertId;

    }
    //insert 副本
    foreach ($_REQUEST['cAct'] as $v) {
        $db = new db();
        $sql = "select title, sTitle from organization where id = '".$v."'";
        $rs  = $db->query($sql);
        $r   = $db->fetch_array($rs);
        $title = $r['title'];
        $sTitle = $r['sTitle'];
        $db->close();

        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $v . "'";
        $db->row['act'] = "'" . 'C' . "'";
        $db->row['title'] = "'" .  $r['title'] . "'";
        $db->row['sTitle'] = "'" .  $r['sTitle'] . "'";
        $db->insert();
        $cActId = $db->lastInsertId;
    }

    //insert歸檔
    //取得發文者單位窗口
    $rRevSql = "select *  from  organization o where o.id=" . "'" . $_REQUEST['sAct'] . "' and orgType='SELF'";
    $rsRrev  = $db->query($rRevSql);
    if ($rsRrev) {
        $rRrev = $db->fetch_array($rsRrev);
    }
    if (!empty($rRrev['mainRevMan'])) {
        //insert發文者單位窗口
        $db                = new db('map_orgs_sign');
        $db->row['edisid'] = $edisId;
        //$db->row['moid']       = $rActId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '1' . "'";
        $db->row['signLevel']  = "'" . '4' . "'";
        $db->row['signMan']    = "'" . str_pad($rRrev['mainRevMan'], 5, "0", STR_PAD_LEFT) . "'";
        $db->insert();
        $revManId = $db->lastInsertId;
    }

    //處理主文附件
    if ($_FILES['mFile']['tmp_name'] != "") {
        if (!empty($_REQUEST['mTitle'])) {
        //名稱已填寫insert 主文附件
            $ulpath = '../../data/edis/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $ulpath = '../../data/edis/' . $edisId . '/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $ulpath = '../../data/edis/' . $edisId . '/' . 'main/';
            if (!file_exists($ulpath)) {
                mkdir($ulpath);
            }

            $srcfn = '';
            $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
            $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

            if ($rzt) {
                $sLocation = strripos($srcfn, '/') + 1;
                $length    = strlen($srcfn) - strripos($srcfn, '/');
                $fileName  = substr($srcfn, $sLocation, $length);

                //insert 主文附件
                $db               = new db('map_files');
                $db->row['eid']   = $edisId;
                $db->row['aType'] = "'" . 'M' . "'";
                $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                $db->row['fName'] = "'" . $fileName . "'";
                $db->insert();

            } else {

                echo '主文上傳作業失敗！';exit;

            }
        } else {
            //名稱未填寫
        }
    }

    //處理其他附件
    foreach ($_FILES['oFile']['name'] as $k => $v) {
        if ($_FILES['oFile']['tmp_name'][$k] != "") {
            if (!empty($_REQUEST['oTitle'][$k])) {
                //名稱已填寫 新增上傳資料
                $ulpath = '../../data/edis/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $ulpath = '../../data/edis/' . $edisId . '/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $ulpath = '../../data/edis/' . $edisId . '/' . 'others/';
                if (!file_exists($ulpath)) {
                    mkdir($ulpath);
                }

                $srcfn = '';
                $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);

                if ($rzt) {
                    //insert 其他附件
                    $sLocation = strripos($srcfn, '/') + 1;
                    $length    = strlen($srcfn) - strripos($srcfn, '/');
                    $fileName  = substr($srcfn, $sLocation, $length);

                    $db               = new db('map_files');
                    $db->row['eid']   = $edisId;
                    $db->row['aType'] = "'" . 'O' . "'";
                    $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                    $db->row['fName'] = "'" . $fileName . "'";
                    $db->insert();
                } else {

                    echo '其他附件上傳作業失敗！' . $ulpath;exit;

                }

            } else {
                //名稱未填寫 none

            }
        }
    }
    //總表(領據/成果冊/執行計畫報告)新增/修改/刪除
    //領據
    echo insertReceipt($edisId,$_REQUEST,$_FILES['attach']);
    header("Location: /index.php?funcUrl=edis/sod/new.php&muID=1&did=".$did);
} else {
    header("Location: /index.php?funcUrl=edis/sod/new.php&muID=1&did=".$did);
}
