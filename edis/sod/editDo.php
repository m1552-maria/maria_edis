<?php 
@session_start();
if ($_REQUEST[Submit]) {
    $edit_edis_id = $_REQUEST['edisId'];
    include '../../config.php';
    include "../../system/db.php";
    include "../../setting.php";
    include "$root/receipt/receiptDiv.php";
    include_once "../inc_vars.php";
    include_once "../func.php";

    $db = new db();
    //簽核否決後修改,複製簽收狀況資料至失效歷程資料 add 20180514
    //簽核否決數量
    if($_REQUEST['mode'] == 'revEdit'){
        $db = new db();
        
        if(is_array($_REQUEST['depID'])){
            $depID = join(',', $_REQUEST['depID']);
        }else{
            $depID = $_REQUEST['depID'];
        }

        $sql = "UPDATE edis SET 
                `sDate`    = '" . $_REQUEST['sDate'] . "',
                `rDate`    = '" . $_REQUEST['rDate'] . "',
                `dSpeed`   = '" . $_REQUEST['dSpeed'] . "',
                `deadline` = '" . $_REQUEST['deadline'] . "',
                `planNo`   = '" . $_REQUEST['planNo'] . "',
                `subjects` = '" . addslashes($_REQUEST['subjects']) . "',
                `note`     = '" . $_REQUEST['note'] . "',
                `creator`  = '" . $_REQUEST['creator'] . "',
                `dType`    = '" . $_REQUEST['dType'] . "',
                `sId`      = '" . $_REQUEST['sId'] . "',
                `sOid`     = '" . $_REQUEST['sOid'] . "',
                `ext`      = '" . $_REQUEST['ext'] . "',
                `dSecret`  = '" . $_REQUEST['dSecret'] . "',
                `contents` = '" . addslashes($_REQUEST['contents']) . "',
                `odType`   = '" . $_REQUEST['odType'] . "',
                `budgetNum`= '" . $_REQUEST['budgetNum'] . "',
                `resultNum`= '" . $_REQUEST['resultNum'] . "',
                `planNum`  = '" . $_REQUEST['planNum'] . "',
                `proofreader` = '" . $_REQUEST['proofreader'] . "',
                `letterAddr`  = '" . $_REQUEST['letterAddr'] . "'
                WHERE `id` = '" . $edit_edis_id . "'";
        $db->query($sql);

        //刪除公文承辦部門
        $sql = "delete from map_dep where eid = $edit_edis_id";
        $db->query($sql);

        //insert 承辦部門
        $op = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );
        $db = new db($op);
        $sql2 = "select depTitle from department where depID = '".$_REQUEST['depID']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['depTitle'];
        $db->close();

        $db               = new db('map_dep');
        $db->row['eid']   = $edit_edis_id;
        $db->row['depID'] = "'" . $_REQUEST['depID'] . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->insert();
        $db->close();

        $db = new db();
        $sql = "UPDATE map_dep SET 
                `depID` = '" . $depID ."' 
                WHERE eid = '" . $edit_edis_id . "'";
        $db->query($sql);

        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['soAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['soAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "'  
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'SO'";
        $db->query($sql);

        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['rAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "'  
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'R'";   
        $db->query($sql);

        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];

        $sql = "UPDATE map_orgs SET 
                `oid` = '" . $_REQUEST['sAct'] ."',
                `title` = '" . $title . "',
                `sTitle` = '" . $sTitle . "'  
                WHERE eid = '" . $edit_edis_id . "'
                  AND act = 'S'";
        $db->query($sql);

        $sql = "DELETE FROM map_orgs WHERE eid = '".$edit_edis_id."' AND act in ('N','C')";
        $db->query($sql);

        foreach ($_REQUEST['nAct'] as $v) {

            $db = new db();
            $sql2 = "select title, sTitle from organization where id = '".$v."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['title'];
            $sTitle = $r2['sTitle'];
            $db->close();

            $db             = new db('map_orgs');
            $db->row['eid'] = $edit_edis_id;
            $db->row['oid'] = "'" . $v . "'";
            $db->row['act'] = "'" . 'N' . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->row['sTitle'] = "'" . $sTitle . "'";
            $db->insert();
        }

        foreach ($_REQUEST['cAct'] as $v) {

            $db = new db();
            $sql2 = "select title, sTitle from organization where id = '".$v."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['title'];
            $sTitle = $r2['sTitle'];
            $db->close();

            $db             = new db('map_orgs');
            $db->row['eid'] = $edit_edis_id;
            $db->row['oid'] = "'" . $v . "'";
            $db->row['act'] = "'" . 'C' . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->row['sTitle'] = "'" . $sTitle . "'";
            $db->insert();
        }


        //其他附件
        $oFileOldSql = "select mf.*  from map_files mf where  mf.eid = " . $edit_edis_id . " and mf.aType ='O' ";
        $rsFileOld   = $db->query($oFileOldSql);
        //delete
        while ($rFileOld = $db->fetch_array($rsFileOld)) {
            $exist = 0;
            foreach ($_REQUEST['oFileId'] as $k => $v) {

                if ($rFileOld['id'] == $v) {

                    $exist = 1;
                    break;

                }

            }
            if ($exist == 0) {
                //delete 畫面上已移除資料

                $fOthers = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                $fn      = $fOthers . $rFileOld['fName'];

                //刪除原上傳資料
                if (file_exists($fn)) {
                    @unlink($fn);
                }
                //刪除資料表 map_files
                $oFileDelSql = "delete from  map_files "
                    . "where id ='" . $rFileOld['id'] . "'";
                $db->query($oFileDelSql);

            }
        }

        foreach ($_FILES['oFile']['name'] as $k => $v) {
            //  echo '$K:' . $k . ' and ' . $_REQUEST['oFileId'][$k] . ',temp_name:' . $_FILES['oFile']['name'][$k];

            if ($_FILES['oFile']['tmp_name'][$k] != "") {
                //有上傳新資料

                if (!empty($_REQUEST['oFileId'][$k])) {

                    //有map_files.id
                    $ofID        = $_REQUEST['oFileId'][$k];
                    $oSql        = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='O' and id ='" . $ofID . "' ";
                    $rsO         = $db->query($oSql);
                    $rO          = $db->fetch_array($rsO);
                    $fOthers     = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                    $fn          = $fOthers . $rO['fName'];
                    $fileOldName = substr($rO['fName'], 0, strripos($rO['fName'], '.'));
                    if (!empty($_REQUEST['oTitle'][$k])) {

                        //名稱已填寫 更新上傳資料
                        // echo '刪除 更新上傳資料:' . $fn . ',name:' . $ofID;
                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //更新上傳資料
                        $fileNewName = $fileOldName . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                        $srcfn       = $fOthers . $fileNewName;
                        //echo '更新上傳資料路徑:' . $srcfn;
                        $rzt = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                        if ($rzt) {

                            //更新資料表 map_files
                            $oUSql = "update  map_files set "
                                . "title='" . $_REQUEST['oTitle'][$k] . "',"
                                . "fName='" . $fileNewName . "'"
                                . "where id ='" . $ofID . "'";

                            $db->query($oUSql);

                        } else {

                            echo '其他附件上傳作業失敗1！';exit;

                        }

                    } else {
                        //名稱未填寫 刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $oDSql = "delete from map_files "
                            . "where id ='" . $ofID . "'";
                        $db->query($oDSql);

                    }
                } else {
                    //無map_files.id
                    if (!empty($_REQUEST['oTitle'][$k])) {
                        //名稱已填寫 新增上傳資料
                        $ulpath = '../../data/edis/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edit_edis_id . '/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edit_edis_id . '/' . 'others/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $srcfn = '';
                        $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                        $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                        if ($rzt) {
                            //insert 其他附件
                            // echo ' ofile_name:' . $_FILES['oFile']['name'][$k];
                            // echo ' ofile_title:' . $_REQUEST['oTitle'][$k];

                            $sLocation = strripos($srcfn, '/') + 1;
                            $length    = strlen($srcfn) - strripos($srcfn, '/');
                            $fileName  = substr($srcfn, $sLocation, $length);

                            $db               = new db('map_files');
                            $db->row['eid']   = $edit_edis_id;
                            $db->row['aType'] = "'" . 'O' . "'";
                            $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                            $db->row['fName'] = "'" . $fileName . "'";
                            $db->insert();
                        } else {
                            echo '其他附件上傳作業失敗2！';exit;

                        }
                    } else {
                        //名稱未填寫 none

                    }

                }

            } else {
                //無上傳新資料

                if (!empty($_REQUEST['oFileId'][$k])) {
                //有map_files.id
                    $ofID = $_REQUEST['oFileId'][$k];

                    if (!empty($_REQUEST['oTitle'][$k])) {
                        //名稱已填寫 更新map_files 名稱
                        $oUSql = "update  map_files set "
                            . "title='" . $_REQUEST['oTitle'][$k] . "'"
                            . "where id ='" . $ofID . "'";
                        $db->query($oUSql);

                    } else {
                        //名稱未填寫 刪除上傳資料
                        $oSql    = "select * from map_files mf where mf.eid ='" . $edit_edis_id . "'and aType='O' and id ='" . $ofID . "' ";
                        $rsO     = $db->query($oSql);
                        $rO      = $db->fetch_array($rsO);
                        $fOthers = $root . $ulpath . '/data/edis/' . $edit_edis_id . '/others/';
                        $fn      = $fOthers . $rO['fName'];

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $oDSql = "delete from map_files "
                            . "where id ='" . $ofID . "'";
                        $db->query($oDSql);

                    }
                }

            }
        }

        if ($_REQUEST['odMenu'] == 'sod') {
            header("Location: /index.php?funcUrl=edis/sod/odfind.php&muID=1&isMenu=1");
        }

    }else{

        if($_REQUEST['mode'] != 'copy'){
            $signSql = "select count(1) num from map_orgs_sign where  edisid ='" . $edit_edis_id . "' and  isSign='0'";
            $rsSign  = $db->query($signSql);
            if ($rsSign) {
                $rSign = $db->fetch_array($rsSign);
            }
            if ($rSign['num'] > 0) {
                $versionSql = "select ifnull(max(version),0) num from sign_history where  edisid ='" . $edit_edis_id . "'";
                $rsVersion  = $db->query($versionSql);
                if ($rsVersion) {
                    $rVersion   = $db->fetch_array($rsVersion);
                    $nowVersion = $rVersion['num'] + 1;
                }
                if (!empty($rVersion)) {
                    //複製至失效歷程資料
                    $copySignSql = " set @row_num = 0; insert into sign_history(
                                `recno`,
                                `signid`,
                                `edisid`,
                                `version`,
                                `disableTime`,
                                `moid`,
                                `signLevel`,
                                `isOriginal`,
                                `isWait`,
                                `signMan`,
                                `signTime`,
                                `signState`,
                                `signContent`,
                                `isSign`,
                                `agent`
                            )
                            SELECT @row_num := @row_num + 1 as recno ,
                                al.`id`,
                                al.`edisid`," .
                                $nowVersion . ",
                                now(),
                                al.`moid`,
                                al.`signLevel`,
                                al.`isOriginal`,
                                al.`isWait`,
                                al.`signMan`,
                                al.`signTime`,
                                al.`signState`,
                                al.`signContent`,
                                al.`isSign`,
                                al.`agent`
                            FROM (SELECT *
                                  FROM (  SELECT mos.*
                                            FROM map_orgs_sign mos
                                           WHERE edisid = '" . $edit_edis_id . "' AND ifnull(signlevel, 0) != '".$sodLastLevel."'
                                        ORDER BY FIND_IN_SET(id, getEdisChildList(edisid)) DESC) d1
                                UNION ALL
                                SELECT *
                                  FROM (  SELECT mos.*
                            FROM map_orgs_sign mos
                           WHERE edisid = '" . $edit_edis_id . "' AND ifnull(signlevel, 0) = '".$sodLastLevel."'
                        ORDER BY FIND_IN_SET(id, getEdisChildList(edisid))) d2) al";
            
                    $db->query($copySignSql);
                }

            }
            // echo $copySignSql;exit;
            //刪除舊發文資料
            //公文主檔
            $sql = "select * from edis where id=" . $edit_edis_id;
            $rs  = $db->query($sql);
            if ($rs) {
                $r = $db->fetch_array($rs);
            }
            $otype = $r['odType'];
            $id    = $edit_edis_id;
            //刪除公文承辦部門
            $sql = "delete from map_dep where eid =$id";
            $db->query($sql);
            //刪除公文組織機關關連檔
            $sql = "delete from map_orgs where eid =$id";
            $db->query($sql);
            //刪除公文簽核關連檔
            $sql = "delete from map_orgs_sign where edisid =$id";
            $db->query($sql);
            //刪除設定機密之公文,可查看人員
            $sql = "delete from secret_members where edisid =$id";
            $db->query($sql);
            //刪除公文關連總表_領據
            // $sql = "delete from receipt where edisid =$id";
            // $db->query($sql);
            //刪除公文主檔
            // $sql = "delete from edis where id=$id";
            // $db->query($sql);
            //刪除舊發文資料 end
        }
        //新增修改後發文資料
        //insert 公文主檔
        $db        = new db('edis');
        $budgetNum = isset($_REQUEST['budgetNum']) ? '1' : '0';
        $resultNum = isset($_REQUEST['resultNum']) ? '1' : '0';
        $planNum = isset($_REQUEST['planNum']) ? '1' : '0';

        if($_REQUEST['mode'] != 'copy'){
            // $db->row['id']  = "'" . $edit_edis_id . "'";
            // $db->row['did']      = "'" . $_REQUEST['did'] . "'";
            $sql = "UPDATE edis SET 
                `sDate`    = '" . $_REQUEST['sDate'] . "',
                `rDate`    = '" . $_REQUEST['rDate'] . "',
                `dSpeed`   = '" . $_REQUEST['dSpeed'] . "',
                `deadline` = '" . $_REQUEST['deadline'] . "',
                `planNo`   = '" . $_REQUEST['planNo'] . "',
                `subjects` = '" . addslashes($_REQUEST['subjects']) . "',
                `note`     = '" . $_REQUEST['note'] . "',
                `depID`    = '" . $_REQUEST['depID'] . "',
                `creator`  = '" . $_REQUEST['creator'] . "',
                `dType`    = '" . $_REQUEST['dType'] . "',
                `sId`      = '" . $_REQUEST['sId'] . "',
                `sOid`     = '" . $_REQUEST['sOid'] . "',
                `ext`      = '" . $_REQUEST['ext'] . "',
                `dSecret`  = '" . $_REQUEST['dSecret'] . "',
                `contents` = '" . addslashes($_REQUEST['contents']) . "',
                `odType`   = '" . $_REQUEST['odType'] . "',
                `budgetNum`= '" . $budgetNum . "',
                `resultNum`= '" . $resultNum . "',
                `planNum`  = '" . $planNum . "',
                `proofreader` = '" . $_REQUEST['proofreader'] . "',
                `letterAddr`  = '" . $_REQUEST['letterAddr'] . "',
                `signStage`   = '1',
                `loginType`   = '" . $_REQUEST['loginType'] . "'
                WHERE `id` = '" . $edit_edis_id . "'";
            $db->query($sql);
            $edisId = $_REQUEST['edisId'];
        }else{        
            $did = getodNum('發文',$_REQUEST['sAct'],'',$_REQUEST['sDate']);
            $db->row['did']      = "'" . $did . "'";
            //$db->row['odDeadlineType']      = "'" . $_REQUEST['odDeadlineType'] . "'";
            $db->row['proofreader'] = "'" . $_REQUEST['proofreader'] . "'";
            $db->row['creator']     = "'" . $_REQUEST['creator'] . "'";
            $db->row['dType']       = "'" . $_REQUEST['dType'] . "'";
            $db->row['sId']         = "'" . $_REQUEST['sId'] . "'";
            $db->row['sOid']        = "'" . $_REQUEST['sOid'] . "'";
            $db->row['ext']         = "'" . $_REQUEST['ext'] . "'";
            $db->row['sDate']       = "'" . $_REQUEST['sDate'] . "'";
            $db->row['dSpeed']      = "'" . $_REQUEST['dSpeed'] . "'";
            $db->row['dSecret']     = "'" . $_REQUEST['dSecret'] . "'";
            $db->row['subjects']    = "'" . addslashes($_REQUEST['subjects']) . "'";
            $db->row['contents']    = "'" . addslashes($_REQUEST['contents']) . "'";
            $db->row['odType']      = "'" . $_REQUEST['odType'] . "'";
            $db->row['planNo']      = "'" . $_REQUEST['planNo'] . "'";
            $db->row['budgetNum']   = "'" . $budgetNum . "'";
            $db->row['resultNum']   = "'" . $resultNum . "'";
            $db->row['planNum']     = "'" . $planNum . "'"; 
            $db->row['note']        = "'" . $_REQUEST['note'] . "'";
            $db->row['depID']       = "'" . $_REQUEST['depID'] . "'";
            $db->row['letterAddr']  = "'" . $_REQUEST['letterAddr'] . "'";
            $db->row['loginType']   = "'" . $_REQUEST['loginType'] . "'";
            $db->row['signStage']   = "'1'";
            $db->insert();
            $edisId = $db->lastInsertId;
        }

        //insert 承辦部門
        $op = array(
            'dbSource'   => DB_ADMSOURCE,
            'dbAccount'  => DB_ACCOUNT,
            'dbPassword' => DB_PASSWORD,
            'tableName'  => '',
        );
        $db = new db($op);
        $sql2 = "select depTitle from department where depID = '".$_REQUEST['depID']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['depTitle'];
        $db->close();

        $db               = new db('map_dep');
        $db->row['eid']   = $edisId;
        $db->row['depID'] = "'" . $_REQUEST['depID'] . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->insert();
        $db->close();

        $db = new db();
        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['soAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];
        $db->close();

        //insert 發文機關
        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['soAct'] . "'";
        $db->row['act'] = "'" . 'SO' . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->row['sTitle'] = "'" . $sTitle . "'";
        $db->insert();
        $soActId = $db->lastInsertId;

        $db = new db();
        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['sAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];
        $db->close();
        //insert 文號選取
        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['sAct'] . "'";
        $db->row['act'] = "'" . 'S' . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->row['sTitle'] = "'" . $sTitle . "'";
        $db->insert();
        $sActId = $db->lastInsertId;

        $db = new db();
        $sql2 = "select title, sTitle from organization where id = '".$_REQUEST['rAct']."'";
        $rs2  = $db->query($sql2);
        $r2   = $db->fetch_array($rs2);
        $title = $r2['title'];
        $sTitle = $r2['sTitle'];
        $db->close();

        //insert 受文者
        $db             = new db('map_orgs');
        $db->row['eid'] = $edisId;
        $db->row['oid'] = "'" . $_REQUEST['rAct'] . "'";
        $db->row['act'] = "'" . 'R' . "'";
        $db->row['title'] = "'" . $title . "'";
        $db->row['sTitle'] = "'" . $sTitle . "'";
        $db->insert();
        $rActId = $db->lastInsertId;

        //insert 校對者
        $db                    = new db('map_orgs_sign');
        $db->row['edisid']     = $edisId;
        $db->row['isOriginal'] = "'" . '1' . "'";
        $db->row['isWait']     = "'" . '1' . "'";
        $db->row['signLevel']  = "'" . '1' . "'";
        $db->row['signMan']    = "'" . $_REQUEST['proofreader'] . "'";
        $db->insert();
        $pfId = $db->lastInsertId;

        $sidmgrAry = array();
        //insert承辦人主管(單或多個)
        $pcount = -1;
        foreach ($_REQUEST['sidMgr'] as $v) {
            $pcount++;
            $db                    = new db('map_orgs_sign');
            $db->row['edisid']     = $edisId;
            $db->row['isOriginal'] = "'" . '1' . "'";
            $db->row['isWait']     = "'" . '1' . "'";
            $db->row['signLevel']  = "'" . '2' . "'";
            $db->row['signMan']    = "'" . trim($v) . "'";
            $db->insert();
            $sidMgrId           = $db->lastInsertId;
            $sidmgrAry[$pcount] = $sidMgrId;
        }

        //insert 正本
        foreach ($_REQUEST['nAct'] as $v) {

            $db = new db();
            $sql2 = "select title, sTitle from organization where id = '".$v."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['title'];
            $sTitle = $r2['sTitle'];
            $db->close();

            $db             = new db('map_orgs');
            $db->row['eid'] = $edisId;
            $db->row['oid'] = "'" . $v . "'";
            $db->row['act'] = "'" . 'N' . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->row['sTitle'] = "'" . $sTitle . "'";
            $db->insert();
            $nActId = $db->lastInsertId;
        }
        //insert 副本
        foreach ($_REQUEST['cAct'] as $v) {

            $db = new db();
            $sql2 = "select title, sTitle from organization where id = '".$v."'";
            $rs2  = $db->query($sql2);
            $r2   = $db->fetch_array($rs2);
            $title = $r2['title'];
            $sTitle = $r2['sTitle'];
            $db->close();

            $db             = new db('map_orgs');
            $db->row['eid'] = $edisId;
            $db->row['oid'] = "'" . $v . "'";
            $db->row['act'] = "'" . 'C' . "'";
            $db->row['title'] = "'" . $title . "'";
            $db->row['sTitle'] = "'" . $sTitle . "'";
            $db->insert();
            $cActId = $db->lastInsertId;
        }

        //insert歸檔
        //取得發文者單位窗口
        $rRevSql = "select *  from  organization o where o.id=" . "'" . $_REQUEST['sAct'] . "' and orgType='SELF'";
        $rsRrev  = $db->query($rRevSql);
        if ($rsRrev) {
            $rRrev = $db->fetch_array($rsRrev);
        }
        if (!empty($rRrev['mainRevMan'])) {
            //insert發文者單位窗口
            $db                = new db('map_orgs_sign');
            $db->row['edisid'] = $edisId;
            //$db->row['moid']       = $rActId;
            $db->row['isOriginal'] = "'" . '1' . "'";
            $db->row['isWait']     = "'" . '1' . "'";
            $db->row['signLevel']  = "'" . '4' . "'";
            $db->row['signMan']    = "'" . $rRrev['mainRevMan'] . "'";
            $db->insert();
            $revManId = $db->lastInsertId;
        }
        //新增修改後發文資料 end;

        if($_REQUEST['mode'] == 'copy'){
            $ulpath1 = '../../data/edis/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $ulpath1 = '../../data/edis/' . $edisId . '/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $ulpath1 = '../../data/edis/' . $edisId . '/main/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }
            //其他附件
            $mSql = "select * from map_files where eid ='" . $_REQUEST['sourceID'] . "' and aType ='O'";
            $rsM  = $db->query($mSql);

            $ulpath1 = '../../data/edis/' . $edisId . '/others/';
            if (!file_exists($ulpath1)) {
                mkdir($ulpath1);
            }

            $rM   = $db->fetch_all($rsM);
            foreach ($rM as $key => $value) {
                if(empty($_FILES['oFile']['tmp_name'][$key]) && $_REQUEST['oFileId'][$key] == $rM[$key]['id'] && $_REQUEST['delFile'] != $rM[$key]['id']){
                    $fMain = $root . $ulpath . '/data/edis/' . $value['eid'] . '/others/';
                    $fn    = $fMain . $value['fName'];

                    $fCopy = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                    $extension = explode('.', $value['fName']);
                    $CopyFileName = time() . "_" . $key . '.' . $extension[1];
                    $fnCopy = $fCopy . $CopyFileName;

                    if (file_exists($fn)) {
                        copy($fn, $fnCopy);
                    }

                    $db               = new db('map_files');
                    $db->row['eid']   = $edisId;
                    $db->row['aType'] = "'" . 'O' . "'";
                    $db->row['title'] = "'" . $_REQUEST['oTitle'][$key] . "'";
                    $db->row['fName'] = "'" . $CopyFileName . "'";
                    $db->insert();
                }
            }

            foreach ($_FILES['oFile']['name'] as $k => $v) {
                if ($_FILES['oFile']['tmp_name'][$k] != "") {
                    $srcfn = $ulpath1 . time() . "_" . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                    $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                    if($rzt){
                        $sLocation = strripos($srcfn, '/') + 1;
                        $length    = strlen($srcfn) - strripos($srcfn, '/');
                        $fileName  = substr($srcfn, $sLocation, $length);

                        $db               = new db('map_files');
                        $db->row['eid']   = $edisId;
                        $db->row['aType'] = "'" . 'O' . "'";
                        $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                        $db->row['fName'] = "'" . $fileName . "'";
                        $db->insert();
                    }
                }
            }
        }else{

            //主文附件
            if ($_FILES['mFile']['tmp_name'] != "") {
                //有map_file.id
                if (!empty($_REQUEST['mFileId'])) {
                    $mSql = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='M'";
                    $rsM  = $db->query($mSql);
                    $rM   = $db->fetch_array($rsM);

                    $fileOldName = substr($rM['fName'], 0, strripos($rM['fName'], '.'));

                    $fMain = $root . $ulpath . '/data/edis/' . $edisId . '/main/';
                    $fn    = $fMain . $rM['fName'];
                    //echo $fileOldName;
                    //名稱已填寫
                    if (!empty($_REQUEST['mTitle'])) {
                        //update 主文附件

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }
                        //更新上傳資料
                        $fileNewName = $fileOldName . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                        $srcfn       = $fMain . $fileNewName;
                        $rzt         = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);
                        if ($rzt) {
                            //更新資料表 map_files
                            $mUSql = "update  map_files set "
                                . "title='" . $_REQUEST['mTitle'] . "',"
                                . "fName='" . $fileNewName . "'"
                                . "where id ='" . $_REQUEST['mFileId'] . "'";

                            $db->query($mUSql);
                        } else {

                            echo '主文上傳作業失敗！';exit;

                        }

                    } else {
                        // 名稱未填寫 delete 主文附件

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $mDSql = "delete from map_files "
                            . "where id ='" . $_REQUEST['mFileId'] . "'";
                        $db->query($mDSql);
                    }

                } else {
                    //無map_file.id

                    if (!empty($_REQUEST['mTitle'])) {
                        //名稱已填寫insert 主文附件

                        $ulpath = '../../data/edis/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edisId . '/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $ulpath = '../../data/edis/' . $edisId . '/' . 'main/';
                        if (!file_exists($ulpath)) {
                            mkdir($ulpath);
                        }

                        $srcfn = '';
                        $srcfn = $ulpath . time() . '.' . pathinfo($_FILES['mFile']['name'], PATHINFO_EXTENSION);
                        $rzt   = move_uploaded_file($_FILES['mFile']['tmp_name'], $srcfn);

                        if ($rzt) {
                            $sLocation = strripos($srcfn, '/') + 1;
                            $length    = strlen($srcfn) - strripos($srcfn, '/');
                            $fileName  = substr($srcfn, $sLocation, $length);

                            //insert 主文附件
                            $db               = new db('map_files');
                            $db->row['eid']   = $edisId;
                            $db->row['aType'] = "'" . 'M' . "'";
                            $db->row['title'] = "'" . $_REQUEST['mTitle'] . "'";
                            $db->row['fName'] = "'" . $fileName . "'";
                            $db->insert();

                        } else {

                            echo '主文上傳作業失敗！';exit;

                        }

                    } else {
                        //名稱未填寫

                        echo '名稱未填寫,主文上傳作業失敗！';exit;

                    }

                }

            } else {
                //無上傳新檔案
                if (!empty($_REQUEST['mFileId'])) {
                    if (!empty($_REQUEST['mTitle'])) {
                        //名稱非空值則更新名稱
                        $mUSql = "update  map_files set "
                            . "title='" . $_REQUEST['mTitle'] . "'"
                            . "where id ='" . $_REQUEST['mFileId'] . "'";

                        $db->query($mUSql);

                    } else {
                        //名稱為空值則刪除上傳資料
                        $mSql  = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='M'";
                        $rsM   = $db->query($mSql);
                        $rM    = $db->fetch_array($rsM);
                        $fMain = $root . $ulpath . '/data/edis/' . $edisId . '/main/';
                        $fn    = $fMain . $rM['fName'];

                        //刪除原上傳資料
                        if (file_exists($fn)) {
                            @unlink($fn);
                        }

                        //刪除資料表 map_files
                        $mDSql = "delete from map_files "
                            . "where id ='" . $_REQUEST['mFileId'] . "'";
                        $db->query($mDSql);

                    }
                }

            }

            //其他附件
            $oFileOldSql = "select mf.*  from map_files mf where  mf.eid = " . $edisId . " and mf.aType ='O' ";
            $rsFileOld   = $db->query($oFileOldSql);
            //delete
            while ($rFileOld = $db->fetch_array($rsFileOld)) {
                $exist = 0;
                foreach ($_REQUEST['oFileId'] as $k => $v) {

                    if ($rFileOld['id'] == $v) {

                        $exist = 1;
                        break;

                    }

                }
                if ($exist == 0) {
                    //delete 畫面上已移除資料

                    $fOthers = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                    $fn      = $fOthers . $rFileOld['fName'];

                    //刪除原上傳資料
                    if (file_exists($fn)) {
                        @unlink($fn);
                    }
                    //刪除資料表 map_files
                    $oFileDelSql = "delete from  map_files "
                        . "where id ='" . $rFileOld['id'] . "'";
                    $db->query($oFileDelSql);

                }
            }

            foreach ($_FILES['oFile']['name'] as $k => $v) {
                //  echo '$K:' . $k . ' and ' . $_REQUEST['oFileId'][$k] . ',temp_name:' . $_FILES['oFile']['name'][$k];

                if ($_FILES['oFile']['tmp_name'][$k] != "") {
                    //有上傳新資料

                    if (!empty($_REQUEST['oFileId'][$k])) {

                        //有map_files.id
                        $ofID        = $_REQUEST['oFileId'][$k];
                        $oSql        = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='O' and id ='" . $ofID . "' ";
                        $rsO         = $db->query($oSql);
                        $rO          = $db->fetch_array($rsO);
                        $fOthers     = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                        $fn          = $fOthers . $rO['fName'];
                        $fileOldName = substr($rO['fName'], 0, strripos($rO['fName'], '.'));
                        if (!empty($_REQUEST['oTitle'][$k])) {

                            //名稱已填寫 更新上傳資料
                            // echo '刪除 更新上傳資料:' . $fn . ',name:' . $ofID;
                            //刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }

                            //更新上傳資料
                            $fileNewName = $fileOldName . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                            $srcfn       = $fOthers . $fileNewName;
                            //echo '更新上傳資料路徑:' . $srcfn;
                            $rzt = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                            if ($rzt) {

                                //更新資料表 map_files
                                $oUSql = "update  map_files set "
                                    . "title='" . $_REQUEST['oTitle'][$k] . "',"
                                    . "fName='" . $fileNewName . "'"
                                    . "where id ='" . $ofID . "'";

                                $db->query($oUSql);

                            } else {

                                echo '其他附件上傳作業失敗1！';exit;

                            }

                        } else {
                            //名稱未填寫 刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }

                            //刪除資料表 map_files
                            $oDSql = "delete from map_files "
                                . "where id ='" . $ofID . "'";
                            $db->query($oDSql);

                        }
                    } else {
                        //無map_files.id
                        if (!empty($_REQUEST['oTitle'][$k])) {
                            //名稱已填寫 新增上傳資料
                            $ulpath = '../../data/edis/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $ulpath = '../../data/edis/' . $edisId . '/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $ulpath = '../../data/edis/' . $edisId . '/' . 'others/';
                            if (!file_exists($ulpath)) {
                                mkdir($ulpath);
                            }

                            $srcfn = '';
                            $srcfn = $ulpath . time() . '_' . $k . '.' . pathinfo($_FILES['oFile']['name'][$k], PATHINFO_EXTENSION);
                            $rzt   = move_uploaded_file($_FILES['oFile']['tmp_name'][$k], $srcfn);
                            if ($rzt) {
                                $sLocation = strripos($srcfn, '/') + 1;
                                $length    = strlen($srcfn) - strripos($srcfn, '/');
                                $fileName  = substr($srcfn, $sLocation, $length);

                                $db               = new db('map_files');
                                $db->row['eid']   = $edisId;
                                $db->row['aType'] = "'" . 'O' . "'";
                                $db->row['title'] = "'" . $_REQUEST['oTitle'][$k] . "'";
                                $db->row['fName'] = "'" . $fileName . "'";
                                $db->insert();
                            } else {
                                echo '其他附件上傳作業失敗2！';exit;

                            }
                        }
                    }
                } else {
                    //無上傳新資料

                    if (!empty($_REQUEST['oFileId'][$k])) {
                        //有map_files.id
                        $ofID = $_REQUEST['oFileId'][$k];

                        if (!empty($_REQUEST['oTitle'][$k])) {
                            //名稱已填寫 更新map_files 名稱
                            $oUSql = "update  map_files set "
                                . "title='" . $_REQUEST['oTitle'][$k] . "'"
                                . "where id ='" . $ofID . "'";
                            $db->query($oUSql);

                        } else {
                            //名稱未填寫 刪除上傳資料
                            $oSql    = "select * from map_files mf where mf.eid ='" . $edisId . "'and aType='O' and id ='" . $ofID . "' ";
                            $rsO     = $db->query($oSql);
                            $rO      = $db->fetch_array($rsO);
                            $fOthers = $root . $ulpath . '/data/edis/' . $edisId . '/others/';
                            $fn      = $fOthers . $rO['fName'];

                            //刪除原上傳資料
                            if (file_exists($fn)) {
                                @unlink($fn);
                            }

                            //刪除資料表 map_files
                            $oDSql = "delete from map_files "
                                . "where id ='" . $ofID . "'";
                            $db->query($oDSql);

                        }
                    }

                }
            }
        }
        //總表(領據/成果冊/執行計畫報告)新增/修改/刪除
        //領據
        echo insertReceipt($edit_edis_id,$_REQUEST,$_FILES['attach']);

        if($_REQUEST['mode'] == 'copy') header("Location: /index.php?funcUrl=edis/sod/myod.php&muID=1&did=$did");
        else header("Location: /index.php?funcUrl=edis/sod/myod.php&muID=1");
    }
} else {
    header("Location: /index.php?funcUrl=edis/sod/myod.php&muID=1");
}
