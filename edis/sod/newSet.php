<?php 
if($_GET['did']){
    echo '<script>alert("取得發文字號:'.$_GET['did'].'");</script>';
}
@session_start();
if ($odMenu == 'rod') {
    $odTypeName = '收文';
    $noteDefault ='';
} else if ($odMenu == 'sod') {
    $odTypeName = '發文';
    $noteDefault = '提醒您~
1.有附件請一定要上傳，若無法上傳，請在此欄說明；同時，請於發文後，另備一份副本（封面請註明「發文文號」）轉回各機構總文書存檔。
2.核銷冊僅需副本一份，請直接轉財務室稽核存檔。
以上，敬請合作！謝謝！';
}

include_once "$root/inc_vars.php";
include_once "$root/system/db.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/edis/func.php";
include_once "$root/group/groupDiv.php";
$empID = $_SESSION['empID'];
$depID = $_SESSION['depID'];

//預設校對(承辦人單位)
$dProofreader = getrevManID($depID);

?>
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<link href="/Scripts/form.css" rel="stylesheet" type="text/css" />
<link href="/css/FormUnset.css" rel="stylesheet" type="text/css" />
<link href="group/group.css" rel="stylesheet" type="text/css" />
<link href="edis/report.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    textarea::-webkit-input-placeholder{ color: red; }
</style>
<script type="text/javascript" src="/Scripts/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script src="/Scripts/ui.datepicker.js"></script>
<script src="/Scripts/form.js" type="text/javascript"></script>
<script src="/Scripts/validation.js" type="text/javascript"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="edis/report.js"></script>
<script src="group/group.js"></script>
<script>
function returnValues(data){
    console.log(data);
    // var group_id = $("input[name='dt_group_id[]']").first().val();
    $.each( data, function( k, v ) {
        if(v['type']=='orgID'){
            if(k != 0) addItem('',data['act'],data['act']);
            $("input[name='"+data['act']+"[]']").last().val(v['id']);
            $("input[name='"+data['act']+"[]']").last().attr('title',v['title']);
            $("input[name='"+data['act']+"[]']").last().after(' '+v['title']);
            // $("input[name='dt_group_id[]']").last().val(group_id);
            
        }
    });
}
function addItem(tbtn,fld,qid) {
if(fld !=='oTitle'){
    if(fld =='nAct' || fld =='cAct'){
    var newone = "<div><input name='"+fld+"[]' id='orgIDs' type='text' size='6' class='queryID' size='6' onkeypress=\"return checkInput(event,'org',this)\" data-act='nAct'>"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
    + " <span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }else{
    var newone = "<div><input name='"+fld+"[]' id='"+qid+"' type='text' size='6' class='queryID' size='6' onkeypress=\"return checkInput(event,'empName',this)\">"
    + "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/><span class='formTxS'></span>"
    + " <span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
    }
}else if(fld =='oTitle'){
   var newone = "<div><input name='"+fld+"[]' id='"+qid+"'  placeholder='請輸入名稱' type='text'>"
   + " <input type='file' name='oFile[]' id='oFile' accept='image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text'>"
   + "<span class='sBtn' id='"+qid+"Remove' onClick='removeItem(this)'> -移除</span></div>";
}
 var divTitle ='#'+qid+'Div';
  $(divTitle).append(newone);
}

function removeItem(tbtn) {
      $(tbtn).parent().remove();
   }


 $(function(){
    $('#sDate').datepicker();
    $('#deadline').datepicker();
    $('#applyDate').datepicker();
});




function checkOnly(act){//檢核檔號及發文字號是否唯一

    var checkNull;
    var checkValue;
    var InputFiled;
    var errMsg;
    if(act =='did'){
        if($("[name='did']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='did']").val();
        InputFiled = document.querySelector('#did');
        errMsg =<?php  echo $odTypeName?>+'字號已存在';
    };
}else if(act =='dNo'){
    if($("[name='dNo']").val()==""){return}else{checkNull ='true'; checkValue =$("[name='dNo']").val();
    InputFiled = document.querySelector('#dNo');
    errMsg ='來文字號已存在';
};
}
if(checkNull =='true'){
    $.ajax({
        url: 'edis/api.php?act='+act+'&inValue='+checkValue,
        type:"GET",
        dataType:'text',
        success: function(response){
            if (response == 'valid') {
                InputFiled.setCustomValidity('');
                console.log('valid');
            }else if (response == 'invalid') {
                InputFiled.setCustomValidity(errMsg);
                console.log('invalid');
            }
        }

    });
}
}


function getodNum(){//取得文件字號
    $("#Submit").attr('hidden','true');
    var act='odNum';
    var checkNull = 'true';
    var orgid;
    var InputFiled;
    var errMsg;
    var odType =$("[name='odType']").val();
    var result =false;
    var errMsg ='';

    if($("[name='depID']").val()==""){
        $("[name='depID']").focus();
        checkNull ='false'; 
        errMsg ='承辦部門未填入。'; 
    }
    if($("[name='soAct']").val()==""){
        $("[name='soAct']").focus();
        checkNull ='false'; 
        errMsg ='發文機關未填入。'; 
    }
    if($("[name='sAct']").val()==""){
        $("[name='sAct']").focus();
        checkNull ='false'; 
        errMsg ='文號選取未填入。'; 
    }
    if($("[name='dSpeed']").val()==""){
        $("[name='dSpeed']").focus();
        checkNull ='false'; 
        errMsg ='速別未填入。'; 
    }
    if($("[name='dType']").val()==""){
        $("[name='dType']").focus();
        checkNull ='false'; 
        errMsg ='公文類型未填入。'; 
    }
    if($("[name='rAct']").val()==""){
        $("[name='rAct']").focus();
        checkNull ='false'; 
        errMsg ='受文者未填入。'; 
    }
    if($("[name='sId']").val()==""){
        $("[name='sId']").focus();
        checkNull ='false'; 
        errMsg ='承辦人未填入。'; 
    }
    if($("[name='sDate']").val()==""){
        $("[name='sDate']").focus();
        checkNull ='false'; 
        errMsg ='發文日期未填入。'; 
    }
    if($("[name='sidMgr']").val()==""){
        $("[name='sidMgr']").focus();
        checkNull ='false'; 
        errMsg ='承辦主管未填入。'; 
    }
    if($("[name='proofreader']").val()==""){
        $("[name='proofreader']").focus();
        checkNull ='false'; 
        errMsg ='校對未填入。'; 
    }
    if($("[name='subjects']").val()==""){
        $("[name='subjects']").focus();
        checkNull ='false'; 
        errMsg ='主旨未填入。'; 
    }

    $.each($('[name="oFile[]"]'), function(k,v){
        if(this.files.length>0){
            if($('[name="oTitle[]"]').get(k).value == ''){
                errMsg ='附件名稱未填入。'; 
                $("#Submit").removeAttr('hidden');
                checkNull ='false'; 
            }
        }
    });

    if($("[name='resultNum']").attr('checked') || $("[name='planNum']").attr('checked')){
        $.each($('[name="oFile[]"]'), function(k,v){
            if(this.files.length <= 0){
                checkNull ='false'; 
                errMsg ='成果冊或計畫書請至少上傳一份。'; 
            }
        });
    }

    if(errMsg != '') alert(errMsg);

    if(checkNull =='true'){
        result = true;
    }

    if(checkNull =='false') $("#Submit").removeAttr('hidden');
    return result;
}



//選擇上傳檔案(多個)需檢核有無輸入名稱
function checkMutiAtt(f,t){
    //f:input type=file id
    //t:title id
    var fileName =f+'[]';
    var titleName =t+'[]';
    var titleId ='#'+t;
    var fl=document.getElementsByName(fileName);

    for(var i=0;i<fl.length;i++){
        var fv = fl[i].value;
        var otValue= document.getElementsByName(titleName)[i].value;
        if(fv.length>0){
            if(otValue.length==0){
                document.querySelectorAll(titleId)[i].setCustomValidity('請填入附件名稱');                
            }else{
                document.querySelectorAll(titleId)[i].setCustomValidity('');
            }
        }else{
            document.querySelectorAll(titleId)[i].setCustomValidity('');
       }
    }
}
//選擇上傳檔案(單一)需檢核有無輸入名稱
function checkAloneAtt(f,t){
    //f:input type=file id
    //t:title id
    var titleName =t;
    var titleId ='#'+t;
    var fv =document.getElementById(f).value;
    var otValue= document.getElementById(titleName).value;
    if(fv.length>0){
        if(otValue.length==0){
            document.querySelector(titleId).setCustomValidity('請填入附件名稱');            
        }else{
            document.querySelector(titleId).setCustomValidity('');
        }
    }else{
        document.querySelector(titleId).setCustomValidity('');
    }
}

//文號選取變更,副本第一欄需連動
function setcAct(id,title){
 var params = $('input[name="cAct[]"]');
 $(params[0]).val(id);
 $(params[0]).next().next('span').html(title); 

}


//受文單位變更,正本第一欄需連動
function setnAct(id,title){
    var params = $('input[name="nAct[]"]');
    $(params[0]).val(id);
    $(params[0]).next().next('span').html(title); 
}

$("#oFile").live('change', function(){

    checkMutiAtt('oFile','oTitle');

    })

$("#oTitle").live('input', function(){

    checkMutiAtt('oFile','oTitle');

    })

$("#mFile").live('change', function(){

    checkAloneAtt('mFile','mTitle');

    })

$("#mTitle").live('input', function(){

    checkAloneAtt('mFile','mTitle');

    })

function setRroofreader(id,title){
    var returnValue = '';
    $.ajax({
        url: 'edis/api.php?act=getrevMan'+'&inValue='+id+'&type=emp',
        type:"GET",
        dataType:"json",
        async: false,
        success: function(response){
            if(response !==''){
                returnValue = response;
                $.each(returnValue, function(key, value) {
                     var proof = $('input[name="proofreader"]');
                     $(proof[0]).val(value['id']);
                     $(proof[0]).next().next('span').html(value['name']);
                });

            result=true;
            }else{
            result=false;    
            alert('error');
            }

        }
    }) 
}
</script>
<style>
.require{
   color:red;
}
</style>
<div class="row-fluid">
  <div class="span12">
      <table width="100%" cellpadding="0" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana; padding:5px">
        <tr><th bgcolor="#e0e0e0">建立<?php  echo $odTypeName?></th></tr>
    <!--
    <tr><td bgcolor="#f8f8f8"><div style="overflow:auto; height:100px; width:100%;"><?php  echo nl2br(file_get_contents('data/absence.txt'))?></div></td></tr>
-->
</table>
</div>
</div>
<?php  echo genGroupDom('sidMgr');  ?>
<?php  echo genGroupDom('nAct');  ?>
<?php  echo genGroupDom('cAct');  ?>
<div style="height:5px"></div><div style="border-top:1px #999999 dashed; height=1px;"></div><div style="height:5px"></div>

<form id="odform"  method="post" action="<?php  echo "edis/sod/newDo.php?odMenu=".$odMenu?>"  enctype="multipart/form-data">

  <!--flag 控制是否insert 總表:領據/成果冊/執行計畫報告 -->
<input name="receiptFlag" type="hidden" id="receiptFlag" value="F"/>
<input name="" type="hidden" id="" value="F"/>
<input name="" type="hidden" id="" value="F"/>

    <table width="100%" border="0" cellpadding="4" cellspacing="0" style="font:normal 13px '微軟正黑體',Verdana">

        <tr>
            <td align="right"><span class="require">*</span>發文機關：</td>
            <td align="left"><input name="soAct" type="text" class="queryID" id="soAct" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true" readonly onkeypress="return checkInput(event,'depName',this)" />
                <!-- <input name="depID" type="hidden" id="depID" value="<?php  echo $depID?>"/>                 -->
            <span></span>文號選取：
            <input name="sAct" type="text" class="queryID" id="sAct" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true" readonly onkeypress="return checkInput(event,'depName',this)" />
                <!-- <input name="depID" type="hidden" id="depID" value="<?php  echo $depID?>"/> -->
            </td>              
            <td align="right"><?php  echo $odTypeName?>字號：</td>
            <td>
            <input type="text" name="did" id="did"  placeholder='系統取號' value='' readonly="true" />
            <input style="display:none" name="odType" type="text" id="odType" required ="true" size="6" value="<?php  echo $odTypeName?>"/>
            </td>
            <?php if ($mobile) {
    echo '</tr><tr>';
}
?>   
    </tr>
        <tr>
        <td align="right"><span class="require">*</span>速別：</td>
        <td><select name="dSpeed" id="dSpeed" required ="true"/>
            <?php foreach ($odSpeed as $v) {
            if ($_REQUEST['dSpeed'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                echo "<option>$v</option>";
            }
            }?>
                    </select>
                </td>
                <?php if ($mobile) {
                echo '</tr><tr>';
            }
            ?>
    <td align="right">密等：</td>
    <td align="left"><select name="dSecret" id="dSecret" />
        <?php 
    $count = 0;
    foreach ($odSecret as $v) {
        $count++;
        if ($_REQUEST['dSecret'] == $v) {
            echo "<option selected='selected'>$v</option>";
        } else {
            if ($count == 1) {
                echo "<option></option>";

            }
            echo "<option>$v</option>";
        }
     }?>
        </select>
    </td>
    </tr>
    
    <tr>       
        <td align="right">建檔者：</td>
        <td><input  type="text" name="creatorName" id="creatorName" readonly="true" value="<?php  echo $emplyeeinfo[$empID]?>"/></td><input name="creator" type="hidden" id="creator" value="<?php  echo $empID?>"/>
        <td align="right"><span class="require">*</span>公文類型：</td>
        <td align="left"><select name="dType" id="dType" required ="true" />
            <?php foreach ($dType as $v) {
            if ($_REQUEST['dType'] == $v) {
                echo "<option selected='selected'>$v</option>";
            } else {
                echo "<option>$v</option>";
            }
            }?>
        </select>
        </td>         
        <?php if ($mobile) {
    echo '</tr><tr>';
}
?>        
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>受文者：</td>
        <td align="left"><input name="rAct" type="text" class="queryID" id="rAct" data-odtype="<?php echo  $odMenu;?>" size="6" required ="true" readonly onkeypress="return checkInput(event,'depName',this)"/>
        </td>         
        <?php if ($mobile) {
    echo '</tr><tr>';
}
?>
        <td align="right"><span class="require">*</span>承辦人：</td>
        <td align="left"><input name="sId" type="text" class="queryID" id="sId" required ="true" size="6" value="<?php  echo $empID?>" title="<?php  echo $emplyeeinfo[$empID]?>" data-odtype="<?php  echo $odMenu?>" onkeypress="return checkInput(event,'empName',this)" /><input name="sOid" type="hidden" id="sOid" value="<?php  echo $emplyeeDept[$empID]?>"/>
         分機：<input name="ext" type="text" id="ext"  size="6" value="" />
        </td>
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>發文日期：</td>
        <td><input type="text" name="sDate" id="sDate" pattern="^(1[9][0-9][0-9]|2[0][0-9][0-9])[- / .]([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])$" required ="true" /></td>
        <td align="right"><span class="require">*</span>承辦主管：</td>
        <td>
            <input type="button" name="groupEnter" id="groupEnter" onclick="setGroup('group_sidMgr');" value="群組">
            <input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="<?php  echo $departmentlead[$depID]?>" title="<?php  echo $emplyeeinfo[$departmentlead[$depID]]?>" onkeypress="return checkInput(event,'empName',this)" />
            <span class="sBtn" id="sidMgrAdd" onclick="addItem(this,'sidMgr','sidMgr')"> +新增承辦主管</span>
            <div id="sidMgrDiv">
            </div>
        </td>
    </tr>
<tr>
          <!--<td align="right">類別：</td>
            <td align="left"><select name="odDeadlineType" id="odDeadlineType" required ="true" />
                <?php /* foreach ($odDeadlineType as $v) {
    if ($_REQUEST['odDeadlineType'] == $v) {
        echo "<option selected='selected'>$v</option>";
    } else {
        echo "<option>$v</option>";
    }
} */ ?>
            </select>
        </td> -->
        <td align="right"><span class="require">*</span>校對：</td>
        <td align="left"><input name="proofreader" type="text" class="queryID" id="proofreader" required ="true" size="6" value="<?php  echo $dProofreader; ?>" title="<?php  echo $emplyeeinfo[$dProofreader]?>" onkeypress="return checkInput(event,'empName',this)"/>
        <td align="right">計畫編號：</td>
        <td align="left"><input name="planNo" type="text" id="planNo" /></td>                      
    </tr>
    <tr>
        <td align="right"><span class="require">*</span>承辦部門：</td>
        <td align="left">
            <input name="depID" class="queryID" required="true"  id="admDep" readonly type="text" size="6" onkeypress="return checkInput(event,'depID',this)"/>
        </td>
        <td align="right">發文機構地址：</td>
        <td>
           <input name="letterAddr" type="text" class="queryID" id="depInnerID" readonly size="6" onkeypress="return checkInput(event,'org',this)"/>    
        </td>        
    </tr>
    <tr>
        <td align="right">附件類型：</td>
        <td>
            <input name="budgetNum" type="checkbox" id="budgetNum"  size="6" />核銷冊
            <input name="resultNum" type="checkbox" id="resultNum"  size="6" />成果冊
            <input name="planNum" type="checkbox" id="planNum"  size="6" />計畫書
        </td>
    </tr>
    <tr>
        <td align="right">附件：</td>
        <td align="left" colspan="3">

            <button type="button" data-toggle="collapse" data-target="#demo">新增</button>
            <div id="demo" class="collapse">
                <input name="oTitle[]" id="oTitle" type="text"  placeholder='請輸入名稱' />
                <input type="file" name="oFile[]" id="oFile" accept="image/jpeg, image/png, application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf,application/vnd.oasis.opendocument.text"><span class="sBtn" id="oTitleAdd" onclick="addItem(this,'oTitle','oTitle')"> +新增其他附件</span>
                <div id="oTitleDiv">
                </div>
            </div>
        </td>        
    </tr>
    <tr>
        <td align="right">備註：</td>
        <td colspan="3"><textarea name="note" id="note" rows="4" style="width:75%" placeholder="<?php  echo $noteDefault?>"></textarea></td>
    </tr>    
    <tr>
        <td align="right"><span class="require">*</span>主旨：</td>
        <td colspan="3"><textarea name="subjects" id="subjects" rows="3" style="width:75%" required ="true"></textarea></td>
    </tr>
    <tr>
        <td align="right">說明：</td>
        <td colspan="3"><textarea name="contents" id="contents" rows="3" style="width:90%" ></textarea>
            <script>
              CKEDITOR.replace( 'contents', {
          // width: 1000,//設定內容編輯器寬度
      });

    </script>
    </td>
    </tr>
    <!--<tr>
        <td td colspan="4" align="center">
            <input type="button" name="receiptEnter" id="receiptEnter" onclick="setReport('receipt');" value="領據" />
            <input type="button" name="" id="" value="成果冊" />
            <input type="button" name="" id="" value="執行計畫報告" />
        </td>
    </tr>-->
    <tr>
        
        <td align="right">正本：</td>
        <td>
            <input type="button" name="groupnAct" onclick="setGroup('group_nAct');" value="群組">
            <input name="nAct[]" type="text" class="queryID" id="orgIDs" size="6"  onkeypress="return checkInput(event,'org',this)" data-act="nAct"/><span class="sBtn" id="nActAdd" onclick="addItem(this,'nAct','nAct')"> +新增</span>
            <div id="nActDiv">
            </div>
        </td>
        <?php   if ($mobile) {
                echo '</tr><tr>';
            }
        ?>
        <td align="right">副本：</td>
        <td>
            <input type="button" name="groupcAct" onclick="setGroup('group_cAct');" value="群組">
            <input name="cAct[]" type="text" class="queryID" id="orgIDs" size="6" onkeypress="return checkInput(event,'org',this)" data-act="cAct" /><span class="sBtn" id="cActAdd" onclick="addItem(this,'cAct','cAct')"> +新增</span>
            <div id="cActDiv">
            </div>
        </td>

    </tr>
    <?php 
    if($odMenu=='rod'){
    $locUrl="/index.php?funcUrl=edis/".$odMenu."/myod.php&muID=0";
    }else if($odMenu=='sod'){
    $locUrl="/index.php?funcUrl=edis/".$odMenu."/myod.php&muID=1";
    }
    ?>
    <tr>
        <td td colspan="4" align="center">
            <input type="submit" name="Submit" id="Submit" onclick="return getodNum();" value="送出" />
            <input type="reset" name="button2" id="button2" value="重設" />
            <input type="button" value="取消" onclick="location.href='<?php  echo $locUrl?>'">
        </td>
    </tr>

</table>
</form>
