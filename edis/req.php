<?php 
    include_once "../system/db.php";
    include_once "../config.php";
    include_once "../getEmplyeeInfo.php";
    include_once "../inc_vars.php";
    
    include_once 'func.php';
    $db       = new db();

    $signSql = "select * from map_orgs_sign where id ='" . $_REQUEST['mosid'] . "'";
    $rsSign  = $db->query($signSql);
    if ($rsSign) {
        $rSign = $db->fetch_array($rsSign);
    }

    //增加加簽
    if($_POST['addSign']){
        foreach ($_REQUEST['newSigner'] as $k => $v) {
            if (!empty($v)) {
                $db                   = new db('map_orgs_sign');
                $db->row['edisid']    = "'" . $rSign['edisid'] . "'";
                $db->row['signLevel'] = '3';
                $db->row['signMan']   = "'" . $v . "'";
                $db->insert();
            }
        }

        $signSSql = "select signStage from edis where id ='" . $rSign['edisid'] . "'";
        $rsSignS  = $db->query($signSSql);
        $rSignS   = $db->fetch_array($rsSignS);

        if($rSignS['signStage'] > 3){
            $updatSignState = "update edis set signStage = '3' where id = '" . $rSign['edisid'] . "'";
            $db->query($updatSignState);
        }

        echo json_encode(1);
        exit;
    }

    //是否代理簽核
    if ($_REQUEST['empID'] == $rSign['signMan']) {
        $agent = '';
    } else {
        $agent = $_REQUEST['empID'];
    }
    $signTime = date('Y/m/d H:i:s', time());

    $signSql = "update map_orgs_sign set "
        . "signTime ='" . $signTime . "',"
        . "signState ='" . $_REQUEST['signState'] . "',"
        . "signContent ='" . $_REQUEST['signContent'] . "',"
        . "isSign ='" . $_REQUEST['isSign'] . "',"
        . "isFinalNotify ='" . $_REQUEST['isFinalNotify'] . "',"//最後通知
        . "agent ='" . $agent . "'"
        . "where id='" . $_REQUEST['mosid'] . "'";
    $db->query($signSql);

    if ($_REQUEST['isSign'] == '1') {
        foreach ($_REQUEST['newSigner'] as $k => $v) {
            //檢查重複
            $checkDB = new db();
            $checkSQL = "select COUNT(*) as _count from map_orgs_sign where edisid='".$rSign['edisid']."' and signMan='".$v."' and signLevel <> '4'";

            $rs = $checkDB->query($checkSQL);
            $r  = $checkDB->fetch_array($rs);
            if($r['_count'] >= 1) continue;

            if (!empty($v)) {
                $db                   = new db('map_orgs_sign');
                $db->row['edisid']    = "'" . $rSign['edisid'] . "'";
                $db->row['signLevel'] = '3';
                $db->row['signMan']   = "'" . $v . "'";
                $db->insert();
            }
        }
        //機密更新edis:isSecret
        if ($_REQUEST['isSecret']=='1') {
            $filSql = "update edis set "
                . "isSecret ='" . $_REQUEST['isSecret'] . "'"
                . "where id='" . $rSign['edisid'] . "'";

            $db->query($filSql);

            $db = new db();
            if ($rSql['odType'] == '收文') {   
                $sql = "select signMan as sId from map_orgs_sign where edisid='" . $rSign['edisid'] . "' and signLevel='1' and moid is null";
            }
            if($rSql['odType'] == '發文'){
                $sql = "select sId from edis where id='" . $rSign['edisid'] . "'";
            }
            $rs = $db->query($sql);
            //新增機密公文可查看人員
            //主管
            $db                = new db('secret_members');
            $db->row['edisid'] = "'" . $rSign['edisid'] . "'";
            $db->row['empid'] = "'" . $_REQUEST['empID'] . "'";
            $db->row['createrid'] = "'" . $_REQUEST['empID'] . "'";
            $db->insert();

            // 承辦人
            while ($sId = $db->fetch_array($rs)) {
                if($sId['sId'] == $_REQUEST['empID']) continue;
                $db                = new db('secret_members');
                $db->row['edisid'] = "'" . $rSign['edisid'] . "'";
                $db->row['empid']  = "'" . $sId['sId'] . "'";
                $db->row['createrid'] = "'" . $_REQUEST['empID'] . "'";
                $db->insert();
            }            
            
            foreach ($_REQUEST['secretViewer'] as $k => $v) {
                if (!empty($v)) {
                    $db                = new db('secret_members');
                    $db->row['edisid'] = "'" . $rSign['edisid'] . "'";
                    $db->row['empid']  = "'" . $v . "'";
                    $db->row['createrid'] = "'" . $_REQUEST['empID'] . "'";
                    $db->insert();
                }
            }
        }        

        //判斷同階層是否全簽核完成
        $checkLevel = "SELECT signLevel, edisid FROM map_orgs_sign WHERE id='" . $_REQUEST['mosid'] . "'";
        $rs         = $db->query($checkLevel);
        $pdata      = $db->fetch_array($rs);

        $checkCountL = "SELECT min(signLevel) as signLevel FROM map_orgs_sign WHERE edisid = '" . $pdata['edisid'] . "' AND isSign IS NULL";
        $rs         = $db->query($checkCountL);
        $r          = $db->fetch_array($rs);

        $signStage  = empty($r['signLevel']) ? '4' : $r['signLevel'];

        $sqlEdis = "UPDATE edis set signStage = '". $signStage ."' WHERE id ='".$pdata['edisid']."'";
        $db->query($sqlEdis);

        //歸檔更新edis:歸檔日期/歸檔者/歸檔編號
        if (!empty($_REQUEST['filingNo'])) {
            $filSql = "update edis set "
                . "filingDate ='" . $signTime . "', "
                . "filingMan ='" . $_REQUEST['empID'] . "', "
                . "odDeadlineType ='" . $_REQUEST['odDeadlineType'] . "', "
                . "filingNo ='" . $_REQUEST['filingNo'] . "' "
                . "where id='" . $rSign['edisid'] . "'";

            $db->query($filSql);
            //發送最後通知
            sendmsg($_REQUEST['empID'], $rSign['edisid']); 
        }
    }

    echo json_decode(1);
?>