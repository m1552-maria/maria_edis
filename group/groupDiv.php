<?php
include_once '../system/db.php';
include_once "../config.php";
include_once "$root/getEmplyeeInfo.php";
include_once "$root/inc_vars.php";
include_once 'func.php';

function genGroupDom($fld){
    $db = new db();
    //group主檔
    if($fld == 'nAct' || $fld == 'cAct'){
      $sql = "SELECT * FROM groups_org where FIND_IN_SET('".$_SESSION['depID']."',depID)";
      $rs  = $db->query($sql);
    }else if($fld == 'sidMgr' || $fld == 'sId' || $fld == 'sIds'){
      $sql = "SELECT * FROM groups where creator_id = " . $_SESSION['empID'];
      $rs  = $db->query($sql);
    }else{
      $sql = "SELECT * FROM groups where creator_id = " . $_SESSION['empID'];
      $rs  = $db->query($sql);
    }


    $html_code = '<div id="group_'.$fld.'" style="display:none;"> <table id="groupTable" cellpadding="4">
        <caption>選擇群組</caption>
      <tr>
         <td align="right">群組名稱：</td>
        <td><select name="grup'.$fld.'" />';
    while ($r = $db->fetch_array($rs)) {
       $html_code .= '<option value="'.$r['group_id'].'">'.$r['name'].'</option>';
    }
    $html_code .='</select>  
      </tr>
      <tr>
        <td colspan="2" align="center">
        <button type="button" onclick="return insertGroup(\'group\',\''.$fld.'\')">確定</button>
        <button type="button" onclick="return closeGroup(\'group\',\''.$fld.'\')">取消</button></td></tr></table> </div>';
    $entity = html_entity_decode($html_code);

    echo $entity;

}

function getGroupEmp($groupId){
    global $emplyeeinfo;
    $list =array();
    $db = new db();
    //group主檔
    $sql = "SELECT gd.emp_id FROM groups_detail gd where gd.group_id = " .$groupId;
    $rs  = $db->query($sql);
    while ($r = $db->fetch_array($rs)) {
        $list[]=array('emp_id'=>$r['emp_id'],'emp_name'=>$emplyeeinfo[$r['emp_id']]);
    }
    return json_encode($list);
}

function getGroupOrg($groupId){
    $orgDB = new db();
    $sql ="select id ,title from organization";
    $rs = $orgDB->query($sql);
    while($r=$orgDB->fetch_array($rs)){
        $allOrg[$r[0]] = $r[1];
    }
    $orgDB->close();

    $list = array();
    $db = new db();
    //group主檔
    $sql = "SELECT org_id FROM groups_org_detail where group_id = " .$groupId;
    $rs  = $db->query($sql);
     while ($r = $db->fetch_array($rs)) {
      $list[]=array('org_id'=>$r['org_id'],'org_name'=>$allOrg[$r['org_id']]);
     }
    return json_encode($list);
}