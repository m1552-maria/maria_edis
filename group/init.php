<?
	/*
		This page is for Definition 
		Create By Michael Rou on 2017/9/21
	*/
	error_reporting(E_ALL);
	/* ::Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	} */
	include_once '../config.php';
	include_once "$root/inc_vars.php";
	include_once "$root/system/utilities/miclib.php";
	include_once "$root/system/db.php"; 
	include_once "$root/getEmplyeeInfo.php";
	
	//:: include System Class ============================================================
	include "$root/system/PageInfo.php";
	include "$root/system/PageControl.php";
	include "$root/system/ListControl.php";
	include "ViewControl.php";
    //include "$root/system/ViewControl.php";	
	include "$root/system/TreeControl.php";

	$pageTitle = "群組人員設定";
	//$pageSize=3; 使用預設
	$tableName = "groups";
	$templetPath = "group";
	// for Upload files and Delete Record use
	$ulpath = "/data/groups/";
	$delField = '';
	$delFlag = false;
	

	//:PageControl ------------------------------------------------------------------
	// Key => [名稱,diabled,visible,自定事件]
	$opPage = array(
		"firstBtn"=>array("第一頁",false,true,firstBtnClick),
		"prevBtn"=>array("前一頁",false,true,prevBtnClick),
		"nextBtn"=>array("下一頁",false,true,nextBtnClick),
		"lastBtn"=>array("最末頁",false,true,lastBtnClick),
		"goBtn"=>array("確定",false,true,goBtnClick),	//NumPageControl
		"numEdit"=>array(false,true),									//
		"searchBtn" => array("◎搜尋",false,true,SearchKeyword),
		"newBtn" => array("＋新增",false,true,doAppend),
		"editBtn" => array("－修改",false,true,doEdit),
		"delBtn" => array("Ｘ刪除",false,true,doDel)
	);
	//按鈕限制
	$btnContraint = array(
		"editBtn" => "isFinish == 1",
		"delBtn" => "isFinish == 1"
	);
	
	//:ListControl  ---------------------------------------------------------------
	$opList = array(
		"alterColor"=>true,			//交換色
		"curOrderField"=>'group_id',	//主排序的欄位
		"sortMark"=>'ASC',			//升降序 (ASC | DES)
		"searchField"=>array('group_id','name')	//要搜尋的欄位	
		/*"filterOption"=>array(
			'autochange'=>false,
			'rDate'=>array(date('Y-m-d',strtotime('-6 month')).' ~ '.date('Y-m-d'),false),	//預設Key值,是否隱藏
			'goBtn'=>array("確定送出",false,true,'')
		),
		"filterType"=>array(
			'rDate'=>'DateRange'	//不定義就是 <select>, Month:月選取， DateRange:日期範圍
		),
		"filters"=>array(
			'rDate'=>array('placeholder'=>'選取月份','MinMonth'=>'-6','MaxMonth'=>'+6')
		),
		"extraButton"=>array(
			//array("統計報表",false,true,'')
		)*/
	);

		
	$fieldsList = array(
		"group_id"=>array("群組編號","80px",true,array('Id',"gorec(%d)")),
		"name"=>array("群組名稱","80px",true,array('Text'))
	);
	
	//:ViewControl -----------------------------------------------------------------
	$fieldA = array(
		"group_id"=>array("群組ID","readonly",20),
		"name"=>array("群組名稱","Define",20,'',add_name),
		"creator_id"=>array("creator_id","Define",20,'',getCreator)
	);
	$fieldA_Data = array();
	
	$fieldE = array(
		"group_id"=>array("群組ID","readonly",20),
		"name"=>array("群組名稱","text",20,'','',array(true,'','',50)),
		"creator_id"=>array("creator_id","hidden",20,'','',)
	);
	function add_name(){
	
	$rtn = "<input name='name' type='text' id='name' size='30' required />";
	return $rtn;

	}

	function getCreator(){
	$rtn = "<input name='creator_id' type='hidden' id='creator_id' size='30' value='".$_SESSION['empID']."' />";
	return $rtn;

	}	
	//::Detail ======================================================================
	$DetailTable = "groups_detail";
	$mkID = "group_id";		//master KEY field
	$fkID = "group_id";	//foreign KEY field
	$Dfields = array(
		"emp_id"=>array("人員","Define",5,'',getMan),
		"group_id"=>array("group_id","hidden",20,'','',),
		"group_detail_id"=>array("group_detail_id","hidden",20,'','')
	);
	$Dfields_Data = array();
	function getMan($id){
		global $emplyeeinfo;
		return "<input type='text' name='dt_emp_id[]' class='input' readonly title='".$emplyeeinfo[$id]."' value='".$id."'> ".$emplyeeinfo[$id];
	}	

?>