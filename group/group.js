function insertGroup(group_name,fld) {
    var divName = '#' + group_name+'_'+fld;
    var group_id = $('select[name="grup'+fld+'"').val();
    var count = 0;
    var html = [];
    if (group_id.length > 0) {
        id_numbers = new Array();
        if(fld == 'sidMgr' || fld == 'newSigner' || fld == 'sId' || fld == 'sIds'){
            $.ajax({
                url: "group/api.php?act=getGroupEmp&inValue=" + group_id,
                type: "GET",
                dataType: "json",
                async: false,
                success: function(msg) {
                    id_numbers = msg;
                },
            });
            $.each(id_numbers, function(key, value) {
                var divTitle = '#' + fld + 'Div';
                $(divTitle).empty();
                count++;
                if (count == 1) {
                    if(fld == 'sIds'){
                        html.push(addItemGroup('sId', fld,value['emp_id'],value['emp_name']));
                        $.ajax({
                            url: 'edis/api.php?act=leadID&empID='+value['emp_id'],
                            type:"GET",
                            dataType:'json',
                            success: function(response){
                                if (response == 0){
                                    var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="" title="" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                                    $('#sidMgrDiv').empty();
                                    $('#sidMgrDiv').html(sIdDiv);
                                }else{
                                    var sIdDiv = '<input name="sidMgr[]" type="text" class="queryID" id="sidMgr" size="6" value="'+response['leadID']+'" title="'+response['empName']+'" onkeypress="return checkInput(event,\'empName\',this)"/><img src="/scripts/form_images/search.png" align="absmiddle" onclick="showDialog(this)"/><span class="sBtn" id="sidMgrAdd" onclick="addItem(this,\'sidMgr\',\'sidMgr\')"> +新增</span>';
                                    $('#sidMgrDiv').empty();
                                    $('#sidMgrDiv').html(sIdDiv);

                                    $("input[name='sidMgr[]']").first().val(response['leadID']);
                                    $("input[name='sidMgr[]']").first().value = response['leadID'];
                                    $("input[name='sidMgr[]']").first().attr('title',response['empName']);
                                    $("input[name='sidMgr[]']").first().after(' '+response['empName']);
                                }
                            }
                        });
                    }else{
                        $("#groupEnter").next().val(value['emp_id']);
                        $("#groupEnter").next().next().next().html(value['emp_name']);
                    }
                } else if (count > 1) {
                    if(fld == 'sIds'){
                        html.push(addItemGroup('sId', fld,value['emp_id'],value['emp_name']));
                    }else{
                        html.push(addItemGroup(fld, fld,value['emp_id'],value['emp_name']));
                    }
                }
                 
            });
        }else{
            $.ajax({
                url: "group/api.php?act=getGroupOrg&inValue=" + group_id,
                type: "GET",
                dataType: "json",
                async: false,
                success: function(msg) {
                    id_numbers = msg;
                },
            });

            $.each(id_numbers, function(key, value) {
                var divTitle = '#' + fld + 'Div';
                // $(divTitle).empty();
                count++;
                if (count == 1 && $("input[name='group"+fld+"'").next().val() == '') {
                   $("input[name='group"+fld+"'").next().val(value['org_id']);
                   $("input[name='group"+fld+"'").next().next().next().html(value['org_name']);
                } else{
                   html.push(addItemGroup(fld, fld,value['org_id'],value['org_name']));
                }
                 
            });
        }        

        var divTitle = '#' + fld + 'Div';
        $(divTitle).append(html.join(''));
    }
    $(divName).hide();
}

function closeGroup(group_name,fld) {
    var divName = '#' + group_name+'_'+fld;
    $(divName).hide();
}
//出現總表DIV畫面
function setGroup(divName) {
    var div = '#' + divName;
    $(div).show();
};

function addItemGroup(fld, qid, emp_id, emp_name) {
    var newone = "<div><input name='" + fld + "[]' id='" + qid + 
    "' type='text' size='6' onkeypress=\"return checkInput(event,'empName',this)\" value ='"+emp_id+"' title = '"+emp_name+"'>" +
    "<img src='/scripts/form_images/search.png' align='absmiddle' onclick='showDialog(this)'/> <span class='formTxS'>"+emp_name+"</span> " +
    "<span class='sBtn' id='" + qid + "Remove' onClick='removeItem(this)'> -移除</span></div>";

    return newone;
}



